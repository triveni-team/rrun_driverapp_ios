//
//  SignatureVC.swift
//  Driver
//
//  Created by Arthonsys Ben on 16/02/19.
//  Copyright © 2019 Arthonsys. All rights reserved.
//

import UIKit
import YPDrawSignatureView

class SignatureVC_old: UIViewController {
    
    @IBOutlet weak var imgSignutureBefore: UIImageView!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnSubmiHard: ZFRippleButton!
    
    @IBOutlet weak var lblPleaseSignHereHard: UILabel!
    
    @IBOutlet weak var drawView: YPDrawSignatureView!
    
    var signatureImage : UIImage!
    
    var yourTripDetailsobj : YourTripDetailsVC!
    
    var revivedData = NSMutableDictionary()
    
    var UpadateValueDic = NSMutableDictionary()
    
    var imgSinStopArray = NSMutableArray()
    
    @IBOutlet weak var btnCloseCrossbtn: ZFRippleButton!
    
    @IBOutlet weak var btnRefress: ZFRippleButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(imgSinStopArray)
        
        if imgSinStopArray.count > 0 {
            
            let id = self.UpadateValueDic["stop_trip_id"] as! String
            let getImageArray = imgSinStopArray.filter{($0 as! NSDictionary)["stop_trip_id"] as! String == String(id)}
            if getImageArray.count > 0 {
                let getimage = (getImageArray[0] as! NSDictionary)["signture_image"]
                imgSignutureBefore.image = getimage as? UIImage
              
                btnRefress.isUserInteractionEnabled = false
                drawView.isUserInteractionEnabled = false

            }
            else {
                btnRefress.isUserInteractionEnabled = true
                drawView.isUserInteractionEnabled = true
            }

        } else {
            
            btnRefress.isUserInteractionEnabled = true
            drawView.isUserInteractionEnabled = true

        }
        
        print(revivedData)
        
        colorCodeSetup()
        
        let fullName = revivedData["customer_name"]
        print(fullName as Any)
        if fullName != nil {
            let name = "HI \(getUsreFirstName(FullName: fullName as? String ?? ""))!"
            lblUserName.text = (name).uppercased()
        } else {
            lblUserName.text = ""
        }
        
        //        let ItemDicArray = NSMutableArray()
        //
        //        for i in itemArray {
        //
        //            let dic = i as! NSMutableDictionary
        //            ItemDicArray.add(dic)
        //
        //        }
        //
        //        let jsonString = json(from: ItemDicArray)
        //
        //        let myDictOfDict:NSMutableDictionary = [
        //            "item" : jsonString as Any
        //        ]
        
        
        // Do any additional setup after loading the view.
    }
    // layerView
    @IBAction func btnClearLayerViewDrawing(_ sender: UIButton) {
        
        drawView.clear()
    }
    
    @IBAction func btnSubmitDrawing(_ sender: UIButton) {
        
 
        if imgSignutureBefore.image == nil {
            
            if drawView.getSignature() == nil {
                        return
            } else {
                
                 signatureImage = drawView.getSignature()!
            }
            
        } else {
            
            signatureImage = imgSignutureBefore.image
            
        }
       
        
        signatureImage = GlobalClass.resizeImage(image: signatureImage, newWidth: 200)
        
        if signatureImage != nil {
            
            let ItemDicArray = NSMutableArray()
            
            let itemArray = revivedData["update_Order"] as! NSMutableArray
            var real_quantity = 0
            var qty_ordered = 0
            for i in itemArray {
                
                let dic = i as! NSMutableDictionary
                var FullUrl = String()
                if dic["item_image"] as! String != "" {
                    let url = dic["item_image"] as? String ?? ""
                    FullUrl = url //ImageBaseUrl + url
                } else{
                    FullUrl = ""
                }
                
                let dicUpdate = ["name":dic["name"] as Any,
                                 "barcode":dic["barcode"] as Any,
                                 "quantity":dic["real_quantity"] as Any,
                                 "price":dic["price"] as Any,
                                 "item_id":dic["item_id"] as Any,
                                 "unit_price":dic["unit_price"] as Any,
                                 "order_item_id" : dic["order_item_id"] as Any,
                                 "product_id" : dic["product_id"] as Any,
                                 "image":FullUrl]
                
                ItemDicArray.add(dicUpdate)
                real_quantity = real_quantity + (Int(dic["real_quantity"] as? String ?? "0") ?? 0)
                qty_ordered = qty_ordered + (Int(dic["qty_ordered"] as? String ?? "0") ?? 0)
            }
            
            let jsonString = json(from: ItemDicArray)
            
            print(revivedData)
           // let qty_ordered = (Int(revivedData["qty_ordered"] as? String ?? "0") ?? 0)
            
            let myDictOfDict:NSMutableDictionary = [
                "items_array" : jsonString as Any,
                "stop_id" : revivedData["stop_id"] as Any,
                "trip_id" : revivedData["trip_id"] as Any,
                "driver_id" : User("user_id"),
                "order_id" : revivedData["order_id"] as Any,
                "image" : GlobalClass.imageToStrinig(signatureImage),
                "qty_delivered" : String(real_quantity),
                "qty_ordered" : String(qty_ordered),
                "qty_return" : String(qty_ordered - real_quantity)
                ]
            
            
            
            // trip_id:4
            //stop_id:5
            //driver_id:10
            
            print(myDictOfDict)
            
//            if revivedData["order_type"] == "return" {
//                apiReturnOrder(parm: myDictOfDict)
//            } else{
//                apiSubmitOrder(parm: myDictOfDict)
//            } else{
//                // no api
//            }
//
            
            apiSubmitOrder(parm: myDictOfDict)
            
        }
      }
    
    @IBAction func btnClosed(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
        // text perpose
//        self.UpadateValueDic.setValue("3", forKey: "stop_status")
//        self.yourTripDetailsobj.statusSetUp()
        
    }
    
    func colorCodeSetup() {
        
        btnSubmiHard.backgroundColor = COLOR_PRIMARY
        btnSubmiHard.setTitleColor(COLOR_WHITE, for: .normal)
        
        lblPleaseSignHereHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        lblUserName.textColor = COLOR_PRIMARY_TEXT_COLOR
    }
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    func apiSubmitOrder(parm : NSDictionary) {
        
        DesableButtonAll()

//        GlobalClass.showHUD()
//        self.viewLoderHightConstant.constant = 35

        GlobalClass.multipartData("order_confirm", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
            
//            UIImageWriteToSavedPhotosAlbum(self.signatureImage, nil, nil, nil)

            let massage = (response as! NSDictionary).object(forKey: "responsemsg") as! String
            
            GlobalClass.showAlertOKAction(APP_NAME, massage, "OK", self, successClosure: { (str) in
                
                let userDefoult = UserDefaults.standard
                
                userDefoult.set("relodeVC", forKey: "isVcFrome")
                self.UpadateValueDic.setValue("confirm", forKey: "order_status") //order_status
                self.yourTripDetailsobj.statusSetUp()
                self.dismiss(animated: true, completion: nil)

              let backvcIndexCell = self.UpadateValueDic["indexpath"] as! Int
                
            self.yourTripDetailsobj.delegate?.setOrderStatus(status: "confirm", index: backvcIndexCell)
            self.yourTripDetailsobj.navigationController?.popViewController(animated: true)

                let stop_trip_id = self.UpadateValueDic["stop_trip_id"]
                
                let Dic = ["stop_trip_id" : stop_trip_id,
                           "signture_image" : (self.signatureImage)]
                self.imgSinStopArray.add(Dic)
                print("imgSinStopArray : \(self.imgSinStopArray) ")

            })
            
            self.AnableButtonAll()
            
//            GlobalClass.hideHud()
            
        }, successWithStatus0Closure: { (responce) in
            
            let massage = (responce as! NSDictionary).object(forKey: "responsemsg") as! String
            
            let responsemsg = (responce as! NSDictionary)["responsemsg"] as? String ?? ""
            let responsecode = (responce as! NSDictionary)["responsecode"] as? String ?? ""
            
            if responsecode as? String ?? "" == "-2" {
                
                GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
                    
                    GlobalClass.setUserDefault(value: "", for: "landingPage")
                    GlobalClass.removeUserDefaults("getDistributorList")
                    
                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                    return
                    
                })
            }
            
            GlobalClass.showAlertOKAction(APP_NAME, massage, "OK", self, successClosure: { (str) in

//                self.dismiss(animated: true, completion: nil)
                
                self.dismiss(animated: true, completion: nil)
                
            self.yourTripDetailsobj.navigationController?.popViewController(animated: true)

            })

//            GlobalClass.hideHud()
            self.AnableButtonAll()
            
        }) { (error) in

            self.AnableButtonAll()
            
            if error == "Not Show" {
                return
            }
            
        GlobalClass.showAlertOKAction(APP_NAME, error!, "OK", self, successClosure: { (str) in
                        })
            GlobalClass.hideHud()
            
        }
    }
    
//    func apiReturnOrder(parm : NSDictionary) {
//
//        DesableButtonAll()
//
//        //        GlobalClass.showHUD()
//        //        self.viewLoderHightConstant.constant = 35
//
//        GlobalClass.multipartData("order_confirm", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
//
//            //            UIImageWriteToSavedPhotosAlbum(self.signatureImage, nil, nil, nil)
//
//            let massage = (response as! NSDictionary).object(forKey: "responsemsg") as! String
//
//            GlobalClass.showAlertOKAction(APP_NAME, massage, "OK", self, successClosure: { (str) in
//                self.UpadateValueDic.setValue("confirm", forKey: "order_status")
//                self.yourTripDetailsobj.statusSetUp()
//
//                self.dismiss(animated: true, completion: nil)
//
//                self.yourTripDetailsobj.navigationController?.popViewController(animated: true)
//                let userDefoult = UserDefaults.standard
//                userDefoult.set("relodeVC", forKey: "isVcFrome")
//
//            })
//
//            self.AnableButtonAll()
//
//            //GlobalClass.hideHud()
//
//        }, successWithStatus0Closure: { (responce) in
//
//            let responsemsg = (responce as! NSDictionary)["responsemsg"] as? String ?? ""
//            let responsecode = (responce as! NSDictionary)["responsecode"] as? String ?? ""
//
//            if responsecode as? String ?? "" == "-2" {
//
//                GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
//
//                    GlobalClass.setUserDefault(value: "", for: "landingPage")
//                    GlobalClass.removeUserDefaults("getDistributorList")
//
//                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
//                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
//                    return
//
//                })
//            }
//
//            let massage = (responce as! NSDictionary).object(forKey: "responsemsg") as! String
//            GlobalClass.showAlertOKAction(APP_NAME, massage, "OK", self, successClosure: { (str) in
//
//                self.dismiss(animated: true, completion: nil)
//
//                self.yourTripDetailsobj.navigationController?.popViewController(animated: true)
//
//            })
//
//            //            GlobalClass.hideHud()
//            self.AnableButtonAll()
//
//        }) { (error) in
//
//            self.AnableButtonAll()
//
//            if error == "Not Show" {
//                return
//            }
//
//            GlobalClass.showAlertOKAction(APP_NAME, error!, "OK", self, successClosure: { (str) in
//            })
//            GlobalClass.hideHud()
//
//        }
//    }
    
    func getUsreFirstName(FullName : String) -> String {
        var components = FullName.components(separatedBy: " ")
        if(components.count > 0)
        {             print(components)
            let firstName = components.removeFirst()
            let lastName = components.joined(separator: " ")
            debugPrint(firstName)
            debugPrint(lastName)
            return firstName
        } else{
            return ""
        }
    }
    
    func DesableButtonAll() {
        
        btnRefress.isUserInteractionEnabled = false
        btnSubmiHard.isUserInteractionEnabled = false
        btnCloseCrossbtn.isUserInteractionEnabled = false
        drawView.isUserInteractionEnabled = false
        btnSubmiHard.setTitle("Processing...", for: .normal)
//        btnCloseCrossbtn.isHidden = true
        
    }
    
    func AnableButtonAll() {
        
        btnRefress.isUserInteractionEnabled = true
        btnSubmiHard.isUserInteractionEnabled = true
        btnCloseCrossbtn.isUserInteractionEnabled = true
        drawView.isUserInteractionEnabled = true
        btnSubmiHard.setTitle("SUBMIT", for: .normal)
//        btnCloseCrossbtn.isHidden = false


        
    }
}
