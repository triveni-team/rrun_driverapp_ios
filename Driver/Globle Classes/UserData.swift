/*
 Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import Foundation

/* For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */

public class UserData {
    public var user_id : String?
    public var currency_symbol : String?
    public var firstname : String!
    public var lastname : String!
    public var user_role : String!
    public var email : String!
    public var is_email_confirmed : String?
    public var email_notification_status : String?
    public var social_login_id : String?
    public var social_login_type : String?
    public var password : String?
    public var reset_password_hash : String?
    public var profile_image : String!
    public var profile_video : String?
    public var expertise_keys : String?
    public var user_languages : String?
    public var user_experience : String?
    public var user_price : String?
    public var final_koach_price : String?
    public var user_pricing_policy : String?
    public var user_currency : String?
    public var last_login_time : String?
    public var last_login_ip : String?
    public var is_active : String?
    public var is_deleted : String?
    public var created_time : String?
    public var modified_time : String?
    public var thumb_profile_image : String!
    public var display_image : String!
    public var profile_video_thumb : String!

    
    /**
     Returns an array of models based on given dictionary.
     
     Sample usage:
     let json4Swift_Base_list = Json4Swift_Base.modelsFromDictionaryArray(someDictionaryArrayFromJSON)
     
     - parameter array:  NSArray from JSON dictionary.
     
     - returns: Array of Json4Swift_Base Instances.
     */
    public class func modelsFromDictionaryArray(array:NSArray) -> [UserData]
    {
        var models:[UserData] = []
        for item in array
        {
            models.append(UserData(dictionary: item as! NSDictionary)!)
        }
        return models
    }
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let json4Swift_Base = Json4Swift_Base(someDictionaryFromJSON)
     
     - parameter dictionary:  NSDictionary from JSON.
     
     - returns: Json4Swift_Base Instance.
     */
    required public init?(dictionary: NSDictionary) {
        
        user_id = (dictionary["user_id"] as! String)
        currency_symbol = dictionary["currency_symbol"] as? String
        firstname = dictionary["firstname"] as! String
        lastname = dictionary["lastname"] as! String
        user_role = dictionary["user_role"] as! String
        email = dictionary["email"] as! String
        is_email_confirmed = dictionary["is_email_confirmed"] as? String
        email_notification_status = dictionary["email_notification_status"] as? String
        social_login_id = dictionary["social_login_id"] as? String
        social_login_type = dictionary["social_login_type"] as? String
        password = dictionary["password"] as? String
        reset_password_hash = dictionary["reset_password_hash"] as? String
        profile_image = dictionary["profile_image"] as! String
        profile_video = dictionary["profile_video"] as? String
        expertise_keys = dictionary["expertise_keys"] as? String
        user_languages = dictionary["user_languages"] as? String
        user_experience = dictionary["user_experience"] as? String
        user_price = dictionary["user_price"] as? String
        final_koach_price = dictionary["final_koach_price"] as? String
        user_pricing_policy = dictionary["user_pricing_policy"] as? String
        user_currency = dictionary["user_currency"] as? String
        last_login_time = dictionary["last_login_time"] as? String
        last_login_ip = dictionary["last_login_ip"] as? String
        is_active = dictionary["is_active"] as? String
        is_deleted = dictionary["is_deleted"] as? String
        created_time = dictionary["created_time"] as? String
        modified_time = dictionary["modified_time"] as? String
        thumb_profile_image = dictionary["thumb_profile_image"] as? String
        display_image = dictionary["display_image"] as? String
        profile_video_thumb = dictionary["profile_video_thumb"] as? String
    }
    
    /**
     Returns the dictionary representation for the current instance.
     
     - returns: NSDictionary.
     */
    public func dictionaryRepresentation() -> NSDictionary {
        
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(self.user_id, forKey: "user_id")
        dictionary.setValue(self.currency_symbol, forKey: "currency_symbol")
        dictionary.setValue(self.firstname, forKey: "firstname")
        dictionary.setValue(self.lastname, forKey: "lastname")
        dictionary.setValue(self.user_role, forKey: "user_role")
        dictionary.setValue(self.email, forKey: "email")
        dictionary.setValue(self.is_email_confirmed, forKey: "is_email_confirmed")
        dictionary.setValue(self.email_notification_status, forKey: "email_notification_status")
        dictionary.setValue(self.social_login_id, forKey: "social_login_id")
        dictionary.setValue(self.social_login_type, forKey: "social_login_type")
        dictionary.setValue(self.password, forKey: "password")
        dictionary.setValue(self.reset_password_hash, forKey: "reset_password_hash")
        dictionary.setValue(self.profile_image, forKey: "profile_image")
        dictionary.setValue(self.profile_video, forKey: "profile_video")
        dictionary.setValue(self.expertise_keys, forKey: "expertise_keys")
        dictionary.setValue(self.user_languages, forKey: "user_languages")
        dictionary.setValue(self.user_experience, forKey: "user_experience")
        dictionary.setValue(self.user_price, forKey: "user_price")
        dictionary.setValue(self.final_koach_price, forKey: "final_koach_price")
        dictionary.setValue(self.user_pricing_policy, forKey: "user_pricing_policy")
        dictionary.setValue(self.user_currency, forKey: "user_currency")
        dictionary.setValue(self.last_login_time, forKey: "last_login_time")
        dictionary.setValue(self.last_login_ip, forKey: "last_login_ip")
        dictionary.setValue(self.is_active, forKey: "is_active")
        dictionary.setValue(self.is_deleted, forKey: "is_deleted")
        dictionary.setValue(self.created_time, forKey: "created_time")
        dictionary.setValue(self.modified_time, forKey: "modified_time")
        dictionary.setValue(self.thumb_profile_image, forKey: "thumb_profile_image")
        dictionary.setValue(self.display_image, forKey: "display_image")
        dictionary.setValue(self.profile_video_thumb, forKey: "profile_video_thumb")
        
        return dictionary
    }
    
}
