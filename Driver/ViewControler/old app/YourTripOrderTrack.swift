//
//  YourTripOrderTrackVC.swift
//  Driver
//
//  Created by Apple on 11/02/19.
//  Copyright © 2019 Arthonsys. All rights reserved.
//

import UIKit
import MapKit
import NVActivityIndicatorView

class YourTripOrderTrackVC: UIViewController, MKMapViewDelegate {
    
    @IBOutlet weak var viewLoderHightConstant: NSLayoutConstraint!
    @IBOutlet weak var viewLoder: UIView!
    @IBOutlet weak var viewLoderAnim: UIView!
    
    @IBOutlet weak var btnComHight: NSLayoutConstraint!
    
    @IBOutlet weak var ViewCellUpper: UIView!
    
    @IBOutlet weak var btnCompletHard: ZFRippleButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    var trackArray = NSMutableArray()
    
    var RecivedData = NSDictionary()
    
    let locationManager = CLLocationManager()
    
    var tripDate = String()
    var backVCparm = NSDictionary()
    
    var imgSinStopArray = NSMutableArray()
    
    var locationArray = NSMutableArray()
    
    // cellTopView
    
    // Hard Label Outlet
    @IBOutlet weak var lblRoutHard: UILabel!
    @IBOutlet weak var lblNumberOfStopHard: UILabel!
    @IBOutlet weak var lblNoOfPackagesHard: UILabel!
    @IBOutlet weak var lblPikerNameHard: UILabel!
    
    @IBOutlet weak var lblTripID: UILabel!
    
    @IBOutlet weak var lblrouteName: UILabel!
    @IBOutlet weak var lblNofStop: UILabel!
    @IBOutlet weak var lblNofPackage: UILabel!
    @IBOutlet weak var lblPickName: UILabel!

    var isApiCall = true
    
    let userDefoult = UserDefaults.standard
    
    var YourTripsVC_obj : YourTripsVC!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
        colorsetup()
        statusSetUp()
        updateTableviewHeder()
        self.DataSetUp()

    }
    
    func DataSetUp(){
        
        // Ben10 print(""Your_trip_Order_Data  :- \(RecivedData)")
        
        lblTripID.text = "TRIP ID \(RecivedData["trip_id"] as! String)"
        let trip_name = (RecivedData["trip_name"] as! String).uppercased()
        lblrouteName.text = trip_name.replacingOccurrences(of: "<BR>", with: "\n")
        lblNofStop.text = (RecivedData["total_stops"] as! String)
        lblNofPackage.text = String((RecivedData["total_orders"] as? String ?? ""))
        let multipleStopsAray = (RecivedData["trip_stops"] as! NSMutableArray)
        trackArray = multipleStopsAray

        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        statusSetUp()
        tableView.reloadData()
        if userDefoult.object(forKey: "isVcFrome") as! String == "relodeVC"{

            if Reachability.isConnectedToNetwork() {
                getAllTrip()
                userDefoult.set("nil", forKey: "isVcFrome")
            } else {
                 GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "No Network Avalable", view: self)
                
            }
        }
    }
    
    func updateTableviewHeder () {
        
        let address = (RecivedData["trip_name"] as? String ?? "").uppercased()
        let addressHeight = address.height(withConstrainedWidth: SCREEN_WIDTH - 180, font: lblrouteName.font)
        ViewCellUpper.frame.size.height = CGFloat(addressHeight) + CGFloat(127)//CGFloat(153)
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    // cell Actions
    
    @IBAction func btnStart(_ sender: UIButton) {
        
        if let indexPath = tableView.indexPathForView(sender as UIView) {
            
            let index = trackArray[indexPath.row] as! NSDictionary
            
            apiStarTrip(dic: indexPath.row)
                        
        }
    }
    
    @IBAction func btnSkip(_ sender: UIButton) {
        
        GlobalClass.showAlertOKCancelAction(APP_NAME, "Are you sure want to skip stop?", self, successClosure: { (yes) in
            
            if let indexPath = self.tableView.indexPathForView(sender as UIView) {
                
                self.apiSkipTrip(dic: indexPath.row)
            }
            
        }) { (no) in
            // Ben10 print(""no")
        }
    }
    
    @IBAction func btnComplete(_ sender: ZFRippleButton) {
        
        GlobalClass.showAlertOKCancelAction(APP_NAME, "Are you sure want to complete all trip stops?", self, successClosure: { (yes) in
            self.apiComplateTrip()
            
        }) { (no) in
            // Ben10 print(""no")
        }
        
    }
    
    @IBAction func btnTrackLocation(_ sender: ZFRippleButton) {
        
        if !isApiCall { return }
        
        let totalData = RecivedData
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MapVC") as? MapVC
        vc!.RecivedData = totalData
        self.navigationController!.pushViewController(vc!, animated: true)
        
        if RecivedData["trip_status"] as? String == "start" {
            
           
            
        } else {
            
            // Ben10 print(""Stop is stop & skip")
            
        }
        
        if locationArray.count > 0 {
            
            
            
        }

       
        
        
        // by deepak some time
//        let latitude = Double(((RecivedData["trip_stops"] as! NSArray).object(at: 0) as! NSDictionary)["latitude"] as! String)
//        let longitude = Double(((RecivedData["trip_stops"] as! NSArray).object(at: 0) as! NSDictionary)["longitude"] as! String)
//
//        // Ben10 print("latitude as Any, longitude as Any)
//
//        if (latitude != nil) && (longitude != nil) {
//
//            let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!)))
//            destination.name = "Destination"
//
//            MKMapItem.openMaps(with: [destination], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
//
//        }
    }
    
    @IBAction func btnRestartAction(_ sender: UIButton) {
        
        GlobalClass.showAlertOKCancelAction(APP_NAME, "Are you sure want to restart stop?", self, successClosure: { (yes) in
            
            if let indexPath = self.tableView.indexPathForView(sender as UIView) {
                let index = self.trackArray[indexPath.row] as! NSDictionary
                self.apiResumeStart(dic: index)
            }
            
        }) { (no) in
            // Ben10 print(""no")
        }
        
       
    }
    
    @IBAction func btnSkipAgainAction(_ sender: UIButton) {
        
        GlobalClass.showAlertOKCancelAction(APP_NAME, "Are you sure want to skip stop?", self, successClosure: { (yes) in
            
            if let indexPath = self.tableView.indexPathForView(sender as UIView) {
                
                self.apiSkipTrip(dic: indexPath.row)
            }
            
        }) { (no) in
            // Ben10 print(""no")
        }
    }
    
    
    @IBAction func btnCallAction(_ sender: UIButton) {
        
       
        if let indexPath = self.tableView.indexPathForView(sender as UIView) {
            
            let index = trackArray[indexPath.row] as? NSDictionary
            
            let callNumber = (index?["mobile"] as? String ?? "").uppercased().removingWhitespaces()
            
            self.callNumber(phoneNumber: callNumber)

        }
    }
    
    func callNumber(phoneNumber:String) {
        let phoneNo = GlobalClass.makePhoneNo(number: phoneNumber)
        if let phoneCallURL = URL(string: "telprompt://\(phoneNo)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    application.openURL(phoneCallURL as URL)
                }
            }
        }
    }
}


class YourTripOrderTrackCell : UITableViewCell {
    
    @IBOutlet weak var cellBtnViewHightConstant: NSLayoutConstraint!
    
    @IBOutlet weak var lblRestorentName: UILabel!
    @IBOutlet weak var lblRestorentAddress: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblStatusHight: NSLayoutConstraint!
    
    // view Btn
    @IBOutlet weak var lblBtnView: UIView!
    @IBOutlet weak var lblStart: ZFRippleButton!
    @IBOutlet weak var lblSkip: ZFRippleButton!
    
    // Status View
    @IBOutlet weak var viewRoundPoint: UIView!
    @IBOutlet weak var ViewSingalLine: UIView!
    
    // After
    
    @IBOutlet weak var btnRestartCon: NSLayoutConstraint!
    @IBOutlet weak var btnRestart: ZFRippleButton!
    @IBOutlet weak var btnSkipAgain: ZFRippleButton!

    @IBOutlet weak var btnCallNumber: UIButton!
    
}

extension YourTripOrderTrackVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trackArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! YourTripOrderTrackCell
        
        colorCodeSetUpCell(cell: cell)
        
        let index = trackArray[indexPath.row] as! NSDictionary

        let stopStatus = Int(index["stop_status"] as! String)
        
////        var lat = index["lat"] as! String
////        var long = index["long"] as! String
//
//        let lat = "26.876626"
//        let long = "75.748441"
//
//        let dic = ["lat" : lat,
//                   "long" : long]
//
//        locationArray.add(dic)
        
        let address = (index["address"] as? String ?? "").uppercased()
        cell.lblRestorentAddress.text = address.replacingOccurrences(of: "<BR>", with: "\n")
        cell.lblRestorentName.text = (index["reciever_name"] as? String ?? "").uppercased()
        let callNumber = (index["mobile"] as? String ?? "").uppercased()
        cell.btnCallNumber.setTitle(callNumber, for: .normal)

        switch stopStatus {
        case 0:
            
            if (RecivedData["trip_status"] as! String).lowercased() == "pending" {
                
//                let address = (index["address"] as? String ?? "").uppercased()
//                cell.lblRestorentAddress.text = address.replacingOccurrences(of: "<BR>", with: "\n")
//                cell.lblRestorentName.text = (index["reciever_name"] as? String ?? "").uppercased()
                
                cell.lblStatus.text = ""
                cell.lblStatusHight.constant = 0
                cell.lblStatus.borderWidth = 0
                cell.lblStatus.borderColor = COLOR_PRIMARY_TEXT_COLOR
                
                cell.lblBtnView.isHidden = true
                cell.cellBtnViewHightConstant.constant = 0
                
            } else {
                
//                let address = (index["address"] as? String ?? "").uppercased()
//                cell.lblRestorentAddress.text = address.replacingOccurrences(of: "<BR>", with: "\n")
//                cell.lblRestorentName.text = (index["reciever_name"] as? String ?? "").uppercased()
                
                cell.lblStatus.text = ""
                cell.lblStatusHight.constant = 0
                cell.lblStatus.borderWidth = 0
                cell.lblStatus.borderColor = COLOR_PRIMARY_TEXT_COLOR
                
                cell.lblBtnView.isHidden = false
                cell.cellBtnViewHightConstant.constant = 30
            }
        
            cell.btnRestart.isHidden = true
            cell.btnRestartCon.constant = 0
            cell.btnSkipAgain.isHidden = true
            
        // cell.viewRoundPoint.backgroundColor = COLOR_PRIMARY_TEXT_COLOR
            
        case 1:
            
//            let address = (index["address"] as? String ?? "").uppercased()
//            cell.lblRestorentAddress.text = address.replacingOccurrences(of: "<BR>", with: "\n")
//            cell.lblRestorentName.text = (index["reciever_name"] as? String ?? "").uppercased()
            
//            if ((index["stop_status_label"] as! String).count == 0) || ((index["stop_status_label"] as! String) == "") {
////                cell.lblStatus.text = (index["order_status"] as? String)?.uppercased()
//                  cell.lblStatus.text = " SKIPPED "
//            } else {
//                cell.lblStatus.text = (index["stop_status_label"] as? String)?.uppercased()
//            }
            
            cell.lblStatus.text = " SKIPPED "
            cell.lblStatusHight.constant = 30

            cell.lblStatus.borderWidth = 1
            cell.lblStatus.borderColor = COLOR_PRIMARY_TEXT_COLOR
            
            cell.lblBtnView.isHidden = true
            cell.cellBtnViewHightConstant.constant = 0
            
            if RecivedData["trip_status"] as? String == "stop" {
                cell.btnRestart.isHidden = true
                cell.btnRestartCon.constant = 0

            } else {
                cell.btnRestart.isHidden = false
                cell.btnRestartCon.constant = 90

            }
            
            cell.btnSkipAgain.isHidden = true
//
//            if trackArray.count == 1 {
//                apiComplateTrip()
//            }

        case 2:
            
//            let address = (index["address"] as? String ?? "").uppercased()
//            cell.lblRestorentAddress.text = address.replacingOccurrences(of: "<BR>", with: "\n")
//            cell.lblRestorentName.text = (index["reciever_name"] as? String ?? "").uppercased()
            
            
//            if ((index["stop_status_label"] as! String).count == 0) || ((index["stop_status_label"] as! String) == "") {
//                //                cell.lblStatus.text = (index["order_status"] as? String)?.uppercased()
//                cell.lblStatus.text = " IN PROCESS "
//            } else {
//                cell.lblStatus.text = (index["stop_status_label"] as? String)?.uppercased()
//            }
            
             cell.lblStatus.text = " IN PROCESS "
            
            cell.lblStatusHight.constant = 30
            cell.lblStatus.borderWidth = 1
            cell.lblStatus.borderColor = COLOR_PRIMARY_TEXT_COLOR
//
            cell.lblBtnView.isHidden = true
            cell.cellBtnViewHightConstant.constant = 0
             
             if index["trip_stops_orders"] is NSMutableArray {
                
                let all_orders = index["trip_stops_orders"] as! NSMutableArray
                if all_orders.count > 0 {
                    //order_status //order_status
                    let remeningItem = all_orders.filter{($0 as! NSDictionary)["stop_status"] as! String == "2"}
                    if remeningItem.count == 0 {
                        cell.lblStatus.text = " DELIVERED "
                        cell.lblStatusHight.constant = 30
                    }
                }
             }
            
            cell.btnRestart.isHidden = true
            cell.btnRestartCon.constant = 0
           
            cell.btnSkipAgain.isHidden = false

            
        case 3:
            
//            let address = (index["address"] as? String ?? "").uppercased()
//            cell.lblRestorentAddress.text = address.replacingOccurrences(of: "<BR>", with: "\n")
//            cell.lblRestorentName.text = (index["reciever_name"] as? String ?? "").uppercased()
//
            
//            if ((index["stop_status_label"] as? String ?? "").count == 0) || ((index["stop_status_label"] as? String ?? "") == "") {
//                //                cell.lblStatus.text = (index["order_status"] as? String)?.uppercased()
//                cell.lblStatus.text = " DELIVERED "
//            } else {
//                cell.lblStatus.text = (index["stop_status_label"] as? String)?.uppercased()
//            }
            
             cell.lblStatus.text = " DELIVERED "
            
            
            cell.lblStatusHight.constant = 30
            cell.lblStatus.borderWidth = 1
            cell.lblStatus.borderColor = COLOR_PRIMARY_TEXT_COLOR
            
            cell.lblBtnView.isHidden = true
            cell.cellBtnViewHightConstant.constant = 0
            
            cell.btnRestart.isHidden = true
            cell.btnRestartCon.constant = 0

            cell.btnSkipAgain.isHidden = true

        default:
             print("default")
        }
        
        //        0 = pending
        //        1= skip
        //        2= inprogress
        //        3= success
        //        4= default
        //
        
        if indexPath.row == trackArray.count - 1 {
            
            cell.ViewSingalLine.isHidden = true
            cell.viewRoundPoint.backgroundColor = COLOR_PRIMARY_TEXT_COLOR
            
        } else{
            
            cell.ViewSingalLine.isHidden = false
            cell.viewRoundPoint.backgroundColor = COLOR_PRIMARY_TEXT_COLOR
            
        }
        return cell
    }
    
    func colorCodeSetUpCell(cell  : YourTripOrderTrackCell) {
        
        cell.lblRestorentName .textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblRestorentAddress .textColor = COLOR_PRIMARY_TEXT_COLOR
        
        cell.ViewSingalLine.backgroundColor = COLOR_PRIMARY_TEXT_COLOR
        
        cell.viewRoundPoint.borderColor = COLOR_PRIMARY_TEXT_COLOR
        cell.viewRoundPoint.borderWidth = 1
        cell.viewRoundPoint.backgroundColor = COLOR_WHITE
        
        cell.lblStart.setTitleColor(COLOR_WHITE, for: .normal)
        cell.lblSkip.setTitleColor(COLOR_PRIMARY_TEXT_COLOR, for: .normal)
        
        cell.lblStart.backgroundColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblSkip.backgroundColor = COLOR_WHITE
        
        cell.lblStart.borderColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblSkip.borderColor = COLOR_PRIMARY_TEXT_COLOR
        
        cell.lblStart.borderWidth = 1.5
        cell.lblSkip.borderWidth = 1.5
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        if isApiCall {
//
//        } else {
//            return
//        }
        
        if !isApiCall {
            return
        }
        
//        if RecivedData["trip_status"] as! String == "pending" {
//            return
//        }
        
        let sendData = (trackArray[indexPath.row] as! NSMutableDictionary)
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OrdersTripVC") as? OrdersTripVC
        vc!.RecivedData = sendData
        vc!.stop_status = RecivedData["stop_status"] as? String ?? ""
        vc!.imgSinStopArray = imgSinStopArray
        self.navigationController!.pushViewController(vc!, animated: true)
        
        // Ben10 print(""imgSinStopArray :- \(imgSinStopArray)")

    }
    
    func colorsetup () {
        
        lblTripID.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        lblRoutHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        lblNumberOfStopHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        lblNoOfPackagesHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        lblPikerNameHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        lblrouteName.textColor = COLOR_PRIMARY_TEXT_COLOR
        lblNofStop.textColor = COLOR_PRIMARY_TEXT_COLOR
        lblNofPackage.textColor = COLOR_PRIMARY_TEXT_COLOR
        lblPickName.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        btnCompletHard.setTitleColor(COLOR_WHITE, for: .normal)
        btnCompletHard.backgroundColor = COLOR_PRIMARY
    }
    
    func statusSetUp () {
        
        if (RecivedData["trip_status"] as! String).lowercased() == "pending" || trackArray.count == 0 {
            
            btnComHight.constant = 0
            btnCompletHard.isHidden = true
            
        }
        else if (RecivedData["trip_status"] as! String).lowercased() == "skip" || trackArray.count == 0 {
            
            btnComHight.constant = 45
            btnCompletHard.isHidden = false
            
        }
            
        else if (RecivedData["trip_status"] as! String).lowercased() == "stop" || trackArray.count == 0 {
            
            btnComHight.constant = 0
            btnCompletHard.isHidden = true
            
        }
        else {
            
            btnComHight.constant = 45
            btnCompletHard.isHidden = false
            
        }
    }
    
    func apiStarTrip (dic : Int) {
        
        if !isApiCall { return }

        isApiCall = false
        
        //        guard let stop_id = (dic)["stop_id"] as? Int else { return }
        //
        //        guard let trip_id = (dic)["trip_id"] as? Int else { return }
        
        let index = trackArray[dic] as! NSMutableDictionary
        
//        GlobalClass.showHUD()
        
        self.viewLoderHightConstant.constant = 35

        let parm = ["trip_id" : (index)["trip_id"],
                    "stop_id" : (index)["id"],
                    "status" : "start",
                    "driver_id": User("user_id")]
        
        GlobalClass.multipartData("trip_status", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
            
            
            
            self.isApiCall = true
            
            index.setValue("2", forKey: "stop_status")
            
            self.getAllTrip()
            
//            self.tableView.reloadData()
            
            let userDefoult = UserDefaults.standard
            
            userDefoult.set("relodeVC", forKey: "isVcFrome")
            
//            GlobalClass.hideHud()
            
            self.viewLoderHightConstant.constant = 0

            
        }, successWithStatus0Closure: { (response) in
            
//            GlobalClass.hideHud()
            
            self.viewLoderHightConstant.constant = 0

            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: response?.object(forKey: "responsemsg") as! String, view: self)
            
            self.isApiCall = true
            
            let responsemsg = (response as! NSDictionary)["responsemsg"] as? String ?? ""
            let responsecode = (response as! NSDictionary)["responsecode"] as? String ?? ""
            
            if responsecode as? String ?? "" == "-2" {
                
                GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
                    
                    GlobalClass.setUserDefault(value: "", for: "landingPage")
                    GlobalClass.removeUserDefaults("getDistributorList")
                    
                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                    return
                    
                })
            }


        }) { (error) in
            
            self.viewLoderHightConstant.constant = 0
            
            self.isApiCall = true
            
            if error == "Not Show" {
                return
            }
        }
    }
    
    func apiResumeStart (dic : NSDictionary) {

        //        guard let stop_id = (RecivedData)["stop_id"] as? String else { return }
        //        guard let trip_id = (RecivedData)["trip_id"] as? String else { return }

//        GlobalClass.showHUD()
        
        self.viewLoderHightConstant.constant = 35

        let parm = ["trip_id" : (dic)["trip_id"],
                    "stop_id" : (dic)["id"],
                    "status" : "resume",
                    "driver_id": User("user_id")]

        // resume_trip_stop // deepak some time
        
        GlobalClass.multipartData("trip_status", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in

//            GlobalClass.hideHud()

            self.getAllTrip()

            self.viewLoderHightConstant.constant = 0

            let userDefoult = UserDefaults.standard

            userDefoult.set("relodeVC", forKey: "isVcFrome")

        }, successWithStatus0Closure: { (response) in

//            GlobalClass.hideHud()
            self.viewLoderHightConstant.constant = 0

          
            
            let responsemsg = (response as! NSDictionary)["responsemsg"] as? String ?? ""
            let responsecode = (response as! NSDictionary)["responsecode"] as? String ?? ""
            
            if responsecode as? String ?? "" == "-2" {
                
                GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
                    
                    GlobalClass.setUserDefault(value: "", for: "landingPage")
                    GlobalClass.removeUserDefaults("getDistributorList")
                    
                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                    return
                    
                })
            }
            
              GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: response?.object(forKey: "responsemsg") as! String, view: self)

        }) { (error) in
            
//            GlobalClass.hideHud()
            
            self.viewLoderHightConstant.constant = 0

            if error == "Not Show" {
                return
            }
            
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: error as? String ?? "", view: self)

        }
    }
    
    func apiSkipTrip (dic : Int) {
        
        if !isApiCall {
            return
        }
        
//        if isApiCall {
//
//        } else {
//            return
//        }
        
        isApiCall = false
        
        let index = trackArray[dic] as! NSMutableDictionary
        
//        GlobalClass.showHUD()
        self.viewLoderHightConstant.constant = 35

        
        let parm = ["trip_id" : (index)["trip_id"],
                    "stop_id" : (index)["id"],
                    "status" : "skip",
                    "driver_id": User("user_id")]
        
        GlobalClass.multipartData("trip_status", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
            
//            GlobalClass.hideHud()
            
            self.isApiCall = true
            
            self.viewLoderHightConstant.constant = 0

            index.setValue("1", forKey: "stop_status")
            
            self.getAllTrip()
            
//            self.tableView.reloadData()
            
            let userDefoult = UserDefaults.standard
            
            userDefoult.set("relodeVC", forKey: "isVcFrome")
            
            
            
        }, successWithStatus0Closure: { (response) in
            
//            GlobalClass.hideHud()
            
            self.viewLoderHightConstant.constant = 0
            
            self.isApiCall = true
            
            let responsecode = (response as! NSDictionary)["responsecode"] as? String ?? ""
            
            // Ben10 print("responsecode)


            let responsemsg = (response as! NSDictionary)["responsemsg"] as? String ?? ""
//            let responsecode = (response as! NSDictionary)["responsecode"] as? String ?? ""
            
            if responsecode as? String ?? "" == "-2" {
                
                GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
                    
                    GlobalClass.setUserDefault(value: "", for: "landingPage")
                    GlobalClass.removeUserDefaults("getDistributorList")
                    
                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                    return
                    
                })
            }
            
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: response?.object(forKey: "responsemsg") as! String, view: self)
            
        }) { (error) in
//            GlobalClass.hideHud()
            self.viewLoderHightConstant.constant = 0
            
            self.isApiCall = true
            
            if error == "Not Show" {
                return
            }
        }
    }

    
    
    func apiComplateTrip (){
        
        if !isApiCall {
            return
        }

        isApiCall = false
        
    
        
        self.viewLoderHightConstant.constant = 35

        
        let parm = ["trip_id" : RecivedData["trip_id"]]
                
        GlobalClass.multipartData("on_stop_trip", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
         
            self.isApiCall = true
            
            self.getAllTrip()
            
            let userDefoult = UserDefaults.standard
            
            userDefoult.set("relodeVC", forKey: "isVcFrome")
            
            self.viewLoderHightConstant.constant = 0
            
            //locationManager
            DispatchQueue.main.async {
                self.YourTripsVC_obj.ApiCallCurrentLocation()
                self.YourTripsVC_obj.locationManager.stopUpdatingLocation()
                
                
            }
            
        }, successWithStatus0Closure: { (response) in
            
            
            self.viewLoderHightConstant.constant = 0

            self.isApiCall = true

            
            
            let responsemsg = (response as! NSDictionary)["responsemsg"] as? String ?? ""
            let responsecode = (response as! NSDictionary)["responsecode"] as? String ?? ""
            
            if responsecode as? String ?? "" == "-2" {
                
                GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
                    
                    GlobalClass.setUserDefault(value: "", for: "landingPage")
                    GlobalClass.removeUserDefaults("getDistributorList")
                    
                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                    return
                    
                })
            }
            
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: response?.object(forKey: "responsemsg") as! String, view: self)
        }) { (error) in
//            GlobalClass.hideHud()
            
            self.viewLoderHightConstant.constant = 0
            
            self.isApiCall = true
            
            if error == "Not Show" {
                return
            }


        }
    }
    
    func setUpView(){
        
        let activityIndicator = NVActivityIndicatorView(frame: self.viewLoderAnim.bounds)
        activityIndicator.type = .ballPulseSync
        activityIndicator.color = UIColor.black
        self.viewLoderAnim.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }

    
}

extension YourTripOrderTrackVC  {
    
    func getAllTrip() {
        
        if !isApiCall {
            return
        }

//        if isApiCall {
//            
//        } else {
//            return
//        }
        
        isApiCall = false
        
//        let one = (tripDate).replacingOccurrences(of: ".", with: "")
//        let DATE = (one).replacingOccurrences(of: ",", with: "")
//
//        let Date = GlobalClass.StringDateToDate(dateString: DATE, dateFormatte: "MMM dd yyyy")
//        //  2017/11/05
//        let newDate = GlobalClass.DateToString(date: Date, dateFormatte: "yyyy-MM-dd")
//
//        let parm = ["driver_id": User("user_id"),
//                    "created_date" : String(newDate)]
        
        GlobalClass.multipartData("all_trip", parameter: backVCparm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
            
            self.isApiCall = true
            
            let result = self.IfDic(dicM: response!.mutableCopy() as! NSMutableDictionary)
            var AllArrayDic = (result["data"] as! NSDictionary)["tripData"] as? NSMutableArray

            
            if ((result["data"] as! NSDictionary)["tripData"] as? NSArray != nil)  {
                
//                let AllArrayDic = (result["data"] as! NSMutableArray)
           
                let AllArrayDic = (result["data"] as! NSDictionary)["tripData"] as! NSMutableArray
                
                let filterDic = AllArrayDic.filter{((($0 as! NSDictionary)["trip"]  as! NSDictionary)["trip_id"] as! String == "\(self.RecivedData["trip_id"] as! String)")}
                
                // Ben10 print("filterDic)
                
                let tripDic = (filterDic[0] as! NSDictionary)["trip"] as! NSDictionary
               
                // Ben10 print("tripDic)
                
                self.RecivedData = tripDic
                
                
                
                if tripDic["trip_status"] as! String == "stop"  {
                    
                    DispatchQueue.main.async {
                        self.YourTripsVC_obj.ApiCallCurrentLocation()
                        self.YourTripsVC_obj.locationManager.stopUpdatingLocation()
                        
                    }
                    
                }
                
                
                self.DataSetUp()
                
                self.statusSetUp()
                
                self.userDefoult.set("relodeVC", forKey: "isVcFrome")

                
            }
            
            
            self.viewLoderHightConstant.constant = 0
            
        }, successWithStatus0Closure: { (responce) in
            
            self.viewLoderHightConstant.constant = 0
            
            self.tableView.reloadData()
            
            self.isApiCall = true
            
            let responsemsg = (responce as! NSDictionary)["responsemsg"] as? String ?? ""
            let responsecode = (responce as! NSDictionary)["responsecode"] as? String ?? ""
            
            if responsecode as? String ?? "" == "-2" {
                
                GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
                    
                    GlobalClass.setUserDefault(value: "", for: "landingPage")
                    GlobalClass.removeUserDefaults("getDistributorList")
                    
                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                    return
                    
                })
            }

            
        }) { (error) in
            
            self.viewLoderHightConstant.constant = 0
            
            self.isApiCall = true
            
            self.tableView.reloadData()
            
            self.isApiCall = true
            
            if error == "Not Show" {
                return
            }
            
//            let responceError = (error as! NSDictionary)["responsemsg"] as? String ?? ""
            
            GlobalClass.showAlertOKAction(APP_NAME, error!, "OK", self, successClosure: { (str) in
            })
            
            GlobalClass.hideHud()
        }
    }
    
    func IfDic(dicM : NSMutableDictionary) -> NSMutableDictionary {
        for (key, val) in dicM {
            
            if let array = val as? NSArray{
                let temp = array.mutableCopy() as! NSMutableArray
                let tp = self.IfArray(arrayM: temp)
                dicM.setValue(tp, forKey: key as! String)
            }
            else  if let dic = val as? NSDictionary{
                let temp = dic.mutableCopy() as! NSMutableDictionary
                let tp = self.IfDic(dicM: temp)
                dicM.setValue(tp, forKey: key as! String)
            }
        }
        
        return dicM
    }
    
    func IfArray(arrayM : NSMutableArray) -> NSMutableArray {
        var i = 0
        for val in arrayM {
            
            if let array = val as? NSArray{
                let temp = array.mutableCopy() as! NSMutableArray
                let tp = self.IfArray(arrayM: temp)
                arrayM.replaceObject(at: i, with: tp)
            }
            else  if let dic = val as? NSDictionary{
                let temp = dic.mutableCopy() as! NSMutableDictionary
                let tp = self.IfDic(dicM: temp)
                arrayM.replaceObject(at: i, with: tp)
            }
            i = i + 1
        }
        return arrayM
    }
    
}
