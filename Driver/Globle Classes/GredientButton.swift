//
//  GredientButton.swift
//  Koachfilm
//
//  Created by Mac on 10/07/18.
//  Copyright © 2018 Arthonsys Technologies LLP. All rights reserved.
//

import UIKit

@IBDesignable
class GredientButton: UIButton {

    // MARK: - IBInspectable properties
    /// Renders vertical gradient if true else horizontal
    public var verticalGradient: Bool = false {
        didSet {
            updateUI()
        }
    }
    
    /// Start color of the gradient
     public var startColor1: UIColor =  UIColor.init(red: 95/255.0, green: 200/255.0, blue: 247/255.0, alpha: 1.0) {
        didSet {
            updateUI()
        }
    }
    
     public var startColor2: UIColor = UIColor.init(red: 101/255.0, green: 201/255.0, blue: 247/255.0, alpha: 1.0) {
        didSet {
            updateUI()
        }
    }
    
    /// End color of the gradient
    public var endColor1: UIColor = UIColor.init(red: 115/255.0, green: 152/255.0, blue: 226/255.0, alpha: 1.0) {
        didSet {
            updateUI()
        }
    }
    
     public var endColor2: UIColor = UIColor.init(red: 100/255.0, green: 152/255.0, blue: 211/255.0, alpha: 1.0) {
        didSet {
            updateUI()
        }
    }
    
    /// Border color of the view
   
    // MARK: - Variables
  
    private var gradientlayer = CAGradientLayer()
    
    // MARK: - init methods
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
    
    // MARK: - Layout
    override public func layoutSubviews() {
        super.layoutSubviews()
        updateFrame()
    }
    
    // MARK: - UI Setup
    private func setupUI() {
        gradientlayer = CAGradientLayer()
        updateUI()
        layer.addSublayer(gradientlayer)
    }
    
    // MARK: - Update frame
    private func updateFrame() {
        gradientlayer.frame = bounds
    }
    
    // MARK: - Update UI
    private func updateUI() {
        gradientlayer.colors = [endColor1.cgColor, endColor2.cgColor, startColor1.cgColor, startColor2.cgColor]
        gradientlayer.locations = [0.0, 0.25, 0.75, 1.0]
        
        /* make it horizontal */
       
        if verticalGradient {
            gradientlayer.startPoint = CGPoint(x: 0, y: 0)
            gradientlayer.endPoint = CGPoint(x: 0, y: 1)
        } else {
            gradientlayer.startPoint = CGPoint(x: 1, y: 0)
            gradientlayer.endPoint = CGPoint(x: 0, y: 0)
        }
        layer.cornerRadius = CGFloat(cornerRadius)
        layer.borderWidth = CGFloat(borderWidth)
        layer.borderColor = borderColor?.cgColor ?? tintColor.cgColor
        if cornerRadius > 0 {
            layer.masksToBounds = true
        }
        updateFrame()
    }
    
    // MARK: - On Click
    
}
