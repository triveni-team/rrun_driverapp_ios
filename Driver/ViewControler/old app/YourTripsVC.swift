//
//  YourTripsVC.swift
//  Restaurant Driver
//
//  Created by Apple on 06/02/19.
//  Copyright © 2019 Arthonsys. All rights reserved.
//

import UIKit
import MapKit
import CoreMotion
import NVActivityIndicatorView
import CoreLocation

var apiTimer: Timer?

class YourTripsVC: UIViewController, MKMapViewDelegate {
    
    var viewFilterMainGesture = UITapGestureRecognizer()
    
    @IBOutlet weak var filterViewBG: UIView!
    @IBOutlet weak var viewFilterMain: UIView!
    @IBOutlet weak var viewFilter: UIView!
    
    @IBOutlet weak var filterVIewHIghtCon: NSLayoutConstraint!
    
    var locationManager = CLLocationManager()
    
    // uper loder
    @IBOutlet weak var viewLoderHightConstant: NSLayoutConstraint!
    @IBOutlet weak var viewLoder: UIView!
    @IBOutlet weak var viewLoderAnim: UIView!
    //
    
    // Down loder
    @IBOutlet weak var viewDownLoderHightConstant: NSLayoutConstraint!
    @IBOutlet weak var viewDownLoder: UIView!
    @IBOutlet weak var viewDownLoderAnim: UIView!
    //
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var noDataImage: UIImageView!
    @IBOutlet weak var txtDatePiker: UITextField!
    
    var refresher:UIRefreshControl!
    var datePicker = UIDatePicker()
    var toolBar = UIToolbar()
    
    var future_date = Int()
    var past_date =  Int()
    
    var PastDate = Date()
    var futureDate = Date()
    var currentDate = Date()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblYourTripHard: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    var isFrom = ""
    var allDataTrip = NSMutableArray()
    var userData = NSDictionary()
    
    var lat : CGFloat!
    var long : CGFloat!
    
    let motionActivityManager = CMMotionActivityManager()
    var RelodeAPiData = NSDictionary()
    
    var pagenination = false
    var limit = 10 //limit //limit
    var offset = 0 // offset // offset
    var isApiCall = true
    
    var arrayFilterStatus = NSArray()
    
    //for filter
    
    var trip_status = "all"
    var filter_type = "date"
    
    @IBOutlet weak var lblDateFilterName: UILabel!
    @IBOutlet weak var lblStatusFilterName: UILabel!
    
    var selectedDate = Date()
    var selectedDateFromPicker = Date()
    var dicDateFormat = NSDictionary()
    //
    var customDatePicker: UIPickerView!
    var arrayMonth = NSArray()
    var ParmApi = NSDictionary()

    var imgSinStopArray = NSMutableArray()
    
    var activity = true

    var isFirstTime = true
    
    var count = 0
    
    var isApiCalled_trip_tracking = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrayOfMonth()
        
        dicDateFormat = ["date" : ["forShow" : "MMM. d, yyyy", "forAPI" : "yyyy-MM-dd"] as NSDictionary,
                         "month" : ["forShow" : "MMM, yyyy", "forAPI" : "yyyy-MM"] as NSDictionary,
                         "year" : ["forShow" : "yyyy", "forAPI" : "yyyy"] as NSDictionary]
        
        customDatePicker = UIPickerView()
        customDatePicker.delegate = self
        customDatePicker.dataSource = self
        
        viewFilterMain.isHidden = true
        filterViewTap()
        future_date = 2
        past_date = 3
        viewDidLoadAll()
        
        lblDateFilterName.text = "DATE"
        lblStatusFilterName.text = "ALL"
        
    }
    
    func viewDidLoadAll() {
        
        GlobalClass.setUserDefault(value: "YourTripsNavVC", for: "landingPage")
        
        colorCodeSetup()
        setUpViewLoderUP()
        setUpViewLoderDown()
     //   StartUpdateLocation()
        ImageSetUser()
        GlobalClass.hideHud()
        tableView.reloadData()
        setCurrentDate()
        pikerViewSetup()
        refresView()
        noDataImage.isHidden = true
        tableviewKeybordSetUp()
     
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func StartUpdateLocation() {
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self as? CLLocationManagerDelegate
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
          //  locationManager.startUpdatingLocation()
            locationManager.activityType = .automotiveNavigation
            locationManager.distanceFilter = 100
            locationManager.startUpdatingLocation()
            locationManager.allowsBackgroundLocationUpdates = true
        }
    }
    
    func setCurrentDate() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = ((dicDateFormat[filter_type] as! NSDictionary)["forShow"] as! String)
        let result = formatter.string(from: selectedDate)
        lblDate.text = result.uppercased()
        
        self.apiAllTrip(isShowHUD: true)
        
    }
    
    func refresView () {
        
        self.refresher = UIRefreshControl()
        self.tableView.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.clear
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.tableView.addSubview(refresher)
    }
    
    @objc func loadData() {
        
        offset = 0
        
        if Reachability.isConnectedToNetwork(){
            apiAllTrip(isShowHUD: true)
        } else {
             GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "No Network Avalable", view: self)
        }
        stopRefresher()
        
    }
    
    func stopRefresher() {  self.refresher.endRefreshing() }
    
    func pikerViewSetup () {
        
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(DoneActionPickerView))
        doneButton.tintColor = COLOR_WHITE
        
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(CancelActionPickerView))
        
        cancelButton.tintColor = COLOR_WHITE
        
        toolBar.frame = CGRect(x: 0, y: self.view.frame.size.height-50, width: self.view.frame.size.width, height: 50)
        toolBar.barStyle = UIBarStyle.blackOpaque
        toolBar.setItems([doneButton,spacer,cancelButton], animated: false)
        
        self.txtDatePiker.inputView = datePicker
        self.txtDatePiker.inputAccessoryView = toolBar
        datePicker.datePickerMode = .date
        datePicker.minimumDate = GlobalClass.StringDateToDate(dateString: "01 01 2015", dateFormatte: "dd MM yyyy")
        datePicker.maximumDate = GlobalClass.StringDateToDate(dateString: "31 12 2024", dateFormatte: "dd MM yyyy")
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd, YYYY"
        
        //  datePicker
        
    }
    
    @objc func DoneActionPickerView() {
        
        self.view.endEditing(true)
        
        if filter_type == "date" {
            selectedDate = datePicker.date
        }
            
        else {
            selectedDate = selectedDateFromPicker
        }
        self.setCurrentDate()
        self.offset = 0
    }
    
    @objc func CancelActionPickerView() {
        self.view.endEditing(true)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func tableviewKeybordSetUp() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)}
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.2, animations: {
            self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        })
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
         imgSinStopArray = []
        
         self.view.endEditing(true)
        
        offset = 0
        
        UIApplication.shared.statusBarStyle = .lightContent //or .default
        setNeedsStatusBarAppearanceUpdate()
        
        ImageSetUser()
        
        let userDefoult = UserDefaults.standard
        
        if Reachability.isConnectedToNetwork(){
            apiAllTrip(isShowHUD: true)
            limit = 10
            offset = 0
            userDefoult.set("nil", forKey: "isVcFrome")
        }
        else{
             GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "No Network Avalable", view: self)
        }
        
        if userDefoult.object(forKey: "isVcFrome") as! String == "islogin" {
            
            if Reachability.isConnectedToNetwork(){
//                apiAllTrip(isShowHUD: true)
                self.setCurrentDate()
                userDefoult.set("nil", forKey: "isVcFrome")
                
            } else {
                 GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "No Network Avalable", view: self)
            }
        }

        else{
            // Ben10 print(""no Api Call")
        }
        
    }
    
    @IBAction func btnProfileAction(_ sender: UIButton) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ProfileVC") as? ProfileVC
        self.navigationController!.pushViewController(vc!, animated: true)
        
    }
    
    @IBAction func btnLogout(_ sender: UIButton) {
        
        GlobalClass.showAlertOKCancelAction(APP_NAME, "Are you sure you want to logout?", self, successClosure: { (yes) in
            
            GlobalClass.setUserDefault(value: "", for: "landingPage")
            GlobalClass.removeUserDefaults("getDistributorList")
            
            let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
            UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
            
        }) { (no) in
            // Ben10 print(""no")
        }
    }
    
    
    @IBAction func btnLessDate(_ sender: UIButton) {
        
        offset = 0
        
        if filter_type == "date" {
            let Dic = ["day" : lblDay.text,
                       "date" : lblDate.text]
            
            let NextDateDic = convertLessDate(dateDic: Dic as NSDictionary)
            
            lblDay.text = (NextDateDic["day"] as? String)!.uppercased()
            lblDate.text = (NextDateDic["date"] as? String)!.uppercased()
            
            selectedDate  = GlobalClass.StringDateToDate(dateString: NextDateDic["date"] as! String, dateFormatte: "MMM. d, yyyy")
            
            if Reachability.isConnectedToNetwork() {
                apiAllTrip(isShowHUD: true)
            }
            else{
                 GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "No Network Avalable", view: self)
            }
        }
        else{
            
            let date = Int(GlobalClass.DateToString(date: selectedDate, dateFormatte: "dd") ?? "0")!
            var month = Int(GlobalClass.DateToString(date: selectedDate, dateFormatte: "MM") ?? "0")!
            var year = Int(GlobalClass.DateToString(date: selectedDate, dateFormatte: "yyyy") ?? "0")!
            
            
            if filter_type == "month" {
                
                if month == 1 {
                    if year <= 2015 {
                        return
                    }
                    month = 12
                    year = year - 1
                }
                else {
                    month = month - 1
                }
                
            }
            else if filter_type == "year" {
                if year <= 2015 {
                    return
                }
                else {
                    year = year - 1
                }
                
            }
            
            let newDate = "\(date)" + " " + "\(month)" + " " + "\(year)"
            selectedDate = GlobalClass.StringDateToDate(dateString: newDate, dateFormatte: "dd MM yyyy")
            self.setCurrentDate()
        }
        
        
        
    }
    
    @IBAction func btnIncressDate(_ sender: UIButton) {
        
        offset = 0

        if filter_type == "date" {
            let Dic = ["day" : lblDay.text,
                       "date" : lblDate.text]
            
            let NextDateDic = convertNextDate(dateDic: Dic as NSDictionary)
            
            lblDay.text = (NextDateDic["day"] as? String)!.uppercased()
            lblDate.text = (NextDateDic["date"] as? String)!.uppercased()
            
            selectedDate  = GlobalClass.StringDateToDate(dateString: NextDateDic["date"] as! String, dateFormatte: "MMM. d, yyyy")

            if Reachability.isConnectedToNetwork() {
                apiAllTrip(isShowHUD: true)
            }
            else{
                 GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "No Network Avalable", view: self)
            }
            
        }
        else {
            let date = Int(GlobalClass.DateToString(date: selectedDate, dateFormatte: "dd") ?? "0")!
            var month = Int(GlobalClass.DateToString(date: selectedDate, dateFormatte: "MM") ?? "0")!
            var year = Int(GlobalClass.DateToString(date: selectedDate, dateFormatte: "yyyy") ?? "0")!
            
            
            if filter_type == "month" {
                
                if month == 12 {
                    if year >= 2024 {
                        return
                    }
                    month = 1
                    year = year + 1
                }
                else {
                    month = month + 1
                }
                
            }
            else if filter_type == "year" {
                if year >= 2024 {
                    return
                }
                else {
                    year = year + 1
                }
                
            }
            let newDate = "\(date)" + " " + "\(month)" + " " + "\(year)"
            selectedDate = GlobalClass.StringDateToDate(dateString: newDate, dateFormatte: "dd MM yyyy")
            self.setCurrentDate()
        }
    }
    
    @IBAction func btnCellRatePickerAction(_ sender: UIButton) {
        
        //        if let indexPath = tableView.indexPathForView(sender as UIView) {
        //
        //            // Ben10 print("indexPath.row)
        //
        //            let sendData = allDataTrip[indexPath.row] as! NSDictionary
        //
        //            // Ben10 print("sendData)
        //
        //            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WriteReviewVC") as? WriteReviewVC
        //            vc!.RecivedData = sendData
        //            self.navigationController!.pushViewController(vc!, animated: true)
        //
        // }
    }
    
    @IBAction func btnCellAcceptAction(_ sender: UIButton) {
        
        if !isApiCall { return }

        if let indexPath = tableView.indexPathForView(sender as UIView) {
            
            let sendData = (allDataTrip[indexPath.row] as! NSDictionary)["trip"] as! NSDictionary
            
            if sendData["trip_status"] as! String == "pending" {
                
                if String((allDataTrip[indexPath.row] as! NSDictionary)["is_acceptable"] as! Int)  == "0" {
                    GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "You can't accept this order.", view: self)
                    return
                } else {
                    
                    statusUpdateApi(indexPath: indexPath.row)

                }
                
            } else {
                
                // go to next vc any time
 
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "YourTripOrderTrackVC") as? YourTripOrderTrackVC
                vc!.RecivedData = sendData
                vc!.tripDate = lblDate.text!
                vc!.backVCparm = ParmApi
                vc!.imgSinStopArray = imgSinStopArray
                vc!.YourTripsVC_obj = self
                self.navigationController!.pushViewController(vc!, animated: true)
                
                
                tableView.reloadData()
            }
        }
    }
    
    @IBAction func btnFilter(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        filterStatus()
        
    }
    
    @IBAction func btnDateFilter(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        let optionMenu = UIAlertController(title: nil, message: "CHOOSE DATE TYPE", preferredStyle: .actionSheet)
        
        if let popoverController = optionMenu.popoverPresentationController {
            popoverController.sourceView = lblDate
            
            popoverController.sourceRect = CGRect(x: 0, y: 0, width: lblDate.frame.width, height: lblDate.frame.height)
        }
        
        let dateAction = UIAlertAction(title: "DATE", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            self.filter_type = "date"
            self.lblDateFilterName.text = "DATE"
            self.setCurrentDate()
            self.offset = 0
            //  self.apiAllTrip(isShowHUD: true)
            
        })
        
        let monthAction = UIAlertAction(title: "MONTH", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            self.filter_type = "month"
            self.lblDateFilterName.text = "MONTH"
            self.setCurrentDate()
            self.offset = 0
            //    self.apiAllTrip(isShowHUD: true)
            
            
        })
        
        let yearAction = UIAlertAction(title: "YEAR", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            self.filter_type = "year"
            self.lblDateFilterName.text = "YEAR"
            self.setCurrentDate()
            self.offset = 0

            //  self.apiAllTrip(isShowHUD: true)
            
        })
        
        let cancel = UIAlertAction(title: "CANCEL", style: .cancel) { (dsf) in
            
        }
        
        optionMenu.addAction(dateAction)
        optionMenu.addAction(monthAction)
        optionMenu.addAction(yearAction)
        optionMenu.addAction(cancel)
        
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    func filterViewTap() {
        
        viewFilterMainGesture = UITapGestureRecognizer(target: self, action: #selector(YourTripsVC.myMenuView(_:)))
        viewFilterMainGesture.numberOfTapsRequired = 1
        viewFilterMainGesture.numberOfTouchesRequired = 1
        viewFilterMain.addGestureRecognizer(viewFilterMainGesture)
        viewFilterMain.isUserInteractionEnabled = true
    }
    
    @objc func myMenuView(_ sender: UITapGestureRecognizer) {
        
        UIView.animate(withDuration: 0.2, animations: {
            self.filterVIewHIghtCon.constant = 0
            self.view.layoutIfNeeded()
            
        }) { (true) in
            UIView.animate(withDuration: 0.1, animations: {
                self.viewFilterMain.isHidden = true
                self.view.layoutIfNeeded()
            }) { (true) in
            }
        }
    }
    
    func filterStatus() {
        
        if arrayFilterStatus.count == 0 { return }
        
        let optionMenu = UIAlertController(title: nil, message: "CHOOSE STATUS", preferredStyle: .actionSheet)
        
        if let popoverController = optionMenu.popoverPresentationController {
            popoverController.sourceView = lblDate
            popoverController.sourceRect = CGRect(x: 0, y: 0, width: lblDate.frame.width, height: lblDate.frame.height)
        }
        
        for i in 0 ..< self.arrayFilterStatus.count {
            let dic = self.arrayFilterStatus[i] as! NSDictionary
            
            let ALL = UIAlertAction(title: (dic["value"] as? String ?? "").uppercased(), style: .default, handler:
            {
                (alert: UIAlertAction!) -> Void in
                // Ben10 print("dic["key"])
                self.trip_status = dic["key"] as? String ?? "all"
                self.lblStatusFilterName.text = (dic["value"] as? String ?? "ALL").uppercased()
                self.apiAllTrip(isShowHUD: true)
                self.offset = 0
                
            })
            optionMenu.addAction(ALL)
            
        }
        
        let cancel = UIAlertAction(title: "CANCEL", style: .cancel) { (dsf) in
            
        }
        optionMenu.addAction(cancel)
        
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    
    
    
    
    
    
    
}

class yourTripCell : UITableViewCell {
    
    // Hard Labels OutLets
    
    @IBOutlet weak var lblTripNameHard: UILabel!
    @IBOutlet weak var lblNoOFStopHard: UILabel!
    @IBOutlet weak var lblNoOFPackgesHard: UILabel!
    @IBOutlet weak var lblPickerNameHard: UILabel!
    
    //
    
    @IBOutlet weak var lblPickupID: UILabel!
    
    @IBOutlet weak var lblTripName: UILabel!
    @IBOutlet weak var lblNumberOFStop: UILabel!
    @IBOutlet weak var LblNumberOfPackege: UILabel!
    @IBOutlet weak var lblPickerName: UILabel!
    
    @IBOutlet weak var btnCellAccept: ZFRippleButton!
    @IBOutlet weak var btnCellAcceptHightCon: NSLayoutConstraint!
    
    @IBOutlet weak var btnCellRate: ZFRippleButton!
    
    @IBOutlet weak var viewMainBG: UIView!
    
    @IBOutlet weak var lblTripDate: UILabel!
    
    @IBOutlet weak var lbltripConstant: NSLayoutConstraint!
    
    @IBOutlet weak var lblTripNameTopConstant: NSLayoutConstraint!
    
    
    
}

extension YourTripsVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if allDataTrip.count == 0 {
            
            noDataImage.isHidden = false
            
        } else{
            
            noDataImage.isHidden = true
            
        }
        
        return allDataTrip.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! yourTripCell
        if allDataTrip.count == 0{
            return cell
        }
        cell.viewMainBG.backgroundColor = COLOR_ACCENT_VIEW_BG
        
        // Hard Labels Outlet
        
        cell.lblTripNameHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblNoOFStopHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblNoOFPackgesHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblPickerNameHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        cell.lblPickupID.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        cell.lblTripName.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblNumberOFStop.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.LblNumberOfPackege.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblPickerName.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        cell.btnCellRate.borderWidth = 1
        cell.btnCellRate.borderColor = COLOR_PRIMARY_TEXT_COLOR
        cell.btnCellRate.setTitleColor(COLOR_PRIMARY_TEXT_COLOR, for: .normal)
        cell.btnCellRate.backgroundColor = COLOR_WHITE
        
        cell.btnCellAccept.borderWidth = 1
        cell.btnCellAccept.borderColor = COLOR_PRIMARY_TEXT_COLOR
        cell.btnCellAccept.setTitleColor(COLOR_PRIMARY_TEXT_COLOR, for: .normal)
        cell.btnCellAccept.backgroundColor = COLOR_WHITE
        
        cell.lblTripDate.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        let index = (allDataTrip[indexPath.row] as! NSDictionary)["trip"] as! NSDictionary
        //        cell.lblPickupID.text = "TRIP ID \(index["trip_id"] as! String)"
        cell.lblTripName.text = (index["trip_name"] as! String).uppercased()
        cell.lblNumberOFStop.text = index["total_stops"] as? String
        //        cell.LblNumberOfPackege.text = String((index["trip_stops"] as? NSArray ?? []).count)
        cell.LblNumberOfPackege.text = (index["total_orders"] as? String ?? "0")
        cell.lblPickerName.text = userData["name"] as? String ?? "<BR>"//(index["picker_name"] as! String).uppercased()
        let tripStartDate = index["start_time"] as! String
        if tripStartDate.count > 0 {
            let DateType = GlobalClass.StringDateToDate(dateString: tripStartDate, dateFormatte: "yyyy-MM-dd HH:mm:ss")
            let Date = GlobalClass.DateToString(date: DateType, dateFormatte: "MM-dd-yyyy")
            cell.lblTripDate.text = "\(Date)"
        }
        
        if filter_type == "date" {
            cell.lbltripConstant.constant = 0
            cell.lblTripNameTopConstant.constant = 21
        } else {
            cell.lbltripConstant.constant = 18
            cell.lblTripNameTopConstant.constant = 42
        }
        
        //        // Ben10 print(""is_acceptable : -\((allDataTrip[indexPath.row] as! NSDictionary)["is_acceptable"] as! Int)")
        
        let is_acceptable = String((allDataTrip[indexPath.row] as! NSDictionary)["is_acceptable"] as! Int)
        //
        
        if (index["trip_status"] as! String).lowercased() == "pending" {
            // Ben10 print("is_acceptable)
            
            if is_acceptable == "0" {
                cell.btnCellAccept.isHidden = true
                cell.btnCellAcceptHightCon.constant = 0
                
            } else {
                
                cell.btnCellAccept.setTitle("START TRIP".uppercased(), for: .normal)
                cell.btnCellAccept.borderColor = COLOR_PRIMARY_TEXT_COLOR
                cell.btnCellAccept.borderWidth = 1.5
                cell.btnCellAccept.backgroundColor = COLOR_PRIMARY_TEXT_COLOR
                cell.btnCellAccept.setTitleColor(COLOR_WHITE, for: .normal)
                
                cell.btnCellAccept.isHidden = false
                cell.btnCellAcceptHightCon.constant = 30

                
                
            }
        }
        else if (index["trip_status"] as! String).lowercased() == "stop" {
            
            cell.btnCellAccept.setTitle("complete".uppercased(), for: .normal)
            cell.btnCellAccept.borderColor = COLOR_PRIMARY_TEXT_COLOR
            cell.btnCellAccept.borderWidth = 1.5
            cell.btnCellAccept.backgroundColor = COLOR_PRIMARY_TEXT_COLOR
            cell.btnCellAccept.setTitleColor(COLOR_WHITE, for: .normal)
            
            cell.btnCellAccept.isHidden = false
            cell.btnCellAcceptHightCon.constant = 30

            
        }
            
        else if (index["trip_status"] as! String).lowercased() == "start" {
            
            cell.btnCellAccept.setTitle("In Progress".uppercased(), for: .normal)
//            cell.btnCellAccept.setTitle("accepted".uppercased(), for: .normal)
            cell.btnCellAccept.borderColor = COLOR_PRIMARY_TEXT_COLOR
            cell.btnCellAccept.borderWidth = 1.5
            cell.btnCellAccept.backgroundColor = COLOR_PRIMARY_TEXT_COLOR
            cell.btnCellAccept.setTitleColor(COLOR_WHITE, for: .normal)
            
            cell.btnCellAccept.isHidden = false
            cell.btnCellAcceptHightCon.constant = 30

            
            
        } else {
            
            // complete order
            
            cell.btnCellAccept.setTitle("complete".uppercased(), for: .normal)
            cell.btnCellAccept.borderColor = COLOR_PRIMARY_TEXT_COLOR
            cell.btnCellAccept.borderWidth = 1.5
            cell.btnCellAccept.backgroundColor = COLOR_PRIMARY_TEXT_COLOR
            cell.btnCellAccept.setTitleColor(COLOR_WHITE, for: .normal)

            cell.btnCellAccept.isHidden = false
            cell.btnCellAcceptHightCon.constant = 30
            
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !isApiCall { return }
        
        let sendData = (allDataTrip[indexPath.row] as! NSDictionary)["trip"] as! NSDictionary
        
        if sendData.count != 0 {
            
            if sendData["trip_status"] as! String == "pending" {
                

            }
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "YourTripOrderTrackVC") as? YourTripOrderTrackVC
            vc!.RecivedData = sendData
            vc!.tripDate = lblDate.text!
            vc!.backVCparm = ParmApi
            vc!.imgSinStopArray = imgSinStopArray
            vc!.YourTripsVC_obj = self
            self.navigationController!.pushViewController(vc!, animated: true)
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    
}

class DateHelper {
    
    lazy var formatter:DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd,yyyy"
        return formatter
    }()
    
    lazy var dateComponents:DateComponents = {
        var dateComp = DateComponents()
        dateComp.day = 1
        return dateComp
    }()
    
    func getNext(dateString:String) -> String?
    {
        if let date     =  self.formatter.date(from: dateString),
            let nextDate = Calendar.current.date(byAdding: self.dateComponents, to: date)
        {
            return self.formatter.string(from: nextDate)
        }
        return nil
    }
}

extension YourTripsVC {
    
    func convertNextDate(dateDic : NSDictionary) -> NSDictionary  {
        
        let day = dateDic["day"] as! String
        //        let date = dateDic["date"] as! String
        
        let dateFormatter = DateFormatter()
        
        let ONE = (dateDic["date"] as! String).replacingOccurrences(of: ".", with: "")
        let date = ONE.replacingOccurrences(of: ",", with: "")
        
        
        // 1.
        
        dateFormatter.dateFormat = "MMM. d, yyyy"
        let myDate = dateFormatter.date(from: date)!
        let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: myDate)
        let NextDate = dateFormatter.string(from: tomorrow!)
        
        // 2 .
        
        dateFormatter.dateFormat = "EEEE"
        let DayName = dateFormatter.date(from: day)!
        let tomorrow2 = Calendar.current.date(byAdding: .day, value: 1, to: DayName)
        let nextDay = dateFormatter.string(from: tomorrow2!)
        
        let Dic = ["day" : nextDay,
                   "date" : NextDate]
        
        return Dic as NSDictionary
    }
    
    func convertLessDate(dateDic : NSDictionary) -> NSDictionary  {
        
        let day = dateDic["day"] as! String
        //        let date = dateDic["date"] as! String
        
        let ONE = (dateDic["date"] as! String).replacingOccurrences(of: ".", with: "")
        let date = ONE.replacingOccurrences(of: ",", with: "")
        
        let dateFormatter = DateFormatter()
        
        // 1.
        
        dateFormatter.dateFormat = "MMM. d, yyyy"
        let myDate = dateFormatter.date(from: date)!
        let tomorrow = Calendar.current.date(byAdding: .day, value: -1, to: myDate)
        let NextDate = dateFormatter.string(from: tomorrow!)
        
        // 2 .
        
        dateFormatter.dateFormat = "EEEE"
        let DayName = dateFormatter.date(from: day)!
        let tomorrow2 = Calendar.current.date(byAdding: .day, value: -1, to: DayName)
        let nextDay = dateFormatter.string(from: tomorrow2!)
        
        let Dic = ["day" : nextDay,
                   "date" : NextDate]
        
        return Dic as NSDictionary
    }
    
    func filterDateWiseArray(totalArray : NSArray) -> NSArray {
        
        let filteredArray = totalArray.filter{($0 as! NSDictionary)["date"] as! String == "\(lblDate.text!)"}
        
        return filteredArray as NSArray
        
    }
    
}


extension YourTripsVC {
    
    func apiAllTrip(isShowHUD : Bool) {
        
        self.allDataTrip.removeAllObjects()
        
        if !isApiCall { return }
        
        isApiCall = false
        
        if isShowHUD == true {
            
            self.viewLoderHightConstant.constant = 35
            self.viewDownLoderHightConstant.constant = 0

            } else {
            
            self.viewLoderHightConstant.constant = 0
            
        }
        
        
        let formate = ((dicDateFormat[filter_type] as! NSDictionary)["forAPI"] as! String)
        
        let newDate = GlobalClass.DateToString(date: selectedDate, dateFormatte: formate)
        
        let parm = ["driver_id": User("user_id"),
                    "created_date" : String(newDate),
                    "limit" : limit,
                    "offset" : offset,
                    "filter_type" : filter_type,
                    "trip_status" : trip_status] as [String : Any]
        
        self.RelodeAPiData = [:]
        apiTimer?.invalidate()
        apiTimer = nil
        ParmApi = parm as NSDictionary
        GlobalClass.multipartData("all_trip", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
            
            self.isApiCall = true
            
            let result = self.IfDic(dicM: response!.mutableCopy() as! NSMutableDictionary)
            
            if ((result["data"] as! NSDictionary)["driverData"] as? NSDictionary) != nil  {
                let driverData = ((result["data"] as! NSDictionary)["driverData"] as? NSDictionary)
                GlobalClass.setUserDefaultDICT(value: driverData!)
                self.ImageSetUser()
                
            }

            if ((result["data"] as! NSDictionary)["tripData"] as? NSArray) != nil  {
                
                self.stopRefresher()
                
             
                
                let AllArrayDic = (result["data"] as! NSDictionary)["tripData"] as! NSMutableArray
                
                self.allDataTrip = AllArrayDic
                
                if  (((response as! NSDictionary)["data"] as! NSDictionary)["tripData"] as! NSArray).count == self.limit {
                    self.pagenination = true
                }
                
                self.arrayFilterStatus = (((response as! NSDictionary)["data"] as! NSDictionary)["trip_all_status"] as! NSArray)
                
                let LocationArray = self.allDataTrip.filter{ (((($0 as! NSDictionary)["trip"] as! NSDictionary)["trip_status"] as! String)) == "start"}
                
                // Ben10 print("LocationArray)
                
                if LocationArray.count > 0 {
                    
                    let trip = ((LocationArray as NSArray)[0] as! NSDictionary)["trip"] as! NSDictionary
                    
                    if trip.count > 0 {
                        
                        self.RelodeAPiData = trip
                        self.tripStartOne()
                      //  self.locationManager.startUpdatingLocation()
                        self.StartUpdateLocation()
                        
                    }
                } else if LocationArray.count == 0 {
                    
                    self.RelodeAPiData = [:]
                    apiTimer?.invalidate()
                    apiTimer = nil
                    // Ben10 print(""NO ONE TRIP STATRT THIS DATE")
                    
                } else {
                    
                    self.RelodeAPiData = [:]
                    apiTimer?.invalidate()
                    apiTimer = nil
                    // Ben10 print(""NO ONE TRIP STATRT THIS DATE")
                    
                }
            }
            
            //            GlobalClass.hideHud()
            
            self.viewLoderHightConstant.constant = 0
            self.tableView.reloadData()
         //   self.StartUpdateLocation()
            
        }, successWithStatus0Closure: { (responce) in
            
            self.isApiCall = true

            self.allDataTrip = []
            
            self.tableView.reloadData()
            
            self.viewLoderHightConstant.constant = 0

            
            let responsemsg = (responce as! NSDictionary)["responsemsg"] as? String ?? ""
            let responsecode = (responce as! NSDictionary)["responsecode"] as? String ?? ""
            
            if responsecode as? String ?? "" == "-2" {
                
                GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
                    
                    GlobalClass.setUserDefault(value: "", for: "landingPage")
                    GlobalClass.removeUserDefaults("getDistributorList")
                    
                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                    
                })
                
//                GlobalClass.showAlertOKCancelAction(APP_NAME, responsemsg, self, successClosure: { (ok) in
//
//                    GlobalClass.setUserDefault(value: "", for: "landingPage")
//                    GlobalClass.removeUserDefaults("getDistributorList")
//
//                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
//                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
//
//                }, cancelClosure: { (cancel) in
//                })
                return
            }
            
            if responsemsg == "Not Show" {
                return
            }
            
            GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (str) in
            })
            //
            //            self.isApiCall = true
            self.StartUpdateLocation()
            
        }) { (error) in
            
            self.isApiCall = true

            
            self.allDataTrip = []
            
            self.viewLoderHightConstant.constant = 0
            
            self.tableView.reloadData()
            
            // self.isApiCall = true
            
            if error == "Not Show" {
                return
            }
            
            // Ben10 print("error as Any)
            
            GlobalClass.showAlertOKAction(APP_NAME, error!, "OK", self, successClosure: { (str) in })
            self.StartUpdateLocation()
        }
    }
    
    func statusUpdateApi(indexPath : Int) {
        
        guard let tripId = ((allDataTrip[indexPath] as! NSDictionary)["trip"] as! NSDictionary)["trip_id"] as? String else { return }
        
        if !isApiCall { return }
        
        self.viewLoderHightConstant.constant = 35
        
        isApiCall = false
        
        let parm = ["trip_id": tripId]
        
        GlobalClass.multipartData("startTrip", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
            
            self.isApiCall = true
            self.StartUpdateLocation()
            if Reachability.isConnectedToNetwork(){
                self.dataApiRelod(indexPath: indexPath, tripID: tripId)
            } else {
                                GlobalClass.showAlertOKAction(APP_NAME, "No Network Avalable", "OK", self) { (result) in
                                                        self.apiAllTrip(isShowHUD: false)
                                }
            }
           
            
        }, successWithStatus0Closure: { (responce) in
            
            GlobalClass.hideHud()
            
            self.viewLoderHightConstant.constant = 0
            
            self.isApiCall = true
            
            let responceError = (responce as! NSDictionary)["responsemsg"] as? String ?? ""
            
            if responceError == "Not Show" {
                return
            }
            
            let responsemsg = (responce as! NSDictionary)["responsemsg"] as? String ?? ""
            let responsecode = (responce as! NSDictionary)["responsecode"] as? String ?? ""
            
            if responsecode as? String ?? "" == "-2" {
                
                GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
                    
                    GlobalClass.setUserDefault(value: "", for: "landingPage")
                    GlobalClass.removeUserDefaults("getDistributorList")
                    
                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                    return
                    
                })
            }
            
            GlobalClass.showAlertOKAction(APP_NAME, responceError, "OK", self, successClosure: { (str) in })
            
            
        }) { (error) in
            
            // Ben10 print("error as Any)
            
            self.viewLoderHightConstant.constant = 0
            
            self.tableView.reloadData()
            
            self.isApiCall = true
            
            if error == "Not Show" {
                return
            }
            
            GlobalClass.showAlertOKAction(APP_NAME, error!, "OK", self, successClosure: { (str) in
            })
        }
    }
    
    
    func dataApiRelod(indexPath : Int, tripID : String) {
        // Ben10 print("tripID)
        
        if !isApiCall { return }
        
        isApiCall = false
        
        self.viewLoderHightConstant.constant = 0
        
        let one = (lblDate.text!).replacingOccurrences(of: ".", with: "")
        
        let DATE = (one).replacingOccurrences(of: ",", with: "")
        
        let Date = GlobalClass.StringDateToDate(dateString: DATE, dateFormatte: "MMM dd yyyy")
        //  2017/11/05
        let newDate = GlobalClass.DateToString(date: Date, dateFormatte: "yyyy-MM-dd")
        
        let parm = ["driver_id": User("user_id"),
                    "created_date" : String(newDate),
                    "limit" : limit,
                    "offset" : offset,
                    "filter_type" : filter_type,
                    "trip_status" : trip_status] as [String : Any]
        
        self.RelodeAPiData = [:]
        apiTimer?.invalidate()
        apiTimer = nil
        
        GlobalClass.multipartData("all_trip", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
            
            self.isApiCall = true
            
            let result = self.IfDic(dicM: response!.mutableCopy() as! NSMutableDictionary)

            self.allDataTrip = []
            


            if ((result["data"] as! NSDictionary)["tripData"] as? NSArray != nil)  {
                
                self.stopRefresher()
                
                let AllArrayDic = (result["data"] as! NSDictionary)["tripData"] as? NSMutableArray
                self.allDataTrip = AllArrayDic!
                
                //                self.allDataTrip = GlobalClass.convertListToMutalbe(array: AllArrayDic)
                
                self.tableView.reloadSections([0], with: UITableView.RowAnimation.automatic)
                
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "YourTripOrderTrackVC") as? YourTripOrderTrackVC
                
                let filterData = self.allDataTrip.filter{ (((($0 as! NSDictionary)["trip"] as! NSDictionary)["trip_id"] as! String)) == tripID}
                
                if filterData.count > 0 {
                    
                    let sendData = (filterData[0] as! NSDictionary)["trip"] as! NSDictionary
                    
                    self.RelodeAPiData = sendData
                    
                    self.tripStartOne()
                    
                    if let nextVC = vc {
                        nextVC.RecivedData = sendData
                        vc!.tripDate = self.lblDate.text!
                        vc!.backVCparm = self.ParmApi
                        vc!.imgSinStopArray = self.imgSinStopArray
                        vc!.YourTripsVC_obj = self
                        self.navigationController!.pushViewController(nextVC, animated: true)
                    }
                }
                
                
            //
                
                let LocationArray = self.allDataTrip.filter{ (((($0 as! NSDictionary)["trip"] as! NSDictionary)["trip_status"] as! String)) == "start"}
                
                // Ben10 print("LocationArray)
                
                if LocationArray.count > 0 {
                    
                    let trip = ((LocationArray as NSArray)[0] as! NSDictionary)["trip"] as! NSDictionary
                    
                    if trip.count > 0 {
                        
                        self.RelodeAPiData = trip
                        
                        self.StartUpdateLocation()
                        self.ApiCallCurrentLocation()
                        
                    }
                } else if LocationArray.count == 0 {
                    
                    self.RelodeAPiData = [:]
                    apiTimer?.invalidate()
                    apiTimer = nil
                    // Ben10 print(""NO ONE TRIP STATRT THIS DATE")
                    
                } else {
                    
                    self.RelodeAPiData = [:]
                    apiTimer?.invalidate()
                    apiTimer = nil
                    // Ben10 print(""NO ONE TRIP STATRT THIS DATE")
                    
                }
            
                //
            
            
            self.viewLoderHightConstant.constant = 0
            
            self.tableView.reloadData()
            
                
            }
            
        }, successWithStatus0Closure: { (responce) in
            
//            self.allDataTrip = []
            
            //            GlobalClass.hideHud()
            
            self.viewLoderHightConstant.constant = 0
            
            self.tableView.reloadData()
            
            self.isApiCall = true
            
            let responsemsg = (responce as! NSDictionary)["responsemsg"] as? String ?? ""
            let responsecode = (responce as! NSDictionary)["responsecode"] as? String ?? ""
            
            if responsecode as? String ?? "" == "-2" {
                
                GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
                    
                    GlobalClass.setUserDefault(value: "", for: "landingPage")
                    GlobalClass.removeUserDefaults("getDistributorList")
                    
                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                    
                })
                
//                GlobalClass.showAlertOKCancelAction(APP_NAME, responsemsg, self, successClosure: { (ok) in
//
//                    GlobalClass.setUserDefault(value: "", for: "landingPage")
//                    GlobalClass.removeUserDefaults("getDistributorList")
//
//                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
//                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
//
//                }, cancelClosure: { (cancel) in
//                })
                return
            }
            
            self.StartUpdateLocation()
            
        }) { (error) in
            
            
            self.viewLoderHightConstant.constant = 0
            
            self.isApiCall = true
            
//            self.allDataTrip = []
            
            self.tableView.reloadData()
            
            self.isApiCall = true
            
            if error == "Not Show" {
                return
            }
            
            //            let responceError = (error as! NSDictionary)["responsemsg"] as? String ?? ""
            //
            GlobalClass.showAlertOKAction(APP_NAME, error ?? "", "OK", self, successClosure: { (str) in
            })
            GlobalClass.hideHud()
            
            self.StartUpdateLocation()
            
        }
    }
    
    func pageninationApi() {
        
           self.viewLoderHightConstant.constant = 0
           self.viewDownLoderHightConstant.constant = 35

            let formate = ((dicDateFormat[filter_type] as! NSDictionary)["forAPI"] as! String)
            
            // Ben10 print(""selectedDate : \(selectedDate)")
            // Ben10 print(""CurrentDate : \(lblDate.text!)")
            
            let newDate = GlobalClass.DateToString(date: selectedDate, dateFormatte: formate)
            
            let parm = ["driver_id": User("user_id"),
                        "created_date" : String(newDate),
                        "limit" : limit,
                        "offset" : offset,
                        "filter_type" : filter_type,
                        "trip_status" : trip_status] as [String : Any]
            
            self.RelodeAPiData = [:]
//            apiTimer?.invalidate()
//            apiTimer = nil
            ParmApi = parm as NSDictionary
            GlobalClass.multipartData("all_trip", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
                
                self.isApiCall = true
                
                let result = self.IfDic(dicM: response!.mutableCopy() as! NSMutableDictionary)
                
                if ((result["data"] as! NSDictionary)["driverData"] as? NSDictionary) != nil  {

                    let driverData = ((result["data"] as! NSDictionary)["driverData"] as? NSDictionary)
                    GlobalClass.setUserDefaultDICT(value: driverData!)
                      self.ImageSetUser()

                }
                
                
                if ((result["data"] as! NSDictionary)["tripData"] as? NSArray) != nil  {
                    
                    self.stopRefresher()
                    
                    let AllArrayDic = (result["data"] as! NSDictionary)["tripData"] as! NSMutableArray
                    
                    if  AllArrayDic.count == self.limit {
                        self.pagenination = true
                    }
                    
                    // Ben10 print(""before Data : \(self.allDataTrip.count)")
                    
                    let dataMutable = GlobalClass.convertListToMutalbe(array: AllArrayDic)
                    
                    for i in dataMutable {
                        
                      self.allDataTrip.add(i)
                        
                    }
                    
                    // Ben10 print(""After Data : \(self.allDataTrip.count)")
                    
                    self.arrayFilterStatus = (((response as! NSDictionary)["data"] as! NSDictionary)["trip_all_status"] as! NSArray)

                    let LocationArray = self.allDataTrip.filter{ (((($0 as! NSDictionary)["trip"] as! NSDictionary)["trip_status"] as! String)) == "start"}

                    // Ben10 print("LocationArray)

                    if LocationArray.count > 0 {

                        let trip = ((LocationArray as NSArray)[0] as! NSDictionary)["trip"] as! NSDictionary

                        if trip.count > 0 {

                            self.RelodeAPiData = trip
                            self.tripStartOne()

                        }
                    } else if LocationArray.count == 0 {

                        self.RelodeAPiData = [:]
                        apiTimer?.invalidate()
                        apiTimer = nil
                        // Ben10 print(""NO ONE TRIP STATRT THIS DATE")

                    } else {

                        self.RelodeAPiData = [:]
                        apiTimer?.invalidate()
                        apiTimer = nil
                        // Ben10 print(""NO ONE TRIP STATRT THIS DATE")

                    }
                    
                }
                
                // GlobalClass.hideHud()
                
                self.viewDownLoderHightConstant.constant = 0
                
                self.tableView.reloadData()
                
                self.StartUpdateLocation()
                
            }, successWithStatus0Closure: { (responce) in
                
                self.isApiCall = true
                
//                self.allDataTrip = []
                
                //          GlobalClass.hideHud()
                
                self.viewLoderHightConstant.constant = 0
                self.viewDownLoderHightConstant.constant = 0
                
                self.tableView.reloadData()
                
//                self.viewLoderHightConstant.constant = 0
                
                let responsemsg = (responce as! NSDictionary)["responsemsg"] as? String ?? ""
                let responsecode = (responce as! NSDictionary)["responsecode"] as? String ?? ""
                
                if responsecode as? String ?? "" == "-2" {
                    
                    GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
                        
                        GlobalClass.setUserDefault(value: "", for: "landingPage")
                        GlobalClass.removeUserDefaults("getDistributorList")
                        
                        let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
                        UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                        
                    })
                    
//                    GlobalClass.showAlertOKCancelAction(APP_NAME, responsemsg, self, successClosure: { (ok) in
//
//
//
//                    }, cancelClosure: { (cancel) in
//                    })
                    return
                }
                
                if responsemsg == "Not Show" {
                    return
                }
                
                GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (str) in })
                
                //
                //            self.isApiCall = true
                self.StartUpdateLocation()
                
            }) { (error) in
                
                self.isApiCall = true
                
//                self.allDataTrip = []
                
                self.viewLoderHightConstant.constant = 0
                self.viewDownLoderHightConstant.constant = 0
                
                
                self.tableView.reloadData()
                
                // self.isApiCall = true
                
                if error == "Not Show" {
                    return
                }
                
                // Ben10 print("error as Any)
                
                GlobalClass.showAlertOKAction(APP_NAME, error!, "OK", self, successClosure: { (str) in })
                self.StartUpdateLocation()
         }
        
    }
    
    func colorCodeSetup () {
        
        lblYourTripHard.textColor = COLOR_WHITE
        lblDay.textColor = COLOR_WHITE
        lblDate.textColor = COLOR_WHITE
        filterViewBG.backgroundColor = COLOR_ACCENT_VIEW_BG
        
    }
    
    func ImageSetUser() {
        
        userData = GlobalClass.getUserDefaultDICT()
        
        let imageUrl = userData["profile"] as? String
        
//        var FullUrl = String()
        
        if let validUrl = imageUrl {
            //            FullUrl = ImageBaseUrl + validUrl
            userImage.sd_setImage(with: URL(string: validUrl), placeholderImage: UIImage.init(named: "userProfile"), options: .transformAnimatedImage) { (img, error, type , url) in
                if error == nil {
                }
            }
        } else{
        }
    }
    
    func IfDic(dicM : NSMutableDictionary) -> NSMutableDictionary {
        for (key, val) in dicM {
            
            if let array = val as? NSArray{
                let temp = array.mutableCopy() as! NSMutableArray
                let tp = self.IfArray(arrayM: temp)
                dicM.setValue(tp, forKey: key as! String)
            }
            else  if let dic = val as? NSDictionary{
                let temp = dic.mutableCopy() as! NSMutableDictionary
                let tp = self.IfDic(dicM: temp)
                dicM.setValue(tp, forKey: key as! String)
            }
        }
        
        return dicM
    }
    
    func IfArray(arrayM : NSMutableArray) -> NSMutableArray {
        var i = 0
        for val in arrayM {
            
            if let array = val as? NSArray{
                let temp = array.mutableCopy() as! NSMutableArray
                let tp = self.IfArray(arrayM: temp)
                arrayM.replaceObject(at: i, with: tp)
            }
            else  if let dic = val as? NSDictionary{
                let temp = dic.mutableCopy() as! NSMutableDictionary
                let tp = self.IfDic(dicM: temp)
                arrayM.replaceObject(at: i, with: tp)
            }
            i = i + 1
        }
        return arrayM
    }
}


extension YourTripsVC : CLLocationManagerDelegate {
    
    func setUpViewLoderUP(){
        
        let activityIndicator = NVActivityIndicatorView(frame: self.viewLoderAnim.bounds)
        activityIndicator.type = .ballPulseSync
        activityIndicator.color = UIColor.black
        
        self.viewLoderAnim.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
    
    func setUpViewLoderDown(){
        
        let activityIndicator = NVActivityIndicatorView(frame: self.viewDownLoderAnim.bounds)
        activityIndicator.type = .ballPulseSync
        activityIndicator.color = UIColor.black
        
        self.viewDownLoderAnim.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.count = self.count + 1
        // Ben10 print(""\(self.count)")

        
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        lat = CGFloat(locValue.latitude)
        long =  CGFloat(locValue.longitude)
        
       
        
        
        if RelodeAPiData.count > 0 {
            
            let dic = ["trip_id" : (RelodeAPiData["trip_id"] as? String ?? ""),
                       "let" : "\(lat!)",
                        "long" : "\(long!)",
                        "status" : "0"] as NSDictionary
            
            InsertInTo_TableDriverLocation(dataDic: dic)
            
            
            self.updateLocation()
            
            if isFirstTime {
                self.ApiCallCurrentLocation()
                isFirstTime = false
            }
        }
    }
    
    
    func tripStartOne() {
        
        if RelodeAPiData.count > 0 {
            
//            apiTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.updateLocation()), userInfo: nil, repeats: true)
//            RunLoop.current.add(apiTimer!, forMode: RunLoop.Mode.default)
            
            
            
            
        } else {
            
            apiTimer?.invalidate()
            apiTimer = nil
            // Ben10 print(""NO ONE TRIP STATRT THIS DATE")
            
        }
    }
    
    @objc func updateLocation() {
        
        activity = true
        
        switch CMMotionActivityManager.authorizationStatus() {
            
        case CMAuthorizationStatus.denied:
            
            GlobalClass.showAlertOKCancelAction(APP_NAME, "Please go to Settings and turn on the motions & Fitness.", self, successClosure: { (Ok) in
                
                // Ben10 print(""OK")
                
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl){
                    UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
                }
                
            }) { (Cancel) in
                // Ben10 print(""Cancel")
            }
            
            // Ben10 print(""Not Authorize")
            
            return
            
        case CMAuthorizationStatus.authorized :
            
            print("Authorize")
            
        default: break
            
        }
        
         self.ApiCallCurrentLocation()
   
    }
    
    @objc func ApiCallCurrentLocation() {
        
        activity = false
        
        // Ben10 print(""API Call")
        
        if RelodeAPiData["trip_id"] as? String ?? "" == "" || RelodeAPiData["trip_id"] as? String ?? "" == "<null>" ||  RelodeAPiData["driver_id"] as? String ?? "" == "" || RelodeAPiData["driver_id"] as? String ?? "" == "<null>"{
            return
        } else if lat == 0.0 || long == 0.0 || lat == nil || long == nil {
            return
        } else {
            
            if isApiCalled_trip_tracking {
                return
            }
            isApiCalled_trip_tracking = true
            
            
            let arrayDriverLocation = get_data_from_Table(trip_id: RelodeAPiData["trip_id"] as! String)
            
            var locationArray = NSMutableArray()
            for i in 0 ..< arrayDriverLocation.count {
                let dic = arrayDriverLocation[i] as! NSDictionary
                
                let dicTamp = ["lat" : dic["let"] as! String, "lng" : dic["long"] as! String]
                locationArray.add(dicTamp)
            }
            
            if locationArray.count == 0 {
                let dicTamp = ["lat" : lat, "lng" : long]
                locationArray.add(dicTamp)
            }
            
            
            let parm = ["trip_id" : RelodeAPiData["trip_id"] as Any,
                        "driver_id" : RelodeAPiData["driver_id"] as Any,
                        "location" : GlobalClass.arrayToJson(from: locationArray)] as [String : Any]
            
            
            
            GlobalClass.multipartData("trip_tracking", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
                self.isApiCalled_trip_tracking = false
                
                
                for i in 0 ..< arrayDriverLocation.count {
                    let dic = arrayDriverLocation[i] as! NSDictionary
                    // Ben10 print("dic)
                    UpdateStatus(id: "\(dic["id"] as! Int)")
                }
                
            }, successWithStatus0Closure: { (responce) in
                self.isApiCalled_trip_tracking = false
                
                let responsemsg = (responce as! NSDictionary)["responsemsg"] as? String ?? ""
                let responsecode = (responce as! NSDictionary)["responsecode"] as? String ?? ""
                
                if responsecode as? String ?? "" == "-2" {
                    
                    GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
                        
                        GlobalClass.setUserDefault(value: "", for: "landingPage")
                        GlobalClass.removeUserDefaults("getDistributorList")
                        
                        let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
                        UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                        return
                        
                    })
                }

                
            }) { (error) in
                self.isApiCalled_trip_tracking = false
            }
        }
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (self.tableView.frame.height + scrollView.contentOffset.y) >= scrollView.contentSize.height {
            if pagenination {
                self.pagenination = false
//                limit += 10
                offset += 1
                // Ben10 print(""api call more data pagenination offset\(offset)")
                self.pageninationApi()
            } else {
//                // Ben10 print(""api dont't call scrollView pagenination false")
            }
        }
    }
}

extension YourTripsVC : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func arrayOfMonth() {
        arrayMonth = ["January","February","March","April","May","June","July","August","September","October","November","December"]
        
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if filter_type == "month" {
            // customDatePicker.selectRow(3, inComponent: 0, animated: false)
            return 2
        }
        else {
            
            return 1
        }
    }
    

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if filter_type == "month" {
            if component == 0 {
                return arrayMonth.count
            }
            
            
        }
        
        
        return 10
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if filter_type == "month" {
            if component == 0 {
                return arrayMonth[row] as! String
            }
        }
        return "\(row + 2015)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let date = (GlobalClass.DateToString(date: Date(), dateFormatte: "dd") )
        let month = (GlobalClass.DateToString(date: selectedDateFromPicker, dateFormatte: "MM") )
        let year = GlobalClass.DateToString(date: selectedDateFromPicker, dateFormatte: "yyyy")
        
        
        var newDate = ""
        if filter_type == "month" {
            if component == 0 {
                // Ben10 print(""month")
                newDate = date + " " + "\(String(format: "%02d", row + 1))" + " " + year
                // Ben10 print("newDate)
            }
            else if component == 1 {
                // Ben10 print(""year")
                newDate = date + " " + month + " " + "\(row + 2015)"
                // Ben10 print("newDate)
            }
        }
        else if filter_type == "year" {
            // Ben10 print(""year")
            newDate = date + " " + month + " " + "\(row + 2015)"
            // Ben10 print("newDate)
        }
        
        selectedDateFromPicker = GlobalClass.StringDateToDate(dateString: newDate, dateFormatte: "dd MM yyyy")
    }
    
}

extension YourTripsVC : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtDatePiker {
            if filter_type == "date" {
                self.txtDatePiker.inputView = datePicker
                self.txtDatePiker.inputAccessoryView = toolBar
            }
                
            else {
                self.txtDatePiker.inputView = customDatePicker
                self.txtDatePiker.inputAccessoryView = toolBar
                
                selectedDateFromPicker = selectedDate
                
                // Ben10 print("selectedDate)
                
                let month = Int(GlobalClass.DateToString(date: selectedDate, dateFormatte: "MM") as? String ?? "1")!
                // Ben10 print(""case 1")
                let year = Int(GlobalClass.DateToString(date: selectedDate, dateFormatte: "yyyy") as? String ?? "1")!
                // Ben10 print(""case 2")
                
                if filter_type == "month" {
                    // 1860
                    customDatePicker.reloadAllComponents()
                    customDatePicker.selectRow(month - 1, inComponent: 0, animated: false)
                    // Ben10 print("customDatePicker.numberOfComponents)
                    if customDatePicker.numberOfComponents == 2 {
                        customDatePicker.selectRow((year) - 2015 , inComponent: 1, animated: false)
                    }
                    else {
                        
                    }
                    // Ben10 print(""case 4")

                    
                }
                else {
                    customDatePicker.selectRow((year) - 2015 , inComponent: 0, animated: false)
                    // Ben10 print(""case 5")

                }
            }
        }
    }
}


