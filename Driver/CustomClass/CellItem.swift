//
//  CellItem.swift
//  Restaurant Depot
//
//  Created by Bijender Singh on 30/01/19.
//  Copyright © 2019 Bijender Singh. All rights reserved.
//

import UIKit

class CellItem: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblLbsPerPack: UILabel!
    @IBOutlet weak var lblCost: UILabel!
    @IBOutlet weak var imgItem: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setupView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
       // self.setupView()
        // Configure the view for the selected state
    }
    
    func setupView(){
       
        self.lblName.textColor = COLOR_PRIMARY_TEXT_COLOR
        self.lblCategory.textColor = COLOR_PRIMARY_TEXT_COLOR
        self.lblLbsPerPack.textColor = COLOR_PRIMARY_TEXT_COLOR
        self.lblCost.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        self.imgItem.backgroundColor = COLOR_PRIMARY_TEXT_COLOR
    }

}
