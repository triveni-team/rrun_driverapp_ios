//
//  ViewLableHeader.swift
//  Restaurant Depot
//
//  Created by Bijender Singh on 30/01/19.
//  Copyright © 2019 Bijender Singh. All rights reserved.
//

import UIKit

class ViewLableHeader: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
       self.setupView()
    }
    
    @IBOutlet weak var lblHeader: UILabel!
    var viewWidth : CGFloat! = 0
    @IBOutlet weak var viewTopLine: UIView!
    @IBOutlet weak var viewBottomLine: UIView!

    
    
    required init?(coder aDecoder: NSCoder) {
       // fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    func setupView(){
        // 192 , 194 , 197
        let view = Bundle.main.loadNibNamed("ViewLableHeader", owner: self, options: nil)?[0] as! UIView
       
        self.addSubview(view)
        view.frame  = self.bounds
        viewTopLine.backgroundColor = COLOR_PRIMARY
        viewBottomLine.backgroundColor = COLOR_PRIMARY
        
        lblHeader.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        let textWidth = lblHeader.text?.width(withConstraintedHeight: 40, font: lblHeader.font)
        
        
        viewWidth = textWidth! + 70 + 100
        
    }
}
