//
//  ViewLableHeader.swift
//  Restaurant Depot
//
//  Created by Bijender Singh on 30/01/19.
//  Copyright © 2019 Bijender Singh. All rights reserved.
//

import UIKit
var CurrentLetLon = ""
class ViewShowRoot: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
       self.setupView()
    }
    
    var letLon = ""
    
    required init?(coder aDecoder: NSCoder) {
       // fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    func setupView(){
        // 192 , 194 , 197
        let view = Bundle.main.loadNibNamed("ViewShowRoot", owner: self, options: nil)?[0] as! UIView
       
        self.addSubview(view)
        view.frame  = self.bounds
        
    }
    
    @IBAction func btnAction(_ sender: Any) {
        let str = CurrentLetLon + "/" + letLon
        // Ben10 print(""str34 \(str)")
        
        let url = "https://www.google.com/maps/dir/\(str)"
        if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
            UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
        } else {
            // Ben10 print(""Can't use comgooglemaps://")
            UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
        }
        
    }
    
}
