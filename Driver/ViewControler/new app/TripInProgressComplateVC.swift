//
//  TripInProgressComplateVC.swift
//  Driver New
//
//  Created by Arthonsys Ben on 08/07/19.
//  Copyright © 2019 Bijender Singh. All rights reserved.
//

import UIKit

class TripInProgressComplateVC: UIViewController {

    
    
    @IBOutlet weak var lblHeaderTripStatus: UILabel!
    @IBOutlet weak var lblHeaderTripName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblDriverName: UILabel!
    
    
    @IBOutlet weak var btnSignAll: UIButton!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var imgRightTick: UIImageView!
    @IBOutlet weak var tableViewTrip: UITableView!
    @IBOutlet weak var lblTripId: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblOreders: UILabel!
    @IBOutlet weak var lblItems: UILabel!
    var collapesedHeader = NSMutableArray()
    
    var isType = ""
    var trip_current_status = ""
    var stopDataFromBack = NSMutableDictionary()
    var nextStopDataFromBack = NSMutableDictionary()
    var dataFromBack = NSMutableDictionary()
    var ordersArray = NSMutableArray()
    var isForCompletTrip = false

    @IBOutlet weak var constHeightSighAll: NSLayoutConstraint!
    
    var stopStatus = "CURRENT STOP"
    
    
    // from back
    var TripInProgressVC_obj : TripInProgressVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Ben10 print(""stopDataFromBack = \(stopDataFromBack)")
        // Ben10 print(""is type = \(isType)")
        trip_current_status = (stopDataFromBack["stop"] as! NSMutableDictionary)["trip_current_status"] as! String
        // Do any additional setup after loading the view.
        
        refresh()
        
        
        self.lblHeaderTripName.text = dataFromBack["trip_name"] as? String
        if dataFromBack["trip_status"] as! String == "start" {
            self.lblHeaderTripStatus.text = "TRIP IN PROGRESS"
        }
        else {
            self.lblHeaderTripStatus.text = "VIEW TRIP"
        }
        setUserData()
    }
    
    // set user data
    func setUserData() {
        
        let userData = GlobalClass.getUserDefaultDICT()
        
        lblDriverName.text = userData["name"] as? String
        
        let imageUrl = userData["profile"] as? String
        if let validUrl = imageUrl {
            imgProfile.sd_setImage(with: URL(string: validUrl), placeholderImage: UIImage.init(named: "userProfile"), options: .transformAnimatedImage) { (img, error, type , url) in
                if error == nil {
                }
            }
        } else{
        }
    }
    
    
    
    func refresh() {
        self.ordersArray = (stopDataFromBack["stop"] as! NSMutableDictionary)["orders"] as! NSMutableArray
        
        if (self.isType == "current") && ((self.stopDataFromBack["stop"] as! NSMutableDictionary)["trip_current_status"] as! String == "current") {
            self.setDataInHeader()
        }
        
        self.sizeHeaderToFit()
        self.tableViewTrip.reloadData()
    }
    
    func refreshForNextStop() {
        self.stopStatus = "DELIVERED"
        setDataInHeader()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if isForCompletTrip  {
            isForCompletTrip = false
            self.performSegue(withIdentifier: "toVC", sender: nil)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tableViewTrip.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    func sizeHeaderToFit() {
        let headerView = tableViewTrip.tableHeaderView!
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        var frame = headerView.frame
        frame.size.height = height
        headerView.frame = frame
        
        if (self.isType == "current") && ((self.stopDataFromBack["stop"] as! NSMutableDictionary)["trip_current_status"] as! String == "current") {
            tableViewTrip.tableHeaderView = headerView
        }
        else {
            tableViewTrip.tableHeaderView = nil
        }
    }
    
    func setDataInHeader() {
        self.lblTitle.text = (stopDataFromBack["stop"] as! NSMutableDictionary)["stop_name"] as? String
        self.lblNumber.text = (stopDataFromBack["stop"] as! NSMutableDictionary)["priority"] as? String
        self.lblOreders.text = (stopDataFromBack["stop"] as! NSMutableDictionary)["total_order"] as? String
        self.lblItems.text = (stopDataFromBack["stop"] as! NSMutableDictionary)["total_item"] as? String ?? String((stopDataFromBack["stop"] as! NSMutableDictionary)["total_item"] as? String ?? "0")
        
        if self.stopStatus == "CURRENT STOP" {
            self.btnSignAll.setTitle("SIGN ALL", for: .normal)
            self.btnSignAll.tag = 6666
            self.imgRightTick.isHidden = true
            self.lblStatus.text = "CURRENT STOP"
        }
        else {
            self.btnSignAll.setTitle("NEXT STOP", for: .normal)
            self.btnSignAll.tag = 7777
            self.imgRightTick.isHidden = false
            self.lblStatus.text = "DELIVERED"
        }
        

    }
    
    //MARK:- All Button Action
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDriverProfile(_ sender: UIButton) {
        NavigationCustom.navToDriverProfile(viewController: self)
    }
    
    @IBAction func btnSignAllAction(_ sender: UIButton) {
        if sender.tag == 6666 {
            self.performSegue(withIdentifier: "toVC", sender: nil)
        }
        else if sender.tag == 7777 {
            TripInProgressVC_obj.startCurrentTrip()
            self.stopDataFromBack = self.nextStopDataFromBack
            self.stopStatus = "CURRENT STOP"
            self.refresh()
        }
        
    }
    
    @IBAction func btnCellReviewOrder(_ sender: UIButton) {
        let point = sender.convert(CGPoint.zero, to: tableViewTrip as UIView)
        let indexPath = tableViewTrip.indexPathForRow(at: point)
        
        self.performSegue(withIdentifier: "ToOrderDetailVC", sender: indexPath)
    }
    
    @IBAction func btnCellDetails(_ sender: UIButton) {
        let point = sender.convert(CGPoint.zero, to: tableViewTrip as UIView)
        let indexPath = tableViewTrip.indexPathForRow(at: point)
        
        self.performSegue(withIdentifier: "ToOrderDetailVC", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToOrderDetailVC" {
            let indexPath = sender as? IndexPath
            var dic = NSMutableDictionary()
           // var isType = ""
            
            dic = ordersArray[indexPath?.row ?? 0] as! NSMutableDictionary
            let vc = segue.destination as! OrderDetailVC
            vc.orderDataFromBack = dic
            vc.dataFromBack = self.dataFromBack
            vc.isType = trip_current_status
            // send at index path if you want
            
        }
        else if  segue.identifier == "toVC" {
            let vc = segue.destination as! SignatureVC
            vc.TripInProgressComplateVC_obj = self
            vc.titleStr =  lblTitle.text ?? ""
        }
    }
}

class TableViewReviewOrderCell: UITableViewCell {
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblItems: UILabel!
    @IBOutlet weak var lblReturned: UILabel!
}

class TableViewDeliveredCell: UITableViewCell {
    @IBOutlet weak var lblOrderId: UILabel!
    @IBOutlet weak var lblItems: UILabel!
    @IBOutlet weak var lblReturned: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    
}

extension TripInProgressComplateVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.collapesedHeader.contains(section) {
            return 0
        }
        return self.ordersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dic = self.ordersArray[indexPath.row] as! NSMutableDictionary
        // order_return_item_count
        if dic["order_status"] as! String == "pending" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewReviewOrderCell", for: indexPath) as! TableViewReviewOrderCell
            cell.lblOrderId.text = "Order #: \(dic["order_increment_id"] as? String ?? "")"
            cell.lblItems.text = dic["order_item_count"] as? String ?? String(dic["order_item_count"] as? Int ?? 0)
            cell.lblReturned.text = String(dic["order_return_item_count"] as? String ?? "0")
            return cell
        }
        else {  // change this else to else if condition
            let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewDeliveredCell", for: indexPath) as! TableViewDeliveredCell
            cell.lblOrderId.text = "Order #: \(dic["order_increment_id"] as? String ?? "")"
            cell.lblItems.text = dic["order_item_count"] as? String ?? String(dic["order_item_count"] as? Int ?? 0)
            cell.lblReturned.text = (dic["return_qty"] as? String == "") ? "0" : dic["return_qty"] as? String
            return cell
        }
      
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width:tableView.frame.size.width, height: 35))
        viewHeader.backgroundColor = #colorLiteral(red: 0.1960784314, green: 0.5215686275, blue: 0.2509803922, alpha: 1)
        
        let labelTitle = UILabel(frame: CGRect(x: 50, y: 0, width: (tableView.frame.size.width) - 50 * 2, height: 35))
        labelTitle.text = "ORDERS"
        labelTitle.textColor = .white
        labelTitle.textAlignment = .center
        labelTitle.font = UIFont(name: "Arial", size: 17)
        viewHeader.addSubview(labelTitle)
        
        let buttonTap = UIButton(frame: CGRect(x: (viewHeader.frame.size.width) - 35, y: 0, width: 35, height: 35))
        if self.collapesedHeader.contains(section) {
            buttonTap.setImage(UIImage(named: "down-arrow"), for: .normal)
        }else{
            buttonTap.setImage(UIImage(named: "up-arrow"), for: .normal)
        }
        buttonTap.addTarget(self, action: #selector(btnAction(_:)), for: .touchUpInside)
        viewHeader.addSubview(buttonTap)
        buttonTap.tag = section + 1
        // Ben10 print("buttonTap.tag - 1)
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    @objc func btnAction(_ sender: UIButton) {
        let section = sender.tag - 1
        if self.collapesedHeader.contains(section) {
            self.collapesedHeader.remove(section)
        }else{
            self.collapesedHeader.add(section)
        }
        // Ben10 print("self.collapesedHeader)
        self.tableViewTrip.reloadSections([section], with: .automatic)
    }
    
}

extension TripInProgressComplateVC {
    func signAll(imgSign : UIImage) {
        // call api for sign all
        // and update data on home of current trip
        api(imgSign : imgSign)
        
        self.TripInProgressVC_obj.oneTimeRefresh = true
        self.TripInProgressVC_obj.completAction()
        // we call this when api called
    }
    
    func api(imgSign : UIImage) {
        
        let arrayOrder = NSMutableArray()
        
        for i in self.ordersArray {
            let order = i as! NSMutableDictionary
            
            let itemsArray = order["items"] as! NSMutableArray
            let resItemArray = NSMutableArray()
            for j in itemsArray {
                let item = j as! NSMutableDictionary
                let ordItemQtyInt = Int(item["real_quantity"] as! String) ?? 0
                let retItemQtyInt = Int(item["return_qty"] as! String == "" ? "0" : item["return_qty"] as! String) ?? 0
                let finalQty = ordItemQtyInt - retItemQtyInt
                let dic34 = ["qty_delivered" : String(finalQty),
                             "qty_ordered" : item["real_quantity"] as! String,
                             "qty_return" : String(retItemQtyInt)]
                
                
                
                

                let dic = [item["order_item_id"] as! String : dic34]
                resItemArray.add(dic)
                
                
            }
            let dic2 = ["order_status" : order["order_status"] as! String ,
                        "trip_id" : CURRENT_TRIP["trip_id"] as Any,
                        "distributor_code" : order["distributor_code"] as! String,  // ben change it this is hard code
                        "items" : resItemArray] as [String : Any]
            let dic3 = [order["id"] as! String : dic2]
            arrayOrder.add(dic3)
        }
        
       // // Ben10 print("arrayOrder)
        
        
        let jsonString = GlobalClass.arrayToJson(from: arrayOrder)
        
        let myDictOfDict:NSMutableDictionary = [
            "orders" : jsonString as Any,
            "stop_id" : (self.stopDataFromBack["stop"] as! NSMutableDictionary)["stop_id"] as Any,
            "trip_id" : CURRENT_TRIP["trip_id"] as Any,
            "device_token" : DEVICE_TOKEN,
            "image" : GlobalClass.imageToStrinig(imgSign),
            "status" : "success",
            "driver_id" : User("user_id")]
           
            // status as ('pending'/'skip'/'inprogress'/'success'/'default')
  
        // Ben10 print("myDictOfDict)
        

        self.apiOrderComplete(parm: myDictOfDict) // ben uncomment this for call orderComplete API
    }
    
    
    func apiOrderComplete(parm : NSMutableDictionary) {
        
        let dicLog = ["api" : "orderComplete", "parms" : GlobalClass.dicToJson(dictionary: parm as NSDictionary), "status" : "1"] as NSDictionary
        let last_id = InsertInTo_TableBackLog(is_type: "backLog", dataDic: dicLog)
        let backlog_data = GlobalClass.makeBackLogKey()
        parm.setValue(backlog_data, forKey: "backlog_data")

        TripInProgressVC_obj.TripsVC_obj.getNewLetLon()
        GlobalClass.multipartData("orderComplete", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
            DeleteItem_TableBackLog(is_type: "backLog", id: last_id) // this is for remove all backlogs
            // Ben10 print("response)
//            self.TripInProgressVC_obj.oneTimeRefresh = true
//            self.TripInProgressVC_obj.completAction()
            
        }, successWithStatus0Closure: { (responce) in
            
            let massage = (responce as! NSDictionary).object(forKey: "responsemsg") as! String
            
            let responsemsg = (responce as! NSDictionary)["responsemsg"] as? String ?? ""
            let responsecode = (responce as! NSDictionary)["responsecode"] as? String ?? ""
            
            if responsecode == "-2" {
                
                GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
                    
                    GlobalClass.setUserDefault(value: "", for: "landingPage")
                    GlobalClass.removeUserDefaults("getDistributorList")
                    
                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                    return
                    
                })
            }
            
            GlobalClass.showAlertOKAction(APP_NAME, massage, "OK", self, successClosure: { (str) in
                
            self.dismiss(animated: true, completion: nil)
                
                
            })
            
            
        }) { (error) in
            
            if error == "Not Show" {
                return
            }
            
            GlobalClass.showAlertOKAction(APP_NAME, error!, "OK", self, successClosure: { (str) in
            })
            GlobalClass.hideHud()
            
        }
    }
}
