//
//  TripsVC.swift
//  Driver New
//
//  Created by Arthonsys Ben on 05/07/19.
//  Copyright © 2019 Bijender Singh. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import NVActivityIndicatorView


var CURRENT_TRIP = NSMutableDictionary()
var BACKLOG_DATA = NSMutableDictionary()


class TripsVC: UIViewController, MKMapViewDelegate {
    
    //MARK:- All Outlet
    @IBOutlet weak var tableViewTrip: UITableView!
    //    @IBOutlet weak var lblDateFilterName: UILabel!
    //    @IBOutlet weak var lblStatusFilterName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblNoTrips: UILabel!
    
    @IBOutlet weak var lblReturnToTrip: UILabel!
    @IBOutlet weak var viewBackRetuenToTrip: UIView!
    @IBOutlet weak var imageRetuenToTrip: UIImageView!
    @IBOutlet weak var viewLoderAnim: UIView!
    @IBOutlet weak var viewLoder: UIView!
    @IBOutlet weak var constHeightLoading: NSLayoutConstraint!
    @IBOutlet weak var viewLoderAnimBottom: UIView!
    @IBOutlet weak var viewLoderBottom: UIView!
    @IBOutlet weak var constHeightLoadingBottom: NSLayoutConstraint!
    
    //for filter
    var trip_status = "all"
    var filter_type = "date"
    @IBOutlet weak var lblDateFilterName: UILabel!
    @IBOutlet weak var lblStatusFilterName: UILabel!
    var selectedDate = Date()
    var selectedDateFromPicker = Date()
    var dicDateFormat = NSDictionary()
    var customDatePicker: UIPickerView!
    var arrayMonth = NSArray()
    @IBOutlet weak var txtDatePiker: UITextField!
    var datePicker = UIDatePicker()
    var toolBar = UIToolbar()
    
    var refresher:UIRefreshControl!
    
    
    // data variable
    var arrayFilterStatus = NSArray()
    var allDataTrip = NSMutableArray()
    var userData = NSDictionary()
    var offset = 0
    var isApiCall = false
    
    var pagenination = false
    var limit = 10 //limit
    
    // for next VC
    var TripInProgressVC_obj : TripInProgressVC!
    var selectedIndexPath : IndexPath?
    
    //from next page
    var isTripComplit = ""
    
    // for location
    var locationManager = CLLocationManager()
    var lastLocation : CLLocationCoordinate2D!
    var distanceFilter = CLLocationDistance(100)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpViewLoderUP()
        GlobalClass.setUserDefault(value: "TripsNavVC", for: "landingPage")
        filter()
        setUserData()
        refresView ()
        testing()
        oneTimeUpdateLocation()
        //    self.changeLandingScreen()
        // StartUpdateLocation()
        
        
    }
    
    func changeLandingScreen() {
        if CURRENT_TRIP.count != 0 && self.TripInProgressVC_obj == nil {
            self.performSegue(withIdentifier: "ToTripInProgressVC", sender: "notForShow")
            self.StartUpdateLocation()
        }
    }
    
    func testing() {
        DeleteItem_TableBackLog(is_type: "sd", id: "43")
        let sks = GlobalClass.makeBackLogKey()
        // Ben10 print("sks)
        return
        let dic = ["api" : "demoAPI", "parms" : "demoPrms", "status" : "1"] as NSDictionary
        //  let jsonDIc = GlobalClass.dicToJson(dictionary: dic)
        InsertInTo_TableBackLog(is_type: "backLog", dataDic: dic)
        
        let da = get_data_from_TableBackLog(is_type: "backLog")
        //// Ben10 print("da)
        let parms = (da[5] as! NSDictionary)["parms"] as! String
        let parmDic = GlobalClass.jsonToDic(jsonString: parms)
        // Ben10 print("parmDic)
        let img = parmDic["image"] as! String
        let i = GlobalClass.stringToImage(img)
        // Ben10 print(""df")
        
        DeleteItem_TableBackLog(is_type: "sd", id: "19")
        
    }
    
    var isSetup = false
    override func viewDidAppear(_ animated: Bool) {
        
        if !isSetup {
            isSetup = true
            print("ben**1")
            self.changeLandingScreen()
            // self.setUpViewLoderUP()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.TripInProgressVC_obj = nil
        self.selectedIndexPath = nil
        tripCompletChangeUI()
        
    }
    
    func refresView () {
        
        self.refresher = UIRefreshControl()
        self.tableViewTrip.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.black
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.tableViewTrip.addSubview(refresher)
    }
    
    func tripCompletChangeUI() {
        self.tableViewTrip.reloadData()
        self.setDataInUI()
    }
    
    @objc func loadData() {
        self.offset = 0
        apiAllTrip(isShowHUD: false, callFrom: "self")
        
        stopRefresher()
        
    }
    
    var hodForBottom = false
    func showHod() {
        //        self.viewLoder.isHidden = false
        if hodForBottom {
            self.constHeightLoadingBottom.constant = 30
        }
        else{
            self.constHeightLoading.constant = 30
        }
    }
    func hideHod() {
        //        self.viewLoder.isHidden = true
        self.hodForBottom = false
        self.constHeightLoading.constant = 0
        self.constHeightLoadingBottom.constant = 0
        
    }
    func setUpViewLoderUP(){
        
        let activityIndicatorBottom = NVActivityIndicatorView(frame: self.viewLoderAnimBottom.bounds)
        activityIndicatorBottom.type = .ballPulseSync
        activityIndicatorBottom.color = UIColor.black
        self.viewLoderAnimBottom.addSubview(activityIndicatorBottom)
        activityIndicatorBottom.startAnimating()
        
        let activityIndicator = NVActivityIndicatorView(frame: self.viewLoderAnim.bounds)
        activityIndicator.type = .ballPulseSync
        activityIndicator.color = UIColor.black
        self.viewLoderAnim.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        hideHod() // for starting
    }
    
    func stopRefresher() {  self.refresher.endRefreshing() }
    
    func StartUpdateLocation() {
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            self.lastLocation = nil
            locationManager.delegate = self as? CLLocationManagerDelegate
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.activityType = .automotiveNavigation
            locationManager.distanceFilter = distanceFilter
            locationManager.allowsBackgroundLocationUpdates = true
            locationManager.startUpdatingLocation()
        }
    }
    
    func oneTimeUpdateLocation() {
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            self.lastLocation = nil
            locationManager.delegate = self as? CLLocationManagerDelegate
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.activityType = .automotiveNavigation
            locationManager.distanceFilter = distanceFilter
            locationManager.startUpdatingLocation()
        }
    }
    
    func stopLocationUpdate() {
        self.locationManager.stopUpdatingLocation()
      //  self.oneTimeUpdateLocation()
    }
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            break
        default:
            break
        }
    }
    
    
    // set user data
    func setUserData() {
        
        let userData = GlobalClass.getUserDefaultDICT()
        
        lblDriverName.text = userData["name"] as? String
        
        let imageUrl = userData["profile"] as? String
        if let validUrl = imageUrl {
            imgProfile.sd_setImage(with: URL(string: validUrl), placeholderImage: UIImage.init(named: "userProfile"), options: .transformAnimatedImage) { (img, error, type , url) in
                if error == nil {
                }
            }
        } else{
        }
    }
    
    // filter methods
    
    func filter() {
        arrayOfMonth()
        dicDateFormat = ["date" : ["forShow" : "MMM. d, yyyy", "forAPI" : "yyyy-MM-dd"] as NSDictionary,
                         "month" : ["forShow" : "MMM, yyyy", "forAPI" : "yyyy-MM"] as NSDictionary,
                         "year" : ["forShow" : "yyyy", "forAPI" : "yyyy"] as NSDictionary]
        customDatePicker = UIPickerView()
        customDatePicker.delegate = self
        customDatePicker.dataSource = self
        lblDateFilterName.text = "DATE"
        lblStatusFilterName.text = "ALL"
        pikerViewSetup()
        self.setCurrentDate()
        
    }
    
    func pikerViewSetup () {
        
        let doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(DoneActionPickerView))
        doneButton.tintColor = COLOR_WHITE
        
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(CancelActionPickerView))
        
        cancelButton.tintColor = COLOR_WHITE
        
        toolBar.frame = CGRect(x: 0, y: self.view.frame.size.height-50, width: self.view.frame.size.width, height: 50)
        toolBar.barStyle = UIBarStyle.blackOpaque
        toolBar.setItems([doneButton,spacer,cancelButton], animated: false)
        
        self.txtDatePiker.inputView = datePicker
        self.txtDatePiker.inputAccessoryView = toolBar
        datePicker.datePickerMode = .date
        datePicker.minimumDate = GlobalClass.StringDateToDate(dateString: "01 01 2015", dateFormatte: "dd MM yyyy")
        datePicker.maximumDate = GlobalClass.StringDateToDate(dateString: "31 12 2024", dateFormatte: "dd MM yyyy")
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd, YYYY"
        
        //  datePicker
        
    }
    
    @objc func DoneActionPickerView() {
        
        self.view.endEditing(true)
        
        if filter_type == "date" {
            selectedDate = datePicker.date
        }
            
        else {
            selectedDate = selectedDateFromPicker
        }
        self.setCurrentDate()
        
    }
    
    @objc func CancelActionPickerView() {
        self.view.endEditing(true)
    }
    
    func setCurrentDate() {
        let formatter = DateFormatter()
        formatter.dateFormat = ((dicDateFormat[filter_type] as! NSDictionary)["forShow"] as! String)
        let result = formatter.string(from: selectedDate)
        txtDatePiker.text = result
        offset = 0
        self.allDataTrip.removeAllObjects()
        self.tableViewTrip.reloadData()
        self.apiAllTrip(isShowHUD: true, callFrom: "self")
    }
    
    func convertLastDate(dateDic : NSDictionary) -> NSDictionary  {
        
        //  let day = dateDic["day"] as! String
        //        let date = dateDic["date"] as! String
        
        let ONE = (dateDic["date"] as! String).replacingOccurrences(of: ".", with: "")
        let date = ONE.replacingOccurrences(of: ",", with: "")
        
        let dateFormatter = DateFormatter()
        
        // 1.
        
        dateFormatter.dateFormat = "MMM. d, yyyy"
        let myDate = dateFormatter.date(from: date)!
        let tomorrow = Calendar.current.date(byAdding: .day, value: -1, to: myDate)
        let NextDate = dateFormatter.string(from: tomorrow!)
        
        // 2 .
        
        //        dateFormatter.dateFormat = "EEEE"
        //        let DayName = dateFormatter.date(from: day)!
        //        let tomorrow2 = Calendar.current.date(byAdding: .day, value: -1, to: DayName)
        //        let nextDay = dateFormatter.string(from: tomorrow2!)
        
        let Dic = ["date" : NextDate]
        
        return Dic as NSDictionary
    }
    
    func convertNextDate(dateDic : NSDictionary) -> NSDictionary  {
        
        //  let day = dateDic["day"] as! String
        //        let date = dateDic["date"] as! String
        
        let dateFormatter = DateFormatter()
        
        let ONE = (dateDic["date"] as! String).replacingOccurrences(of: ".", with: "")
        let date = ONE.replacingOccurrences(of: ",", with: "")
        
        
        // 1.
        
        dateFormatter.dateFormat = "MMM. d, yyyy"
        let myDate = dateFormatter.date(from: date)!
        let tomorrow = Calendar.current.date(byAdding: .day, value: 1, to: myDate)
        let NextDate = dateFormatter.string(from: tomorrow!)
        
        // 2 .
        //
        //        dateFormatter.dateFormat = "EEEE"
        //        let DayName = dateFormatter.date(from: day)!
        //        let tomorrow2 = Calendar.current.date(byAdding: .day, value: 1, to: DayName)
        //        let nextDay = dateFormatter.string(from: tomorrow2!)
        
        let Dic = ["date" : NextDate]
        
        return Dic as NSDictionary
    }
    
    @IBAction func btnLastDate(_ sender: UIButton) {
        
        //        offset = 0
        
        if filter_type == "date" {
            let Dic = ["date" : txtDatePiker.text]
            
            let NextDateDic = convertLastDate(dateDic: Dic as NSDictionary)
            
            // txtDatePiker.text = (NextDateDic["date"] as? String)!.uppercased()
            
            selectedDate  = GlobalClass.StringDateToDate(dateString: NextDateDic["date"] as! String, dateFormatte: "MMM. d, yyyy")
            
            if Reachability.isConnectedToNetwork() {
                // apiAllTrip(isShowHUD: true)
            }
            else{
                GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "No Network Avalable", view: self)
            }
        }
        else{
            
            let date = Int(GlobalClass.DateToString(date: selectedDate, dateFormatte: "dd") ?? "0")!
            var month = Int(GlobalClass.DateToString(date: selectedDate, dateFormatte: "MM") ?? "0")!
            var year = Int(GlobalClass.DateToString(date: selectedDate, dateFormatte: "yyyy") ?? "0")!
            
            
            if filter_type == "month" {
                
                if month == 1 {
                    if year <= 2015 {
                        return
                    }
                    month = 12
                    year = year - 1
                }
                else {
                    month = month - 1
                }
                
            }
            else if filter_type == "year" {
                if year <= 2015 {
                    return
                }
                else {
                    year = year - 1
                }
                
            }
            
            let newDate = "\(date)" + " " + "\(month)" + " " + "\(year)"
            selectedDate = GlobalClass.StringDateToDate(dateString: newDate, dateFormatte: "dd MM yyyy")
            
        }
        
        self.setCurrentDate()
        
    }
    
    @IBAction func btnNextDate(_ sender: UIButton) {
        
        //        offset = 0
        
        if filter_type == "date" {
            let Dic = ["date" : txtDatePiker.text]
            
            let NextDateDic = convertNextDate(dateDic: Dic as NSDictionary)
            
            //   txtDatePiker.text = (NextDateDic["date"] as? String)!.uppercased()
            
            selectedDate  = GlobalClass.StringDateToDate(dateString: NextDateDic["date"] as! String, dateFormatte: "MMM. d, yyyy")
            
            if Reachability.isConnectedToNetwork() {
                //                apiAllTrip(isShowHUD: true)
            }
            else{
                GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "No Network Avalable", view: self)
            }
            
        }
        else {
            let date = Int(GlobalClass.DateToString(date: selectedDate, dateFormatte: "dd") ?? "0")!
            var month = Int(GlobalClass.DateToString(date: selectedDate, dateFormatte: "MM") ?? "0")!
            var year = Int(GlobalClass.DateToString(date: selectedDate, dateFormatte: "yyyy") ?? "0")!
            
            
            if filter_type == "month" {
                
                if month == 12 {
                    if year >= 2024 {
                        return
                    }
                    month = 1
                    year = year + 1
                }
                else {
                    month = month + 1
                }
                
            }
            else if filter_type == "year" {
                if year >= 2024 {
                    return
                }
                else {
                    year = year + 1
                }
                
            }
            let newDate = "\(date)" + " " + "\(month)" + " " + "\(year)"
            selectedDate = GlobalClass.StringDateToDate(dateString: newDate, dateFormatte: "dd MM yyyy")
            
        }
        self.setCurrentDate()
    }
    
    @IBAction func btnStatusFilter(_ sender: UIButton) {
        self.view.endEditing(true)
        filterStatus()
    }
    
    func filterStatus() {
        
        if arrayFilterStatus.count == 0 { return }
        
        let optionMenu = UIAlertController(title: nil, message: "CHOOSE STATUS", preferredStyle: .actionSheet)
        
        if let popoverController = optionMenu.popoverPresentationController {
            popoverController.sourceView = txtDatePiker
            popoverController.sourceRect = CGRect(x: 0, y: 0, width: txtDatePiker.frame.width, height: txtDatePiker.frame.height)
        }
        
        for i in 0 ..< self.arrayFilterStatus.count {
            let dic = self.arrayFilterStatus[i] as! NSDictionary
            
            let ALL = UIAlertAction(title: (dic["value"] as? String ?? "").uppercased(), style: .default, handler:
            {
                (alert: UIAlertAction!) -> Void in
                // Ben10 print("dic["key"])
                self.trip_status = dic["key"] as? String ?? "all"
                self.lblStatusFilterName.text = (dic["value"] as? String ?? "ALL").uppercased()
                self.offset = 0
                self.apiAllTrip(isShowHUD: true, callFrom: "self")
                
            })
            optionMenu.addAction(ALL)
            
        }
        
        let cancel = UIAlertAction(title: "CANCEL", style: .cancel) { (dsf) in
            
        }
        optionMenu.addAction(cancel)
        
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    @IBAction func btnDateFilter(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        let optionMenu = UIAlertController(title: nil, message: "CHOOSE DATE TYPE", preferredStyle: .actionSheet)
        
        if let popoverController = optionMenu.popoverPresentationController {
            popoverController.sourceView = txtDatePiker
            
            popoverController.sourceRect = CGRect(x: 0, y: 0, width: txtDatePiker.frame.width, height: txtDatePiker.frame.height)
        }
        
        let dateAction = UIAlertAction(title: "DATE", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            self.filter_type = "date"
            self.lblDateFilterName.text = "DATE"
            self.setCurrentDate()
            //  self.apiAllTrip(isShowHUD: true)
        })
        
        let monthAction = UIAlertAction(title: "MONTH", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            self.filter_type = "month"
            self.lblDateFilterName.text = "MONTH"
            self.setCurrentDate()
            //    self.apiAllTrip(isShowHUD: true)
            
        })
        
        let yearAction = UIAlertAction(title: "YEAR", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            self.filter_type = "year"
            self.lblDateFilterName.text = "YEAR"
            self.setCurrentDate()
            
            //  self.apiAllTrip(isShowHUD: true)
            
        })
        
        let cancel = UIAlertAction(title: "CANCEL", style: .cancel) { (dsf) in
            
        }
        
        optionMenu.addAction(dateAction)
        optionMenu.addAction(monthAction)
        optionMenu.addAction(yearAction)
        optionMenu.addAction(cancel)
        
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:- All Button Action
    @IBAction func btnPreviousAction(_ sender: UIButton) {
    }
    
    @IBAction func btnNextAction(_ sender: UIButton) {
    }
    
    @IBAction func btnDriverAction(_ sender: Any) {
        NavigationCustom.navToDriverProfile(viewController: self)
    }
    
    
    
    @IBAction func btnShowDetails(_ sender: UIButton) {
        let point = sender.convert(CGPoint.zero, to: tableViewTrip as UIView)
        let indexPath: IndexPath! = tableViewTrip.indexPathForRow(at: point)
        let dic = self.allDataTrip[indexPath.row] as! NSMutableDictionary
        self.performSegue(withIdentifier: "ToTripInProgressVC", sender: dic)
        self.selectedIndexPath = indexPath
        
    }
    
    @IBAction func btnContinueTripAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "ToTripInProgressVC", sender: "notForShow")
    }
    @IBAction func btnReturnToTripAction(_ sender: Any) {
        if CURRENT_TRIP.count == 0 { return }
        self.performSegue(withIdentifier: "ToTripInProgressVC", sender: "notForShow")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToTripInProgressVC" {
            let vc = segue.destination as! TripInProgressVC
            
            
            if let val = sender as? String {
                vc.isForDetailShow = false
            }
            else {
                vc.dataFromBack = sender as! NSMutableDictionary
            }
            vc.TripsVC_obj = self
            self.TripInProgressVC_obj = vc
        }
    }
    
}

//MARK:- TableViewStartTrip
class TableViewStartTripCell: UITableViewCell {
    
    @IBOutlet weak var lblTripId: UILabel!
    @IBOutlet weak var lblStop: UILabel!
    @IBOutlet weak var lblOrders: UILabel!
    @IBOutlet weak var lblItems: UILabel!
    
    @IBOutlet weak var btnStartContinueTrip: UIButton!
    
    @IBOutlet weak var viewBack: UIView!
    
}

//MARK:- TableViewInProgress
class TableViewInProgressCell: UITableViewCell {
    
    @IBOutlet weak var lblTripId: UILabel!
    @IBOutlet weak var lblStop: UILabel!
    @IBOutlet weak var lblOrders: UILabel!
    @IBOutlet weak var lblItems: UILabel!
    @IBOutlet weak var btnStartContinueTrip: UIButton!
    
}

//MARK:- TableView Extension
extension TripsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allDataTrip.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.allDataTrip.count == 0 {
            return UITableViewCell()
        }
        
        let dic = self.allDataTrip[indexPath.row] as! NSMutableDictionary
        let trip_id = dic["trip_id"] as! String
        if (dic["trip_status"] as? String) == "start" && trip_id == CURRENT_TRIP["trip_id"] as? String{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewInProgressCell", for: indexPath) as! TableViewInProgressCell
            cell.lblTripId.text = dic["trip_name"] as? String
            cell.lblStop.text = dic["total_stops"] as? String
            cell.lblOrders.text = dic["total_orders"] as? String
            cell.lblItems.text = dic["total_items"] as? String
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewStartTripCell", for: indexPath) as! TableViewStartTripCell
            cell.lblTripId.text = dic["trip_name"] as? String
            cell.lblStop.text = dic["total_stops"] as? String
            cell.lblOrders.text = dic["total_orders"] as? String
            cell.lblItems.text = dic["total_items"] as? String
            cell.btnStartContinueTrip.setTitle("START TRIP", for: .normal)
            cell.viewBack.backgroundColor = UIColor.init(hexString: "C7EBF6")
            
            if dic["trip_status"] as? String == "stop" {
                cell.btnStartContinueTrip.setTitleColor(UIColor.init(hexString: "#4F22A8"), for: .normal)
                cell.btnStartContinueTrip.backgroundColor = UIColor.clear
                cell.btnStartContinueTrip.isUserInteractionEnabled = false
                cell.btnStartContinueTrip.setTitle("COMPLETED", for: .normal)
                cell.viewBack.backgroundColor = UIColor.init(hexString: "8ECEE2")
            }
            else if CURRENT_TRIP.count != 0 || dic["can_accept"] as? String == "0" {
                cell.btnStartContinueTrip.setTitleColor(UIColor.init(hexString: "#61CBE9"), for: .normal)
                cell.btnStartContinueTrip.backgroundColor = UIColor.init(hexString: "#4BA6C1")
                cell.btnStartContinueTrip.isUserInteractionEnabled = false
            }
            else {
                cell.btnStartContinueTrip.setTitleColor(UIColor.white, for: .normal)
                cell.btnStartContinueTrip.backgroundColor = UIColor.init(hexString: "#00617E")
                cell.btnStartContinueTrip.isUserInteractionEnabled = true
            }
            return cell
        }
    }
    
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (self.tableViewTrip.frame.height + scrollView.contentOffset.y) >= scrollView.contentSize.height {
            if pagenination {
                self.pagenination = false
                offset += 1
                self.hodForBottom = true
                self.apiAllTrip(isShowHUD: true, callFrom: "self")
            }
        }
    }
}


extension TripsVC {
    
    //MARK:- All API
    
    func apiAllTrip(isShowHUD : Bool, callFrom : String) {
        
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.denied {
       
            self.lblNoTrips.text = "Please go to Settings and \n turn on Location Service for this app."
            self.lblNoTrips.isHidden = false
            return
        }
        else{
            self.lblNoTrips.text = "NO TRIPS"
            self.lblNoTrips.isHidden = true
        }
        
        //   if !isApiCall { return }
        
        //   isApiCall = false
        
        if isShowHUD == true {
            
            //            self.viewLoderHightConstant.constant = 35
            //            self.viewDownLoderHightConstant.constant = 0
            
        } else {
            
        }
        showHod()
       // self.lblNoTrips.isHidden = true
        
        let formateForShow = ((dicDateFormat[filter_type] as! NSDictionary)["forShow"] as! String)
        let newDateForShow = GlobalClass.DateToString(date: selectedDate, dateFormatte: formateForShow)

        let formate = ((dicDateFormat[filter_type] as! NSDictionary)["forAPI"] as! String)

        let newDate = GlobalClass.DateToString(date: selectedDate, dateFormatte: formate)
        
        
        let parm = ["driver_id": User("user_id"),
                    "created_date" : String(newDate),
                    "limit" : limit,
                    "offset" : offset,
                    "filter_type" : filter_type,
                    "trip_status" : trip_status] as NSMutableDictionary
        
        
        let dicLog = ["api" : "all_trip_listing", "parms" : GlobalClass.dicToJson(dictionary: parm as NSDictionary), "status" : "1"] as NSDictionary
        let last_id = InsertInTo_TableBackLog(is_type: "backLog", dataDic: dicLog)
        let backlog_data = GlobalClass.makeBackLogKey()
        parm.setValue(backlog_data, forKey: "backlog_data")
        
        GlobalClass.multipartData("all_trip_listing", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
            
            if newDateForShow != self.txtDatePiker.text {
                return
            }
            DeleteItem_TableBackLog(is_type: "backLog", id: last_id) // this is for remove all backlogs
            self.hideHod()
            
            let result = self.IfDic(dicM: response!.mutableCopy() as! NSMutableDictionary)
            
            self.arrayFilterStatus = (((response as! NSDictionary)["data"] as! NSDictionary)["status"] as! NSArray)
            
            
            self.setDataInUI()
            
            if ((result["data"] as! NSDictionary)["driverData"] as? NSDictionary) != nil  {
                let driverData = ((result["data"] as! NSDictionary)["driverData"] as? NSDictionary)
                GlobalClass.setUserDefaultDICT(value: driverData!)
                self.setUserData()
            }
            
            if (((result as NSDictionary)["data"] as! NSDictionary)["trips"] as! NSMutableArray).count == self.limit {
                self.pagenination = true
            }
            else {
                self.pagenination = false
            }
            
            if self.offset == 0 {
                self.allDataTrip.removeAllObjects()
                self.allDataTrip = (((result as NSDictionary)["data"] as! NSDictionary)["trips"] as! NSMutableArray)
            }
            else {
                self.allDataTrip.addObjects(from: ((((result as NSDictionary)["data"] as! NSDictionary)["trips"] as! NSMutableArray) as! [Any]))
            }
            
            if self.allDataTrip.count == 0 {
                self.lblNoTrips.isHidden = false
            }
            else {
                self.lblNoTrips.isHidden = true
            }
            
            self.tableViewTrip.reloadData()
            
            
            if self.TripInProgressVC_obj != nil {
                self.TripInProgressVC_obj.refresh()
            }
            
            let cTrip = (((result as NSDictionary)["data"] as! NSMutableDictionary)["current_trip"] as! NSMutableArray)
            if CURRENT_TRIP.count == 0 {
                if cTrip.count != 0 {
                    CURRENT_TRIP = cTrip[0] as! NSMutableDictionary
                    print("ben**2")
                    self.changeLandingScreen()
                    
                }
                else {
                    CURRENT_TRIP.removeAllObjects()
                }
            }
            else if  CURRENT_TRIP.count != 0 && cTrip.count == 0 {
                CURRENT_TRIP.removeAllObjects()
                GlobalClass.removeUserDefaultCurrentTrip()
                self.TripInProgressVC_obj?.btnTripListAction(UIButton())
                
            }
            
        }, successWithStatus0Closure: { (responce) in
            self.pagenination = false
            self.hideHod()
            
            let responsemsg = (responce as! NSDictionary)["responsemsg"] as? String ?? ""
            let responsecode = (responce as! NSDictionary)["responsecode"] as? String ?? ""
            
            if responsecode == "-2" {
                
                GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
                    
                    GlobalClass.setUserDefault(value: "", for: "landingPage")
                    GlobalClass.removeUserDefaults("getDistributorList")
                    
                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                    
                })
                return
            }
            
        }) { (error) in
            self.hideHod()
        }
    }
    
    func IfDic(dicM : NSMutableDictionary) -> NSMutableDictionary {
        for (key, val) in dicM {
            
            if let array = val as? NSArray{
                let temp = array.mutableCopy() as! NSMutableArray
                let tp = self.IfArray(arrayM: temp)
                dicM.setValue(tp, forKey: key as! String)
            }
            else  if let dic = val as? NSDictionary{
                let temp = dic.mutableCopy() as! NSMutableDictionary
                let tp = self.IfDic(dicM: temp)
                dicM.setValue(tp, forKey: key as! String)
            }
        }
        
        return dicM
    }
    
    func IfArray(arrayM : NSMutableArray) -> NSMutableArray {
        var i = 0
        for val in arrayM {
            
            if let array = val as? NSArray{
                let temp = array.mutableCopy() as! NSMutableArray
                let tp = self.IfArray(arrayM: temp)
                arrayM.replaceObject(at: i, with: tp)
            }
            else  if let dic = val as? NSDictionary{
                let temp = dic.mutableCopy() as! NSMutableDictionary
                let tp = self.IfDic(dicM: temp)
                arrayM.replaceObject(at: i, with: tp)
            }
            i = i + 1
        }
        return arrayM
    }
    
    func setDataInUI() {
        
        if CURRENT_TRIP.count != 0 {
            self.lblReturnToTrip.text = "RETURN TO TRIP"
            self.lblReturnToTrip.textColor = UIColor.init(hexString: "#4F22A8")
            self.imageRetuenToTrip.image = #imageLiteral(resourceName: "truck")
            self.viewBackRetuenToTrip.backgroundColor = UIColor.white
        }
        else {
            self.lblReturnToTrip.text = "No Trip in progress"
            self.lblReturnToTrip.textColor = UIColor.init(hexString: "#C7EBF6")
            self.imageRetuenToTrip.image = #imageLiteral(resourceName: "truck_w")
            self.viewBackRetuenToTrip.backgroundColor = UIColor.init(hexString: "#4F22A8")
        }
        
    }
    
    
    
    @IBAction func btnCellAcceptAction(_ sender: UIButton) {
        
        if isApiCall { return }
        
        let point = sender.convert(CGPoint.zero, to: tableViewTrip as UIView)
        let indexPath = tableViewTrip.indexPathForRow(at: point)
        
        guard let tripId = (allDataTrip[indexPath?.row ?? 0] as! NSDictionary)["trip_id"] as? String else { return }
        isApiCall = true
        apiStartTrip(tripId: tripId, indexPath2: indexPath)
        
    }
    
    func apiStartTrip(tripId : String, indexPath2 : IndexPath?) {
        
        var indexPath = IndexPath()
        if indexPath2 == nil {
            if selectedIndexPath != nil {
                indexPath = selectedIndexPath!
            }
            else {
                return
            }
        }
        else {
            if indexPath2 != nil {
                indexPath = indexPath2!
            }
            else {
                return
            }
        }
        let parm = ["trip_id": tripId] as NSMutableDictionary
        
        let dicLog = ["api" : "startTrip", "parms" : GlobalClass.dicToJson(dictionary: parm as NSDictionary), "status" : "1"] as NSDictionary
        let last_id = InsertInTo_TableBackLog(is_type: "backLog", dataDic: dicLog)
        let backlog_data = GlobalClass.makeBackLogKey()
        parm.setValue(backlog_data, forKey: "backlog_data")
        
        self.showHod()
        GlobalClass.multipartData("startTrip", parameter: parm , MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
            DeleteItem_TableBackLog(is_type: "backLog", id: last_id) // this is for remove all backlogs
            self.hideHod()
            self.isApiCall = false
            (self.allDataTrip[indexPath.row] as! NSMutableDictionary).setValue("start", forKey: "trip_status")
            CURRENT_TRIP = (self.allDataTrip[indexPath.row] as! NSMutableDictionary)
            self.setDataInUI()
            self.tableViewTrip.reloadData()
            
            if self.TripInProgressVC_obj != nil {
                self.TripInProgressVC_obj.refresh()
            }
            else{
                self.btnReturnToTripAction(UIButton())
            }
            
            self.StartUpdateLocation()
            self.getNewLetLon()
            
            
        }, successWithStatus0Closure: { (responce) in
            self.hideHod()
            GlobalClass.hideHud()
            self.isApiCall = false
            let responceError = (responce as! NSDictionary)["responsemsg"] as? String ?? ""
            if responceError == "Not Show" {
                return
            }
            
            let responsemsg = (responce as! NSDictionary)["responsemsg"] as? String ?? ""
            let responsecode = (responce as! NSDictionary)["responsecode"] as? String ?? ""
            
            if responsecode == "-2" {
                GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
                    GlobalClass.setUserDefault(value: "", for: "landingPage")
                    GlobalClass.removeUserDefaults("getDistributorList")
                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                    return
                })
            }
            GlobalClass.showAlertOKAction(APP_NAME, responceError, "OK", self, successClosure: { (str) in })
            
        }) { (error) in
            self.hideHod()
            // Ben10 print("error as Any)
            // self.viewLoderHightConstant.constant = 0
            //  self.tableView.reloadData()
            self.isApiCall = false
            if error == "Not Show" {
                return
            }
            GlobalClass.showAlertOKAction(APP_NAME, error!, "OK", self, successClosure: { (str) in
            })
        }
    }
    
    func callApiTripTracking(parm : NSDictionary, arrayDriverLocation : NSArray) {
        
        GlobalClass.multipartData("trip_tracking", parameter: parm, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
            
            for i in 0 ..< arrayDriverLocation.count {
                let dic = arrayDriverLocation[i] as! NSDictionary
                // Ben10 print("dic)
                UpdateStatus(id: "\(dic["id"] as! Int)")
            }
            
        }, successWithStatus0Closure: { (responce) in
            
            let responsemsg = (responce as! NSDictionary)["responsemsg"] as? String ?? ""
            let responsecode = (responce as! NSDictionary)["responsecode"] as? String ?? ""
            
            if responsecode as? String ?? "" == "-2" {
                
                GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
                    GlobalClass.setUserDefault(value: "", for: "landingPage")
                    GlobalClass.removeUserDefaults("getDistributorList")
                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                    return
                })
            }
        }) { (error) in
        }
    }
    
    @objc func ApiCallCurrentLocation() {
        
        let arrayDriverLocation = get_data_from_Table(trip_id: "")
        
        var locationArray = NSMutableArray()
        var tripId = ""
        for i in 0 ..< arrayDriverLocation.count {
            let dic = arrayDriverLocation[i] as! NSDictionary
            
            let dicTamp = ["lat" : dic["let"] as! String, "lng" : dic["long"] as! String, "trip_id" : dic["trip_id"] as! String]
            locationArray.add(dicTamp)
            
            
        }
        
        if locationArray.count == 0 {
            return
        }
        
        let parm = [//"trip_id" : RelodeAPiData["trip_id"] as Any,
            "driver_id" : User("user_id"),
            "location" : GlobalClass.arrayToJson(from: locationArray)] as NSDictionary
        
        self.callApiTripTracking(parm: parm, arrayDriverLocation: arrayDriverLocation)
        
    }
}


extension TripsVC : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func arrayOfMonth() {
        arrayMonth = ["January","February","March","April","May","June","July","August","September","October","November","December"]
        
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if filter_type == "month" {
            // customDatePicker.selectRow(3, inComponent: 0, animated: false)
            return 2
        }
        else {
            
            return 1
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if filter_type == "month" {
            if component == 0 {
                return arrayMonth.count
            }
            
            
        }
        
        
        return 10
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if filter_type == "month" {
            if component == 0 {
                return arrayMonth[row] as! String
            }
        }
        return "\(row + 2015)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let date = (GlobalClass.DateToString(date: Date(), dateFormatte: "dd") )
        let month = (GlobalClass.DateToString(date: selectedDateFromPicker, dateFormatte: "MM") )
        let year = GlobalClass.DateToString(date: selectedDateFromPicker, dateFormatte: "yyyy")
        
        
        var newDate = ""
        if filter_type == "month" {
            if component == 0 {
                // Ben10 print(""month")
                newDate = date + " " + "\(String(format: "%02d", row + 1))" + " " + year
                // Ben10 print("newDate)
            }
            else if component == 1 {
                // Ben10 print(""year")
                newDate = date + " " + month + " " + "\(row + 2015)"
                // Ben10 print("newDate)
            }
        }
        else if filter_type == "year" {
            // Ben10 print(""year")
            newDate = date + " " + month + " " + "\(row + 2015)"
            // Ben10 print("newDate)
        }
        
        selectedDateFromPicker = GlobalClass.StringDateToDate(dateString: newDate, dateFormatte: "dd MM yyyy")
    }
    
}



extension TripsVC : CLLocationManagerDelegate {
    
    func getNewLetLon() {
        lastLocation = nil
        locationManager.stopUpdatingLocation()
        locationManager.startUpdatingLocation()
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        if lastLocation == nil {
            lastLocation = locValue
        }
        else {
            let loc1 = CLLocation(latitude: lastLocation.latitude, longitude: lastLocation.longitude)
            let loc2 = CLLocation(latitude: locValue.latitude , longitude: locValue.longitude)
            let distance = loc1.distance(from: loc2)
            
            
            if distance < distanceFilter * 0.7 {
                return
            }
        }
        
        lastLocation = locValue
        
        let lat = CGFloat(locValue.latitude)
        let long =  CGFloat(locValue.longitude)
        CurrentLetLon = "\(lat),\(long)"
        
        if CURRENT_TRIP.count == 0 {
            lastLocation = nil
            return
        }
        let dic = ["trip_id" : (CURRENT_TRIP["trip_id"] as? String ?? ""),
                   "let" : "\(lat)",
            "long" : "\(long)",
            "status" : "0"] as NSDictionary
        
        print(dic)
        InsertInTo_TableDriverLocation(dataDic: dic)
        
        
        
        self.ApiCallCurrentLocation()
        
    }
}


extension TripsVC : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtDatePiker {
            if filter_type == "date" {
                self.txtDatePiker.inputView = datePicker
                self.txtDatePiker.inputAccessoryView = toolBar
            }
                
            else {
                self.txtDatePiker.inputView = customDatePicker
                self.txtDatePiker.inputAccessoryView = toolBar
                
                selectedDateFromPicker = selectedDate
                
                // Ben10 print("selectedDate)
                
                let month = Int(GlobalClass.DateToString(date: selectedDate, dateFormatte: "MM") as? String ?? "1")!
                // Ben10 print(""case 1")
                let year = Int(GlobalClass.DateToString(date: selectedDate, dateFormatte: "yyyy") as? String ?? "1")!
                // Ben10 print(""case 2")
                
                if filter_type == "month" {
                    // 1860
                    customDatePicker.reloadAllComponents()
                    customDatePicker.selectRow(month - 1, inComponent: 0, animated: false)
                    // Ben10 print("customDatePicker.numberOfComponents)
                    if customDatePicker.numberOfComponents == 2 {
                        customDatePicker.selectRow((year) - 2015 , inComponent: 1, animated: false)
                    }
                    else {
                        
                    }
                    // Ben10 print(""case 4")
                    
                    
                }
                else {
                    customDatePicker.selectRow((year) - 2015 , inComponent: 0, animated: false)
                    // Ben10 print(""case 5")
                    
                }
            }
        }
    }
}

