//
//  TripAllCompleteVC.swift
//  Driver New
//
//  Created by Technical Lead on 08/07/19.
//  Copyright © 2019 Bijender Singh. All rights reserved.
//

import UIKit

class TripAllCompleteVC: UIViewController {
    
    @IBOutlet weak var tableViewTripComplete: UITableView!
    @IBOutlet weak var lblTripIdHeader: UILabel!
    @IBOutlet weak var lblStops: UILabel!
    @IBOutlet weak var lblOrders: UILabel!
    @IBOutlet weak var lblItems: UILabel!
    @IBOutlet weak var lblSkipped: UILabel!
    @IBOutlet weak var lblCanceled: UILabel!
    @IBOutlet weak var lblReturned: UILabel!
    
    var titleArr = ["Zack's Oak Bar & Restaurent","Place Dinning Room","Greasy Spoon Chophouse"]
    var addressArr = ["777 Total Dhammaal Road Clifton, NJ 07014","380 Super Cool Road Clifton, NJ 07014","456 Neato Street Clifton, NJ 07014"]
    var collapesedHeader = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
   
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:- All Button Action
    @IBAction func btnTripListAction(_ sender: UIButton) {
    }
    
    @IBAction func btnDriverProfileAction(_ sender: UIButton) {
    }
    
    @IBAction func btnBackToTripAction(_ sender: UIButton) {
    }
    
    @IBAction func btnDetailsCellAction(_ sender: UIButton) {
    }
    
    
}

class TableViewTripCompleteCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var imgRightClick: UIImageView!
}

extension TripAllCompleteVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.collapesedHeader.contains(section) {
            return 0
        }
        return titleArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewTripCompleteCell", for: indexPath) as! TableViewTripCompleteCell
        cell.lblTitle.text = titleArr[indexPath.row]
        cell.lblAddress.text = addressArr[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width:tableView.frame.size.width, height: 35))
        viewHeader.backgroundColor = #colorLiteral(red: 0.1960784314, green: 0.5215686275, blue: 0.2509803922, alpha: 1)
        
        let labelTitle = UILabel(frame: CGRect(x: 50, y: 0, width: (tableView.frame.size.width) - 50 * 2, height: 35))
        labelTitle.text = "COMPLETED STOPS"
        labelTitle.textColor = .white
        labelTitle.textAlignment = .center
        labelTitle.font = UIFont(name: "Arial", size: 17)
        viewHeader.addSubview(labelTitle)
        
        let buttonTap = UIButton(frame: CGRect(x: (viewHeader.frame.size.width) - 35, y: 0, width: 35, height: 35))
        if self.collapesedHeader.contains(section) {
            buttonTap.setImage(UIImage(named: "down-arrow"), for: .normal)
        }else{
            buttonTap.setImage(UIImage(named: "up-arrow"), for: .normal)
        }
        buttonTap.addTarget(self, action: #selector(btnTapAction(_:)), for: .touchUpInside)
        viewHeader.addSubview(buttonTap)
        buttonTap.tag = section + 1
        // Ben10 print("buttonTap.tag - 1)
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    @objc func btnTapAction(_ sender: UIButton) {
        let section = sender.tag - 1
        if self.collapesedHeader.contains(section) {
            self.collapesedHeader.remove(section)
        }else{
            self.collapesedHeader.add(section)
        }
        // Ben10 print("self.collapesedHeader)
        self.tableViewTripComplete.reloadSections([section], with: .automatic)
    }

}
