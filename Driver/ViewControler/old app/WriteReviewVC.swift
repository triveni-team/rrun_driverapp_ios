//
//  WriteReviewVC.swift
//  Driver
//
//  Created by Apple on 08/02/19.
//  Copyright © 2019 Arthonsys. All rights reserved.
//

import UIKit

class WriteReviewVC: UIViewController, RateViewDelegate {
    
    @IBOutlet weak var txtMassageHight: NSLayoutConstraint!
    @IBOutlet weak var Viewtxtxmsg: UIView!
    
    //  @IBOutlet weak var textReview: UITextField!
    @IBOutlet weak var lblRatePickerHard: UILabel!
    @IBOutlet weak var lblPickerNameHard: UILabel!
    @IBOutlet weak var lblEmailIDHard: UILabel!
    
    @IBOutlet weak var lblSingalLineHard: UILabel!
    
    @IBOutlet weak var btnSubmitHard: ZFRippleButton!
    // view
    
    @IBOutlet weak var lblpickerName: UILabel!
    @IBOutlet weak var lblEmailID: UILabel!
    
    // view Review

    @IBOutlet weak var rateView: RateView!
    
    @IBOutlet weak var txtViewMessage: UITextView!
    @IBOutlet weak var placeHolder: UITextField!
    
    @IBOutlet weak var constraintTextViewHeight: NSLayoutConstraint!
    
    var RecivedData = NSDictionary()
    
    var pikerRate = Float()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        colorCodeSetup()
        userDataSet()
        rateViewSetup()
        apiGettRate()
        
        self.placeHolder.isHidden = false
        self.txtViewMessage.text = ""
        self.rateView.rating = 0.0
        self.btnSubmitHard.isHidden = false
        
        //        placeHolder.attributedPlaceholder = NSAttributedString(string: "Write a Review",attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        
        //        lblpickerName.text = (RecivedData["PikerName"] as! String).uppercased()
        //        lblEmailID.text = (RecivedData["PikerName"] as! String + "@restaurant.com").trimmingCharacters(in: .whitespaces).uppercased()
        
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func rateViewSetup() {
        rateView.starSize = (screenWidth - 20)/5
        rateView.rating = 5
        rateView.canRate = true
        rateView.step = 0.5
        rateView.delegate = self
        rateView.starNormalColor = UIColor.init(red: 222/255.0, green: 223/255.0, blue: 225/255.0, alpha: 1.0)
        rateView.starBorderColor = UIColor.init(red: 222/255.0, green: 223/255.0, blue: 225/255.0, alpha: 1.0)
        rateView.starFillColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 1.0)
    }
    
    @IBAction func btnSubmit(_ sender: UIButton) {
        
        if txtViewMessage.text == "" {
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Please Enter Write a Review", view: self)
            return
        } else if pikerRate == 0.0 {
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Please Select Rate", view: self)
            return
        }
        
        apiSubmitRate()
        
    }
    
    func userDataSet() {
        
        let userData = GlobalClass.getUserDefaultDICT()
        lblpickerName.text = (userData["name"] as? String ?? "").uppercased()
        lblEmailID.text = (userData["email"] as? String ?? "").uppercased()
        
    }
}


extension WriteReviewVC : UITextViewDelegate {
    
    //MARK:- text view
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let text2 = textView.text as NSString? {
            
            let txtAfterUpdate = text2.replacingCharacters(in: range, with: text as String)
            let font : UIFont = textView.font!
            let txtMsgHeight = String.height(txtAfterUpdate) (withConstrainedWidth: textView.frame.size.width, font : font) + 15
            
            if txtMsgHeight > 120 {
                constraintTextViewHeight.constant = 120
            }
                
            else{
                
                constraintTextViewHeight.constant = txtMsgHeight
                if textView.text == "" {
                }
            }
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "" {
            placeHolder.isHidden = true
            textView.textColor = COLOR_PRIMARY_TEXT_COLOR
        }
        else{
            textView.textColor = COLOR_PRIMARY_TEXT_COLOR
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            placeHolder.isHidden = false
        }
    }
    
    func colorCodeSetup () {
        
        lblRatePickerHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        lblPickerNameHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        lblEmailIDHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        lblSingalLineHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        txtViewMessage.textColor = COLOR_PRIMARY_TEXT_COLOR
        lblpickerName.textColor = COLOR_PRIMARY_TEXT_COLOR
        lblEmailID.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        btnSubmitHard.setTitleColor(COLOR_WHITE, for: .normal)
        btnSubmitHard.backgroundColor = COLOR_PRIMARY
        
    }
    
    func apiSubmitRate() {
        
        let driverId = ((RecivedData)["trip"] as! NSDictionary)["driver_id"]
        //        guard let pikerrate = pikerRate as? Float else { return }
        let trip_id = ((RecivedData)["trip"] as! NSDictionary)["trip_id"]
        GlobalClass.showHUD()
        
//        driver_id:6
//        rate:2
//        comment:hello
//        status:post
        
        let parm = ["driver_id": driverId as Any,
                    "rate":pikerRate,
                    "comment":txtViewMessage.text,
                    "status":"post",
                    "trip_id" : trip_id as Any] as [String : Any]
        
        GlobalClass.multipartData("rate_picker", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
            
            
            let msg = (response as! NSDictionary).object(forKey: "responsemsg") as! String
            GlobalClass.showAlertOKAction(APP_NAME, msg, "OK", self, successClosure: { (str) in
                
                self.navigationController?.popViewController(animated: true)
            })
            
            GlobalClass.hideHud()
            
        }, successWithStatus0Closure: { (responce) in
            
            let msg = (responce as! NSDictionary).object(forKey: "responsemsg") as! String
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: msg, view: self)
            GlobalClass.hideHud()
            
        }) { (error) in
            
            GlobalClass.hideHud()

            if error == "Not Show" {
                return
            }
            
            GlobalClass.showAlertOKAction(APP_NAME, error!, "OK", self, successClosure: { (str) in
            })
        }
    }
    
    // MARK:- Rate View Delegate
    
    func rateView(_ rateView: RateView!, didUpdateRating rating: Float) {
        
        pikerRate = rating
        
    }
    
    func apiGettRate() {
       let driverId = ((RecivedData)["trip"] as! NSDictionary)["driver_id"]
//        guard let pikerrate = pikerRate as? Float else { return }
        let trip_id = ((RecivedData)["trip"] as! NSDictionary)["trip_id"]
        
        GlobalClass.showHUD()
        
        //        driver_id:6
        //        rate:2
        //        comment:hello
        //        status:post
        
        let parm = ["driver_id": driverId,
                    "status":"get",
                    "trip_id":trip_id] as [String : Any]
        
        GlobalClass.multipartData("rate_picker", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
            
            let Data = (response as! NSDictionary)["data"]
            
            if Data == nil {
                
                self.placeHolder.isHidden = false
                self.txtViewMessage.text = ""
                self.rateView.rating = 0.0
                self.btnSubmitHard.isHidden = false
                
            } else {
                
                self.placeHolder.isHidden = true
                self.txtViewMessage.textColor = COLOR_PRIMARY_TEXT_COLOR
                
                let comment = ((Data as! NSDictionary)["comment"]as? String ?? "")?.removingWhitespaces()
                
                self.txtViewMessage.text = comment

                let addressHeight = comment!.height(withConstrainedWidth: SCREEN_WIDTH - 40, font: self.txtViewMessage.font!)
                self.txtMassageHight.constant = CGFloat(addressHeight) + CGFloat(20)

                self.rateView.rating = Float((Data as! NSDictionary)["rating"] as? String ?? "0.0") ?? 0.0
                self.btnSubmitHard.isHidden = true
                
            }
            
            GlobalClass.hideHud()
            
        }, successWithStatus0Closure: { (responce) in
            
            let msg = (responce as! NSDictionary).object(forKey: "responsemsg") as! String
//            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: msg, view: self)
            GlobalClass.hideHud()
            
        }) { (error) in
            
            GlobalClass.hideHud()

            if error == "Not Show" {
                return
            }
            
            GlobalClass.showAlertOKAction(APP_NAME, error!, "OK", self, successClosure: { (str) in
            })
        }
    }
    
}
