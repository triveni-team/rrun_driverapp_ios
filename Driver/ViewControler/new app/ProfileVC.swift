//
//  ProfileVC.swift
//  Driver
//
//  Created by Apple on 11/02/19.
//  Copyright © 2019 Arthonsys. All rights reserved.
//

import UIKit
import GooglePlaces
import GooglePlacePicker
import NVActivityIndicatorView
import Alamofire


class ProfileVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate , UITextFieldDelegate, GMSPlacePickerViewControllerDelegate {
    
    @IBOutlet weak var btnBack: ZFRippleButton!
    
    @IBOutlet weak var viewLoderHightConstant: NSLayoutConstraint!
    @IBOutlet weak var viewLoder: UIView!
    @IBOutlet weak var viewLoderAnim: UIView!
    
    @IBOutlet weak var btnSetImage: UIButton!
    @IBOutlet weak var btnSaveEditOutlet: UIButton!
    
    @IBOutlet weak var lblProfileHard: UILabel!
    
    @IBOutlet weak var lblNameHard: UILabel!
    @IBOutlet weak var lblEmailHard: UILabel!
    //   @IBOutlet weak var lblPhoneNumberHard: UILabel!
    @IBOutlet weak var lblAddressHard: UILabel!
    @IBOutlet weak var lblAlternatePhoneNumberHard: UILabel!
    
    // button hard All
    
    @IBOutlet weak var btnEdit1Hard: ZFRippleButton!
    @IBOutlet weak var btnEdit2Hard: ZFRippleButton!
    //    @IBOutlet weak var btnSaveHard: ZFRippleButton!
    
    
    // SingalLine Hard VIew
    @IBOutlet weak var viewSingalLineName: UIView!
    @IBOutlet weak var viewSingalLineEmail: UIView!
    // @IBOutlet weak var viewSingalLinePhNO: UIView!
    @IBOutlet weak var viewSingalLineAddress: UIView!
    @IBOutlet weak var viewSingalLineALPhNO: UIView!
    
    // Mobile View
    
    
    @IBOutlet weak var moNumberView: ViewTextPhone!
    @IBOutlet weak var altMoNumberView: ViewTextPhone!
    
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    
    //   @IBOutlet weak var txtPhoneNumber: UITextField!
    //     @IBOutlet weak var txtAddress: UITextField!
    //    @IBOutlet weak var txtAlterphno: UITextField!
    
    @IBOutlet weak var lblAddress: UILabel!
    var picker:UIImagePickerController? = UIImagePickerController()
    
    @IBOutlet weak var imageProfile: UIImageView!
    
    var imagePic : UIImage!
    
    var userData = NSDictionary()
    
    var isApiCall = true
    
    var selectImagPath = String()
    
    
    @IBOutlet weak var viewchangepas: UIView!
    @IBOutlet weak var btnchangepass: ZFRippleButton!
    
    @IBOutlet weak var viewlogout: UIView!
    @IBOutlet weak var btnlogout: ZFRippleButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
        
        userData = GlobalClass.getUserDefaultDICT()
        
        userDataSet()
        
        colorCodeSetup()
        
        btnSaveEditOutlet.setTitle("Edit", for: .normal)
        btnSaveEditOutlet.setTitleColor(.white, for: .normal)
        txtName.isEnabled = false
        moNumberView.text_Field.isEnabled = false
        altMoNumberView.text_Field.isEnabled = false
        btnEdit1Hard.isEnabled = false
        //        txtAlterphno.isEnabled = false
        btnSetImage.isEnabled = false
        
        txtEmail.isEnabled = false
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtName {
            
            txtName.returnKeyType = .next
            moNumberView.text_Field.becomeFirstResponder()
            
        }
            
            //        else if textField == moNumberView.text_Field {
            //
            //            moNumberView.text_Field.returnKeyType = .next
            //            txtAlterphno.becomeFirstResponder()
            //        }
            
            //        else if textField == txtAlterphno {
            //
            //            txtAlterphno.returnKeyType = .done
            //            textField.resignFirstResponder()
            //
            //        }
        else {
            textField.resignFirstResponder()
        }
        return true
        
    }
    
    @IBAction func btnOpenGallary(_ sender: UIButton) {
        
        OpenActionseet()
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        
        if !isApiCall {
            return
        }
        //        if isApiCall {
        //
        //        } else {
        //            return
        //        }
        
        //        apiEditProfile()
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnLogOut(_ sender: UIButton) {
        
        if !isApiCall {
            return
        }
        
        //        if isApiCall {
        //
        //        } else {
        //            return
        //        }
        
        GlobalClass.showAlertOKCancelAction(APP_NAME, "Are you sure you want to logout?", self, successClosure: { (yes) in
            
            GlobalClass.setUserDefault(value: "", for: "landingPage")
            GlobalClass.removeUserDefaults("getDistributorList")
            
            let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
            UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
            
        }) { (no) in
            // Ben10 print(""no")
        }
        
        
    }
    
    @IBAction func btnEditAddress(_ sender: UIButton) {
//
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self as GMSPlacePickerViewControllerDelegate
        present(placePicker, animated: true, completion: nil)
//        //
        
        
//        let autocompleteController = GMSAutocompleteViewController()
//        autocompleteController.delegate = self
//        let filter = GMSAutocompleteFilter()
//        filter.country = "US"  //appropriate country code
//        autocompleteController.autocompleteFilter = filter
//        present(autocompleteController, animated: true, completion: nil)
//        
        
    }
    
    @IBAction func btnEditElterNetPhNO(_ sender: UIButton) {
        
    }
    
    @IBAction func btnEditASaveToogle(_ sender: UIButton) {
        
        if sender.isSelected == true {
            
            self.view.endEditing(true)
           // altMoNumberView.text_Field.isEnabled = false
            textValidtion()
            
        } else {
            
            sender.setTitle("Save", for: .normal)
            sender.setTitleColor(.white, for: .selected)
            txtName.isEnabled = true
            moNumberView.text_Field.isEnabled = true
            altMoNumberView.text_Field.isEnabled = true
            btnEdit1Hard.isEnabled = true
            //            txtAlterphno.isEnabled = true
            btnSetImage.isEnabled = true
            
            txtName.becomeFirstResponder()
            
            sender.isSelected = true
            
        }
    }
    
    @IBAction func btnChangePassword(_ sender: UIButton) {
        
        if !isApiCall {
            return
        }
        
        //        if isApiCall {
        //
        //        } else {
        //            return
        //        }
        //
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ChangePassVC") as? ChangePassVC
        self.navigationController!.pushViewController(vc!, animated: true)
        
    }
    
}

extension ProfileVC : UIPickerViewDelegate {
    
    func OpenActionseet() {
        
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        
        if let popoverController = optionMenu.popoverPresentationController {
            popoverController.sourceView = btnSetImage
            popoverController.sourceRect = CGRect(x: 0, y: 0, width: btnSetImage.frame.width, height: btnSetImage.frame.height)
        }
        
        let saveAction = UIAlertAction(title: "Camera", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            self.openCamera()
            // Ben10 print(""Camera")
        })
        
        let deleteAction = UIAlertAction(title: "Gallary", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            self.openGallary()
            // Ben10 print(""openGallary")
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
            // Ben10 print(""Cancelled")
        })
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    func openGallary() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
            picker!.allowsEditing = false
            picker!.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
            picker!.sourceType = UIImagePickerController.SourceType.photoLibrary
            present(picker!, animated: true, completion: nil)
            
        }
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker!.allowsEditing = false
            picker!.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
            picker!.sourceType = UIImagePickerController.SourceType.camera
            picker!.cameraCaptureMode = .photo
            present(picker!, animated: true, completion: nil)
            
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Ben10 print("info)
        
        if ((info[UIImagePickerController.InfoKey.originalImage] as? UIImage) != nil) {
            
            imageProfile.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            
            imagePic = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            imagePic = GlobalClass.resizeImage(image: imagePic, newWidth: 300)
            
            //            imageString = GlobalClass.imageToStrinig(info[UIImagePickerController.InfoKey.originalImage] as! UIImage)
            //            let imgpath = info[UIImagePickerController.InfoKey.imageURL]
            //            selectImagPath = imgpath as! String
            
        } else{
            // Ben10 print(""No Image Found")
        }
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        
    }
}

extension ProfileVC {
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        // Ben10 print(""Place name \(place.name)")
        // Ben10 print(""Place address \(place.formattedAddress ?? "no found")")
        // Ben10 print(""Place attributions \(String(describing: place.attributions))")
        
        if let address = place.formattedAddress {
            lblAddress.text = address

        } else {
            lblAddress.text = ""
        }
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        // Ben10 print(""No place selected")
    }
    
}

extension ProfileVC {
    
    func userDataSet() {
        
        // Ben10 print(""userData : \(userData)")
        
        txtName.text = userData["name"] as? String
        txtEmail.text = userData["email"] as? String
        //   txtPhoneNumber.text = userData["mobile"] as? String
        lblAddress.text = userData["address"] as? String
        //        txtAlterphno.text = userData["alternate_mobile"] as? String
        let imageUrl = userData["profile"] as? String
        
        if let validUrl = imageUrl {
            //            FullUrl = ImageBaseUrl + validUrl
            imageProfile.sd_setImage(with: URL(string: validUrl), placeholderImage: UIImage.init(named: "userProfile"), options: .transformAnimatedImage) { (img, error, type , url) in
                if error == nil {
                }
            }
        } else {
        }
        
        let remove = (userData["mobile"] as? String)?.replacingOccurrences(of: "(", with: "").removingWhitespaces()
        let number = remove?.replacingOccurrences(of: ")", with: "") ?? ""
        
        
        moNumberView.text_Field.keyboardType = .numberPad
        moNumberView.lblTitle.text = "PHONE NUMBER"
        moNumberView.text_Field.placeholder = "Enter phone number"
        moNumberView.text_Field.text = number
        moNumberView.setValue(string : number)
        
        altMoNumberView.text_Field.keyboardType = .numberPad
        altMoNumberView.lblTitle.text = "ALTERNET PHONE NUMBER"
        altMoNumberView.text_Field.placeholder = "Enter alternat phone number"
        altMoNumberView.text_Field.text = userData["alternate_mobile"] as? String
        altMoNumberView.setValue(string : (userData["alternate_mobile"] as? String ?? "").removingWhitespaces())
        
        
    }
    
    //    func apiEditProfile() {
    //
    //        isApiCall = false
    //
    //        self.viewLoderHightConstant.constant = 35
    //        vcUnTuchALlButton()
    //
    //        let imageBase64 = GlobalClass.imageToStrinig(imageProfile.image!)
    //
    //        let parm = ["driver_id": User("user_id"),
    //                    "username" : txtName.text as Any,
    //                    "mobile_no" : moNumberView.text_Field.text as Any,
    //                    "address" : lblAddress.text as Any,
    //                    "alternate_mobile" : altMoNumberView.text_Field.text as Any,
    //                    "image" : imageBase64] as [String : Any]
    //
    //        //"image" : imageBase64
    //
    //        //        "image" : "\(imagePic)",
    //        //        "mobile_no" : txtPhoneNumber.text as Any,
    //
    ////        let MediaParams: NSDictionary = ["image" : imageBase64]
    //
    //        GlobalClass.multipartData("edit_profile", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
    //
    //            self.isApiCall = true
    //
    //            self.btnSaveEditOutlet.setTitle("Edit", for: .normal)
    //
    //            self.vcUnTuchALlButton()
    //
    //            let data = response?.object(forKey: "data") as! NSDictionary
    //
    //            GlobalClass.setUserDefaultDICT(value: data)
    //
    //            let massage = (response as! NSDictionary).object(forKey: "responsemsg") as! String
    //
    //            GlobalClass.showAlertOKAction(APP_NAME, massage, "OK", self, successClosure: { (str) in
    //                self.navigationController?.popViewController(animated: true)
    //            })
    //
    //            let userDefoult = UserDefaults.standard
    //
    //            userDefoult.set("ProfileUpdate", forKey: "isVcFrome")
    //
    //
    //            self.viewLoderHightConstant.constant = 0
    //
    //            self.vcTuchAllButton()
    //
    //
    //        }, successWithStatus0Closure: { (responce) in
    //
    //            // Ben10 print(""responce = \(responce as! NSDictionary)")
    //            let msg = (responce as! NSDictionary).object(forKey: "responsemsg") as! String
    //            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: msg, view: self)
    ////            GlobalClass.hideHud()
    //            self.viewLoderHightConstant.constant = 0
    //            self.vcTuchAllButton()
    //            self.isApiCall = true
    //
    //
    //        }) { (error) in
    //
    //            self.isApiCall = true
    //
    //            self.viewLoderHightConstant.constant = 0
    //
    //            self.vcTuchAllButton()
    //
    //            if error == "Not Show" {
    //                return
    //            }
    //
    //            GlobalClass.showAlertOKAction(APP_NAME, error!, "OK", self, successClosure: { (str) in
    //            })
    //        }
    //    }
    
    func colorCodeSetup () {
        
        //        lblProfileHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        //        lblNameHard.textColor = COLOR_ACCENT_TEXT_COLOR
        //        lblEmailHard.textColor = COLOR_ACCENT_TEXT_COLOR
        //      lblPhoneNumberHard.textColor = COLOR_ACCENT_TEXT_COLOR
        //        lblAddressHard.textColor = COLOR_ACCENT_TEXT_COLOR
        //    lblAlternatePhoneNumberHard.textColor = COLOR_ACCENT_TEXT_COLOR
        
        //        btnEdit1Hard.setTitleColor(COLOR_PRIMARY_TEXT_COLOR, for: .normal)
        //    btnEdit2Hard.setTitleColor(COLOR_PRIMARY_TEXT_COLOR, for: .normal)
        //        btnSaveHard.setTitleColor(COLOR_PRIMARY_TEXT_COLOR, for: .normal)
        
        //        txtName.textColor = COLOR_PRIMARY_TEXT_COLOR
        //        txtEmail.textColor = COLOR_PRIMARY_TEXT_COLOR
        //     txtPhoneNumber.textColor = COLOR_PRIMARY_TEXT_COLOR
        //        txtAlterphno.textColor = COLOR_PRIMARY_TEXT_COLOR
        //        lblAddress.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        txtName.returnKeyType = .next
        txtEmail.returnKeyType = .next
        moNumberView.text_Field.returnKeyType = .next
        moNumberView.viewLineHight.constant = 2
        moNumberView.viewLine.backgroundColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        moNumberView.lbl1.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        moNumberView.lbl2.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        moNumberView.lbl3.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        moNumberView.lbl4.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        moNumberView.lbl5.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        moNumberView.lbl6.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        moNumberView.lbl7.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        moNumberView.lbl8.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        moNumberView.lbl9.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        moNumberView.lbl10.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        moNumberView.lblFirstBreakt.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        moNumberView.lblLastBreakt.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        moNumberView.lblFirstDesh.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        moNumberView.lblLastDesh.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        moNumberView.lblTitle.textColor = #colorLiteral(red: 0.4, green: 0.4745098039, blue: 0.631372549, alpha: 1)
        moNumberView.lblTitle.font = moNumberView.lblTitle.font.withSize(20)
        
        altMoNumberView.text_Field.returnKeyType = .next
        altMoNumberView.viewLineHight.constant = 2
        altMoNumberView.viewLine.backgroundColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        altMoNumberView.lbl1.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        altMoNumberView.lbl2.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        altMoNumberView.lbl3.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        altMoNumberView.lbl4.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        altMoNumberView.lbl5.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        altMoNumberView.lbl6.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        altMoNumberView.lbl7.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        altMoNumberView.lbl8.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        altMoNumberView.lbl9.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        altMoNumberView.lbl10.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        altMoNumberView.lblFirstBreakt.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        altMoNumberView.lblLastBreakt.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        altMoNumberView.lblFirstDesh.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        altMoNumberView.lblLastDesh.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)
        altMoNumberView.lblTitle.textColor = #colorLiteral(red: 0.4, green: 0.4745098039, blue: 0.631372549, alpha: 1)
        altMoNumberView.lblTitle.font = altMoNumberView.lblTitle.font.withSize(20)
        
        txtName.attributedPlaceholder = NSAttributedString(string: "Enter email",attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "6679A1")])
        
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Enter email",attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "6679A1")])
        
        moNumberView.text_Field.attributedPlaceholder = NSAttributedString(string: "Enter email",attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "6679A1")])
        
        
        //        moNumberView.
        
        //        txtAddress.returnKeyType = .next
        //        txtAlterphno.returnKeyType = .done
        
        //        viewSingalLineName.backgroundColor = COLOR_ACCENT_LINE
        //        viewSingalLineEmail.backgroundColor = COLOR_ACCENT_LINE
        //    viewSingalLinePhNO.backgroundColor = COLOR_ACCENT_LINE
        //        viewSingalLineAddress.backgroundColor = COLOR_ACCENT_LINE
        //   viewSingalLineALPhNO.backgroundColor = COLOR_ACCENT_LINE
        
        //        viewchangepas.backgroundColor = COLOR_WHITE
        //        btnchangepass.backgroundColor = COLOR_PRIMARY
        //        viewlogout.backgroundColor = COLOR_WHITE
        //        btnlogout.backgroundColor = COLOR_PRIMARY
        
    }
    
    func textValidtion () {
        
        if (userData["driver_image"] as? String == nil) {
            
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Please select image", view: self)
            
            return
            
        }else if txtName.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Please enter Name", view: self)
            return
        } else if txtEmail.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Please enter email address", view: self)
            return
        }
        else if (moNumberView.text_Field.text?.trimmingCharacters(in: .whitespaces).count)! < 6 {
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Please enter phone number", view: self)
            return
        }
        else if lblAddress.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Please select address", view: self)
            return
        }
            //        else if altMoNumberView.text_Field.text?.trimmingCharacters(in: .whitespaces).count != 10 {
            //            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Please enter alternate phone number", view: self)
            //            return
            //        }
        else{
            
            //            apiEditProfile()
            
            apiUplodeImage()
        }
    }
    
    func setUpView() {
        
        let activityIndicator = NVActivityIndicatorView(frame: self.viewLoderAnim.bounds)
        activityIndicator.type = .ballPulseSync
        activityIndicator.color = UIColor.black
        
        self.viewLoderAnim.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
    
    func vcUnTuchALlButton(){
        
        txtName.isUserInteractionEnabled = false
        moNumberView.isUserInteractionEnabled = false
        altMoNumberView.isUserInteractionEnabled = false
        btnEdit1Hard.isUserInteractionEnabled = false
        btnSetImage.isUserInteractionEnabled = false
        btnBack.isUserInteractionEnabled = false
        
        self.moNumberView.text_Field.isEnabled = false
        self.altMoNumberView.text_Field.isEnabled = false
        self.btnEdit1Hard.isEnabled = false
        self.btnSetImage.isEnabled = false
        self.btnSaveEditOutlet.isSelected = false
        
        
        
    }
    
    func vcTuchAllButton() {
        
        txtName.isUserInteractionEnabled = true
        moNumberView.isUserInteractionEnabled = true
        altMoNumberView.isUserInteractionEnabled = true
        btnEdit1Hard.isUserInteractionEnabled = true
        btnSetImage.isUserInteractionEnabled = true
        btnBack.isUserInteractionEnabled = true
        
        self.moNumberView.text_Field.isEnabled = true
        self.altMoNumberView.text_Field.isEnabled = true
        self.btnEdit1Hard.isEnabled = true
        self.btnSetImage.isEnabled = true
        self.btnSaveEditOutlet.isSelected = true
        
    }
    
    func apiUplodeImage () {
        
        isApiCall = false
        
        self.viewLoderHightConstant.constant = 35
        
        vcUnTuchALlButton()
        
        //        let image = imageProfile.image
        var imgData = Data()
        if imagePic != nil {
            imgData = imagePic.jpegData(compressionQuality: 0.05)!
        } else {
            imgData = imageProfile.image!.jpegData(compressionQuality: 0.05)!
        }
        
        
        
        
        
        // Ben10 print("imageProfile.image as Any)
        //
        let parm = ["driver_id": User("user_id"),
                    "username" : txtName.text as Any,
                    "mobile_no" : moNumberView.text_Field.text as Any,
                    "address" : lblAddress.text as Any,
                    "alternate_mobile" : altMoNumberView.text_Field.text as Any] as [String : Any]
        //
        let MediaData = NSMutableArray()
        
        let ImageParm = ["Data": imgData,
                         "withName" : "image",
                         "fileName" : "file.jpg",
                         "mimeType" : "image/jpg"] as [String : Any]
        
        MediaData.add(ImageParm)
        
        // Ben10 print("MediaData)
        
        GlobalClass.API_PostData_multipartFormData("edit_profile", parameter: parm as NSDictionary, MediaParams: NSDictionary(), MediaData: MediaData, successWithStatus1Closure: { (response) in
            
            self.isApiCall = true
            
            self.btnSaveEditOutlet.setTitle("Edit", for: .normal)
            self.btnSaveEditOutlet.setTitleColor(.white, for: .normal)
            
            self.vcUnTuchALlButton()
            
            let data = response?.object(forKey: "data") as! NSDictionary
            
            GlobalClass.setUserDefaultDICT(value: data)
            
            let massage = (response as! NSDictionary).object(forKey: "responsemsg") as! String
            
            GlobalClass.showAlertOKAction(APP_NAME, massage, "OK", self, successClosure: { (str) in
                self.navigationController?.popViewController(animated: true)
            })
            
            let userDefoult = UserDefaults.standard
            
            userDefoult.set("ProfileUpdate", forKey: "isVcFrome")
            
            
            self.viewLoderHightConstant.constant = 0
            
            self.vcTuchAllButton()
            
            
        }, successWithStatus0Closure: { (responce) in
            
            
            
            // Ben10 print(""responce = \(responce as! NSDictionary)")
            
            let msg = (responce as! NSDictionary).object(forKey: "responsemsg") as! String
            
            if msg == "Not Show" {
                return
            }
            
            let responsemsg = (responce as! NSDictionary)["responsemsg"] as? String ?? ""
            let responsecode = (responce as! NSDictionary)["responsecode"] as? String ?? ""
            
            if responsecode as? String ?? "" == "-2" {
                
                GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
                    
                    GlobalClass.setUserDefault(value: "", for: "landingPage")
                    GlobalClass.removeUserDefaults("getDistributorList")
                    
                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                    return
                    
                })
            }
            
            
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: msg, view: self)
            //            GlobalClass.hideHud()
            self.viewLoderHightConstant.constant = 0
            self.vcTuchAllButton()
            self.isApiCall = true
            
            
        }) { (error) in
            
            self.isApiCall = true
            
            self.viewLoderHightConstant.constant = 0
            
            self.vcTuchAllButton()
            
            if error == "Not Show" {
                return
            }
            
            GlobalClass.showAlertOKAction(APP_NAME, error!, "OK", self, successClosure: { (str) in
                
            })
        }
        
        return
        
        //        Alamofire.upload(multipartFormData: { multipartFormData in
        //            multipartFormData.append(imgData!, withName: "image",fileName: "file.jpg", mimeType: "image/jpg")
        //
        //            for (key, value) in parm {
        //                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
        //            } //Optional for extra parameters
        //        },
        //                         to:BASE_URL + "edit_profile")
        //        { (result) in
        //            switch result {
        //            case .success(let upload, _, _):
        //
        //                upload.uploadProgress(closure: { (progress) in
        //                    // Ben10 print(""Upload Progress: \(progress.fractionCompleted)")
        //                })
        //
        //                upload.responseJSON { response in
        //
        //                    if response.result.value is NSDictionary {
        //
        //                        // Ben10 print("response)
        //
        //                        let responce = response.result.value as? NSDictionary
        //
        //                        self.isApiCall = true
        //
        //                        self.btnSaveEditOutlet.setTitle("Edit", for: .normal)
        //
        //                        self.vcUnTuchALlButton()
        //
        //                        let data = responce!.object(forKey: "data") as! NSDictionary
        //                        // Ben10 print("data)
        //
        //                        GlobalClass.setUserDefaultDICT(value: data)
        //
        //                        let massage = responce!.object(forKey: "responsemsg") as! String
        //
        //                        GlobalClass.showAlertOKAction(APP_NAME, massage, "OK", self, successClosure: { (str) in
        //                            self.navigationController?.popViewController(animated: true)
        //                        })
        //
        //                        let userDefoult = UserDefaults.standard
        //
        //                        userDefoult.set("ProfileUpdate", forKey: "isVcFrome")
        //
        //
        //                        self.viewLoderHightConstant.constant = 0
        //
        //                        self.vcTuchAllButton()
        //
        //
        //                    } else{
        //
        //                        // Ben10 print(""No Data")
        //
        //                        GlobalClass.showAlertOKAction(APP_NAME, "server error", "OK", self, successClosure: { (str) in
        //                        })
        //                    }
        //                }
        //
        //            case .failure(let encodingError):
        //
        //                // Ben10 print("encodingError)
        //
        //                self.isApiCall = true
        //
        //            }
        //        }
        
        
    }
}

extension ProfileVC: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        // Ben10 print(""Place name: \(place.name)")
        // Ben10 print(""Place address: \(place.formattedAddress)")
        // Ben10 print(""Place attributions: \(place.attributions)")
        // Ben10 print(""Place latitude: \(place.coordinate.latitude)")
        // Ben10 print(""Place longitude: \(place.coordinate.longitude)")
        //        self.lat = "\(place.coordinate.latitude)"
        //        self.long = "\(place.coordinate.longitude)"
        
         
        if let address = place.formattedAddress {
            lblAddress.text = address
            lblAddress.textColor = COLOR_PRIMARY_TEXT_COLOR
            
            
            
        } else{
            
            lblAddress.text = "Enter address"
            lblAddress.textColor = UIColor.lightGray
            GlobalClass.showAlertOKAction(APP_NAME, "We could not get the address. Please choose again", "Ok", self) { (ok) in
                self.btnEditAddress(UIButton())
            }
            
        }
        
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        // Ben10 print(""Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
