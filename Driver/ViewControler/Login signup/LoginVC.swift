//
//  LoginVC.swift
//  Restaurant Picker
//
//  Created by Apple on 01/01/01.
//  Copyright © 2001 Arthonsys. All rights reserved.
//

import CoreLocation

class LoginVC: UIViewController , UITextFieldDelegate {
    
    @IBOutlet weak var conHeightOfViewFingerLogin: NSLayoutConstraint!
    
    @IBOutlet weak var lblHIDriverHard: UILabel!
    @IBOutlet weak var lblStartTOHard: UILabel!
    
    @IBOutlet weak var lblDriverIDHard: UILabel!
    @IBOutlet weak var lblPasswordHard: UILabel!
    
    @IBOutlet weak var lblSingalLineIDHard: UILabel!
    @IBOutlet weak var lblSingalLinePassHard: UILabel!
    
    @IBOutlet weak var btnForggotPassHard: UIButton!
    
    @IBOutlet weak var btnLoginhard: ZFRippleButton!
    
    @IBOutlet weak var btnHaveAPingerHard: UIButton!
    
    @IBOutlet weak var btnClickHereHard: UIButton!
    
    @IBOutlet weak var txtDriverID: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
     var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtDriverID.becomeFirstResponder()
        colorCodeSetup()
        stopTimerApi()
        
        txtDriverID.attributedPlaceholder = NSAttributedString(string: "Enter email",attributes: [NSAttributedString.Key.foregroundColor: lblDriverIDHard.textColor ?? UIColor.white])
        
        txtPassword.attributedPlaceholder = NSAttributedString(string: "Enter password",attributes: [NSAttributedString.Key.foregroundColor: lblDriverIDHard.textColor ?? UIColor.white])
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if (GlobalClass.isKeyPresentInUserDefaults(key: "dictUserDefult")) {
            // Ben10 print("UserDefaults.standard.object(forKey: "landingPage") as Any)
            if (UserDefaults.standard.object(forKey: "landingPage")) != nil {
                conHeightOfViewFingerLogin.constant = 120
            }
        }
        stopTimerApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        stopTimerApi()
        CURRENT_TRIP.removeAllObjects()
    }
    
    func stopTimerApi() {
        if apiTimer != nil {
            apiTimer?.invalidate()
            apiTimer = nil
        }
    }

    
    @IBAction func btnLogin(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if txtDriverID.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Please enter driver id.", view: self)
        }
            
        else if !GlobalClass.isValidEmail(testStr: txtDriverID.text!) {
            
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Please enter valid driver id.", view: self)
            
        }
        else if txtPassword.text?.trimmingCharacters(in: .whitespaces).count == 0 {
            
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Please enter password.", view: self)
        }
            
        else {
            
            if Reachability.isConnectedToNetwork(){
                apiLogin()
            } else {
                GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "No Network Avalable", view: self)
            }
        }
        
    }
    
    @IBAction func btnSignIn(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FingerSignInVC") as? FingerSignInVC
        self.navigationController!.pushViewController(vc!, animated: true)
    }
    
    @IBAction func btnForgetPassword(_ sender: UIButton) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ForgotPasswordVC") as? ForgotPasswordVC
        self.navigationController!.pushViewController(vc!, animated: true)
    }
    
    
    func apiLogin() {
        
        GlobalClass.showHUD()
        let parm = ["email": "\(txtDriverID.text!.removingWhitespaces())",
            "password": "\(txtPassword.text!.removingWhitespaces())"] as [String : Any]        
        
        GlobalClass.multipartData("login", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
            
            GlobalClass.hideHud()
            let data = response?.object(forKey: "data") as! NSDictionary
            GlobalClass.setUserDefaultDICT(value: data)
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TripsVC") as? TripsVC
            self.navigationController!.pushViewController(vc!, animated: true)
            let userDefoult = UserDefaults.standard
            userDefoult.set("islogin", forKey: "isVcFrome")
            
        }, successWithStatus0Closure: { (response) in
            
            GlobalClass.hideHud()
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: response?.object(forKey: "responsemsg") as! String, view: self)
        }) { (error) in
            
            GlobalClass.hideHud()
            
            if error == "Not Show" {
                return
            }
        }
    }
    
    
    func colorCodeSetup () {
        
//        lblHIDriverHard.textColor = COLOR_PRIMARY_TEXT_COLOR
//        lblStartTOHard.textColor = COLOR_PRIMARY_TEXT_COLOR
//        
//        lblDriverIDHard.textColor = COLOR_ACCENT_TEXT_COLOR
//        lblPasswordHard.textColor = COLOR_ACCENT_TEXT_COLOR
//        
//        lblSingalLineIDHard.backgroundColor = COLOR_ACCENT_LINE
//        lblSingalLinePassHard.backgroundColor = COLOR_ACCENT_LINE
//        
//        btnLoginhard.setTitleColor(COLOR_WHITE, for: .normal)
//        btnLoginhard.backgroundColor = COLOR_PRIMARY
//        
//        btnClickHereHard.setTitleColor(COLOR_PRIMARY_TEXT_COLOR, for: .normal)
//        
//        btnClickHereHard.borderColor = COLOR_PRIMARY_TEXT_COLOR
//        btnClickHereHard.borderWidth = 1
//        
//        txtDriverID.textColor = COLOR_PRIMARY_TEXT_COLOR
//        txtPassword.textColor = COLOR_PRIMARY_TEXT_COLOR
//        
//        btnForggotPassHard.setTitleColor(COLOR_PRIMARY_TEXT_COLOR, for: .normal)
//        btnHaveAPingerHard.setTitleColor(COLOR_PRIMARY_TEXT_COLOR, for: .normal)
        
        txtDriverID.returnKeyType = .next
        txtPassword.returnKeyType = .done
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == txtDriverID {
            
            txtDriverID.returnKeyType = .next
            txtPassword.becomeFirstResponder()
            
        } else if textField == txtPassword {
            
            txtPassword.returnKeyType = .done
            textField.resignFirstResponder()
            
        } else {
            
            textField.resignFirstResponder()
            
        }
        return true
        
    }
}

