//
//  AppDelegate.swift
//  Driver
//
//  Created by Apple on 08/02/19.
//  Copyright © 2019 Arthonsys. All rights reserved.
//

let screenSize = UIScreen.main.bounds
let screenWidth = screenSize.width
let screenHeight = screenSize.height

import UIKit
import UserNotifications
import GooglePlacePicker
import GoogleMaps

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate  {
    
    var window: UIWindow?
    var databaseName : String!
    var databasePath : String!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        GMSServices.provideAPIKey("AIzaSyBnDPE7kZXeqxz5E2n9jZcgZrqnf9QSpKM")
        GMSPlacesClient.provideAPIKey("AIzaSyBnDPE7kZXeqxz5E2n9jZcgZrqnf9QSpKM")
        
        databaseSetup()
        GlobalClass.getUserDefaultCurrentTrip()
        
        // Ben10 print("launchOptions as Any)
        if launchOptions != nil {
            let userInfo = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification]
            if userInfo != nil {
                let notificationAPS = (userInfo as! NSDictionary)[AnyHashable("aps")] as! NSDictionary
                
                //  let notificationData = (notificationAPS["customData"] as! NSDictionary)
                
                application.applicationIconBadgeNumber = 0
                
                self.goToDocDetailScreen(notificationAPS: notificationAPS)
                
                //  // Ben10 print("customData)
            }
        }
            
        else {
            
        }
        
        self.configureNotification()
        
        setRootVC()
        
      
        
        
        UIApplication.shared.setMinimumBackgroundFetchInterval(10)
        
        if !(GlobalClass.isKeyPresentInUserDefaults(key: "DEVICE_TOKEN")) {
           GlobalClass.setUserDefault(value: "", for: "DEVICE_TOKEN")
        }
        
        application.applicationIconBadgeNumber = 0
        
        return true
        
    }
    
    
    func databaseSetup() {
        databaseName = "RestaurantDB.db"
        var documentPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDir = documentPaths[0]
        databasePath = URL(fileURLWithPath: documentsDir).appendingPathComponent(databaseName).absoluteString
        // Ben10 print(""\(databasePath ?? "")")
        copyDatabaseIfNeeded()
    }
    
    func copyDatabaseIfNeeded() {
        // Move database file from bundle to documents folder
        
        let fileManager = FileManager.default
        
        let documentsUrl = fileManager.urls(for: .documentDirectory,
                                            in: .userDomainMask)
        
        guard documentsUrl.count != 0 else {
            return // Could not find documents URL
        }
        
        let finalDatabaseURL = documentsUrl.first!.appendingPathComponent(databaseName)
        
        if !( (try? finalDatabaseURL.checkResourceIsReachable()) ?? false) {
            // Ben10 print(""DB does not exist in documents folder")
            
            let documentsURL = Bundle.main.resourceURL?.appendingPathComponent(databaseName)
            
            do {
                try fileManager.copyItem(atPath: (documentsURL?.path)!, toPath: finalDatabaseURL.path)
            } catch let error as NSError {
                // Ben10 print(""Couldn't copy file to final location! Error:\(error.description)")
            }
            
        } else {
            // Ben10 print(""Database file found at path: \(finalDatabaseURL.path)")
        }
        
    }
    
    
    func configureNotification() {
        
        UNUserNotificationCenter.current().delegate = self
        
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
        }
        UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        // Ben10 print(""APNs device token: \(deviceTokenString)")
        DEVICE_TOKEN = deviceTokenString
        GlobalClass.setUserDefault(value: DEVICE_TOKEN, for: "DEVICE_TOKEN")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Ben10 print(""APNs registration failed: \(error)")
        DEVICE_TOKEN = UIDevice.current.identifierForVendor!.uuidString
        GlobalClass.setUserDefault(value: DEVICE_TOKEN, for: "DEVICE_TOKEN")

    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        // Ben10 print("userInfo["aps"] as! NSDictionary)
        
        //        let notificationAPS = userInfo["aps"] as! NSDictionary
        //
            //    application.applicationIconBadgeNumber = 4
        //
        //        self.goToDocDetailScreen(notificationAPS: notificationAPS)
        
        
    }
    
    func goToDocDetailScreen( notificationAPS : NSDictionary) {
        //  let notificationData = (notificationAPS["customData"] as! NSDictionary)
        
        
        
        //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //        let controller1: DemoVC2! = storyboard.instantiateViewController(withIdentifier: "DemoVC2") as? DemoVC2
        //        // NotificationscreenName = notificationData["type"] as! String
        //        controller1.dd = "\(notificationAPS)"
        //        // controller1.isFrom = "notification"
        //        self.window?.rootViewController = controller1
        
    }
    
    func setRootVC() {
        
        if (GlobalClass.isKeyPresentInUserDefaults(key: "landingPage")) {
            if (UserDefaults.standard.object(forKey: "landingPage")) != nil {
                
                if (UserDefaults.standard.object(forKey: "landingPage") as! String) == "" {
                    
                }
                else if (UserDefaults.standard.object(forKey: "landingPage") as! String) == "TripsNavVC" {
                    
                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TripsNavVC")
                    self.window!.rootViewController = viewControllerToBeRoot
                    self.window!.makeKeyAndVisible()
                    
                    let userDefoult = UserDefaults.standard
                    
                    userDefoult.set("islogin", forKey: "isVcFrome")
                    
                }
      
            }
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Ben10 print(""applicationWillResignActive")
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        

    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Ben10 print(""applicationWillEnterForeground")

    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Ben10 print(""applicationDidBecomeActive")
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
        // Ben10 print(""applicationWillTerminate")
        GlobalClass.setUserDefaultCurrentTrip()

    }
 
    
}


