//
//  SignatureVC.swift
//  Driver New
//
//  Created by Arthonsys Ben on 08/07/19.
//  Copyright © 2019 Bijender Singh. All rights reserved.
//

import UIKit
import YPDrawSignatureView

class SignatureVC: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var drawView: YPDrawSignatureView!
    var TripInProgressComplateVC_obj : TripInProgressComplateVC!
    var titleStr = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = "Hi, \(titleStr)!"
        // Do any additional setup after loading the view.
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    //MARK:- All Button Action
    @IBAction func btnSyncAction(_ sender: UIButton) {
        drawView.clear() // this is for clear sign
    }
    
    @IBAction func btnCloseAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnConfirmSignature(_ sender: UIButton) {
        if drawView.getSignature() != nil {
            let signatureImage = drawView.getSignature()!
            self.TripInProgressComplateVC_obj.signAll(imgSign: signatureImage)
            self.btnCloseAction(UIButton())
            // Ben10 print(""")
        }
        else {
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Please signature", view: self)
        }
    }
    
}
