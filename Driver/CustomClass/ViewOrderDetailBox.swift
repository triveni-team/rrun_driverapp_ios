//
//  ViewOrderDetailBox.swift
//  Restaurant Depot
//
//  Created by Bijender Singh on 30/01/19.
//  Copyright © 2019 Bijender Singh. All rights reserved.
//

import UIKit

class ViewOrderDetailBox: UIView {

    @IBOutlet weak var viewBackGround: UIView!
    
    @IBOutlet weak var lblHardOrderId: UILabel!
    @IBOutlet weak var lblHardNumberOfItems: UILabel!
    @IBOutlet weak var lblHardPrice: UILabel!
    @IBOutlet weak var lblHardFees: UILabel!
    @IBOutlet weak var lblHardTotal: UILabel!
    
    @IBOutlet weak var lblNumberOfItems: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblFees: UILabel!
    @IBOutlet weak var lblTotal: UILabel!


    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        //fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView(){
        let view = Bundle.main.loadNibNamed("ViewOrderDetailBox", owner: self, options: nil)![0] as! UIView
        self.addSubview(view)
        view.frame  = self.bounds
        
        self.viewBackGround.backgroundColor = COLOR_PRIMARY_TEXT_COLOR
        self.lblHardOrderId.textColor = COLOR_PRIMARY_TEXT_COLOR
        self.lblHardNumberOfItems.textColor = COLOR_PRIMARY_TEXT_COLOR
        self.lblHardPrice.textColor = COLOR_PRIMARY_TEXT_COLOR
        self.lblHardFees.textColor = COLOR_PRIMARY_TEXT_COLOR
        self.lblHardTotal.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        self.lblNumberOfItems.textColor = COLOR_PRIMARY_TEXT_COLOR
        self.lblPrice.textColor = COLOR_PRIMARY_TEXT_COLOR
        self.lblFees.textColor = COLOR_PRIMARY_TEXT_COLOR
        self.lblTotal.textColor = COLOR_PRIMARY_TEXT_COLOR
        
    }
    

   
}
