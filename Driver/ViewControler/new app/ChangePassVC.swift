//
//  ChangePassVC.swift
//  Driver
//
//  Created by Technical Lead on 09/04/19.
//  Copyright © 2019 Arthonsys. All rights reserved.
//

import UIKit

class ChangePassVC: UIViewController , UITextFieldDelegate{
    
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtOldPassword.becomeFirstResponder()

        txtOldPassword.attributedPlaceholder = NSAttributedString(string: "Enter old password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "6679A1")])

        txtNewPassword.attributedPlaceholder = NSAttributedString(string: "Enter old password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "6679A1")])

        txtConfirmPassword.attributedPlaceholder = NSAttributedString(string: "Enter old password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(hexString: "6679A1")])

    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtOldPassword {
            
            txtOldPassword.returnKeyType = .next
            txtNewPassword.becomeFirstResponder()
            
        } else if textField == txtNewPassword {
            
            txtNewPassword.returnKeyType = .next
            txtConfirmPassword.becomeFirstResponder()
            
        }
        else if textField == txtConfirmPassword {
            
            txtConfirmPassword.returnKeyType = .done
            txtConfirmPassword.resignFirstResponder()
            
        }else {
            
            textField.resignFirstResponder()
            
        }
        return true
    }
    
    @IBAction func btnSubmtAction(_ sender: UIButton) {
        
        txtOldPassword.text = txtOldPassword.text?.trimmingCharacters(in: .whitespaces)
        txtNewPassword.text = txtNewPassword.text?.trimmingCharacters(in: .whitespaces)
        txtConfirmPassword.text = txtConfirmPassword.text?.trimmingCharacters(in: .whitespaces)
        
        if txtOldPassword.text?.count == 0 {
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Please enter old password", view: self)
            return
        }
        
        if (txtOldPassword.text?.count)! < 6 {
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Old password must be at least 6 characters long", view: self)
            return
        }
        if txtNewPassword.text?.count == 0 {
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Please enter new password", view: self)
            return
        }
        
        if (txtNewPassword.text?.count)! < 6 {
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "New password must be at least 6 characters long", view: self)
            return
        }
        
        if txtConfirmPassword.text?.count == 0 {
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Please enter confirm password", view: self)
            return
        }
        
        if txtConfirmPassword.text != txtNewPassword.text {
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Password mismatch", view: self)
            return
        }
        
        if txtConfirmPassword.text == txtOldPassword.text {
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "New password should not be same as old password.", view: self)
            return
        }
        
        self.changePasswordApi()
        
    }
    
    
    func changePasswordApi() {
        GlobalClass.showHUD()
        let parm = ["driver_id": User("user_id"),
                    "old_password" : txtOldPassword.text?.removingWhitespaces() as Any,
                    "new_password" : txtNewPassword.text?.removingWhitespaces() as Any] as [String : Any] //txtEmail.text?.removingWhitespaces() as Any
        
        GlobalClass.multipartData("change_driver_password", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (responce) in
                        
            let responce = responce as! NSDictionary
            
            let massage = (responce).object(forKey: "responsemsg") as! String
            
            GlobalClass.showAlertOKAction(APP_NAME, massage, "OK", self, successClosure: { (str) in
                self.navigationController?.popViewController(animated: true)
            })
            
            GlobalClass.hideHud()
            
        }, successWithStatus0Closure: { (responce) in
            
            // Ben10 print(""responce = \(responce as! NSDictionary)")
            let msg = (responce as! NSDictionary).object(forKey: "responsemsg") as! String
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: msg, view: self)
            GlobalClass.hideHud()
            
        }) { (error) in
            
            GlobalClass.hideHud()
            
            if error == "Not Show" {
                return
            }
            
            GlobalClass.showAlertOKAction(APP_NAME, error!, "OK", self, successClosure: { (str) in
            })
        }
    }
}
