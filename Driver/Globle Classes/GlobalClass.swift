
//
//  Common.swift
//  RepydPrototype
//
//  Created by Arthonsys LLP on 15/06/17.
//  Copyright © 2017 Arthonsys Technologies LLP. All rights reserved.
//

import UIKit
import Alamofire

import NVActivityIndicatorView

import SDWebImage

let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate
let SCREEN_WIDTH = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height

var DISTRIBUTOR = ""

// Dev RestaurantRun Driver
// com.DevRestaurantRunDriver.app
let BASE_URL = "http://driver.dev.restaurantrun.net/Webservice/"
let BASE_URL2 = "http://driver.dev.restaurantrun.net/driverApi/"
let APP_NAME = "RestaurantRun Driver"

// RestaurantRun Driver
// com.RestaurantDriver.app
//let BASE_URL = "http://driver.restaurantrun.net/Webservice/"
//let BASE_URL2 = "http://driver.restaurantrun.net/driverApi/"
//let APP_NAME = "RestaurantRun Driver"

// OMS Driver
// com.omsdriver.app
//let BASE_URL = "http://ec2-34-214-25-32.us-west-2.compute.amazonaws.com/driver_oms/Webservice/"
//let BASE_URL2 = "http://ec2-34-214-25-32.us-west-2.compute.amazonaws.com/driver_oms/driverApi/"
//let APP_NAME = "OMS Driver"

var STORE_URL = GlobalClass.setStorURL()

let SECRET_KEY = "hsjkdien6622bsks00hhvsj"
let APP_KEY = "yuhtgeriosn24348jdgdld88"


let SECRET_KEY_D =  GlobalClass.setStorKey("sceretkey")
let APP_KEY_D = GlobalClass.setStorKey("appkey")

var LOCAL_TIME_ZONE: Int { return TimeZone.current.secondsFromGMT() }

var IS_FOR_HOMR = false
var DEVICE_TOKEN = ""
var USER_ID = ""
var USER_EMAIL_ID = ""

var imageAnimationLastPosition = CGRect()

var COLOR_PRIMARY = UIColor(hexString: "#000000") // login backgroud color // #
//var COLOR_PRIMARY = UIColor(hexString: "#FF0000")
var COLOR_PRIMARY_COLOR_DARK = UIColor(hexString: "#000000")
var COLOR_ACCENT = UIColor(hexString: "#A7A7A7") //
var COLOR_ACCENT_LINE = UIColor(hexString: "#CDCDCD") // line color  // #CDCDCD // FF0000
var COLOR_ACCENT_VIEW_BG = UIColor(hexString: "#efedf2") // after changes
var COLOR_YELLOW = UIColor(hexString: "#4286f4")

var COLOR_PRIMARY_TEXT_COLOR = UIColor(hexString: "#000000") // dark black i am using this (text hard) // chack Color : #FF0000    and real is : #000000   //    FF0000
//var COLOR_PRIMARY_TEXT_COLOR = UIColor(hexString: "#FF0000")

var COLOR_PRIMARY_DARK_TEXT_COLOR = UIColor(hexString: "#000000")
var COLOR_ACCENT_TEXT_COLOR = UIColor(hexString: "#A7A7A7") // halka black cloure //

var COLOR_WHITE = UIColor(hexString: "#FFFFFF")


//var TabBarController_VC_Obj : TabBarController_VC!
//
//var CategoryVC_obj : CategoryVC!

let MAIN_STORYBOARD = UIStoryboard(name: "Main", bundle: nil)

enum Sboard : String {
    case Main = "Main"
    case Second = "Second"
    case Third = "Third"
}


class NavigationCustom : NSObject {
    
    class func Push(storyBoard : String, viewController : UIViewController, indentifier : String){
        
        let viewControllerToPush = UIStoryboard(name: storyBoard, bundle: nil).instantiateViewController(withIdentifier: indentifier)
        let navigator = viewController.navigationController
        navigator?.pushViewController(viewControllerToPush, animated: true)
    }
    
    class func Present(storyBoard : String, viewController : UIViewController, indentifier : String){
        
        let viewControllerToPresent = UIStoryboard(name: storyBoard, bundle: nil).instantiateViewController(withIdentifier: indentifier)
        
        viewController.present(viewControllerToPresent, animated: true, completion: nil)
    }
    
    
    class func MakeRootViewController(storyBoard : String, indentifier : String, window : UIWindow){
        
        let viewControllerToBeRoot = UIStoryboard(name: storyBoard, bundle: nil).instantiateViewController(withIdentifier: indentifier)
        window.rootViewController = viewControllerToBeRoot
        
        window.makeKeyAndVisible()
        
    }
    
    class func navToDriverProfile( viewController : UIViewController) {
        let viewControllerToPush = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileVC")
        let navigator = viewController.navigationController
        navigator?.pushViewController(viewControllerToPush, animated: true)
    }
    
    class func navToDriverMap( viewController : UIViewController) {
        let viewControllerToPush = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileVC")
        let navigator = viewController.navigationController
        navigator?.pushViewController(viewControllerToPush, animated: true)
    }
}


public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
}



class GlobalClass: NSObject /*, NVActivityIndicatorViewable*/ {
    
    class func setStorURL() -> String {
        
        if (GlobalClass.isKeyPresentInUserDefaults(key: "store_url")) {
            if UserDefaults.standard.object(forKey: "store_url") != nil {
                let store_url = GlobalClass.getUserDefault("store_url")
                return store_url
            }
        }
        return ""
    }
    class func setStorKey(_ key : String) -> String {
        
        if (GlobalClass.isKeyPresentInUserDefaults(key: "getDistributorList")) {
            if UserDefaults.standard.object(forKey: "getDistributorList") != nil {
                
                let store = GlobalClass.getUserDefaultAny("getDistributorList")
                return store.object(forKey: key) as! String
            }
        }
        return ""
    }
    
    //MARK:- Password Validation
    
    class func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Za-z])(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$")
        return passwordTest.evaluate(with: password)
    }
    
    
    class func setBorderColorWithBorder1(_ myView : UIView, _color : UIColor) {
        
        myView.layer.borderColor = _color.cgColor
        myView.layer.borderWidth = 0
        myView.layer.cornerRadius = 2.0
    }
    
    //  ProjectSetting.MakeRootViewController(storyBoard: "Main", indentifier: "RootViewController", window : self.window!)
    class func MakeRootViewController(storyBoard : String, indentifier : String, window : UIWindow){
        
        let viewControllerToBeRoot = UIStoryboard(name: storyBoard, bundle: nil).instantiateViewController(withIdentifier: indentifier)
        window.rootViewController = viewControllerToBeRoot
        
        window.makeKeyAndVisible()
        
    }
    
    //MARK:- Function for DSActivity View
    
    
    
    class func showHUD()
    {
        
        DSBezelActivityView.newActivityView(for: UIApplication.shared.delegate?.window!, withLabel: "Loading")
        
    }
    class func showHUDOnView(_ view : UIView)
    {
        //   DSBezelActivityView.newActivityView(for: view)
        DSBezelActivityView.newActivityView(for: view, withLabel: "Loading")
        
        
    }
    
    class func hideHud()
    {
        DSBezelActivityView.remove(animated: true)
    }
    
    
    
    //MARK:-  Function for user default
    
    class func setUserDefaultDICT(value: NSDictionary)
    {
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: value), forKey: "dictUserDefult")
    }
    
    class func getUserDefaultDICT()-> NSDictionary
    {
        let dataa = UserDefaults.standard.object(forKey: "dictUserDefult") as! NSData
        let obj = NSKeyedUnarchiver.unarchiveObject(with: dataa as Data) as! NSDictionary
        return obj
    }
    
    class func setUserDefault(value: String, for key: String)
    {
        let defaults =  UserDefaults.standard
        defaults.set(value, forKey: key)
    }
    
    class func setUserDefaultAny(value: Any, for key: String)
    {
        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: value), forKey: key)
    }
    
    class func setUserDefaultCurrentTrip()
    {
        let defaults =  UserDefaults.standard
        defaults.set(CURRENT_TRIP, forKey: "CurrentTrip")
    }

    class func getUserDefaultCurrentTrip()
    {
        if (GlobalClass.isKeyPresentInUserDefaults(key: "CurrentTrip")) {
            if (UserDefaults.standard.object(forKey: "CurrentTrip")) != nil {
                let defaults = UserDefaults.standard
                CURRENT_TRIP = NSMutableDictionary()
                let dic = (defaults.value(forKey: "CurrentTrip") as! NSDictionary).mutableCopy() as! NSMutableDictionary
                CURRENT_TRIP = self.IfDic(dicM: dic as! NSMutableDictionary)
            }
        }
       
    }
    class func removeUserDefaultCurrentTrip()
    {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "CurrentTrip")
    }

    
    class func getUserDefault(_ forKEY: String)-> String
    {
        let defaults = UserDefaults.standard
        return defaults.value(forKey: forKEY) as! String
    }
    class func getUserDefaultAny(_ forKEY: String)-> NSDictionary
    {
        let dataa = UserDefaults.standard.object(forKey: forKEY) as! NSData
        let obj = NSKeyedUnarchiver.unarchiveObject(with: dataa as Data) as! NSDictionary
        return obj
    }
    
    class func removeUserDefaults(_ forKEY : String)
    {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: forKEY)
    }
    
    class func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    class func IfDic(dicM : NSMutableDictionary) -> NSMutableDictionary {
        for (key, val) in dicM {
            
            if let array = val as? NSArray{
                let temp = array.mutableCopy() as! NSMutableArray
                let tp = self.IfArray(arrayM: temp)
                dicM.setValue(tp, forKey: key as! String)
            }
            else  if let dic = val as? NSDictionary{
                let temp = dic.mutableCopy() as! NSMutableDictionary
                let tp = self.IfDic(dicM: temp)
                dicM.setValue(tp, forKey: key as! String)
            }
        }
        
        return dicM
    }
    
    class func IfArray(arrayM : NSMutableArray) -> NSMutableArray {
        var i = 0
        for val in arrayM {
            
            if let array = val as? NSArray{
                let temp = array.mutableCopy() as! NSMutableArray
                let tp = self.IfArray(arrayM: temp)
                arrayM.replaceObject(at: i, with: tp)
            }
            else  if let dic = val as? NSDictionary{
                let temp = dic.mutableCopy() as! NSMutableDictionary
                let tp = self.IfDic(dicM: temp)
                arrayM.replaceObject(at: i, with: tp)
            }
            i = i + 1
        }
        return arrayM
    }
    
    //MARK:- Functions For String
    
    class func trimSpace(text textForTrim : String) -> String
    {
        let trimText = textForTrim.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return trimText
    }
    
    class func doStringContainsNumber( _string : String) -> Bool{
        
        let numberRegEx  = ".*[0-9]+.*"
        let testCase = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let containsNumber = testCase.evaluate(with: _string)
        
        return containsNumber
    }
    class func doStringContainsOnyspecialCharacters( _string : String) -> Bool{
        
        let numberRegEx  = ".*[0-9]+.*"
        let testCase = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let containsNumber = testCase.evaluate(with: _string)
        
        return containsNumber
    }
    
    //MARK:- Functions For Number
    
    class func randomNumber<T : SignedInteger>(inRange range: ClosedRange<T> = 1...6) -> T {
        let length = Int64(range.upperBound - range.lowerBound + 1)
        let value = Int64(arc4random()) % length + Int64(range.lowerBound)
        return T(value)
    }
    
    //MARK:- Functions for alert
    
    class func showAlert(alertTitle : String, alertMsg : String, view: UIViewController) {
        
        let alert = UIAlertController(title: alertTitle, message: alertMsg, preferredStyle: UIAlertController.Style.alert)
        
        let actionOK : UIAlertAction = UIAlertAction(title: "OK", style: .default) { (alt) in
            // Ben10 print(""This is ok action");
        }
        alert.addAction(actionOK)
        
        UIView.appearance(whenContainedInInstancesOf: [UIAlertController.self]).tintColor = COLOR_PRIMARY_TEXT_COLOR

        
        view.present(alert, animated: true, completion: nil)
        
    }
    
    class func showAlertOKAction(_ alertTitle : String, _ alertMsg : String, _ okTitle : String, _ view: UIViewController, successClosure: @escaping (String?) -> () ) {
        
        let alert = UIAlertController(title: alertTitle, message: alertMsg, preferredStyle: UIAlertController.Style.alert)
        
        let actionOK : UIAlertAction = UIAlertAction(title: okTitle, style: .default) { (alt) in
            // Ben10 print(""This is ok action");
            successClosure("OK")
        }
        alert.addAction(actionOK)

         UIView.appearance(whenContainedInInstancesOf: [UIAlertController.self]).tintColor = COLOR_PRIMARY_TEXT_COLOR
        
        view.present(alert, animated: true, completion: nil)
        
    }
    
    //    class func showActionSheetCancel(_ alertTitle : String, _ alertMsg : String, _ view: UIViewController) {
    //
    //        let alert = UIAlertController(title: alertTitle, message: alertMsg, preferredStyle: UIAlertControllerStyle.actionSheet)
    //
    //        let fontAwesomeHeart = alertTitle
    //        let fontAwesomeFont = UIFont(name: "OpenSans-Semibold", size: 17)!
    //        let customTitle:NSString = "\(fontAwesomeHeart)" as NSString as NSString
    //        let systemBoldAttributes:[NSAttributedStringKey : Any] = [
    //
    //            NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue) : UIFont.boldSystemFont(ofSize: 17)
    //        ]
    //        let attributedString = NSMutableAttributedString(string: customTitle as String, attributes:systemBoldAttributes)
    //        let fontAwesomeAttributes:[NSAttributedStringKey : Any] = [
    //            NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): fontAwesomeFont,
    //            NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue) : UIColor.black
    //        ]
    //        let matchRange = customTitle.range(of: fontAwesomeHeart)
    //        attributedString.addAttributes(fontAwesomeAttributes, range: matchRange)
    //        alert.setValue(attributedString, forKey: "attributedTitle")
    //
    //        let actionCancel : UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { (alt) in
    //
    //        }
    //
    //        UIView.appearance(whenContainedInInstancesOf: [UIAlertController.self]).tintColor = COLOR_THEME_TEXT
    //
    //         alert.addAction(actionCancel)
    //
    //        view.present(alert, animated: true, completion: nil)
    //
    //    }
    
    class func showAlertOKCancelAction(_ alertTitle : String, _ alertMsg : String, _ view: UIViewController, successClosure: @escaping (String?) -> (), cancelClosure: @escaping (String?) -> () ) {
        
        let alert = UIAlertController(title: alertTitle, message: alertMsg, preferredStyle: UIAlertController.Style.alert)
        
        let actionOK : UIAlertAction = UIAlertAction(title: "OK", style: .default) { (alt) in
            
            //            // Ben10 print(""This is ok action");
            successClosure("OK")
        }
        let actionCancel : UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { (alt) in
            
            //            // Ben10 print(""This is cancel action");
            cancelClosure("Cancel")
        }
        UIView.appearance(whenContainedInInstancesOf: [UIAlertController.self]).tintColor = COLOR_PRIMARY_TEXT_COLOR
        alert.addAction(actionOK)
        alert.addAction(actionCancel)
        
        view.present(alert, animated: true, completion: nil)
    }
    
    
    class func showActionSheet(_ alertTitle : String, _ alertMsg : String, _ view: UIViewController, camera: @escaping (String?) -> (), ImageGallery: @escaping (String?) -> (), cancel: @escaping (String?) -> () ) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let actionCamera : UIAlertAction = UIAlertAction(title: "Take Picture Using Camera", style: .default) { (alt) in
            //            // Ben10 print(""This is ok action");
            camera("OK")
        }
        
        let actionImageGallery : UIAlertAction = UIAlertAction(title: "Select Picture From Gallery", style: .default) { (alt) in
            //            // Ben10 print(""This is ok action");
            ImageGallery("ImageGallery")
        }
        let actionCancel : UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { (alt) in
            //            // Ben10 print(""This is cancel action");
            cancel("Cancel")
        }
        UIView.appearance(whenContainedInInstancesOf: [UIAlertController.self]).tintColor = COLOR_PRIMARY_TEXT_COLOR
        alert.addAction(actionCamera)
        alert.addAction(actionImageGallery)
        alert.addAction(actionCancel)
        view.present(alert, animated: true, completion: nil)
    }
    
    
    //MARK:- Functions for Image
    
    class func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    // convert base 64
    class func imageToStrinig(_ image : UIImage) ->String {
        let chosenImage = image
        //   chosenImage = self.resizeImage(image: image, newWidth: 600.0)!  // uncomment if u want resize image
        
        var imageData2 = Data()
        // imageData2 = imageData2 = UIImageJPEGRepresentation(chosenImage,0.05)!
        imageData2 = chosenImage.jpegData(compressionQuality: 0.05)!
        
        let img2 = UIImage(data: imageData2)
        
        var pictureData  = NSData()
        pictureData = img2!.pngData()! as NSData
        
        let imgdata2 = pictureData.base64EncodedData(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        return String(data: imgdata2, encoding: .utf8)!
    }
    
    class func stringToImage(_ string : String) -> UIImage {
        
        if let decodedData = Data(base64Encoded: string, options: .ignoreUnknownCharacters) {
            let image = UIImage(data: decodedData)
            return image!
        }
        else{
            return (UIImage.init(named: "pictures"))!
        }
    }
    
    
    //    class func animImageZoom(imageView : UIImageView, onView : UIViewController, dataDic : NSMutableDictionary, done: @escaping (String?)-> ()) {
    //
    //        if let img = onView.view.viewWithTag(50509) as? UIImageView {
    //            img.removeFromSuperview()
    //        }
    //
    //        let animBackView = UIView()
    //        animBackView.frame = onView.view.frame
    //        animBackView.backgroundColor = UIColor.white
    //        animBackView.alpha = 0
    //        animBackView.tag = 505083
    //        onView.view.addSubview(animBackView)
    //
    //        let animImage = UIImageView()
    //        animImage.frame  = GlobalClass.findViewPosition(view: imageView, withView: onView.view)
    //        animImage.image = imageView.image
    //        // animImage.backgroundColor = imageView.backgroundColor
    //        animImage.contentMode = .scaleAspectFit
    //        animImage.tag = 505093
    //        onView.view.addSubview(animImage)
    //        imageAnimationLastPosition = animImage.frame
    //        UIView.animate(withDuration: 0.45, animations: {
    //            animImage.frame = CGRect(x: 0, y: 88, width: SCREEN_WIDTH, height: SCREEN_HEIGHT)
    //            animBackView.alpha = 1
    //        }) { (true) in
    //            // onView.performSegue(withIdentifier: "ToItemDetailVC", sender: animImage.image)
    //            animBackView.removeFromSuperview()
    //            animImage.removeFromSuperview()
    //            done("Done")
    //        }
    //    }
    
    //    class func anim(imageView : UIImageView, onView : UIViewController, dataDic : NSMutableDictionary) {
    //
    //        if let img = onView.view.viewWithTag(50509) as? UIImageView {
    //            img.removeFromSuperview()
    //        }
    //
    //        let animBackView = UIView()
    //        animBackView.frame = onView.view.frame
    //        animBackView.backgroundColor = UIColor.white
    //        animBackView.alpha = 0
    //        animBackView.tag = 50508
    //        onView.view.addSubview(animBackView)
    //
    //        let animImage = UIImageView()
    //        animImage.frame  = GlobalClass.findViewPosition(view: imageView, withView: onView.view)
    //        animImage.image = imageView.image
    //        // animImage.backgroundColor = imageView.backgroundColor
    //        animImage.contentMode = .scaleAspectFit
    //        animImage.tag = 50509
    //        onView.view.addSubview(animImage)
    //        imageAnimationLastPosition = animImage.frame
    //        UIView.animate(withDuration: 0.45, animations: {
    //            animImage.frame = CGRect(x: 0, y: 88, width: SCREEN_WIDTH, height: 173)
    //            animBackView.alpha = 1
    //        }) { (true) in
    //            // onView.performSegue(withIdentifier: "ToItemDetailVC", sender: animImage.image)
    //            GlobalClass.navigatToProductDetail(currentVC: onView, pageTitle: "POTATO", animImage : animImage.image ?? UIImage(), dataDic: dataDic)
    //        }
    //    }
    
    //    class func removeAnim(FormView : UIViewController) {
    //        let animBackView = FormView.view.viewWithTag(50508)
    //        if let img = FormView.view.viewWithTag(50509) as? UIImageView {
    //
    //            UIView.animate(withDuration: 0.45, animations: {
    //                img.frame = imageAnimationLastPosition
    //                animBackView!.alpha = 0
    //                FormView.view.layoutIfNeeded()
    //
    //            }) { (true) in
    //
    //                animBackView!.removeFromSuperview()
    //                img.removeFromSuperview()
    //            }
    //        }
    //    }
    
    //MARK:- Save Image To Directry
    
    class func saveImageDocumentDirectory(_image : UIImage, _name: String) -> String {
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent( _name + ".png")
        // let image = UIImage(named: "as")
        // Ben10 print("paths)
        let imageData = _image.jpegData(compressionQuality: 0.05)
        fileManager.createFile(atPath: paths as String, contents: imageData, attributes: nil)
        
        return paths
    }
    
    class func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    class func getImage(_path : String) -> String {
        let fileManager = FileManager.default
        let imagePAth = _path
        if fileManager.fileExists(atPath: imagePAth){
            //let imgg = UIImage(contentsOfFile: imagePAth)
            // Ben10 print(""dfd")
            return imagePAth
        }else{
            // Ben10 print(""No Image")
            return ""
        }
    }
    
    //MARK:- Functions For Date
    
    class func timeStampToDate(_timestamp : String, _dateFormat : String) -> String{
        
        var date = Date(timeIntervalSince1970: TimeInterval(_timestamp)!)
        date += TimeInterval(truncating: LOCAL_TIME_ZONE as NSNumber)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = _dateFormat //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    class func StringDateToDate(dateString : String, dateFormatte : String) -> Date {
        
        //let dateString = "Thu, 22 Oct 2015 07:45:17 +0000"
        //        let dateFormatte = "EEE, dd MMM yyyy hh:mm:ss +zzzz"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormatte
        var dateObj = dateFormatter.date(from: dateString)
        dateObj = dateObj! + TimeInterval(LOCAL_TIME_ZONE)
        if dateObj == nil {
            return Date()
        } else{
            return dateObj!
        }
    }
    
    class func DateToString(date : Date, dateFormatte : String) -> String {
        
        //        let date = NSDate()
        //        let dateFormatte = "EEE, dd MMM yyyy hh:mm:ss +zzzz"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormatte
        return (dateFormatter.string(from: date  - TimeInterval(LOCAL_TIME_ZONE)))
        
    }
    
    class func sortDataWithDate(arrayData : NSArray) -> NSArray {
        //  var chat: [ChatMessage]!
        
        let returnArray = NSMutableArray()
        var subArray = NSMutableArray()
        let arrayDate = NSMutableArray()
        
        for i in 0 ..< arrayData.count {
            
            let msgDate = ((arrayData[i] as! NSDictionary).object(forKey: "request_created_time") as! String) //GlobalClass.timeStampToDate(_timestamp: ((arrayData[i] as! NSDictionary).object(forKey: "date") as! String), _dateFormat: "dd/MM/yyyy")
            
            // // Ben10 print(""dddt \(msgDate)")
            
            //    // Ben10 print(""date array \(arrayDate)  $$  msgDate \(msgDate)")
            if arrayDate.contains(msgDate) {
                
                subArray.add(arrayData[i])
                
            }
            else{
                arrayDate.add(msgDate)
                if arrayDate.count > 1 {
                    returnArray.add(subArray)
                }
                
                subArray = NSMutableArray()
                subArray.add(arrayData[i])
            }
        }
        
        if subArray != nil {
            returnArray.add(subArray)
        }
        
        // Ben10 print("returnArray)
        return returnArray
    }
    
    
    //MARK:- Functions For Validation
    
    class func isValidEmail(testStr:String) -> Bool {
        // // Ben10 print(""validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
        
    }
    
    class func isVaildPass(testStr: String) -> Bool {
        
        let passRegEx = "^.*(?=.{6,})(?=.*[a-zA-Z])[a-zA-Z0-9]+$"
        let passTest = NSPredicate(format:"SELF MATCHES %@", passRegEx)
        return passTest.evaluate(with: testStr)
    }
    
    
    class func validateUrl (_ urlString: String) -> Bool {
        let urlRegEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        return NSPredicate(format: "SELF MATCHES %@", urlRegEx).evaluate(with: urlString)
    }
    
    //MARK:- Function for UIView
    
    
    class func setBtnAnimation(view: UIView, withView : UIView, image : UIImage) -> CGRect {
        
        var currentX = view.frame.origin.x
        var currentY = view.frame.origin.y
        let currentH = view.frame.size.height
        let currentW = view.frame.size.width
        
        var lastView = view
        while lastView != withView {
            if  lastView.isKind(of: UIScrollView.self) {
                currentX = currentX - (lastView as! UIScrollView).contentOffset.x
                currentY = currentY - (lastView as! UIScrollView).contentOffset.y
            }
            
            let subView = lastView.superview as! UIView
            currentX = currentX + subView.frame.origin.x
            currentY = currentY + subView.frame.origin.y
            
            lastView = subView
            
        }
        //  view.isHidden = true
        let window = UIApplication.shared.keyWindow!
        
        let v2 = UIView(frame: CGRect(x: currentX, y: currentY, width: currentW, height: currentH))
        v2.backgroundColor = UIColor.clear
        window.addSubview(v2);
        
        let img = UIImageView()
        img.frame = CGRect(x: 0, y: 0, width: v2.frame.size.width, height: v2.frame.size.height)
        img.contentMode = .center
        img.image = image
        v2.addSubview(img)
        
        let originalTransform = v2.transform
        let scaledTransform = originalTransform.scaledBy(x: 1.9, y: 1.9)
        let scaledAndTranslatedTransform = scaledTransform.translatedBy(x: 0.0, y: 0.0)
        
        
        
        /*   UIView.animate(withDuration: 0.4, animations: {
         v2.transform = scaledAndTranslatedTransform
         v2.alpha = 0
         }) { (true) in
         v2.removeFromSuperview()
         }*/
        
        // v2.transform = .identity
        UIView.animate(withDuration: 1.2, animations: {
            v2.transform = scaledAndTranslatedTransform
            // v2.alpha = 0
        }) { (true) in
            // v2.removeFromSuperview()
            UIView.animate(withDuration: 1.2, animations: {
                v2.transform = .identity
                // v2.alpha = 0
            }) { (true) in
                v2.removeFromSuperview()
                
                view.isHidden = true
                
                
            }
        }
        
        return CGRect(x: currentX, y: currentY, width: currentW, height: currentH)
    }
    
    class func likeBtnAnimation(btn : UIButton) {
        
        let originalTransform = (btn as UIView).transform
        let scaledTransform = originalTransform.scaledBy(x: 0.1, y: 0.1)
        let scaledAndTranslatedTransform = scaledTransform.translatedBy(x: 0.0, y: 0.0)
        
        btn.transform = scaledAndTranslatedTransform
        
        let originalTransform2 = (btn as UIView).transform
        let scaledTransform2 = originalTransform2.scaledBy(x: 5, y: 5)
        let scaledAndTranslatedTransform2 = scaledTransform2.translatedBy(x: 0.0, y: 0.0)
        
        UIView.animate(withDuration: 6.2, animations: {
            btn.transform = scaledAndTranslatedTransform2
        }) { (true) in
            
        }
        
    }
    
    class func findViewPosition(view: UIView, withView : UIView) -> CGRect {
        
        var currentX = view.frame.origin.x
        var currentY = view.frame.origin.y
        let currentH = view.frame.size.height
        let currentW = view.frame.size.width
        
        var lastView = view
        while lastView != withView {
            if  lastView.isKind(of: UIScrollView.self) {
                currentX = currentX - (lastView as! UIScrollView).contentOffset.x
                currentY = currentY - (lastView as! UIScrollView).contentOffset.y
            }
            
            let subView = lastView.superview as! UIView
            currentX = currentX + subView.frame.origin.x
            currentY = currentY + subView.frame.origin.y
            
            lastView = subView
            
        }
        
        return CGRect(x: currentX, y: currentY, width: currentW, height: currentH)
    }
    
    class func showPopup() {
        let window = UIApplication.shared.keyWindow!
        
        let v2 = UIView(frame: CGRect(x: 0, y: SCREEN_HEIGHT, width: SCREEN_WIDTH, height: 150))
        v2.backgroundColor = UIColor.groupTableViewBackground
        window.addSubview(v2);
        
        let btn = UIButton()
        btn.frame = CGRect(x: 20, y: 20, width: 100, height: 40)
        btn.setTitle("btn", for: .normal)
        btn.backgroundColor = UIColor.lightGray
        btn.addAction {
            // Ben10 print(""button pressed")
            
            UIView.animate(withDuration: 0.3, animations: {
                v2.frame.origin.y = SCREEN_HEIGHT
            }, completion: { (true) in
                v2.removeFromSuperview()
            })
        }
        v2.addSubview(btn)
        
        //        UIView.animate(withDuration: 0.3) {
        //            v2.frame.origin.y = SCREEN_HEIGHT - v2.frame.size.height
        //        }
        
        UIView.animate(withDuration: 0.3, animations: {
            v2.frame.origin.y = SCREEN_HEIGHT - v2.frame.size.height
        }, completion: { (true) in
            
            UIView.animate(withDuration: 0.3, delay: 3.0, options: .curveLinear, animations: {
                v2.frame.origin.y = SCREEN_HEIGHT
            }, completion: { (true) in
                // v2.removeFromSuperview()
            })
            
        })
        
    }
    
    //MARK:- Function For API
    
    class func API_PostData(fromVC : UIViewController, _ strURL: String, parameter Params: NSDictionary, successWithStatus1Closure: @escaping (NSDictionary?) -> (), successWithStatus0Closure: @escaping (NSDictionary?) -> (), failurClosure: @escaping (String?)-> ())
    {
        
        let param = Params.mutableCopy() as! NSMutableDictionary
        
        
        var complite_url = BASE_URL+strURL
        
        let newApi = ["all_trip_listing"] as NSArray
        if newApi.contains(strURL) {
            complite_url = BASE_URL2+strURL
        }
        
        let Param: [String: Any] = param.mutableCopy() as! [String : Any]
        // Ben10 print(""Param:-", Param as NSDictionary);
        // Ben10 print(""complite_url = \(complite_url)")
        
        if Reachability.isConnectedToNetwork(){
            let header = ["Content-Type" : "application/json",
                          "Accept" : "application/json",
                          "appkey":"j5cp4b3uqp62hbicheeb9aqktednck",
                          "secretkey":"t73vefdk28zcby4nzewnv97ez7pmne"
            ]
            Alamofire.request(complite_url, method: .post, parameters: Param ,encoding: JSONEncoding.default, headers: header).responseJSON {
                response in
                // Ben10 print("complite_url)
                if(response.result.error != nil)
                {
                    
                    if param["api_call_count"] == nil {
                        param.setValue(1, forKey: "api_call_count")
                        GlobalClass.API_PostData(fromVC : fromVC, strURL, parameter: param, successWithStatus1Closure: { (res) in
                            successWithStatus1Closure(res)
                        }, successWithStatus0Closure: { (res) in
                            successWithStatus0Closure(res)
                        }, failurClosure: { (error) in
                            failurClosure("Internal server error.")
                        })
                        return
                    }
                        
                    else {
                        if (param["api_call_count"] as! Int) < 2 {
                            
                            param.setValue((param["api_call_count"] as! Int) + 1, forKey: "api_call_count")
                            GlobalClass.API_PostData(fromVC : fromVC, strURL, parameter: param, successWithStatus1Closure: { (res) in
                                successWithStatus1Closure(res)
                            }, successWithStatus0Closure: { (res) in
                                successWithStatus0Closure(res)
                            }, failurClosure: { (error) in
                                failurClosure("Internal server error.")
                            })
                            return
                        }
                    }
                    
                    
                    let error = response.result.error
                    if (error?.localizedDescription == "The request timed out."){
                        failurClosure("The request timed out.");
                        return
                    }
                    if (error?.localizedDescription == "Could not connect to the server."){
                        failurClosure("Could not connect to the server.");
                        return
                    }
                    if (error?.localizedDescription == "The network connection was lost."){
                        failurClosure("The network connection was lost.");
                        return
                    }
                    failurClosure("Error");
                }
                
                let dictResponse = response.result.value as? NSDictionary
                
                // Ben10 print(""Response",response)
                
                if dictResponse is Dictionary<AnyHashable,Any> {
                    // Ben10 print(""Yes, it's a Dictionary")
                }
                else{
                    // Ben10 print(""No, it's a not a Dictionary")
                    failurClosure("Active internet connection required")
                    return
                }
                
                if strURL == "loginCustomer" || strURL == "customerregistration" || strURL == "createcustomer" || strURL == "getDistributorList" {
                    
                    if  dictResponse!["responsecode"] as? String != nil {
                        let isSuccess = dictResponse!["responsecode"] as! String
                        
                        if isSuccess == "200"
                        {
                            successWithStatus1Closure(dictResponse)
                        }else{
                            successWithStatus0Closure(dictResponse)
                        }
                    }
                }
                    
                else {
                    if  dictResponse!["responsecode"] as? String != nil {
                        let isSuccess = dictResponse!["responsecode"] as! String
                        if isSuccess == "200"
                        {
                            successWithStatus1Closure(dictResponse)
                        }else{
                            successWithStatus0Closure(dictResponse)
                        }
                    }
                    else {
                        failurClosure("Internal server error.")
                    }
                }
                
            }
        }else {
            
           
            
            // Ben10 print(""Active internet connection required");
            failurClosure("Active internet connection required")
            return
        }
        
    }
    
    
    class func multipartData(_ strURL: String, parameter Params: NSDictionary, MediaParams: NSDictionary, successWithStatus1Closure: @escaping (AnyObject?) -> (), successWithStatus0Closure: @escaping (AnyObject?) -> (), failurClosure: @escaping (String?)-> ())
    {
        
        let param = Params.mutableCopy() as! NSMutableDictionary
        param.setValue((GlobalClass.getUserDefault("DEVICE_TOKEN")), forKey: "device_token")
        param.setValue("j5cp4b3uqp62hbicheeb9aqktednck", forKey: "appkey")
        param.setValue("t73vefdk28zcby4nzewnv97ez7pmne", forKey: "secretkey")
        
        
        var complite_url = BASE_URL+strURL
        
        let newApi = ["all_trip_listing", "orderComplete", "tripComplete","trip_tracking"] as NSArray
        if newApi.contains(strURL) {
            complite_url = BASE_URL2+strURL
        }
        
        let Param: [String: Any] = param.mutableCopy() as! [String : Any]
        APrint("Param:-", Param as NSDictionary);
        APrint("complite_url = \(complite_url)")
        APrint("***************************************************")
        APrint("***************************************************")
        for (key,val) in Param {
            APrint("\(key) : \(val)")
        }
        APrint("***************************************************")
        APrint("***************************************************")
        
        var header : Any!
        
        if MediaParams.count != 0 {
            
            header = ["Content-Type" : "application/json",
                          "Accept" : "application/json",
                          "appkey":"j5cp4b3uqp62hbicheeb9aqktednck",
                          "secretkey":"t73vefdk28zcby4nzewnv97ez7pmne",
                          "image" : MediaParams["image"]]
            // Ben10 print("header)
            
        } else{
            
             header = ["Content-Type" : "application/json",
                          "Accept" : "application/json",
                          "appkey": "j5cp4b3uqp62hbicheeb9aqktednck",
                          "secretkey": "t73vefdk28zcby4nzewnv97ez7pmne"]
        }
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for (key, value) in Param {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                }
                
            }, usingThreshold: UInt64.init(), to: complite_url, method: .post, headers: (header as! HTTPHeaders)) { (result) in
                switch result{
                case .success(let upload, _, _):
                    
                    // Ben10 print("upload)

                    upload.responseJSON { response in
                        
                        let dictResponse = response.result.value as? NSDictionary
                        
                         APrint(response)
                        
                        if dictResponse is Dictionary<AnyHashable,Any> {
                            // Ben10 print(""Yes, it's a Dictionary")
                        }
                        else {
                            
                            if param["api_call_count"] == nil {
                                param.setValue(1, forKey: "api_call_count")
                                GlobalClass.multipartData(strURL, parameter: param, MediaParams: MediaParams, successWithStatus1Closure: { (res) in
                                    successWithStatus1Closure(res)
                                }, successWithStatus0Closure: { (res) in
                                    successWithStatus0Closure(res)
                                }, failurClosure: { (error) in
                                    failurClosure("Internal server error.")
                                })
                                return
                            }
                                
                            else {
                                if (param["api_call_count"] as! Int) < 2 {
                                    
                                    param.setValue((param["api_call_count"] as! Int) + 1, forKey: "api_call_count")
                                    GlobalClass.multipartData(strURL, parameter: param, MediaParams: MediaParams, successWithStatus1Closure: {  (res) in
                                        successWithStatus1Closure(res)
                                    }, successWithStatus0Closure: { (res) in
                                        successWithStatus0Closure(res)
                                    }, failurClosure: { (error) in
                                        failurClosure("Internal server error.")
                                    })
                                    return
                                }
                            }
                            // Ben10 print(""No, it's a not a Dictionary")
                            
                            failurClosure("Not Show")

                            return

//                            failurClosure("Server Error")

//                            failurClosure("Active internet connection required")
                            
                            return
                        }
                        
                        let isSuccess = dictResponse!["responsecode"] as! String
                        
                        if isSuccess == "200"
                        {
                            successWithStatus1Closure(dictResponse)
                        }else{
                            successWithStatus0Closure(dictResponse)
                        }
                    }
                case .failure(let error):
                    
                    
                        
                        if param["api_call_count"] == nil {
                            param.setValue(1, forKey: "api_call_count")
                            GlobalClass.multipartData(strURL, parameter: param, MediaParams: MediaParams, successWithStatus1Closure: { (res) in
                                successWithStatus1Closure(res)
                            }, successWithStatus0Closure: { (res) in
                                successWithStatus0Closure(res)
                            }, failurClosure: { (error) in
                                failurClosure("Internal server error.")
                            })
                            return
                        }
                            
                        else {
                            if (param["api_call_count"] as! Int) < 2 {
                                
                                param.setValue((param["api_call_count"] as! Int) + 1, forKey: "api_call_count")
                                GlobalClass.multipartData(strURL, parameter: param, MediaParams: MediaParams, successWithStatus1Closure: {  (res) in
                                    successWithStatus1Closure(res)
                                }, successWithStatus0Closure: { (res) in
                                    successWithStatus0Closure(res)
                                }, failurClosure: { (error) in
                                    failurClosure("Internal server error.")
                                })
                                return
                            }
                        }
                    
                    failurClosure("Not Show")
                    
                    return
                    
                    // Ben10 print(""error.localizedDescription = \(error.localizedDescription)")
                    if (error.localizedDescription == "The request timed out."){
                        failurClosure("The request timed out.");
                        return
                    }
                    else if (error.localizedDescription == "Could not connect to the server."){
                        failurClosure("Could not connect to the server.");
                        return
                    }
                    else if (error.localizedDescription == "The network connection was lost."){
                        failurClosure("The network connection was lost.");
                        return
                    }
                    else if (error.localizedDescription == "The URL provided is not a file URL.") {
                        failurClosure("The URL provided is not a file URL.")
                    }
                    else {
                        failurClosure("Active internet connection required")
                    }
                    
                }
            }
            
            
        }else {
            
            self.hideHud()
            
            failurClosure("Not Show")
            
            return
            
            // Ben10 print(""Active internet connection required");
            
            failurClosure("Active internet connection required")
            
            return
        }
    }
    
    class func API_PostData_multipartFormData(_ strURL: String, parameter Params: NSDictionary, MediaParams: NSDictionary, MediaData: NSArray, successWithStatus1Closure: @escaping (AnyObject?) -> (), successWithStatus0Closure: @escaping (AnyObject?) -> (), failurClosure: @escaping (String?)-> ())
    {
        
        let param = Params.mutableCopy() as! NSMutableDictionary
        
        param.setValue(DEVICE_TOKEN, forKey: "device_token")
        param.setValue("j5cp4b3uqp62hbicheeb9aqktednck", forKey: "appkey")
        param.setValue("t73vefdk28zcby4nzewnv97ez7pmne", forKey: "secretkey")
        
        let complite_url = BASE_URL+strURL
        
        let Param: [String: Any] = param.mutableCopy() as! [String : Any]
        // Ben10 print(""Param:-", Param as! NSDictionary);
        // Ben10 print(""complite_url = \(complite_url)")
        
        var header : Any!
        
        if MediaParams.count != 0 {
            header = ["Content-Type" : "application/json",
                      "Accept" : "application/json",
                      "appkey":"j5cp4b3uqp62hbicheeb9aqktednck",
                      "secretkey":"t73vefdk28zcby4nzewnv97ez7pmne",
                      "image" : MediaParams["image"]]
            // Ben10 print("header)
            
        } else {
            
            header = ["Content-Type" : "application/json",
                      "Accept" : "application/json",
                      "appkey":"j5cp4b3uqp62hbicheeb9aqktednck",
                      "secretkey":"t73vefdk28zcby4nzewnv97ez7pmne"
            ]
        }
        
        if Reachability.isConnectedToNetwork() {
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for i in MediaData {
                    
                    let data = i as! NSDictionary
                    
                    multipartFormData.append(data["Data"] as! Data, withName: "\(data["withName"]!)", fileName: "\(data["fileName"]!)", mimeType: "\(data["mimeType"]!)")
                }

                for (key, value) in Param {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                }
                
                // Ben10 print("multipartFormData)
                
            }, usingThreshold: UInt64.init(), to: complite_url, method: .post, headers: (header as! HTTPHeaders)) { (result) in
                switch result{
                case .success(let upload, _, _):
                    
                    // Ben10 print("upload)
                    
                    upload.responseJSON { response in
                        
                        let dictResponse = response.result.value as? NSDictionary
                        
                        // Ben10 print(""Response",response)
                        
                        if dictResponse is Dictionary<AnyHashable,Any> {
                            // Ben10 print(""Yes, it's a Dictionary")
                        }
                        else {
                            
                            // Ben10 print(""No, it's a not a Dictionary")
                            
                            failurClosure("Not Show")
                            
                            return
                            
//                            failurClosure("Server Error")
                            
                            //                            failurClosure("Active internet connection required")
                            
                            return
                        }
                        
                        let isSuccess = dictResponse!["responsecode"] as! String
                        
                        if isSuccess == "200"
                        {
                            successWithStatus1Closure(dictResponse)
                        }else{
                            successWithStatus0Closure(dictResponse)
                        }
                    }
                case .failure(let error):
                    
                    failurClosure("Not Show")
                    
                    return
                        
                        // Ben10 print(""error.localizedDescription = \(error.localizedDescription)")
                    if (error.localizedDescription == "The request timed out."){
                        failurClosure("The request timed out.");
                        return
                    }
                    else if (error.localizedDescription == "Could not connect to the server."){
                        failurClosure("Could not connect to the server.");
                        return
                    }
                    else if (error.localizedDescription == "The network connection was lost."){
                        failurClosure("The network connection was lost.");
                        return
                    }
                    else if (error.localizedDescription == "The URL provided is not a file URL.") {
                        failurClosure("The URL provided is not a file URL.")
                    }
                    else {
                        failurClosure("Active internet connection required")
                    }
                }
            }
        } else {
            
            self.hideHud()
            
            failurClosure("Not Show")
            
            return
                
                // Ben10 print(""Active internet connection required");
            
            failurClosure("Active internet connection required")
            
            return
        }
    }
    
    
    
    /*
     class func API_PostData(_ strURL: String, parameter Params: NSDictionary, successWithStatus1Closure: @escaping (AnyObject?) -> (), successWithStatus0Closure: @escaping (AnyObject?) -> (), failurClosure: @escaping (String?)-> ())
     {
     
     let Param: [String: Any] = Params.mutableCopy() as! [String : Any]
     
     // Ben10 print(""Params:-", Params);
     
     let complite_url = BASE_URL+strURL
     // Ben10 print("complite_url)
     
     if Reachability.isConnectedToNetwork(){
     
     var jwt_token = ""
     if GlobalClass.isKeyPresentInUserDefaults(key: "jwt_token") {
     jwt_token = GlobalClass.getUserDefault("jwt_token")
     }
     let header = ["Content-Type" : "application/json",
     "Accept" : "application/json",
     "api_access_key"  : API_ACCESS_KEY,
     "jwt_token" : jwt_token
     ]
     
     
     Alamofire.request(complite_url, method: .post, parameters: Param ,encoding: JSONEncoding.default, headers: header).responseJSON {
     response in
     if(response.result.error != nil)
     {
     
     let error = response.result.error
     if (error?.localizedDescription == "The request timed out."){
     failurClosure("The request timed out.");
     return
     }
     if (error?.localizedDescription == "Could not connect to the server."){
     failurClosure("Could not connect to the server.");
     return
     }
     if (error?.localizedDescription == "The network connection was lost."){
     failurClosure("The network connection was lost.");
     return
     }
     }
     
     let dictResponse = response.result.value as? NSDictionary
     
     // Ben10 print(""Response",response)
     
     
     
     if dictResponse is Dictionary<AnyHashable,Any> {
     // Ben10 print(""Yes, it's a Dictionary")
     }
     else{
     // Ben10 print(""No, it's a not a Dictionary")
     failurClosure("Active internet connection required")
     return
     }
     
     let isSuccess = dictResponse!["status"] as! Int
     
     if isSuccess == 200
     {
     successWithStatus1Closure(dictResponse)
     }else{
     successWithStatus0Closure(dictResponse)
     }
     
     }
     }else {
     
     self.hideHud()
     // Ben10 print(""Active internet connection required");
     failurClosure("Active internet connection required")
     return
     }
     
     }
     
     class func API_PutData(_ strURL: String, parameter Params: NSDictionary, successWithStatus1Closure: @escaping (AnyObject?) -> (), successWithStatus0Closure: @escaping (AnyObject?) -> (), failurClosure: @escaping (String?)-> ())
     {
     
     let Param: [String: Any] = Params.mutableCopy() as! [String : Any]
     
     
     // Ben10 print(""Params:-", Params);
     
     let complite_url = BASE_URL+strURL
     // Ben10 print("complite_url)
     
     if Reachability.isConnectedToNetwork(){
     var jwt_token = ""
     if GlobalClass.isKeyPresentInUserDefaults(key: "jwt_token") {
     jwt_token = GlobalClass.getUserDefault("jwt_token")
     }
     let header = ["Content-Type" : "application/json",
     "Accept" : "application/json",
     "api_access_key"  : API_ACCESS_KEY,
     "jwt_token" : jwt_token
     ]
     Alamofire.request(complite_url, method: .put, parameters: Param ,encoding: JSONEncoding.default, headers: header).responseJSON {
     response in
     if(response.result.error != nil)
     {
     
     
     let error = response.result.error
     if (error?.localizedDescription == "The request timed out."){
     failurClosure("The request timed out.");
     return
     }
     if (error?.localizedDescription == "Could not connect to the server."){
     failurClosure("Could not connect to the server.");
     return
     }
     if (error?.localizedDescription == "The network connection was lost."){
     failurClosure("The network connection was lost.");
     return
     }
     }
     let dictResponse = response.result.value as? NSDictionary
     
     // Ben10 print(""Response",response)
     
     
     
     if dictResponse is Dictionary<AnyHashable,Any> {
     // Ben10 print(""Yes, it's a Dictionary")
     }
     else{
     // Ben10 print(""No, it's a not a Dictionary")
     failurClosure("Active internet connection required")
     return
     }
     
     let isSuccess = dictResponse!["success"] as! Int
     
     if isSuccess == 1
     {
     successWithStatus1Closure(dictResponse)
     }else{
     successWithStatus0Closure(dictResponse)
     }
     
     }
     }else {
     self.hideHud()
     
     // Ben10 print(""Active internet connection required");
     failurClosure("Active internet connection required")
     return
     }
     
     }
     
     
     class func API_GetParam(_ strURL: String, parameter Params: NSDictionary, successWithStatus1Closure: @escaping (AnyObject?) -> (), successWithStatus0Closure: @escaping (AnyObject?) -> (), failurClosure: @escaping (String?)-> ())
     {
     
     let Param: [String: Any] = Params.mutableCopy() as! [String : Any]
     
     // Ben10 print(""Params:-", Params);
     
     let complite_url = BASE_URL+strURL
     // Ben10 print("complite_url)
     
     if Reachability.isConnectedToNetwork(){
     
     var jwt_token = ""
     if GlobalClass.isKeyPresentInUserDefaults(key: "jwt_token") {
     jwt_token = GlobalClass.getUserDefault("jwt_token")
     }
     let header = ["Content-Type" : "application/json",
     "Accept" : "application/json",
     "api_access_key"  : API_ACCESS_KEY,
     "jwt_token" : jwt_token
     ]
     
     Alamofire.request(complite_url, method: .get, parameters: Param ,encoding: JSONEncoding.default, headers: header).responseJSON {
     response in
     if(response.result.error != nil)
     {
     
     
     let error = response.result.error
     if (error?.localizedDescription == "The request timed out."){
     failurClosure("The request timed out.");
     return
     }
     if (error?.localizedDescription == "Could not connect to the server."){
     failurClosure("Could not connect to the server.");
     return
     }
     if (error?.localizedDescription == "The network connection was lost."){
     failurClosure("The network connection was lost.");
     return
     }
     }
     let dictResponse = response.result.value as? NSDictionary
     
     // Ben10 print(""Response",response)
     
     
     
     if dictResponse is Dictionary<AnyHashable,Any> {
     // Ben10 print(""Yes, it's a Dictionary")
     }
     else{
     // Ben10 print(""No, it's a not a Dictionary")
     failurClosure("Active internet connection required")
     return
     }
     
     let isSuccess = dictResponse!["success"] as! Int
     
     if isSuccess == 1
     {
     successWithStatus1Closure(dictResponse)
     }else{
     successWithStatus0Closure(dictResponse)
     }
     
     }
     }else {
     self.hideHud()
     
     // Ben10 print(""Active internet connection required");
     failurClosure("Active internet connection required")
     return
     }
     
     }
     
     
     class func API_GetData(_ strURL: String, successWithStatus1Closure: @escaping (AnyObject?) -> (), successWithStatus0Closure: @escaping (AnyObject?) -> (), failurClosure: @escaping (String?)-> ())
     {
     
     let complite_url = BASE_URL+strURL
     // Ben10 print("complite_url)
     
     if Reachability.isConnectedToNetwork(){
     
     var jwt_token = ""
     if GlobalClass.isKeyPresentInUserDefaults(key: "jwt_token") {
     jwt_token = GlobalClass.getUserDefault("jwt_token")
     }
     let header = ["Content-Type" : "application/json",
     "Accept" : "application/json",
     "api_access_key"  : API_ACCESS_KEY,
     "jwt_token" : jwt_token
     ]
     
     Alamofire.request(complite_url, method: .get, parameters: nil ,encoding: JSONEncoding.default, headers: header).responseJSON {
     response in
     if(response.result.error != nil)
     {
     
     
     let error = response.result.error
     if (error?.localizedDescription == "The request timed out."){
     failurClosure("The request timed out.");
     return
     }
     if (error?.localizedDescription == "Could not connect to the server."){
     failurClosure("Could not connect to the server.");
     return
     }
     if (error?.localizedDescription == "The network connection was lost."){
     failurClosure("The network connection was lost.");
     return
     }
     }
     let dictResponse = response.result.value as? NSDictionary
     
     // Ben10 print(""Response",response)
     
     
     
     if dictResponse is Dictionary<AnyHashable,Any> {
     // Ben10 print(""Yes, it's a Dictionary")
     }
     else{
     // Ben10 print(""No, it's a not a Dictionary")
     failurClosure("Active internet connection required")
     return
     }
     let isSuccess = dictResponse!["success"] as! Int
     
     if isSuccess == 1
     {
     successWithStatus1Closure(dictResponse)
     }else{
     successWithStatus0Closure(dictResponse)
     }
     
     }
     }else {
     self.hideHud()
     
     // Ben10 print(""Active internet connection required");
     failurClosure("Active internet connection required")
     return
     }
     }
     
     
     class func multipartData(_ strURL: String, parameter Params: NSDictionary, videoUrl : URL, withInput_name: String, fileName: String, successWithStatus1Closure: @escaping (AnyObject?) -> (), successWithStatus0Closure: @escaping (AnyObject?) -> (), failurClosure: @escaping (String?)-> ())
     {
     
     let Param: [String: Any] = Params.mutableCopy() as! [String : Any]
     
     // Ben10 print(""Params:-", Params);
     
     let complite_url = BASE_URL+strURL
     // Ben10 print("complite_url)
     
     if Reachability.isConnectedToNetwork(){
     
     var jwt_token = ""
     if GlobalClass.isKeyPresentInUserDefaults(key: "jwt_token") {
     jwt_token = GlobalClass.getUserDefault("jwt_token")
     }
     
     let header = ["Content-Type" : "multipart/form-data",
     
     "api_access_key"  : API_ACCESS_KEY,
     "jwt_token" : jwt_token
     ]
     
     Alamofire.upload(multipartFormData: { (multipartFormData) in
     
     //                for (key, value) in Param {
     //                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
     //                }
     // Ben10 print(""videoUrl = \(videoUrl)")
     
     let fileManager = FileManager.default
     let imagePAth = videoUrl
     if fileManager.fileExists(atPath: imagePAth.absoluteString){
     //let imgg = UIImage(contentsOfFile: imagePAth)
     // Ben10 print(""dfd")
     
     }else{
     // Ben10 print(""No Image")
     
     }
     
     
     if videoUrl.absoluteString != "" {
     multipartFormData.append(videoUrl, withName: withInput_name, fileName: "\(fileName).mp4", mimeType: "video/mp4")
     }
     
     
     for (key, value) in Param {
     multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
     }
     
     }, usingThreshold: UInt64.init(), to: complite_url, method: .post, headers: header) { (result) in
     switch result{
     case .success(let upload, _, _):
     upload.responseJSON { response in
     let dictResponse = response.result.value as? NSDictionary
     
     // Ben10 print(""Response",response)
     
     
     
     if dictResponse is Dictionary<AnyHashable,Any> {
     // Ben10 print(""Yes, it's a Dictionary")
     }
     else{
     // Ben10 print(""No, it's a not a Dictionary")
     failurClosure("Active internet connection required")
     return
     }
     let isSuccess = dictResponse!["status"] as! Int
     
     if isSuccess == 200
     {
     successWithStatus1Closure(dictResponse)
     }else{
     successWithStatus0Closure(dictResponse)
     }
     
     }
     case .failure(let error):
     // Ben10 print(""error.localizedDescription = \(error.localizedDescription)")
     if (error.localizedDescription == "The request timed out."){
     failurClosure("The request timed out.");
     return
     }
     else if (error.localizedDescription == "Could not connect to the server."){
     failurClosure("Could not connect to the server.");
     return
     }
     else if (error.localizedDescription == "The network connection was lost."){
     failurClosure("The network connection was lost.");
     return
     }
     else if (error.localizedDescription == "The URL provided is not a file URL.") {
     failurClosure("The URL provided is not a file URL.")
     }
     else {
     failurClosure("Active internet connection required")
     }
     
     }
     }
     
     
     }else {
     
     self.hideHud()
     // Ben10 print(""Active internet connection required");
     failurClosure("Active internet connection required")
     return
     }
     
     }
     */
    //MARK:- Function For Json
    
    class func arrayToJson(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    class func dicToJson(dictionary : NSDictionary) -> String {
        
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        // Ben10 print("jsonString!)
        return jsonString!
        
    }
    
    class func jsonToDic(jsonString : String) -> NSDictionary {
        
        let jsonData2 = jsonString.data(using: .utf8)
        let dictionary2 = try? JSONSerialization.jsonObject(with: jsonData2!, options: .mutableLeaves)
        // Ben10 print("dictionary2!)
        
        return dictionary2! as! NSDictionary
        
    }
    
    class func makeBackLogKey() -> String{
        
        var stops = NSMutableArray()
        var trip_status = NSMutableArray()
        var orders = NSMutableArray()
        
        let data = get_data_from_TableBackLog(is_type: "backLog")
        if data.count < 2 {
            return ""
        }
        for i in data {
            let dic = i as! NSDictionary
            let apiName = dic["api"] as! String
            let parms = dic["parms"] as! String
            let parmDic = GlobalClass.jsonToDic(jsonString: parms)
            switch apiName {
            case "trip_status" :

                let dic1 = ["sign" : "", "status" : parmDic["status"] as! String, "trip_id" : parmDic["trip_id"] as! String] as NSMutableDictionary
                
                let dic2 = [parmDic["stop_id"] as! String : dic1] as NSMutableDictionary
                
                stops.add(dic2)
                
            case "tripComplete" :
                // Ben10 print(""tripComplete")
                
             //   let dic40 = ["status" : "stop", "trip_id" : parmDic["trip_id"] as! String] as NSMutableDictionary
                
                let dic41 = ["status" : "stop", "trip_id" : parmDic["trip_id"] as! String] as NSMutableDictionary
                trip_status.add(dic41)
                
            case "orderComplete" :
                // Ben10 print(""orderComplete")

                if stops.count != 0 {
                    let dic30 = stops[stops.count - 1] as! NSMutableDictionary
                    let dic30Key = dic30.allKeys[0] as! String
                    let dic31 = dic30[dic30Key] as! NSMutableDictionary
                    dic31.setValue(parmDic["image"] as! String, forKey: "sign")
                    dic31.setValue("completed", forKey: "status")
                    // 0="pending" , 1="skip" , 2="inprogress" , 3="success", 4="default"  // this is for trip
                 
                }
                else {
                    let dic1 = ["sign" : parmDic["image"] as! String, "status" : parmDic["status"] as! String, "trip_id" : parmDic["trip_id"] as! String] as NSMutableDictionary
                    
                    let dic2 = [parmDic["stop_id"] as! String : dic1] as NSMutableDictionary
                    
                    stops.add(dic2)
                }
                
                orders.add(parmDic["orders"] as! String)
                
            case "all_trip_listing" :
                 print("d")
            case "startTrip" :
                // Ben10 print(""startTrip")
                let dic41 = ["status" : "start", "trip_id" : parmDic["trip_id"] as! String] as NSMutableDictionary
                trip_status.add(dic41)
            default :
                  print("ddd")
                
            }
        }
        
        if stops.count == 0 && trip_status.count == 0 && orders.count == 0 {
            return ""
        }
        
        if trip_status.count == 0 && CURRENT_TRIP.count != 0 {
            let dic41 = ["status" : "start", "trip_id" : CURRENT_TRIP["trip_id"] as! String] as NSMutableDictionary
            trip_status.add(dic41)
        }
        
        
        
        
        
        let d = ["stops" : stops,
                 "trip_status" : trip_status,
                 "orders" : orders] as NSDictionary
        // Ben10 print("d)
        if d.count == 0 {
            return ""
        }
        return GlobalClass.dicToJson(dictionary: d)
    }
    
    
    class func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    // Return IP address of WiFi interface (en0) as a String, or `nil`
    class func getWiFiAddress() -> String? {
        var address : String?
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return nil }
        guard let firstAddr = ifaddr else { return nil }
        
        // For each interface ...
        for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let interface = ifptr.pointee
            
            // Check for IPv4 or IPv6 interface:
            let addrFamily = interface.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                
                // Check interface name:
                let name = String(cString: interface.ifa_name)
                if  name == "en0" {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                &hostname, socklen_t(hostname.count),
                                nil, socklen_t(0), NI_NUMERICHOST)
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)
        
        return address
    }
    
    
    class func getIPAddress() -> String? {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }
                
                let interface = ptr?.pointee
                let addrFamily = interface?.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    
                    if let name: String = String(cString: (interface?.ifa_name)!), name == "en0" {
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address
    }
    
class func convertListToMutalbe(array : NSMutableArray) -> NSMutableArray {
        let returnArray = NSMutableArray()
        for i in array {
            let tampdic = ((i as! NSDictionary).mutableCopy() as! NSMutableDictionary)
            returnArray.add(tampdic)
        }
        return returnArray
    }

class func makePhoneNo(number : String) -> String {
    var  text = number.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
    text = text.replacingOccurrences(of: "(", with: "", options: NSString.CompareOptions.literal, range: nil)
    text = text.replacingOccurrences(of: ")", with: "", options: NSString.CompareOptions.literal, range: nil)
    text = text.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range: nil)
    
    return text
}
    
}



func createBody(boundary: String,
                dataaa: Data,
                mimeType: String,
                filename: String) -> Data {
    let body = NSMutableData()
    
    let boundaryPrefix = "--\(boundary)\r\n"
    
    body.appendString(boundaryPrefix)
    body.appendString("Content-Disposition: form-data; name=\"files\"; filename=\"\(filename)\"\r\n")
    body.appendString("Content-Type: \(mimeType)\r\n\r\n")
    body.append(dataaa)
    body.appendString("\r\n")
    body.appendString("--".appending(boundary.appending("--")))
    
    return body as Data
}

func createAudioBody(parameters: [String: String],
                     boundary: String,
                     dataaa: Data,
                     mimeType: String,
                     filename: String) -> Data {
    let body = NSMutableData()
    
    let boundaryPrefix = "--\(boundary)\r\n"
    
    for (key, value) in parameters {
        body.appendString(boundaryPrefix)
        body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
        body.appendString("\(value)\r\n")
        
    }
    
    body.appendString(boundaryPrefix)
    body.appendString("Content-Disposition: form-data; name=\"files\"; filename=\"\(filename)\"\r\n")
    body.appendString("Content-Type: \(mimeType)\r\n\r\n")
    body.append(dataaa)
    body.appendString("\r\n")
    body.appendString("--".appending(boundary.appending("--")))
    
    return body as Data
}


extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}


// extension for find Height and Width of string
extension String {
    
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.height
        
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.width
        
    }
}

extension Date {
    var ticks: UInt64 {
        return UInt64((self.timeIntervalSince1970 + 62_135_596_800) * 10_000_000)
    }
}

extension Date {
    init?(jsonDate: String) {
        let prefix = "/Date("
        let suffix = ")/"
        let scanner = Scanner(string: jsonDate)
        
        // Check prefix:
        guard scanner.scanString(prefix, into: nil)  else { return nil }
        
        // Read milliseconds part:
        var milliseconds : Int64 = 0
        guard scanner.scanInt64(&milliseconds) else { return nil }
        // Milliseconds to seconds:
        var timeStamp = TimeInterval(milliseconds)/1000.0
        
        // Read optional timezone part:
        var timeZoneOffset : Int = 0
        if scanner.scanInt(&timeZoneOffset) {
            let hours = timeZoneOffset / 100
            let minutes = timeZoneOffset % 100
            // Adjust timestamp according to timezone:
            timeStamp += TimeInterval(3600 * hours + 60 * minutes)
        }
        
        // Check suffix:
        guard scanner.scanString(suffix, into: nil) else { return nil }
        
        // Success! Create NSDate and return.
        self.init(timeIntervalSince1970: timeStamp)
    }
}

extension String {
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
}



extension UIImage {
    func fixOrientation() -> UIImage {
        if self.imageOrientation == UIImage.Orientation.up {
            return self
        }
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        if let normalizedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext() {
            UIGraphicsEndImageContext()
            return normalizedImage
        } else {
            return self
        }
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector("statusBar")) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}

class ClosureSleeve {
    let closure: () -> ()
    
    init(attachTo: AnyObject, closure: @escaping () -> ()) {
        self.closure = closure
        objc_setAssociatedObject(attachTo, "[\(arc4random())]", self, .OBJC_ASSOCIATION_RETAIN)
    }
    
    @objc func invoke() {
        closure()
    }
}



extension UIControl {
    func addAction(for controlEvents: UIControl.Event = .primaryActionTriggered, action: @escaping () -> ()) {
        let sleeve = ClosureSleeve(attachTo: self, closure: action)
        addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: controlEvents)
    }
}


extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}


public func User(_ key : String) -> String {
    //    if key == "customer_id" {
    //        return "22"
    //    }
    
    let userData = GlobalClass.getUserDefaultDICT()
    guard let data = userData.object(forKey: key) as? String else {
        guard let data2 = userData.object(forKey: key) as? Int else {
            return key
        }
        return "\(data2)"
    }
    return data
}
public func Store(_ key : String) -> String {
    
    //    if key == "quote_id" {
    //        return "104"
    //    }
    
    if (GlobalClass.isKeyPresentInUserDefaults(key: "getDistributorList")) {
        if UserDefaults.standard.object(forKey: "getDistributorList") != nil {
            
            let store = GlobalClass.getUserDefaultAny("getDistributorList") as! NSDictionary
            let r1 = store.object(forKey: key) as? String
            let r2 = store.object(forKey: key) as? Int
            if r1 != nil {
                return  r1 as! String
            }
            else if r2 != nil {
                return  "\(r2)"
            }
        }
    }
    return key
}

public extension UITableView {
    func indexPathForView(_ view: UIView) -> IndexPath? {
        let center = view.center
        let viewCenter = self.convert(center, from: view.superview)
        let indexPath = self.indexPathForRow(at: viewCenter)
        return indexPath
    }
}

extension  String {
    func R() -> String {
        return self.replacingOccurrences(of: "'", with: "''")
    }
}

//public func APrint(_ any : Any) {
//    print(any )
//}

public func APrint(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    let output = items.map { "\($0)" }.joined(separator: separator)
    Swift.print(output, terminator: terminator)
}
