//
//  ViewController.swift
//  AllConnectRoute
//
//  Created by Isuru Nanayakkara on 7/31/15.
//  Copyright (c) 2015 BitInvent. All rights reserved.
//

import UIKit
import MapKit

class MapVC: UIViewController {
    
    var locationUpdateTimer: Timer?
    
    @IBOutlet weak var mapView: MKMapView!
    
    private let locationManager = CLLocationManager()
    private var events = [Event]()
    private var coordinates = [CLLocationCoordinate2D]()
    
    var RecivedData = NSDictionary()
    
    var userLocationMK = MKUserLocation()
    
    var multipleStopsAray = NSArray()
    
    var drawRoot = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = 10
        mapView.showsUserLocation = true
        
        // Ben10 print("RecivedData)
        
        getletLong()
        
        startTimer()
    }
    
    func getletLong() {
        
        multipleStopsAray = (RecivedData["trip_stops"] as! NSMutableArray)
        
        let locationArray = multipleStopsAray
        
        for stope in locationArray {
            
            let trip_stope = stope as! NSDictionary
            
            let trip_stope_status = trip_stope["stop_status"] as! String
            
            // Ben10 print("trip_stope_status)
            
            // trip_stope_status != "1" &&
            
            if  trip_stope_status != "3" {
                
                let lat = Double(trip_stope["latitude"] as! String)
                let long = Double(trip_stope["longitude"] as! String)
                
                // Ben10 print("lat as Any, long as Any)
                
                if lat != nil && long != nil {
                    events.append(Event(latitude: lat!, longitude: long!))
                }
            }
        }
        
        // Ben10 print("events)
        // Ben10 print("events.count)
        
        // add annotations
        
        var annotations = [EventAnnotation]()
        for event in events {
            let eventAnnotation = EventAnnotation(coordinate: event.coordinates)
            //            eventAnnotation.
            annotations.append(eventAnnotation)
        }
        
        mapView.addAnnotations(annotations)
        
        focusMapViewToShowAllAnnotations()
        mapView.removeOverlays(mapView.overlays)
        coordinates = events.map({ $0.coordinates })
        
        // Ben10 print("coordinates)
        
        
        //        makeRoutOnMap()
        
        //        let closestLocation = getClosestLocation(location: userLocationMK.coordinate, locations: coordinates)
        
        //        if let closest = closestLocation {
        //            getDirections(fromLocationCoord: userLocationMK.coordinate, toLocationCoord: closest)
        //        }
        
    }
    
    func makeRoutOnMap () {
        
        var startRoot = CLLocationCoordinate2D()
        startRoot = userLocationMK.coordinate
        
        for i in coordinates {
            getDirections(fromLocationCoord: startRoot, toLocationCoord: i)
            startRoot = i
        }
    }
    
    
    private func getDirections(fromLocationCoord: CLLocationCoordinate2D, toLocationCoord: CLLocationCoordinate2D) {
        
        let fromLocationMapItem = MKMapItem(placemark: MKPlacemark(coordinate: fromLocationCoord, addressDictionary: nil))
        let toLocationMapItem = MKMapItem(placemark: MKPlacemark(coordinate: toLocationCoord, addressDictionary: nil))
        
        let directionsRequest = MKDirections.Request()
        directionsRequest.transportType = .automobile
        directionsRequest.source = fromLocationMapItem
        directionsRequest.destination = toLocationMapItem
        
        let directions = MKDirections(request: directionsRequest)
        
        // Ben10 print("directions)
        
        
        directions.calculate { (directionsResponse, error) -> Void in
            if let error = error {
                // Ben10 print(""Error getting directions: \(error.localizedDescription)")
            } else {
                
                // Ben10 print(""location Found")
                
                if directionsResponse?.routes.count == 0 {
                    return
                }
                
                let route = directionsResponse?.routes[0] as! MKRoute
                self.mapView.addOverlay(route.polyline)
                
                //                let closestLocation = self.getClosestLocation(location: toLocationCoord, locations: self.coordinates)
                //
                //                if let closest = closestLocation {
                //                    self.getDirections(fromLocationCoord: toLocationCoord, toLocationCoord: closest)
                //                }
                
                //                // Ben10 print("self.coordinates)
                
                //                self.coordinates = self.coordinates.filter({ (($0 as CLLocationCoordinate2D).latitude != toLocationCoord.latitude) && (($0 as CLLocationCoordinate2D).longitude != toLocationCoord.longitude)})
                
            }
        }
    }
    
    //    private func getClosestLocation(location: CLLocationCoordinate2D, locations: [CLLocationCoordinate2D]) -> CLLocationCoordinate2D? {
    //
    //        var closestLocation: (distance: Double, coordinates: CLLocationCoordinate2D)?
    //
    //        for loc in locations {
    //
    //            let distance = round(location.distance(from: loc)) as Double
    //
    //            if closestLocation == nil {
    //                closestLocation = (distance, loc)
    //            } else {
    //                if distance < closestLocation!.distance {
    //                    closestLocation = (distance, loc)
    //                }
    //            }
    //        }
    //
    //        // Ben10 print("closestLocation?.coordinates as Any)
    //
    //        return closestLocation?.coordinates
    //    }
    
    private func focusMapViewToShowAllAnnotations() {
        
        //        // Ben10 print(""focusMapViewToShowAllAnnotations")
        
        mapView.showAnnotations(mapView.annotations, animated: true)
        
    }
    
    @IBAction func btnBack(_ sender: UIButton) {
        stopTimerTest()
        navigationController?.popViewController(animated: true)
    }
}

// MARK: - MKMapViewDelegate
extension MapVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        // Don't add a custom image to the user location pin
        
        // Ben10 print(""viewFor annotation")
        
        // some time i am commentinng this code below because this code crase the app so i am comment
        
        if annotation is MKUserLocation {
            return nil
        }
        
        let identifier = "Annotation"  //  EventLocationIdentifier
        var view = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        view?.image = UIImage(named: "googleMarker")
        
        
        if view == nil {
            
            view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            
        } else {
            
            view?.annotation = annotation
            
        }
        
        return view
        
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = COLOR_YELLOW
        renderer.lineWidth = 10
        return renderer
        
    }
    
    func mapView(mapView: MKMapView!, didAddAnnotationViews views: [AnyObject]!) {
        focusMapViewToShowAllAnnotations()
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        
        //        // Ben10 print(""update")
        
        userLocationMK = userLocation
        
        if drawRoot {
            selfUpdate()
        }
        
        
        
        //        focusMapViewToShowAllAnnotations()
        //
        //        mapView.removeOverlays(mapView.overlays)
        //
        //        coordinates = events.map({ $0.coordinates })
        //        let closestLocation = getClosestLocation(location: userLocation.coordinate, locations: coordinates)
        //        if let closest = closestLocation {
        //            getDirections(fromLocationCoord: userLocation.coordinate, toLocationCoord: closest)
        
        //        }
    }
    
    func startTimer() {
        locationUpdateTimer = Timer.scheduledTimer(timeInterval: 15, target: self, selector: #selector(drawRootIs), userInfo: nil, repeats: true)
    }
    
    @objc func drawRootIs(){
        drawRoot = true
    }
    
    @objc func selfUpdate () {
        
        drawRoot = false
        
        //        // Ben10 print(""update one time")
        
        // Ben10 print(""route restart")
        
        if multipleStopsAray.count > 0 {
            
            focusMapViewToShowAllAnnotations()
            mapView.removeOverlays(mapView.overlays)
            coordinates = events.map({ $0.coordinates })
            // Ben10 print("coordinates)
            makeRoutOnMap()
            
            //            let closestLocation = getClosestLocation(location: userLocationMK.coordinate, locations: coordinates)
            //            if let closest = closestLocation {
            //                getDirections(fromLocationCoord: userLocationMK.coordinate, toLocationCoord: closest)
            //            }
            
        } else {
            stopTimerTest()
        }
    }
    
    func stopTimerTest() {
        if locationUpdateTimer != nil {
            locationUpdateTimer!.invalidate()
            locationUpdateTimer = nil
        }
    }
}

extension CLLocationCoordinate2D {
    func distance(from: CLLocationCoordinate2D) -> CLLocationDistance {
        let destination = CLLocation(latitude:from.latitude,longitude:from.longitude)
        return CLLocation(latitude: latitude, longitude: longitude).distance(from: destination)
    }
}
























class Event: NSObject {
    
    var latitude: CLLocationDegrees!
    var longitude: CLLocationDegrees!
    
    var coordinates: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    override init() {
        super.init()
    }
    
    init(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        self.latitude = latitude
        self.longitude = longitude
    }
}


class EventAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
}
