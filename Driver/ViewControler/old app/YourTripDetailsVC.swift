//
//  PickerNotificationDetailVC.swift
//  Restaurant Picker
//
//  Created by Apple on 06/02/19.
//  Copyright © 2019 Arthonsys. All rights reserved.


import UIKit
import NVActivityIndicatorView

class YourTripDetailsVC: UIViewController {
    
    @IBOutlet weak var viewLoderHightConstant: NSLayoutConstraint!
    @IBOutlet weak var viewLoder: UIView!
    @IBOutlet weak var viewLoderAnim: UIView!
    @IBOutlet weak var ViewCellUpper: UIView!
    @IBOutlet weak var sigViewHight: NSLayoutConstraint!
    @IBOutlet weak var signutureView: UIView!

    // image Zoom Full View
    @IBOutlet weak var layerZoomView: UIView!
    @IBOutlet weak var zoomScrollView: UIScrollView!
    @IBOutlet weak var zoomImage: UIImageView!
    
    var oldPostion = CGRect()
    // layer View
    // @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var btnBackOutlet: ZFRippleButton!
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var layerView: UIView!
    
    @IBOutlet weak var lblHederTitle: UILabel!
    // TOP VIEW
    
    // Hard Outlets
    
    @IBOutlet weak var lblAddressHard: UILabel!
    @IBOutlet weak var lblNoOfItemHard: UILabel!
    @IBOutlet weak var lblContactDetaildHard: UILabel!
    @IBOutlet weak var lblContactPesonHard: UILabel!
    @IBOutlet weak var lblResturentName: UILabel!
    @IBOutlet weak var lblNumberOfItem: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblContectDetails: UILabel!
    @IBOutlet weak var lblCOntectNamw: UILabel!
    
    @IBOutlet weak var lblOrderType: UILabel!
    @IBOutlet weak var lblOrderStatus: UILabel!
    
    var IsFrome = ""
    var RecivedData = NSMutableDictionary()
    var itemArray = NSMutableArray()
    @IBOutlet weak var btnSignatureHard: ZFRippleButton!
    var itemHeights = [CGFloat](repeating: UITableView.automaticDimension, count: 1000)
    var toolBar = UIToolbar()
    
    var delegate : changeOrderStatusDelegate?

    var imgSinStopArray = NSMutableArray()
    
    var isOrderApiCall = false

    var signatureImage : UIImage?
    
    var orderTripVc : OrdersTripVC!


    override func viewDidLoad() {
        super.viewDidLoad()
    
        setUpView()
        // Ben10 print("RecivedData)
        colorSetup()
        statusSetUp()
        let itemAaary = (RecivedData["item"] as! NSArray).mutableCopy() as! NSMutableArray
        // Ben10 print("itemAaary)
        itemArray = GlobalClass.convertListToMutalbe(array: itemAaary)
        
        for i in itemArray {
            
            let dic = i as! NSMutableDictionary
             dic.setValue("1", forKey: "itemPending")
            
            if RecivedData["stop_status_back_vc"] as? String ?? "nil" == "2" {
                
//                dic.setValue("", forKey: "ItemStatus")

                if dic["real_quantity"] as! String == "0" {
                    dic.setValue("return", forKey: "ItemStatus")
                } else {
                    dic.setValue("confirm", forKey: "ItemStatus")
                }
                
            } else {
                
                if dic["updated_qty"] as! String == "0" {
                    dic.setValue("return", forKey: "ItemStatus")
                } else {
                    dic.setValue("confirm", forKey: "ItemStatus")
                }
            }
            
            
        }
        
        lblNumberOfItem.text = String(itemArray.count)
        let address = (RecivedData["address"] as? String ?? "").uppercased()
        lblAddress.text = address.replacingOccurrences(of: "<BR>", with: "\n")
        lblCOntectNamw.text = (RecivedData["customer_name"] as? String ?? "").uppercased()
        lblContectDetails.text = (RecivedData["customer_mobile"] as? String ?? "").uppercased()
        lblResturentName.text = ("Order id: #\(RecivedData["order_increment_id"] as? String ?? "")").uppercased()
        
        lblOrderType.text = (RecivedData["order_type"] as? String)?.uppercased()
        lblOrderStatus.text = (RecivedData["order_status"] as? String)?.uppercased()
        
        updateTableviewHeder()
        self.tableView.reloadData()
        
        getImageSignatureOrNot()
        
    }
    
    
    func updateTableviewHeder () {
        
        let address = (lblAddress.text ?? "").uppercased()
        let addressHeight = address.height(withConstrainedWidth: SCREEN_WIDTH - 175, font: lblAddress.font)
        ViewCellUpper.frame.size.height = CGFloat(addressHeight) + CGFloat(210)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        statusSetUp()
        UIApplication.shared.statusBarStyle = .lightContent //or .default
        setNeedsStatusBarAppearanceUpdate()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCellImageAction(_ sender: UIButton) {
        
        if let indexPath = tableView.indexPathForView(sender as UIView) {
            
            layerZoomView.isHidden = false
            let buttonPosition = sender.convert(CGPoint.zero, to: self.tableView)
            let cell = tableView.cellForRow(at: indexPath) as! YourTripDetailsCell
            let newImage = UIImageView()
            newImage.tag = 909090
            self.view.addSubview(newImage)
            let postion = GlobalClass.findViewPosition(view: cell.imgItem, withView: self.view)
            oldPostion = postion
            // Ben10 print(""old Postion : \(postion)")
            newImage.image = cell.imgItem.image
            newImage.contentMode = .scaleAspectFit
            newImage.frame = CGRect(x: postion.maxX - 90, y: postion.maxY - 90, width: postion.width, height: postion.height)
            // Ben10 print("postion)
            UIView.animate(withDuration: 0.5, animations: {
                
                self.zoomScrollView.delegate = self
                self.zoomScrollView.minimumZoomScale = 1.0
                self.zoomScrollView.maximumZoomScale = 9.0
                
                // Ben10 print("newImage.frame)
                // Ben10 print("self.zoomImage.frame)
                
                let height = UIScreen.main.bounds.size.height - self.layerZoomView.frame.size.height
                // Ben10 print("height)
                newImage.frame = CGRect(x: 0, y: 44 + height, width: self.zoomImage.frame.width, height: self.zoomImage.frame.height)
                
                self.view.layoutIfNeeded()
                
            }) { (true) in
                
                newImage.removeFromSuperview()
                self.zoomImage.image = cell.imgItem.image
                
            }
        }
    }
    
    @IBAction func btnCellILessQty(_ sender: UIButton) {
        
        if Int(RecivedData["stop_status"] as! String) != 2 {
            return
        }
        
        if let indexPath = tableView.indexPathForView(sender as UIView) {
            
            let cell = tableView.cellForRow(at: indexPath) as! YourTripDetailsCell
            
            let Dic = itemArray.object(at: indexPath.row) as! NSMutableDictionary
            
            let oldQty = Dic["real_quantity"] as? String ?? "0"//cell.txtItemQty.text //
            
            if Int(oldQty)! > 0 {
                
                let endItem = (Int(oldQty)! - 1)
                
                if endItem == 0 {
                    Dic.setValue("return", forKey: "ItemStatus")
                } else{
                    Dic.setValue("confirm", forKey: "ItemStatus")
                }
                
                // Ben10 print(""new less quantity : \(endItem) ")
                Dic.setValue(String(endItem), forKey: "real_quantity")
                
            }
            
            let indexPath = IndexPath(item: indexPath.row, section: 0)
            tableView.reloadRows(at: [indexPath], with: .automatic)
            
        }
    }
    
    @IBAction func btnCellIncreesItemQty(_ sender: UIButton) {
        
        if Int(RecivedData["stop_status"] as! String) != 2 {
            return
        }
        
        if let indexPath = tableView.indexPathForView(sender as UIView) {
            
            let cell = tableView.cellForRow(at: indexPath) as! YourTripDetailsCell
            
            let Dic = itemArray.object(at: indexPath.row) as! NSMutableDictionary
            
            let oldQty = Dic["real_quantity"] as? String ?? "0" // cell.txtItemQty.text
            
            let realQty = Int(Dic["quantity"] as! String)
            
            // Ben10 print("realQty as Any)
            
            let endItem = (Int(oldQty)! + 1)
            
            if realQty! >= endItem {
                
                // Ben10 print(""new incress quantity : \(endItem) ")
                
                if endItem == 0 {
                    Dic.setValue("return", forKey: "ItemStatus")
                } else{
                    Dic.setValue("confirm", forKey: "ItemStatus")
                }
                
                Dic.setValue(String(endItem), forKey: "real_quantity")
                
            } else{
                
            }
            
            let indexPath = IndexPath(item: indexPath.row, section: 0)
            tableView.reloadRows(at: [indexPath], with: .automatic)
            
        }
    }
    
    @IBAction func btnCellConfirm(_ sender: UIButton) {
        
        // Ben10 print("sender.tag)
        
        if let indexPath = tableView.indexPathForView(sender as UIView) {
            
            let cell = tableView.cellForRow(at: indexPath) as! YourTripDetailsCell
            
            let dict = itemArray.object(at: indexPath.row) as! NSMutableDictionary
            
            let picked = dict.object(forKey: "ItemStatus") as! String
            
            // Ben10 print("picked)
            
            dict.setValue("confirm", forKey: "ItemStatus")
            
            let pickedNew = dict.object(forKey: "ItemStatus") as! String
            
            // Ben10 print("picked)

            if Int(dict.object(forKey: "real_quantity") as! String) == 0 {
                
                let real_quantity = String(dict.object(forKey: "quantity") as? String ?? "0")
                
                dict.setValue(real_quantity, forKey: "real_quantity")
                
            }
            
            tableView.reloadData()
            
        }
    
    }
    
    @IBAction func btnCellReturn(_ sender: UIButton) {
        
        if let indexPath = tableView.indexPathForView(sender as UIView) {
            
            let cell = tableView.cellForRow(at: indexPath) as! YourTripDetailsCell
            
            let dict = itemArray.object(at: indexPath.row) as! NSMutableDictionary
            
            let picked = dict.object(forKey: "ItemStatus") as! String
            
            // Ben10 print("picked)
            
            dict.setValue("return", forKey: "ItemStatus")
            
            dict.setValue("0", forKey: "real_quantity")
            
            tableView.reloadData()

        }
        
        
    }
    
    
    @IBAction func btnCellPicked(_ sender: UIButton) {
       
        if let indexPath = tableView.indexPathForView(sender as UIView) {
            
            let cell = tableView.cellForRow(at: indexPath) as! YourTripDetailsCell
            
            let dict = itemArray.object(at: indexPath.row) as! NSMutableDictionary
            
            let picked = dict.object(forKey: "itemPending") as? String ?? ""
            
            if sender.isSelected == true {
                
                sender.isSelected = false
                
                 dict.setValue("1", forKey: "itemPending")
                
            } else {
                
                sender.isSelected = true
                
                 dict.setValue("0", forKey: "itemPending")

            }
            
            tableView.reloadData()
            
        }
        
    }
    
    
    @IBAction func BtnSignature(_ sender: UIButton) {
        
        let remeningItem = itemArray.filter{($0 as! NSDictionary)["ItemStatus"] as! String == ""}
        
        if remeningItem.count > 0 {
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Please mark all items confirm or return", view: self)
            return
        }
        
        // Ben10 print("remeningItem)
        
        // Ben10 print("RecivedData)

        if isOrderApiCall == true {
            
            if signatureImage != nil {
                submitOrder()
            } else{
                goNextVC()
            }

           
            
        } else if isOrderApiCall == false {
            
             goNextVC()
           
          }
        else{
            
            goNextVC()
           
        }
        
    }
    
    
    func submitOrder() {
        
        let stopID = RecivedData["stop_id"] as Any
        
        let trip_id = RecivedData["trip_id"] as Any
        
        let order_id = RecivedData["order_id"] as Any
        
        let customer_name = RecivedData["customer_name"] as Any
        
        let Parm : NSMutableDictionary = ["update_Order" : itemArray,
                                          "stop_id" : stopID,
                                          "trip_id" : trip_id,
                                          "order_id" : order_id,
                                          "customer_name" : customer_name]
        
        submitOrder(parm: Parm)
        
    }
    
    func goNextVC () {
        
        let stopID = RecivedData["stop_id"] as Any
        
        let trip_id = RecivedData["trip_id"] as Any
        
        let order_id = RecivedData["order_id"] as Any
        
        let customer_name = RecivedData["customer_name"] as Any
        
        // Ben10 print("trip_id, trip_id)
        
        let totalData : NSMutableDictionary = ["update_Order" : itemArray,
                                               "stop_id" : stopID,
                                               "trip_id" : trip_id,
                                               "order_id" : order_id,
                                               "customer_name" : customer_name]
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignatureVC_old") as! SignatureVC_old
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .coverVertical
        vc.revivedData = totalData
        vc.UpadateValueDic = RecivedData
        vc.yourTripDetailsobj = self
        vc.imgSinStopArray = self.imgSinStopArray
        self.present(vc, animated: true, completion: nil)
        
    }
    
    // chack the image
    func getImageSignatureOrNot () {
        if imgSinStopArray.count > 0 {
            let id = self.RecivedData["stop_trip_id"] as! String
            let getImageArray = imgSinStopArray.filter{($0 as! NSDictionary)["stop_trip_id"] as! String == String(id)}
            if getImageArray.count > 0 {
                // Ben10 print("getImageArray)
                let getimage = (getImageArray[0] as! NSDictionary)["signture_image"]
                self.signatureImage = getimage as? UIImage
                self.isOrderApiCall = true
                self.btnSignatureHard.setTitle("SUBMIT", for: .normal)
            }
            else {
                self.isOrderApiCall = false
                self.btnSignatureHard.setTitle("SIGNATURE", for: .normal)
            }
        } else {
            self.isOrderApiCall = false
            self.btnSignatureHard.setTitle("SIGNATURE", for: .normal)
        }
    }
    
    // When image arreday signature then call api oredre direct
    func submitOrder (parm : NSMutableDictionary) {
        
        let ItemDicArray = NSMutableArray()
        
        let itemArray = parm["update_Order"] as! NSMutableArray
        
        for i in itemArray {
            
            let dic = i as! NSMutableDictionary
            var FullUrl = String()
            if dic["item_image"] as! String != "" {
                let url = dic["item_image"] as? String ?? ""
                FullUrl = url //ImageBaseUrl + url
            } else{
                FullUrl = ""
            }
            
            let dicUpdate = ["name":dic["name"] as Any,
                             "barcode":dic["barcode"] as Any,
                             "quantity":dic["real_quantity"] as Any,
                             "price":dic["price"] as Any,
                             "item_id":dic["item_id"] as Any,
                             "unit_price":dic["unit_price"] as Any,
                             "order_item_id" : dic["order_item_id"] as Any,
                             "product_id" : dic["product_id"] as Any,
                             "image":FullUrl]
            
            ItemDicArray.add(dicUpdate)
            
        }
        
        let jsonString = json(from: ItemDicArray)
        
        // Ben10 print("parm)
        
        
//        if signatureImage != nil {
//            return
//        }
        
        let myDictOfDict:NSMutableDictionary = [
            "items_array" : jsonString as Any,
            "stop_id" : parm["stop_id"] as Any,
            "trip_id" : parm["trip_id"] as Any,
            "driver_id" : User("user_id"),
            "order_id" : parm["order_id"] as Any,
            "image" : GlobalClass.imageToStrinig(signatureImage ?? UIImage())
        ]
        
        apiSubmitOrder(parm: myDictOfDict)
        
    }
    
    
    func apiSubmitOrder(parm : NSDictionary) {
        
        btnSignatureHard.isEnabled = false
        btnBackOutlet.isEnabled = false

//        DesableButtonAll()
        
        //        GlobalClass.showHUD()
        //        self.viewLoderHightConstant.constant = 35
        
        GlobalClass.multipartData("order_confirm", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
            
            self.btnSignatureHard.isEnabled = true
            self.btnBackOutlet.isEnabled = true
            
            //            UIImageWriteToSavedPhotosAlbum(self.signatureImage, nil, nil, nil)
            
            let massage = (response as! NSDictionary).object(forKey: "responsemsg") as! String
            
            GlobalClass.showAlertOKAction(APP_NAME, massage, "OK", self, successClosure: { (str) in
                
                let userDefoult = UserDefaults.standard
                userDefoult.set("relodeVC", forKey: "isVcFrome")
                
//                self.UpadateValueDic.setValue("confirm", forKey: "order_status") //order_status
//                self.yourTripDetailsobj.statusSetUp()
//                self.dismiss(animated: true, completion: nil)
                
//                self.statusSetUp()
                
                let backvcIndexCell = self.RecivedData["indexpath"] as! Int
                self.orderTripVc.setOrderStatus(status: "confirm", index: backvcIndexCell)
//                orderTripVc.delegate?.setOrderStatus(status: "confirm", index: backvcIndexCell)
//                orderTripVc.navigationController?.popViewController(animated: true)
//
//                let stop_trip_id = self.RecivedData["stop_trip_id"]
////
//                let Dic = ["stop_trip_id" : stop_trip_id,
//                           "signture_image" : (self.signatureImage)]
//                self.imgSinStopArray.add(Dic)
//
//                // Ben10 print(""imgSinStopArray : \(self.imgSinStopArray) ")
                
                self.navigationController?.popViewController(animated: true)
                
                
                
            })
            
//            self.AnableButtonAll()
            
            //            GlobalClass.hideHud()
            
        }, successWithStatus0Closure: { (responce) in
            
            self.btnSignatureHard.isEnabled = true
            self.btnBackOutlet.isEnabled = true
            
            let massage = (responce as! NSDictionary).object(forKey: "responsemsg") as! String
            
            let responsemsg = (responce as! NSDictionary)["responsemsg"] as? String ?? ""
            let responsecode = (responce as! NSDictionary)["responsecode"] as? String ?? ""
            
            if responsecode as? String ?? "" == "-2" {
                
                GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
                    
                    GlobalClass.setUserDefault(value: "", for: "landingPage")
                    GlobalClass.removeUserDefaults("getDistributorList")
                    
                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                    return
                    
                })
            }
            
            GlobalClass.showAlertOKAction(APP_NAME, massage, "OK", self, successClosure: { (str) in
                
                //                self.dismiss(animated: true, completion: nil)
                
                self.dismiss(animated: true, completion: nil)
                
//                self.yourTripDetailsobj.navigationController?.popViewController(animated: true)
                
            })
            
            //            GlobalClass.hideHud()
//            self.AnableButtonAll()
            
        }) { (error) in
            
//            self.AnableButtonAll()
            
            self.btnSignatureHard.isEnabled = true
            self.btnBackOutlet.isEnabled = true
            
            if error == "Not Show" {
                return
            }
            
            GlobalClass.showAlertOKAction(APP_NAME, error!, "OK", self, successClosure: { (str) in
            })
            GlobalClass.hideHud()
            
        }
    }
    
    // layer View
    
    @IBAction func btnCheckReceiptAction(_ sender: UIButton) {
        
        //        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ChackOutVC") as? ChackOutVC
        //
        //        vc!.IsFrome = "ChackRecept"
        //
        //        self.navigationController!.pushViewController(vc!, animated: true)
        
    }
    
    @IBAction func btnEditOrder(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnBackZoomOut(_ sender: UIButton) {
        
        self.zoomScrollView.setZoomScale(0.0, animated: true)
        
        self.layerZoomView.isHidden = true
        
        let newImage = UIImageView()
        
        newImage.image = self.zoomImage.image
        
        newImage.contentMode = .scaleAspectFit
        
        self.zoomImage.image = nil
        
        newImage.tag = 909090
        
        newImage.frame = CGRect(x: 0, y: 60, width: self.zoomImage.frame.width, height: self.zoomImage.frame.height)
        
        self.view.addSubview(newImage)
        
        UIView.animate(withDuration: 0.5, animations: {
            
            newImage.frame = CGRect(x: 0, y: self.oldPostion.maxY - 90, width: 90, height: 90)
            self.view.layoutIfNeeded()
        }) { (true) in
            
            newImage.removeFromSuperview()
            
        }
    }
}

class YourTripDetailsCell : UITableViewCell {
    
    @IBOutlet weak var imageItemBtn: UIButton!
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblItemType: UILabel!
    @IBOutlet weak var lblItemPack: UILabel!
    @IBOutlet weak var lblItemPrice: UILabel!
    
    @IBOutlet weak var txtItemQty: UITextField!
    
    @IBOutlet weak var btnConfirm: UIButton!
    
    @IBOutlet weak var btnRreturn: UIButton!
    
    @IBOutlet weak var btnPending: UIButton!

    @IBOutlet weak var viewCaseUnit: UIView!
    
    @IBOutlet weak var lblCaseAndUnit: UILabel!
    
    @IBOutlet weak var lblCaseUnitQty: UILabel!
    
    @IBOutlet weak var lblOrderQty: UILabel!
    
    @IBOutlet weak var btnIncress: ZFRippleButton!
    
    @IBOutlet weak var btnLess: ZFRippleButton!
    
    @IBOutlet weak var viewStatus: UIView!
    
    @IBOutlet weak var viewReturn: UIView!

    @IBOutlet weak var viewStatusHight: NSLayoutConstraint!
    
    @IBOutlet weak var viewReturnHight: NSLayoutConstraint!
    
    
    
}

extension YourTripDetailsVC : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! YourTripDetailsCell
        
        // set colour
        
        cell.lblItemPack.textColor = COLOR_ACCENT_TEXT_COLOR
        
        cell.lblItemName.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblItemType.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblItemPrice.textColor = COLOR_PRIMARY_TEXT_COLOR
        //      cell.lblItemOrderQtyCase.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.txtItemQty.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblItemPack.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblCaseUnitQty.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblCaseAndUnit.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblOrderQty.textColor = COLOR_PRIMARY_TEXT_COLOR

        
        cell.btnConfirm.backgroundColor = COLOR_WHITE
        cell.btnRreturn.backgroundColor = COLOR_WHITE
        
        cell.btnConfirm.borderColor = COLOR_PRIMARY_TEXT_COLOR
        cell.btnRreturn.borderColor = COLOR_PRIMARY_TEXT_COLOR
        
        cell.btnConfirm.borderWidth = 1.5
        cell.btnRreturn.borderWidth = 1.5
        
        cell.btnConfirm.setTitleColor(COLOR_PRIMARY_TEXT_COLOR, for: .normal)
        cell.btnRreturn.setTitleColor(COLOR_PRIMARY_TEXT_COLOR, for: .normal)
        
        cell.viewCaseUnit.backgroundColor = COLOR_ACCENT_LINE
        
        let index = itemArray[indexPath.row] as! NSDictionary
        
        //        cell.imgItem?.image = UIImage(named: index["image"] as! String)
        
        cell.lblItemName.text = (index["item_name"] as! String).uppercased()
        
        //        cell.lblItemType.text = index["itemCategery"] as? String
        //        cell.lblItemOrderQtyCase.text = index["orderQty"] as? String
        //        cell.lblItemPack.text = index["ItemPackQty"] as? String
//        cell.lblItemPrice.text = index["price"] as? String
        
        cell.lblCaseUnitQty.text = index["quantity"] as? String
        
        // Ben10 print(""updated_qty \(index["updated_qty"] as? String ?? "")")
        
        let imageUrl = index["item_image"] as? String


//        var FullUrl = String()
        if let validUrl = imageUrl {
//            FullUrl = ImageBaseUrl + validUrl
            cell.imgItem.sd_setImage(with: URL(string: validUrl), placeholderImage: UIImage.init(named: "Loading"), options: .transformAnimatedImage) { (img, error, type , url) in
                if error == nil {
                }
            }
        } else{
            
        }

        let select = index.object(forKey: "ItemStatus") as! String
        
        // Ben10 print("select)
        
        if select == "confirm" {
            
                        cell.btnRreturn.borderWidth = 1.5
                        cell.btnRreturn.backgroundColor = COLOR_WHITE
                        cell.btnRreturn.setTitleColor(COLOR_PRIMARY_TEXT_COLOR, for: .normal)
            
                        cell.btnConfirm.borderWidth = 1.5
                        cell.btnConfirm.backgroundColor = COLOR_PRIMARY_TEXT_COLOR
                        cell.btnConfirm.setTitleColor(COLOR_WHITE, for: .normal)
            
        } else if select == "return" {
            
                        cell.btnRreturn.borderWidth = 1.5
                        cell.btnRreturn.backgroundColor = COLOR_PRIMARY_TEXT_COLOR
                        cell.btnRreturn.setTitleColor(COLOR_WHITE, for: .normal)
            
                        cell.btnConfirm.borderWidth = 1.5
                        cell.btnConfirm.backgroundColor = COLOR_WHITE
                        cell.btnConfirm.setTitleColor(COLOR_PRIMARY_TEXT_COLOR, for: .normal)
            
            
        } else {
            
            cell.btnRreturn.borderWidth = 1.5
            cell.btnRreturn.backgroundColor = COLOR_WHITE
            cell.btnRreturn.setTitleColor(COLOR_PRIMARY_TEXT_COLOR, for: .normal)
            
            cell.btnConfirm.borderWidth = 1.5
            cell.btnConfirm.backgroundColor = COLOR_WHITE
            cell.btnConfirm.setTitleColor(COLOR_PRIMARY_TEXT_COLOR, for: .normal)
            
        }

        //  >>>>>>>>>>>>> Data Set acording status <<<<<<<<<<<<<<
        if RecivedData["stop_status_back_vc"] as? String ?? "nil" == "2" {
            
            if (RecivedData["order_status"] as? String ?? "").lowercased() == "pending" {
            
//               if (RecivedData["stop_status"] as! String) == "2" {
            
                if index["quantity"] as? String ?? "" == "0" {
                    cell.viewStatusHight.constant = 0
                    cell.viewStatus.isHidden = true
                } else{
                    cell.viewStatusHight.constant = 55
                    cell.viewStatus.isHidden = false
                }
                
                cell.viewReturnHight.constant = 0
                cell.viewReturn.isHidden = true
                
                // Ben10 print("index["real_quantity"] as? String as Any)
                cell.txtItemQty.text = index["real_quantity"] as? String
                
                let itemQty = index["real_quantity"] as? String ?? "0.00"
                let RealPrice = (index["price"] as? String)
                let removeComa = RealPrice!.replacingOccurrences(of: ",", with: "")
                
                let myFloat = (removeComa as NSString).floatValue
                let TotalValue = (myFloat) * Float(itemQty)!
                let twoDecimalPlaces = String(format: "%.2f", TotalValue)
                // Ben10 print("twoDecimalPlaces)
                cell.lblItemPrice.text = "$" + String(twoDecimalPlaces)
                
            }
                
            else if (RecivedData["order_status"] as? String ?? "").lowercased() == "return" {
                
                cell.viewStatusHight.constant = 0
                cell.viewStatus.isHidden = true
                
                cell.viewReturnHight.constant = 54
                cell.viewReturn.isHidden = false
//
                // BTN ANABALE
                
                // Ben10 print("index["real_quantity"] as? String as Any)
                cell.txtItemQty.text = index["real_quantity"] as? String
                
                let itemQty = index["real_quantity"] as? String ?? "0.00"
                let RealPrice = (index["price"] as? String)
                let removeComa = RealPrice!.replacingOccurrences(of: ",", with: "")
                
                let myFloat = (removeComa as NSString).floatValue
                let TotalValue = (myFloat) * Float(itemQty)!
                let twoDecimalPlaces = String(format: "%.2f", TotalValue)
                // Ben10 print("twoDecimalPlaces)
                cell.lblItemPrice.text = "$" + String(twoDecimalPlaces)
                
            } else {
                
                cell.viewStatusHight.constant = 0
                cell.viewStatus.isHidden = true
                
                cell.viewReturnHight.constant = 0
                cell.viewReturn.isHidden = true
                
                // BTN DISABLE
                
                cell.txtItemQty.text = index["updated_qty"] as? String
                
                let itemQty = index["updated_qty"] as? String ?? "0.00"
                let RealPrice = (index["price"] as? String)
                let removeComa = RealPrice!.replacingOccurrences(of: ",", with: "")
                
                let myFloat = (removeComa as NSString).floatValue
                let TotalValue = (myFloat) * Float(itemQty)!
                let twoDecimalPlaces = String(format: "%.2f", TotalValue)
                // Ben10 print("twoDecimalPlaces)
                cell.lblItemPrice.text = "$" + String(twoDecimalPlaces)
                
            }
            
        } else {
            
            cell.viewStatusHight.constant = 0
            cell.viewStatus.isHidden = true
            
            cell.viewReturnHight.constant = 0
            cell.viewReturn.isHidden = true
            
            // BTN DISABLE
            
            cell.txtItemQty.text = index["updated_qty"] as? String
            
            let itemQty = index["updated_qty"] as? String ?? "0.00"
            let RealPrice = (index["price"] as? String)
            let removeComa = RealPrice!.replacingOccurrences(of: ",", with: "")
            
            let myFloat = (removeComa as NSString).floatValue
            let TotalValue = (myFloat) * Float(itemQty)!
            let twoDecimalPlaces = String(format: "%.2f", TotalValue)
            // Ben10 print("twoDecimalPlaces)
            cell.lblItemPrice.text = "$" + String(twoDecimalPlaces)
            
        }
        
        let cases = index.object(forKey: "is_case") as! String
        
        if cases == "0" {
            
            if cell.txtItemQty.text == "1" || cell.txtItemQty.text == "0" {
                cell.lblCaseAndUnit.text = "UNIT"
            } else{
                cell.lblCaseAndUnit.text = "UNITS"
            }
            
        } else {
            
            if cell.txtItemQty.text == "1" || cell.txtItemQty.text == "0" {
                cell.lblCaseAndUnit.text = "CASE"
            } else{
                cell.lblCaseAndUnit.text = "CASES"
            }
        }
        
        // Panding View
        
        let itemPending = index["itemPending"] as? String
        
        if itemPending == "1" {
            
            cell.btnPending.setTitle("PICK", for: UIControl.State.normal)
            cell.btnPending.borderWidth = 1.5
            cell.btnPending.backgroundColor = COLOR_WHITE
            cell.btnPending.setTitleColor(COLOR_PRIMARY_TEXT_COLOR, for: .normal)
            
        }  else if itemPending == "0" {
            
            cell.btnPending.setTitle("PICKED", for: UIControl.State.normal)
            cell.btnPending.borderWidth = 1.5
            cell.btnPending.backgroundColor = COLOR_PRIMARY_TEXT_COLOR
            cell.btnPending.setTitleColor(COLOR_WHITE, for: .normal)
            
            
        } else {
            
            cell.btnPending.borderWidth = 1.5
            cell.btnPending.backgroundColor = COLOR_WHITE
            cell.btnPending.setTitleColor(COLOR_PRIMARY_TEXT_COLOR, for: .normal)
            
        }
        
        cell.txtItemQty.addTarget(self, action: #selector(txtfieldOrderQuantity(_:)), for: .allEvents)
        
        self.addDoneButtonOnKeyboard(cell, indexPath)
        
        cell.txtItemQty.delegate = self as? UITextFieldDelegate
        
        cell.txtItemQty.adjustsFontSizeToFitWidth = true

        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return  100
//    }
    
    func addDoneButtonOnKeyboard(_ cell: YourTripDetailsCell, _ indexPath: IndexPath){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))

        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(DoneButtonkeyBordInCell(_:)))
       

        done.tag = indexPath.row
        
        doneToolbar.barStyle = UIBarStyle.blackOpaque
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        done.tintColor = COLOR_WHITE
        
        cell.txtItemQty.inputAccessoryView = doneToolbar
        
    }
    

    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        return UITableView.automaticDimension
//
//    }
    
    @objc func DoneButtonkeyBordInCell(_ item : UIBarButtonItem) {
        
        // Ben10 print("item.tag)
        
        self.view.endEditing(true)

       let Dic = itemArray.object(at: item.tag) as! NSMutableDictionary
        
       let cell = tableView.cellForRow(at: IndexPath(row: item.tag, section: 0))  as! YourTripDetailsCell
        
        let setNewValue = cell.txtItemQty.text
        
        if setNewValue == "" {
            
            let oldQty = Dic.object(forKey: "real_quantity") as! String
            Dic.setValue(oldQty, forKey: "real_quantity")
            tableView.reloadData()
            return
        }
        
        if setNewValue == "0" || setNewValue == "00" || setNewValue == "000"{
            Dic.setValue("return", forKey: "ItemStatus")
            cell.txtItemQty.text = "0"
        } else {
            Dic.setValue("confirm", forKey: "ItemStatus")
        }
    
        // Ben10 print(""new less quantity : \(setNewValue ?? "") ")
        
        Dic.setValue(setNewValue, forKey: "real_quantity")
        
        tableView.reloadData()
        
    }
    
    
    
@objc func txtfieldOrderQuantity(_ textField: UITextField) {
    
        if Int(RecivedData["stop_status"] as! String) != 2 {
            self.view.endEditing(true)
            textField.tintColor = UIColor.white
            return
        }
    
        textField.tintColor = UIColor.black

        if let indexPath = tableView.indexPathForView(textField as UIView) {
    
            if let cell = tableView.cellForRow(at: indexPath) as? YourTripDetailsCell {
                
                let txtQty : Int = Int(cell.txtItemQty.text ?? "0") ?? 0
                
                if txtQty == 0 || txtQty == 00 || txtQty == 000 {
                    
                    return
                }
                
                let oldQty : Int =  Int(cell.lblCaseUnitQty.text ?? "0") ?? 0//Int(Dic["real_quantity"] as? String ?? "0")!
                
                if oldQty >= txtQty  {
                    
                    // Ben10 print(""value is less")
                    
                } else {
                    
                    var str = textField.text ?? "1"
                    str = String(str.dropLast())
                    textField.text = str
                    // Ben10 print(""value is greter")
                    
                }
            }
     }
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if itemHeights[indexPath.row] == UITableView.automaticDimension {
            itemHeights[indexPath.row] = cell.bounds.height
        }
        // fetch more cells, etc.
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return itemHeights[indexPath.row]
    }
    
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        return true
//    }
//
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//
////            deleteIitemsApi(index: indexPath.row)
////            itemArray.removeObject(at: indexPath.row)
////            tableView.reloadData()
//
//        }
//    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return zoomImage
    }
}

extension YourTripDetailsVC {
    
    func deleteIitemsApi(index : Int) {
        
        return
        
        guard let stop_id = (itemArray[index] as! NSMutableDictionary)["stop_id"] as? String else { return }
        guard let item_id = (itemArray[index] as! NSMutableDictionary)["item_id"] as? String else { return }
        guard let order_id = (itemArray[index] as! NSMutableDictionary)["order_id"] as? String else { return }
        
        
//        GlobalClass.showHUD()
        
        self.viewLoderHightConstant.constant = 0

        
        let parm = ["order_id": order_id,
                    "item_id": item_id,
                    "stop_id" : order_id] as [String : Any]
        
        GlobalClass.multipartData("delete_items", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (responce) in
            
            let itemAr = self.itemArray
            
            // Ben10 print(""Before Delete Item Count : \(self.itemArray.count)")
            
            itemAr.removeObject(at: index)
            
            
            // Ben10 print(""After Delete Item Count : \(self.itemArray.count)")
            
            // Ben10 print(""responce = \(responce as! NSDictionary)")
            
            let msg = (responce as! NSDictionary).object(forKey: "message") as! String
            
            GlobalClass.showAlertOKAction(APP_NAME, msg, "OK", self, successClosure: { (str) in
                
                self.tableView.reloadData()
                
            })
            
//            GlobalClass.hideHud()
            
            self.viewLoderHightConstant.constant = 0
            
        }, successWithStatus0Closure: { (responce) in
            
            // Ben10 print(""responce = \(responce as! NSDictionary)")
            let msg = (responce as! NSDictionary).object(forKey: "message") as! String
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: msg, view: self)
//            GlobalClass.hideHud()
            self.viewLoderHightConstant.constant = 0

            
        }) { (error) in
            
            self.viewLoderHightConstant.constant = 0

            
            if error == "Not Show" {
                return
            }
            
            GlobalClass.showAlertOKAction(APP_NAME, error!, "OK", self, successClosure: { (str) in
            })
            
//            GlobalClass.hideHud()

        }
    }
    
//    func returnOrderApi(index : Int) {
//        
//        return
//        
//        guard let stop_id = (itemArray[index] as! NSMutableDictionary)["stop_id"] as? String else { return }
//        //        guard let item_id = (itemArray[index] as! NSMutableDictionary)["item_id"] as? String else { return }
//        guard let order_id = (itemArray[index] as! NSMutableDictionary)["order_id"] as? String else { return }
//        
////        GlobalClass.showHUD()
//        
//        self.viewLoderHightConstant.constant = 135
//
//        
//        let parm = ["stop_id": stop_id,
//                    "order_id": order_id]
//        
//        GlobalClass.multipartData("return_order", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (responce) in
//            
////            GlobalClass.hideHud()
//            self.viewLoderHightConstant.constant = 0
//            
//        }, successWithStatus0Closure: { (responce) in
//            
//            // Ben10 print(""responce = \(responce as! NSDictionary)")
//            let msg = (responce as! NSDictionary).object(forKey: "message") as! String
//            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: msg, view: self)
////            GlobalClass.hideHud()
//            self.viewLoderHightConstant.constant = 0
//
//            
//        }) { (error) in
//            
//            self.viewLoderHightConstant.constant = 0
//
//            if error == "Not Show" {
//                return
//            }
//            
//            GlobalClass.showAlertOKAction(APP_NAME, error!, "OK", self, successClosure: { (str) in
//            })
////            GlobalClass.hideHud()
//
//        }
//    }
    
//    func confirmOrderApi(index : NSDictionary) {
//        
//        let itemArray = NSArray() //itemArr
//        
//        // Ben10 print("itemArray)
//        
//        let ItemDicArray = NSMutableArray()
//        
//        if itemArray.count != 0 {
//            
//            for i in itemArray {
//                
//                var qtyOrder = String()
//                
//                let dic = i as! NSDictionary
//                
//                //                if dic["status"] as! String == "outofstock"{
//                //
//                //                    qtyOrder = "0"
//                //
//                //                } else{
//                //                    qtyOrder = String(dic["updated_qty"] as! Int)
//                //                }
//                
//                let ItemDic = ["name": "",
//                               "barcode": "",
//                               "quantity":"",
//                               "price" : "",
//                               "item_id" : "",
//                               "unit_price" :"",
//                               "image" : ""]
//                ItemDicArray.add(ItemDic)
//            }
//            
//            // Ben10 print("ItemDicArray)
//            //
//        } else {
//            return
//        }
//        
//        var parm = NSMutableDictionary()
//        
//        let jsonString = json(from: ItemDicArray)
//        
//        let myDictOfDict:NSDictionary = [
//            "key" : jsonString as Any
//        ]
//        
//        parm.addEntries(from: myDictOfDict as! [AnyHashable : Any])
//        
//        parm = ["stop_id": "",
//                "order_id": ""]
//        
////        GlobalClass.showHUD()
//        
//        self.viewLoderHightConstant.constant = 35
//
//        
//        GlobalClass.multipartData("order_confirm", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (responce) in
//            
//            
////            GlobalClass.hideHud()
//
//            self.viewLoderHightConstant.constant = 0
//
//        }, successWithStatus0Closure: { (responce) in
//            
//            // Ben10 print(""responce = \(responce as! NSDictionary)")
//            let msg = (responce as! NSDictionary).object(forKey: "message") as! String
//            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: msg, view: self)
////            GlobalClass.hideHud()
//            self.viewLoderHightConstant.constant = 0
//
//            
//        }) { (error) in
//            
//            self.viewLoderHightConstant.constant = 0
//
//            if error == "Not Show" {
//                return
//            }
//            
//            GlobalClass.showAlertOKAction(APP_NAME, error!, "OK", self, successClosure: { (str) in
//                
//            })
////            GlobalClass.hideHud()
//
//        }
//    }
    
//    func barcodeReturnItemApi(index : Int) {
//
////        GlobalClass.showHUD()
//        self.viewLoderHightConstant.constant = 35
//
//        let parm = ["stop_id": "",
//                    "order_id": ""]
//
//        GlobalClass.multipartData("barcode_return_item", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (responce) in
//
////            GlobalClass.hideHud()
//            self.viewLoderHightConstant.constant = 0
//
//
//        }, successWithStatus0Closure: { (responce) in
//
//            // Ben10 print(""responce = \(responce as! NSDictionary)")
//            let msg = (responce as! NSDictionary).object(forKey: "message") as! String
//            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: msg, view: self)
////            GlobalClass.hideHud()
//            self.viewLoderHightConstant.constant = 0
//
//
//        }) { (error) in
//
//            self.viewLoderHightConstant.constant = 0
//
//
//            if error == "Not Show" {
//                return
//            }
//
//            GlobalClass.showAlertOKAction(APP_NAME, error!, "OK", self, successClosure: { (str) in
//            })
////            GlobalClass.hideHud()
//        }
//    }
    
    func json(from object:Any) -> String? {
        
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    func colorSetup () {
        
        lblHederTitle.textColor = COLOR_PRIMARY_TEXT_COLOR

        
        lblResturentName.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        lblAddressHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        lblNoOfItemHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        lblContactDetaildHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        lblContactPesonHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        lblNumberOfItem.textColor = COLOR_PRIMARY_TEXT_COLOR
        lblAddress.textColor = COLOR_PRIMARY_TEXT_COLOR
        lblCOntectNamw.textColor = COLOR_PRIMARY_TEXT_COLOR
        lblContectDetails.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        btnSignatureHard.backgroundColor = COLOR_PRIMARY
        
        btnSignatureHard.setTitleColor(COLOR_WHITE, for: .normal)
        
        layerZoomView.isHidden = true
    }
    
    func statusSetUp () {
        
//        // Ben10 print("RecivedData)

        // Ben10 print("RecivedData["order_status"] as! String)
        
        // Ben10 print("RecivedData["stop_status_back_vc"] as! String)
        
        if RecivedData["stop_status_back_vc"] as! String != "2" {
            sigViewHight.constant = 0
            signutureView.isHidden = true
        } else {
            
            if (RecivedData["order_status"] as! String).lowercased() == "pending" { //stop_status // pending
            
//                if (RecivedData["stop_status"] as! String) == "2" {
                
                sigViewHight.constant = 60
                signutureView.isHidden = false
                
            } else {
                
                sigViewHight.constant = 0
                signutureView.isHidden = true
                
            }
        }
        
        tableView.reloadData()
        
    }
    
    func setUpView(){
        
        let activityIndicator = NVActivityIndicatorView(frame: self.viewLoderAnim.bounds)
        activityIndicator.type = .ballPulseSync
        activityIndicator.color = UIColor.black
        
        self.viewLoderAnim.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
}


extension String {
    var onetoNineCrcerter : String {
        let okayChars = Set("1234567890")
        return self.filter {okayChars.contains($0) }
    }
    
    var AtoZCrcterAllow : String {
        let okayChars = Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ")
        return self.filter {okayChars.contains($0) }
    }
}

private var __maxLengths = [UITextField: Int]()
extension UITextField {
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return 150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    @objc func fix(textField: UITextField) {
        let t = textField.text
        textField.text = t?.safelyLimitedTo(length: maxLength)
    }
}

extension String
{
    func safelyLimitedTo(length n: Int)->String {
        if (self.count <= n) {
            return self
        }
        return String( Array(self).prefix(upTo: n) )
    }
}
