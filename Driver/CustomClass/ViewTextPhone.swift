//
//  ViewTextFieldInput.swift
//  Restaurant Depot
//
//  Created by Bijender Singh on 05/02/19.
//  Copyright © 2019 Bijender Singh. All rights reserved.
//

import UIKit

class ViewTextPhone: UIView, UITextFieldDelegate {
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var btnEdit: UIButton!
    
    @IBOutlet weak var text_Field: UITextField!
    @IBOutlet weak var viewLine: UIView!
    
    @IBOutlet weak var viewLineHight: NSLayoutConstraint!
    
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl5: UILabel!
    @IBOutlet weak var lbl6: UILabel!
    @IBOutlet weak var lbl7: UILabel!
    @IBOutlet weak var lbl8: UILabel!
    @IBOutlet weak var lbl9: UILabel!
    @IBOutlet weak var lbl10: UILabel!
    
    @IBOutlet weak var lblFirstBreakt: UILabel!
    @IBOutlet weak var lblLastBreakt: UILabel!
    
    @IBOutlet weak var lblFirstDesh: UILabel!
    @IBOutlet weak var lblLastDesh: UILabel!
    

    @IBOutlet weak var viewAllLbl: UIView!
    var finalString = ""
    
    @IBOutlet weak var viewBlinkMain: UIView!
    @IBOutlet weak var viewBlink: UIView!
    @IBOutlet weak var constLeadingViewBlink: NSLayoutConstraint!
    
    
    var inputText = ""
    var arrayLbl = [UILabel]()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        setupView()
    }
    @IBOutlet weak var consEditBtn: NSLayoutConstraint!
    
    required init?(coder aDecoder: NSCoder) {
        //fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setValue(string : String) {
        
        if string.count == 0 {
            viewAllLbl.isHidden = true
        }
        else {
            viewAllLbl.isHidden = false
        }
        
        let  text = GlobalClass.makePhoneNo(number: string)
        self.text_Field.text = text
        let charArr2 = (Array(text) as NSArray)
  
        for i in 0 ..< charArr2.count {
            if self.arrayLbl.count > i {
                self.arrayLbl[i].text = "\(charArr2[i] as! Character)"
                self.arrayLbl[i].textColor = COLOR_PRIMARY_TEXT_COLOR
            }
        }
    }
    
    func setupView(){
        let view = Bundle.main.loadNibNamed("ViewTextPhone", owner: self, options: nil)![0] as! UIView
        self.addSubview(view)
        view.frame  = self.bounds
        
        var animation = CABasicAnimation(keyPath: "opacity")
        animation.fromValue = NSNumber(value: 1.0)
        animation.toValue = NSNumber(value: 0.0)
        animation.duration = 0.5
        animation.timingFunction = CAMediaTimingFunction(name: .linear)
        animation.autoreverses = true
        animation.repeatCount = 20000
        self.viewBlink.layer.add(animation, forKey: "opacity")
        
        self.constLeadingViewBlink.constant = 600
        self.lblTitle.textColor = COLOR_ACCENT_TEXT_COLOR
        self.text_Field.delegate = self
        self.viewLine.backgroundColor = COLOR_ACCENT_LINE
        self.lblFirstDesh.textColor = COLOR_PRIMARY_TEXT_COLOR
        self.lblLastDesh.textColor = COLOR_PRIMARY_TEXT_COLOR
        self.lblFirstBreakt.textColor = COLOR_PRIMARY_TEXT_COLOR
        self.lblLastBreakt.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        arrayLbl = [lbl1, lbl2, lbl3, lbl4, lbl5, lbl6, lbl7, lbl8, lbl9, lbl10]
        for i in arrayLbl {
            i.textColor = COLOR_ACCENT_TEXT_COLOR
        }
        let charArr2 = Array(inputText)
        for i in 0 ..< charArr2.count {
            self.arrayLbl[i].text = "\(charArr2[i])"
            self.arrayLbl[i].textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)//COLOR_PRIMARY_TEXT_COLOR
        }
    }
    
    
    @IBAction func btnEditEction(_ sender: UIButton) {
        text_Field.isUserInteractionEnabled = !text_Field.isUserInteractionEnabled
        
        if text_Field.isUserInteractionEnabled {
            sender.setTitle("SAVE", for: .normal)
            text_Field.becomeFirstResponder()
        }
        else {
            sender.setTitle("EDIT", for: .normal)
        }
        
    }
    
    @IBAction func textFieldChanged(_ sender: UITextField) {
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        self.constLeadingViewBlink.constant = 600
        if textField.text!.count == 0 {
            viewAllLbl.isHidden = true
        }
        else {
            viewAllLbl.isHidden = false
        }
        
        text_Field.attributedPlaceholder = NSAttributedString(string: text_Field.placeholder ?? "",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        text_Field.attributedPlaceholder = NSAttributedString(string: text_Field.placeholder ?? "",
                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor.clear])
        viewAllLbl.isHidden = false
       // self.constLeadingViewBlink.constant = 6
        if textField.text!.count >= 10 {
            self.constLeadingViewBlink.constant = 600
        }
        else if textField.text!.count == 0{
            self.constLeadingViewBlink.constant = 6
        }
        else{
            let textFieldLast = self.arrayLbl[textField.text!.count]
            // Ben10 print("textFieldLast.frame.origin.x)
            self.constLeadingViewBlink.constant = textFieldLast.frame.origin.x + textFieldLast.frame.size.width * 0.5 - 1
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            // myvalidator(text: updatedText)
            
            if string == "" && updatedText.count > 10 {
                return true
            }
            if updatedText.count > 10 {
                return false
            }
            
            
//            if updatedText.count == 0 {
//                viewAllLbl.isHidden = true
//            }
//            else {
//                viewAllLbl.isHidden = false
//            }
            
            for i in self.arrayLbl {
                i.text = "-"
                i.textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)//COLOR_ACCENT_TEXT_COLOR
            }
            let charArr2 = Array(updatedText)
            
            var finalStringArray = NSMutableArray()
            for i in 0 ..< updatedText.count {
                self.arrayLbl[i].text = "\(charArr2[i])"
                self.arrayLbl[i].textColor = #colorLiteral(red: 0.7815372348, green: 0.9216808677, blue: 0.9658992887, alpha: 1)//COLOR_PRIMARY_TEXT_COLOR
                
                if i == 0 {
                    finalStringArray.add("(")
                }
                
                finalStringArray.add("\(charArr2[i])")
                
                if i == 2 {
                    finalStringArray.add(")-")
                }
                else if i == 5 {
                    finalStringArray.add("-")
                }
            }
            
            if updatedText.count >= 10 {
                self.constLeadingViewBlink.constant = 600
            }
//            else if updatedText.count == 0{
//                self.constLeadingViewBlink.constant = 600
//            }
            else{
                let textFieldLast = self.arrayLbl[updatedText.count]
                // Ben10 print("textFieldLast.frame.origin.x)
                self.constLeadingViewBlink.constant = textFieldLast.frame.origin.x + textFieldLast.frame.size.width * 0.5 - 1
            }
            
            
            finalString = finalStringArray.componentsJoined(by: "")
            
        }
        return true
    }
    
    @IBAction func btnEditTextFieldAction(_ sender: UIButton) {
        
        if !self.text_Field.isUserInteractionEnabled {
            return
        }
        
        self.text_Field.becomeFirstResponder()
        if text_Field.text!.count >= 10 {
            self.constLeadingViewBlink.constant = 600
        }
        else if text_Field.text?.count == 0{
            self.constLeadingViewBlink.constant = 6
        }
        else{
            let textFieldLast = self.arrayLbl[text_Field.text!.count]
            // Ben10 print("textFieldLast.frame.origin.x)
            self.constLeadingViewBlink.constant = textFieldLast.frame.origin.x + textFieldLast.frame.size.width * 0.5 - 1
        }
    }
    
}
