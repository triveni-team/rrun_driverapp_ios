//
//  db.swift
//  dataBase_Ben
//
//  Created by Arthonsys Technologies LLP on 24/05/18.
//  Copyright © 2018 Arthonsys Technologies LLP. All rights reserved.
//

import UIKit
import SQLite3

let databaseName = "RestaurantDB.db"

class dataBase_db: NSObject {
    class func dbStart() -> Any{
        var documentPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDir = documentPaths[0]
        _ = URL(fileURLWithPath: documentsDir).appendingPathComponent(databaseName).absoluteString

        
        
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent(databaseName)
        
        //**** open database
        var db: OpaquePointer?
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            // Ben10 print(""error opening database")
        }
        
        return db
    }
}

func tableInfo() -> (NSMutableArray, NSMutableArray) {
    
    var db: OpaquePointer? = dataBase_db.dbStart() as? OpaquePointer
    let tableName = "TableBackLog"
    var statement: OpaquePointer?
    
    let resultName = NSMutableArray()
    let resultType = NSMutableArray()
    
    let sql = "PRAGMA table_info('\(tableName)')"
    if sqlite3_prepare(db, sql, -1, &statement, nil) != SQLITE_OK {
        // Ben10 print(""Problem with prepare statement tableInfo ")
    }
    
    while sqlite3_step(statement) == SQLITE_ROW {
        if let cString = sqlite3_column_text(statement, Int32(1)) {
            resultName.add(String(cString: cString))
        }
        if let cString = sqlite3_column_text(statement, Int32(2)) {
            resultType.add(String(cString: cString))
        }
    }
    
//    // Ben10 print("resultName)
//    // Ben10 print("resultType)
    
    return (resultName, resultType)
}
func updateDB(column : String, type : String ) {
    // ALTER TABLE employees ADD status VARCHAR;
    
    
    
    var db: OpaquePointer? = dataBase_db.dbStart() as? OpaquePointer
    let tableName = "TableBackLog"   // this table will update
    var statement: OpaquePointer?
    
    
    //***** insert data into database
    
    let stringSql = " ALTER TABLE \(tableName) ADD \(column) \(type)"
    
    
    // Ben10 print(""stringSql  = \(stringSql)")
    if sqlite3_exec(db, stringSql, nil, nil, nil) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        // Ben10 print(""error Insert: \(errmsg)")
    }
    
    if sqlite3_finalize(statement) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        // Ben10 print(""error finalizing prepared statement: \(errmsg)")
    }
    
    
    //*** close data base
    if sqlite3_close(db) != SQLITE_OK {
        // Ben10 print(""error closing database")
    }
    
    db = nil
}

func checkIfAnyUpdate(dataDic : NSDictionary) {
    let allKeysInserted = (dataDic.allKeys as NSArray)
    let allColomnOfTable = tableInfo().0
    
    for i in 0 ..< allKeysInserted.count {
        if !allColomnOfTable.contains(allKeysInserted[i] as? String ?? "") {
            if let k = allKeysInserted[i] as? String {
                
                let val = dataDic[k]
                var type = "TEXT"
                if ((val as? Int) != nil) {
                    type = "INTEGER"
                }
                
                updateDB(column: k, type: type)
            }
        }
    }
}

public func InsertInTo_TableBackLog(is_type : String, dataDic : NSDictionary) -> String {
    
    let dic = dataDic.mutableCopy() as! NSMutableDictionary
    
 //   dic.setValue(nil, forKey: "s_no")
    
    checkIfAnyUpdate(dataDic: dic)
    
//    let dataFromDic = getDataWithQuery(Query: "select * from TableItems WHERE  distributor_code = '\(Store("distributor_code"))' AND user_id = \(User("customer_id")) AND is_type = '2' AND id = '\(dataDic["id"] as! String)'")
//
//    if dataFromDic.count != 0 && is_type == "2" {
//        return
//    }
    
    
    var db: OpaquePointer? = dataBase_db.dbStart() as? OpaquePointer
    
    let tableName = "TableBackLog"
    
    
    var statement: OpaquePointer?
    
    
    for i in 0 ..< dic.count {
        if let value = ((dic.allValues as NSArray)[i] as? String) {
            dic.setValue(value.R(), forKey: ((dic.allKeys as NSArray)[i] as! String))
        }
    }
    
    
    //***** insert data into database
    
    var stringSql = "insert into \(tableName)"
    
    stringSql.append("(\((dic.allKeys as NSArray).componentsJoined(by: ","))) ")
    stringSql.append("values('")
    stringSql.append("\((dic.allValues as NSArray).componentsJoined(by: "','"))")
    stringSql.append("')")
    
    // Ben10 print(""stringSql  = \(stringSql)")
    
    
    if sqlite3_exec(db, stringSql, nil, nil, nil) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        // Ben10 print(""error Insert: \(errmsg)")
    }
    
    let lastRowId = sqlite3_last_insert_rowid(db);
  //  // Ben10 print("lastRowId)
    
    if sqlite3_finalize(statement) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        // Ben10 print(""error finalizing prepared statement: \(errmsg)")
    }
    statement = nil
    
    //*** close data base
    if sqlite3_close(db) != SQLITE_OK {
        // Ben10 print(""error closing database")
    }
    db = nil
    return String(lastRowId)
}

public func get_data_from_TableBackLog(is_type : String) -> NSMutableArray {
    
    var db: OpaquePointer? = dataBase_db.dbStart() as? OpaquePointer
    
    let tableName = "TableBackLog"
    
    let table_Info = tableInfo()
    let arrayKey = table_Info.0
    
    var statement: OpaquePointer?
    
    
    //*** fatch data from database
    if sqlite3_prepare_v2(db, "select * from \(tableName) WHERE status = '1'", -1, &statement, nil) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        // Ben10 print(""error preparing select: \(errmsg)")
    }
    
    let returnArray = NSMutableArray()
    
    while sqlite3_step(statement) == SQLITE_ROW {
        
        let mutabledic = NSMutableDictionary()
        
        for i in 0 ..< arrayKey.count {
  
            if ((table_Info.1)[i] as! String) == "INTEGER" {
                let column_Value = sqlite3_column_int64(statement, Int32(i))
                mutabledic.setValue(column_Value, forKey: String(arrayKey[i] as! String))
            }
                
            else{
                
                if let cString = sqlite3_column_text(statement, Int32(i)) {
                    var val = String(cString: cString)
                    if  ((table_Info.0)[i] as! String) == "media_gallery" {
                        let array = val.split(separator: ",")
                        if array.count == 0 {
                            mutabledic.setValue(([""] as NSArray), forKey: String(arrayKey[i] as! String))
                        }
                        else{
                            mutabledic.setValue(array, forKey: String(arrayKey[i] as! String))
                        }
                    }
                    else{
                        mutabledic.setValue(val, forKey: String(arrayKey[i] as! String))
                    }
                    
                    
                } else {
                 //   // Ben10 print(""name not found")
                    mutabledic.setValue(" ", forKey: String(arrayKey[i] as! String))
                }
            }
        }
        returnArray.add(mutabledic)
    }
    
    
    if sqlite3_finalize(statement) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        // Ben10 print(""error finalizing prepared statement: \(errmsg)")
    }
    statement = nil
    //*** close data base
    if sqlite3_close(db) != SQLITE_OK {
        // Ben10 print(""error closing database")
    }
    db = nil
    return returnArray
}

public func DeleteItem_TableBackLog(is_type : String, id : String) {
    
   
    var db: OpaquePointer? = dataBase_db.dbStart() as? OpaquePointer
    
    let tableName = "TableBackLog"
    
    var statement: OpaquePointer?
    var qry = "DELETE FROM \(tableName) WHERE  id <= '\(id)'"
    
   
    //**** delete row
    if sqlite3_exec(db, qry, nil, nil, nil) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        // Ben10 print(""error DELETE: \(errmsg)")
    }
    
    if sqlite3_finalize(statement) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        // Ben10 print(""error finalizing prepared statement: \(errmsg)")
    }
    statement = nil
    
    //*** close data base
    if sqlite3_close(db) != SQLITE_OK {
        // Ben10 print(""error closing database")
    }
    
    db = nil
}


public func InsertInTo_TableDriverLocation(dataDic : NSDictionary) {
    
    let tableName = "TableDriverLocation"
    

    let dataFromDic = getDataWithQuery(Query: "select * from \(tableName) WHERE  let = '\(dataDic["let"] as! String)' AND long = '\(dataDic["long"] as! String)'")
    
    if dataFromDic.count != 0 {
        return
    }
    
    
    
    var db: OpaquePointer? = dataBase_db.dbStart() as? OpaquePointer
    
    
    
    var statement: OpaquePointer?
    
    let dic = dataDic.mutableCopy() as! NSMutableDictionary
    
    for i in 0 ..< dic.count {
        if let value = ((dic.allValues as NSArray)[i] as? String) {
                dic.setValue(value.R(), forKey: ((dic.allKeys as NSArray)[i] as! String))
        }
        else if let value = ((dic.allValues as NSArray)[i] as? NSArray) {
            if ((dic.allKeys as NSArray)[i] as! String) == "media_gallery" {
                let str = ((dic.allValues as NSArray)[i] as! NSArray).componentsJoined(by: ",")
                dic.setValue(str.R(), forKey: ((dic.allKeys as NSArray)[i] as! String))
            }
        }
    }
    //***** insert data into database
    
    var stringSql = "insert into \(tableName)"
   
    stringSql.append("(\((dic.allKeys as NSArray).componentsJoined(by: ","))) ")
    stringSql.append("values('")
    stringSql.append("\((dic.allValues as NSArray).componentsJoined(by: "','"))")
    stringSql.append("')")

    print("stringSql  = \(stringSql)")

    
    if sqlite3_exec(db, stringSql, nil, nil, nil) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("error Insert: \(errmsg)")
    }

    
    if sqlite3_finalize(statement) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        print("error finalizing prepared statement: \(errmsg)")
    }
    statement = nil

    
   
    //*** close data base
    if sqlite3_close(db) != SQLITE_OK {
        print("error closing database")
    }
    
    db = nil
}

public func UpdateStatus(id : String ) {
    
    
    var db: OpaquePointer? = dataBase_db.dbStart() as? OpaquePointer
    let tableName = "TableDriverLocation"
    var statement: OpaquePointer?
    //***** insert data into database
    
    let stringSql = "UPDATE \(tableName) SET status = '1'  WHERE id = '\(id)'"
    
    
    // Ben10 print(""stringSql  = \(stringSql)")
    if sqlite3_exec(db, stringSql, nil, nil, nil) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        // Ben10 print(""error Insert: \(errmsg)")
    }
    
    if sqlite3_finalize(statement) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        // Ben10 print(""error finalizing prepared statement: \(errmsg)")
    }
    
    
    //*** close data base
    if sqlite3_close(db) != SQLITE_OK {
        // Ben10 print(""error closing database")
    }
    
    db = nil
    
}





public func DeleteItem_Table(is_type : String, item_id : String) {
    if is_type == "2" {
          //  UpdateLikeStatusIn_TableItems(item_id: item_id)
    }
    var db: OpaquePointer? = dataBase_db.dbStart() as? OpaquePointer
    let tableName = "TableItems"
    var statement: OpaquePointer?
    var qry = "DELETE FROM \(tableName) WHERE  distributor_code = '\(Store("distributor_code"))' AND user_id = \(User("customer_id")) AND is_type = '\(is_type)' AND id = '\(item_id)' "
    
    if item_id == "" {
        qry = "DELETE FROM \(tableName) WHERE distributor_code = '\(Store("distributor_code"))' AND  user_id = \(User("customer_id")) AND is_type = '\(is_type)'"
    }
    //**** delete row
    if sqlite3_exec(db, qry, nil, nil, nil) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        // Ben10 print(""error DELETE: \(errmsg)")
    }
    
    if sqlite3_finalize(statement) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        // Ben10 print(""error finalizing prepared statement: \(errmsg)")
    }
    statement = nil

    //*** close data base
    if sqlite3_close(db) != SQLITE_OK {
        // Ben10 print(""error closing database")
    }
    
    db = nil
}


public func get_data_from_Table(trip_id : String) -> NSMutableArray {
    
    var db: OpaquePointer? = dataBase_db.dbStart() as? OpaquePointer
    
    let tableName = "TableDriverLocation"

    let arrayKey = ["id", "trip_id", "let", "long", "status"]

    var statement: OpaquePointer?
    
  //  let sql = "select * from \(tableName) WHERE trip_id = '\(trip_id)' AND status = '0'"
    let sql = "select * from \(tableName) WHERE status = '0'"

    //*** fatch data from database
    if sqlite3_prepare_v2(db,sql , -1, &statement, nil) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
         print("error preparing select: \(errmsg)")
    }
    
    let returnArray = NSMutableArray()
    
    while sqlite3_step(statement) == SQLITE_ROW {
        
        let mutabledic = NSMutableDictionary()
        
        for i in 0 ..< arrayKey.count {
            
            
            if i == 0 {
                let column_Value = sqlite3_column_int64(statement, Int32(i))
                mutabledic.setValue(column_Value, forKey: String(arrayKey[i]))
            }
            else{
                if let cString = sqlite3_column_text(statement, Int32(i)) {
 
                    var val = String(cString: cString)
                    mutabledic.setValue(val, forKey: String(arrayKey[i]))
 
                } else {
                   
                    mutabledic.setValue(" ", forKey: String(arrayKey[i]))
                }
            }
        }
        returnArray.add(mutabledic)
    }

    if sqlite3_finalize(statement) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
         print("error finalizing prepared statement: \(errmsg)")
    }
    statement = nil
    //*** close data base
    if sqlite3_close(db) != SQLITE_OK {
        print("error closing database")
    }
    db = nil
    return returnArray
}



public func getDataWithQuery(Query : String) -> NSArray {
    
    var db: OpaquePointer? = dataBase_db.dbStart() as? OpaquePointer
    
   // let tableName = "TableItems"
    
    let arrayKey = ["s_no", "case_price", "case_qty", "description", "id", "image", "in_wishlist", "is_case", "media_gallery", "name", "price", "product_brand", "qty", "sku", "small_image", "special_price", "stock_status", "thumbnail", "upper_case", "user_id", "is_type", "in_cart","distributor_code","unit_case"]
    
    var statement: OpaquePointer?
    
    //******
    
    //*** fatch data from database
   
    // Ben10 print(""sql = \(Query)")
    if sqlite3_prepare_v2(db, Query, -1, &statement, nil) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        // Ben10 print(""error preparing select: \(errmsg)")
    }
    
    let returnArray = NSMutableArray()
    
    while sqlite3_step(statement) == SQLITE_ROW {
        
        let mutabledic = NSMutableDictionary()
        
        for i in 0 ..< arrayKey.count {
            
            
            if i == 0 {
                let column_Value = sqlite3_column_int64(statement, Int32(i))
                mutabledic.setValue(column_Value, forKey: String(arrayKey[i]))
            }
            else{
                
                if let cString = sqlite3_column_text(statement, Int32(i)) {
                    let val = String(cString: cString)
                    mutabledic.setValue(val, forKey: String(arrayKey[i]))
                    
                } else {
                    // Ben10 print(""name not found")
                    mutabledic.setValue(" ", forKey: String(arrayKey[i]))
                }
                
            }
            
        }
        returnArray.add(mutabledic)
    }
    
    //   // Ben10 print(""returnArray = \(returnArray)")
    
    //******
    
    if sqlite3_finalize(statement) != SQLITE_OK {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        // Ben10 print(""error finalizing prepared statement: \(errmsg)")
    }
    statement = nil
    //*** close data base
    if sqlite3_close(db) != SQLITE_OK {
        // Ben10 print(""error closing database")
    }
    db = nil
    return returnArray
}

