//
//  ViewTextFieldInput.swift
//  Restaurant Depot
//
//  Created by Bijender Singh on 05/02/19.
//  Copyright © 2019 Bijender Singh. All rights reserved.
//

import UIKit

class ViewTextFieldInput: UIView {

    @IBOutlet weak var lblTitle: UILabel!
   
    @IBOutlet weak var btnEdit: UIButton!
    
    @IBOutlet weak var text_Field: UITextField!
    @IBOutlet weak var viewLine: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    @IBOutlet weak var consEditBtn: NSLayoutConstraint!
    
    required init?(coder aDecoder: NSCoder) {
        //fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView(){
        let view = Bundle.main.loadNibNamed("ViewTextFieldInput", owner: self, options: nil)![0] as! UIView
        self.addSubview(view)
        view.frame  = self.bounds
        
        self.lblTitle.textColor = COLOR_ACCENT_TEXT_COLOR
        //self.lblEdit.textColor = COLOR_PRIMARY_TEXT_COLOR
        self.text_Field.textColor = COLOR_PRIMARY_TEXT_COLOR
        self.viewLine.backgroundColor = COLOR_ACCENT_LINE

        
    }
    
    
    @IBAction func btnEditEction(_ sender: UIButton) {
        text_Field.isUserInteractionEnabled = !text_Field.isUserInteractionEnabled
    }
    

}
