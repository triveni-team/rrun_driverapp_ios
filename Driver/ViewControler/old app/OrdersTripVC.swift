//
//  OrdersTripVC.swift
//  Driver
//
//  Created by Apple on 30/03/19.
//  Copyright © 2019 Arthonsys. All rights reserved.
//

import UIKit
import MapKit
import NVActivityIndicatorView

protocol changeOrderStatusDelegate : class {
    func setOrderStatus(status: String, index : Int)
}

class OrdersTripVC: UIViewController, MKMapViewDelegate, changeOrderStatusDelegate {
   
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var lblHederTitle: UILabel!
    
    

    @IBOutlet weak var viewLoderHightConstant: NSLayoutConstraint!
    @IBOutlet weak var viewLoder: UIView!
    @IBOutlet weak var viewLoderAnim: UIView!
    
    @IBOutlet weak var tableview: UITableView!
    var RecivedData = NSMutableDictionary()
    var OrderArray = NSMutableArray()
    
    var AllOrderArray = NSMutableArray()
    
    var stop_status = ""
    
    var isApiCall = true
    
    var imgSinStopArray = NSMutableArray()
    
    var arraySelectedViewOrderBtn = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblHederTitle.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        txtSearch.delegate = self
        
        setUpView()
        
        if let istrue = RecivedData["trip_stops_orders"] {
            AllOrderArray = (RecivedData)["trip_stops_orders"] as! NSMutableArray
            OrderArray = AllOrderArray
        } else{
            
            GlobalClass.showAlertOKAction(APP_NAME, "No Order Found.", "OK", self) { (ok) in
                 self.navigationController?.popViewController(animated: true)
       }
            
//            self.navigationController?.popViewController(animated: true)
        }
}
    
    @IBAction func btnBack(_ sender: Any) {
        
        if !isApiCall {
            return
        }
        
//        if isApiCall {
//
//        } else {
//            return
//        }
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func btnTarckLoction(_ sender: ZFRippleButton) {
        
        if !isApiCall {
            return
        }
        
//        if isApiCall {
//
//        } else {
//            return
//        }
        
        //        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MapVC") as? MapVC
        //        self.navigationController!.pushViewController(vc!, animated: true)
        
        let latitude = Double(RecivedData["latitude"] as! String)
        let longitude = Double(RecivedData["longitude"] as! String)
        
        if (latitude != nil) && (longitude != nil) {
            
            let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!)))
            destination.name = "Destination"
            
            MKMapItem.openMaps(with: [destination], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
            
        }
    }
    
    // When coming to back
    func setOrderStatus(status: String, index : Int) {
        
        // cell Update
        let Dic = OrderArray.object(at: index) as! NSMutableDictionary
        Dic.setValue("confirm", forKey: "order_status")
        let indexPath = IndexPath(item: index, section: 0)
        tableview.reloadRows(at: [indexPath], with: .automatic)
        
    }
    
    @IBAction func btnCellViewOrder(_ sender: ZFRippleButton) {
        
        if let indexPath = tableview.indexPathForView(sender as UIView) {
            
            if !isApiCall { return }
            
            let index = OrderArray[indexPath.row] as! NSMutableDictionary
            if index.count > 0 {
                getData(indexpath: indexPath.row)
            }
        }
    }

}

class ordercell : UITableViewCell {
    
    @IBOutlet weak var lblAdrress: UILabel!
    @IBOutlet weak var lblNoItem: UILabel!
    @IBOutlet weak var lblConDetails: UILabel!
    @IBOutlet weak var lblCoPerson: UILabel!
    @IBOutlet weak var lblOrderIDHeder: UILabel!
    @IBOutlet weak var lblOrderType: UILabel!
    @IBOutlet weak var lblOrderStatus: UILabel!
    
    // Hard Outlet
    
    @IBOutlet weak var lblAddressHard: UILabel!
    @IBOutlet weak var lblNoitemHard: UILabel!
    @IBOutlet weak var lblContectNoHard: UILabel!
    @IBOutlet weak var lblContectPerHard: UILabel!
    @IBOutlet weak var lblOredrTypeHard: UILabel!
    @IBOutlet weak var lblOrderStatusHard: UILabel!

    @IBOutlet weak var btnViewOrder: ZFRippleButton!
    
    
}

extension OrdersTripVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return OrderArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ordercell
        
        cell.lblAddressHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblNoitemHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblContectNoHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblContectPerHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblOredrTypeHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblOrderStatusHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        cell.lblAdrress.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblNoItem.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblConDetails.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblCoPerson.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblOrderIDHeder.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblOrderType.textColor = COLOR_PRIMARY_TEXT_COLOR
        cell.lblOrderStatus.textColor = COLOR_PRIMARY_TEXT_COLOR

        
        let index = OrderArray[indexPath.row] as! NSMutableDictionary
        let Address = index["customer_address"] as? String
        if Address?.count == 0 {
            cell.lblAdrress.text = "<BR>"
        } else{
            cell.lblAdrress.text = (index["customer_address"] as? String)?.uppercased()
        }
        
        cell.lblNoItem.text = String(index["order_item_count"] as? Int ?? 0).uppercased()
        cell.lblCoPerson.text = (index["customer_name"] as? String)?.uppercased()
        cell.lblConDetails.text = (index["customer_mobile_number"] as? String)?.uppercased()
        
        cell.lblOrderType.text = (index["order_type"] as? String)?.uppercased()
        cell.lblOrderStatus.text = (index["order_status"] as? String)?.uppercased()

//        cell.lblOrderID.text = index["order_id"] as? String ?? "0"
        
        let OrderID = index["order_increment_id"] as? String
        cell.lblOrderIDHeder.text = ("Order id: #\(OrderID ?? "0")").uppercased()
        
        
        if self.arraySelectedViewOrderBtn.count == 0 {
            cell.btnViewOrder.isUserInteractionEnabled = true
        }
        else {
            cell.btnViewOrder.isUserInteractionEnabled = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func getData(indexpath : Int) {
        
        if !isApiCall { return }
        isApiCall = false
        
        let index = OrderArray[indexpath] as! NSMutableDictionary
        self.viewLoderHightConstant.constant = 35
        
        let order_id = index["id"]
        let stop_id = index["stop_id"]
        let trip_id = index["trip_id"]
        let stop_status = index["stop_status"]
        let order_increment_id = index["order_increment_id"]
        let stop_status_back_vc = RecivedData["stop_status"] // ben after change // deepak
        let customer_name = index["customer_name"]
        let customer_mobile = index["customer_mobile_number"]
        let address = index["customer_address"]
        let vcSelectIndexPath = indexpath
        let stop_trip_id = RecivedData["retailer_id"] as! String  // ben after change // deepak
        let order_type = index["order_type"] as! String  // ben after change // deepak

        // Ben10 print("stop_trip_id)
    
        
        // Ben10 print(""stop_trip_id : \(stop_trip_id)")
        

        let parm = ["order_id": order_id]
        
        self.arraySelectedViewOrderBtn.add(indexpath)
        tableview.reloadData()
        
        GlobalClass.multipartData("get_order_data", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (responce) in
            self.arraySelectedViewOrderBtn.removeAllObjects()
            self.tableview.reloadData()
            
            self.isApiCall = true
            
            if ((responce as! NSDictionary)["data"] as! NSDictionary).count == 0 {
                GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "No Item Found.", view: self)
                self.viewLoderHightConstant.constant = 0
                return
            }

            let sendData = ((responce as! NSDictionary)["data"] as! NSDictionary).mutableCopy() as! NSMutableDictionary
            
                    if sendData.count != 0 {
                        
                        let order_status = sendData["order_status"]
            
                        // Ben10 print("self.navigationController as Any)
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "YourTripDetailsVC") as? YourTripDetailsVC
                        sendData.setValue(stop_id, forKey: "stop_id")
                        sendData.setValue(trip_id, forKey: "trip_id")
                        sendData.setValue(order_id, forKey: "order_id")
                        sendData.setValue(stop_status, forKey: "stop_status")
                        sendData.setValue(order_increment_id, forKey: "order_increment_id")
                        sendData.setValue(order_status, forKey: "order_status")
                        sendData.setValue(stop_status_back_vc, forKey: "stop_status_back_vc")
                        sendData.setValue(customer_name, forKey: "customer_name")
                        sendData.setValue(customer_mobile, forKey: "customer_mobile")
                        sendData.setValue(address, forKey: "address")
                        sendData.setValue(vcSelectIndexPath, forKey: "indexpath")
                        sendData.setValue(stop_trip_id, forKey: "stop_trip_id")
                        sendData.setValue(order_type, forKey: "order_type")
                        vc!.RecivedData = sendData
                        vc!.delegate = self
                        vc!.orderTripVc = self
                        vc!.imgSinStopArray = self.imgSinStopArray
                        self.navigationController!.pushViewController(vc!, animated: true)

            }
            
            self.viewLoderHightConstant.constant = 0

        }, successWithStatus0Closure: { (responce) in
            self.arraySelectedViewOrderBtn.removeAllObjects()
            self.tableview.reloadData()
            // Ben10 print(""responce = \(responce as! NSDictionary)")
            let msg = (responce as! NSDictionary).object(forKey: "responsemsg") as! String
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: msg, view: self)
            self.viewLoderHightConstant.constant = 0
            self.isApiCall = true
            
            let responsemsg = (responce as! NSDictionary)["responsemsg"] as? String ?? ""
            let responsecode = (responce as! NSDictionary)["responsecode"] as? String ?? ""
            
            if responsecode as? String ?? "" == "-2" {
                
                GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
                    
                    GlobalClass.setUserDefault(value: "", for: "landingPage")
                    GlobalClass.removeUserDefaults("getDistributorList")
                    
                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                    return
                    
                })
            }
            
        }) { (error) in
            self.arraySelectedViewOrderBtn.removeAllObjects()
            self.tableview.reloadData()
            self.viewLoderHightConstant.constant = 0
            self.isApiCall = true
            
            if error == "Not Show" { return }
            GlobalClass.showAlertOKAction(APP_NAME, error!, "OK", self, successClosure: { (str) in
            })
        }
    }
    
    func setUpView() {
        let activityIndicator = NVActivityIndicatorView(frame: self.viewLoderAnim.bounds)
        activityIndicator.type = .ballPulseSync
        activityIndicator.color = UIColor.black
        self.viewLoderAnim.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
}

extension OrdersTripVC : UITextFieldDelegate {
    

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let SearchText = (txtSearch.text?.removingWhitespaces())! + string
        
        var searchString = (txtSearch.text! + string)
        if (range.lowerBound < range.upperBound) {
            searchString = String(txtSearch.text!.dropLast())
        }
        
        if searchString.count != 0 {
            self.OrderArray = self.AllOrderArray
            tableview.reloadData()
            return true
        }
        
        if AllOrderArray.count != 0 {
            if searchString.count > 0 {
                
                let filterArray = self.AllOrderArray.filter{(($0 as! NSDictionary)["order_increment_id"] as? String ?? "").contains(searchString)}
                // Ben10 print(""filterArray :\(filterArray)")
                self.OrderArray = (filterArray as NSArray).mutableCopy() as! NSMutableArray
                tableview.reloadData()

            } else {
                self.OrderArray = self.AllOrderArray
                tableview.reloadData()
            }
        }

        return true
    }
}

