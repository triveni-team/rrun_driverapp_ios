//
//  ViewTripVC.swift
//  Driver New
//
//  Created by Arthonsys Ben on 08/07/19.
//  Copyright © 2019 Bijender Singh. All rights reserved.
//

import UIKit

class ViewTripVC: UIViewController {
    
    @IBOutlet weak var tableViewTrip: UITableView!
    @IBOutlet weak var lblTripIdHeader: UILabel!
    @IBOutlet weak var lblTripId: UILabel!
    @IBOutlet weak var lblStops: UILabel!
    @IBOutlet weak var lblOrders: UILabel!
    @IBOutlet weak var lblItems: UILabel!
    
    var titleArr = ["Zack's Oak Bar & Restaurant","Palace Dinning Room","Greasy Spoon Chophouse","Onion Ring Planet"]
    var AddressArr = ["380 Super Cool Road Clifton, NJ 07014","456 Neato Street Clifton, NJ 07014","777 Total Dhamaal Road Clifton, NJ 07014","99 Yum Time Drive Clifton, NJ 07014"]
    var collapesedHeader = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
   
    //MARK:- All Button Action
    @IBAction func btnTripListAction(_ sender: UIButton) {
    }
    
    @IBAction func btnDriverProfileAction(_ sender: UIButton) {
    }
    
    @IBAction func btnStartTripAction(_ sender: UIButton) {
    }
    
    @IBAction func btnDetailsCellAction(_ sender: UIButton) {
    }
}

class TableViewTripCell: UITableViewCell {
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
}

extension ViewTripVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.collapesedHeader.contains(section) {
            return 0
        }
        return titleArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewTripCell", for: indexPath) as! TableViewTripCell
        cell.lblTitle.text = titleArr[indexPath.row]
        cell.lblAddress.text = AddressArr[indexPath.row]
        cell.lblNumber.text = String(indexPath.row + 1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width:tableView.frame.size.width, height: 35))
        viewHeader.backgroundColor = #colorLiteral(red: 0.1960784314, green: 0.5215686275, blue: 0.2509803922, alpha: 1)
        
        let labelTitle = UILabel(frame: CGRect(x: 50, y: 0, width: (tableView.frame.size.width) - 50 * 2, height: 35))
        labelTitle.text = "STOPS"
        labelTitle.textColor = .white
        labelTitle.textAlignment = .center
        labelTitle.font = UIFont(name: "Arial", size: 17)
        viewHeader.addSubview(labelTitle)
        
        let buttonTap = UIButton(frame: CGRect(x: (viewHeader.frame.size.width) - 35, y: 0, width: 35, height: 35))
        if self.collapesedHeader.contains(section) {
            buttonTap.setImage(UIImage(named: "down-arrow"), for: .normal)
        }else{
            buttonTap.setImage(UIImage(named: "up-arrow"), for: .normal)
        }
        buttonTap.addTarget(self, action: #selector(btnAction(_:)), for: .touchUpInside)
        viewHeader.addSubview(buttonTap)
        buttonTap.tag = section + 1
        // Ben10 print("buttonTap.tag - 1)
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    @objc func btnAction(_ sender: UIButton) {
        let section = sender.tag - 1
        if self.collapesedHeader.contains(section) {
            self.collapesedHeader.remove(section)
        }else{
            self.collapesedHeader.add(section)
        }
        // Ben10 print("self.collapesedHeader)
        self.tableViewTrip.reloadSections([section], with: .automatic)
    }
}
