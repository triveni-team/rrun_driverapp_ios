//
//  FingerSignInVC.swift
//  Driver
//
//  Created by Apple on 11/02/19.
//  Copyright © 2019 Arthonsys. All rights reserved.
//

import UIKit
import LocalAuthentication

class FingerSignInVC: UIViewController {
    
    @IBOutlet weak var lblHiDriverHard: UILabel!
    @IBOutlet weak var lblPlasePlaceTOHard: UILabel!
    
    @IBOutlet weak var btnDontHaveaFingerHard: UIButton!
    @IBOutlet weak var btnStandardLogibBack: ZFRippleButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        colorCodeSetup()
        authenticationWithTouchID()
        
    }
    
    @IBAction func btnStandardSignIn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        CURRENT_TRIP.removeAllObjects()
    }
    
    @IBAction func btnPingerAction(_ sender: UIButton) {
        
    }
}



extension FingerSignInVC {
    
    func authenticationWithTouchID() {
        let localAuthenticationContext = LAContext()
        localAuthenticationContext.localizedFallbackTitle = "Use Passcode"
        
        var authError: NSError?
        
        let reasonString = "Login with \(User("email"))"
        
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            
            localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reasonString) { success, evaluateError in
                
                if success {
                    
                    // Ben10 print(""authenticated successfully")
                    DispatchQueue.main.async {
                        
                        let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TripsNavVC")
                        UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                        
                        let userDefoult = UserDefaults.standard
                        
                        userDefoult.set("islogin", forKey: "isVcFrome")
                        
                    }
                    
                } else {
                    //TODO: User did not authenticate successfully, look at error and take appropriate action
                    guard let error = evaluateError else {
                        
                        // Ben10 print(""error")
                        
                        return
                    }
                    
                    // Ben10 print("self.evaluateAuthenticationPolicyMessageForLA(errorCode: error._code))
                    
                    //TODO: If you have choosen the 'Fallback authentication mechanism selected' (LAError.userFallback). Handle gracefullypikertDrDriver
                }
            }
        } else {
            
            guard let error = authError else {
                return
            }
            //TODO: Show appropriate alert if biometry/TouchID/FaceID is lockout or not enrolled
            // Ben10 print("self.evaluateAuthenticationPolicyMessageForLA(errorCode: error.code))
            let Massage = (self.evaluateAuthenticationPolicyMessageForLA(errorCode: error.code))
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: Massage, view: self)
        }
    }
    
    func evaluatePolicyFailErrorMessageForLA(errorCode: Int) -> String {
        var message = ""
        if #available(iOS 11.0, macOS 10.13, *) {
            switch errorCode {
            case LAError.biometryNotAvailable.rawValue:
                message = "Authentication could not start because the device does not support biometric authentication."
                
            case LAError.biometryLockout.rawValue:
                message = "Authentication could not continue because the user has been locked out of biometric authentication, due to failing authentication too many times."
                
            case LAError.biometryNotEnrolled.rawValue:
                message = "Authentication could not start because the user has not enrolled in biometric authentication."
                
            default:
                message = "Did not find error code on LAError object"
            }
        } else {
            switch errorCode {
            case LAError.touchIDLockout.rawValue:
                message = "Too many failed attempts."
                
            case LAError.touchIDNotAvailable.rawValue:
                message = "TouchID is not available on the device"
                
            case LAError.touchIDNotEnrolled.rawValue:
                message = "TouchID is not enrolled on the device"
                
            default:
                message = "Did not find error code on LAError object"
            }
        }
        
        return message;
    }
    
    func evaluateAuthenticationPolicyMessageForLA(errorCode: Int) -> String {
        
        var message = ""
        
        switch errorCode {
            
        case LAError.authenticationFailed.rawValue:
            message = "The user failed to provide valid credentials"
            
        case LAError.appCancel.rawValue:
            message = "Authentication was cancelled by application"
            
        case LAError.invalidContext.rawValue:
            message = "The context is invalid"
            
        case LAError.notInteractive.rawValue:
            message = "Not interactive"
            
        case LAError.passcodeNotSet.rawValue:
            message = "Passcode is not set on the device"
            
        case LAError.systemCancel.rawValue:
            message = "Authentication was cancelled by the system"
            
        case LAError.userCancel.rawValue:
            message = "The user did cancel"
            
        case LAError.userFallback.rawValue:
            message = "The user chose to use the fallback"
            
        default:
            message = evaluatePolicyFailErrorMessageForLA(errorCode: errorCode)
        }
        
        return message
    }
    
    func colorCodeSetup () {
        
        //        lblHiDriverHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        //        lblPlasePlaceTOHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        
        //        btnDontHaveaFingerHard.setTitleColor(COLOR_PRIMARY_TEXT_COLOR, for: .normal)
        
        //        btnStandardLogibBack.setTitleColor(COLOR_WHITE, for: .normal)
        //        btnStandardLogibBack.backgroundColor = COLOR_PRIMARY
        
    }
    
}

