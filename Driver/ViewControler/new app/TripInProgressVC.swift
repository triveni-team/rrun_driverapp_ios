//
//  TripInProgressVC.swift
//  Driver New
//
//  Created by Arthonsys Ben on 09/07/19.
//  Copyright © 2019 Bijender Singh. All rights reserved.
//

import UIKit

class TripInProgressVC: UIViewController {
    
    @IBOutlet weak var lblHeaderTripStatus: UILabel!
    @IBOutlet weak var lblHeaderTripName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblDriverName: UILabel!
    
    
    @IBOutlet weak var tableViewTrip: UITableView!
    @IBOutlet weak var lblTripIdHeader: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblOrder: UILabel!
    @IBOutlet weak var lblItems: UILabel!
    @IBOutlet weak var imgBoderLine: UIImageView!
    @IBOutlet weak var lblCurrentOrNextStop: UILabel!
    @IBOutlet weak var btnStartHeader: UIButton!
    @IBOutlet weak var viewShowRootHeader: ViewShowRoot!
    
    @IBOutlet weak var lblItems2: UILabel!
    @IBOutlet weak var lblOrder2: UILabel!
    @IBOutlet weak var lblStops2: UILabel!
    @IBOutlet weak var lblTripName2: UILabel!
    
    @IBOutlet weak var lblItems3: UILabel!
    @IBOutlet weak var lblOrder3: UILabel!
    @IBOutlet weak var lblStops3: UILabel!
    @IBOutlet weak var lblSkipped3: UILabel!
    @IBOutlet weak var lblCanceled3: UILabel!
    @IBOutlet weak var lblReturens3: UILabel!
    
    
    @IBOutlet weak var lblItems4: UILabel!
    @IBOutlet weak var lblOrder4: UILabel!
    @IBOutlet weak var lblStops4: UILabel!
    @IBOutlet weak var lblSkipped4: UILabel!
    @IBOutlet weak var lblCanceled4: UILabel!
    @IBOutlet weak var lblReturens4: UILabel!
    

    
    var sectionArr = ["REMANING STOPS","SKIPPED STOPS","COMPLETED STOPS"]
    var titleArr = [["Onion Ring Planet"],["Place Dinning Room"],["Zack's Oak Bar & Restaurent"]]
 //   var addressArr = [["99 Yum Time Drive Clifton, NJ 07014"],["456 Neato Street Clifton, NJ 07014"],["380 Super Cool Road Clifton, NJ 07014"]]
    var collapesedHeader = NSMutableArray()
    var dataFromBack = NSMutableDictionary()
    var allOrders = NSMutableArray()
    var allTripStops = NSMutableDictionary()
    var isForDetailShow = true
    
    @IBOutlet weak var viewHeaderTable: UIView!
    
    @IBOutlet weak var viewTripNotStart: UIView!
    
    @IBOutlet weak var constHeightViewStartStop: NSLayoutConstraint!
    @IBOutlet weak var constHeightViewStartTrip: NSLayoutConstraint!
    @IBOutlet weak var constHeightViewCompletedAllStops: NSLayoutConstraint!
    @IBOutlet weak var constHeightViewCompletedOrSkiptStop: NSLayoutConstraint!
    var isTripComplet = false
    
    var isCurrentTrip = false

    // from back
    var TripsVC_obj : TripsVC!
    var indexPathBack : IndexPath?
    
    // next vc
    var TripInProgressComplateVC_obj : TripInProgressComplateVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //constHeightViewStartStop.relation = .equal
        if !isForDetailShow {
            dataFromBack = CURRENT_TRIP
        }
        
        if dataFromBack["trip_status"] as? String == "stop" {
           // viewHeaderTable.removeFromSuperview()
        }
        
        allTripStops = dataFromBack["trip_stop"] as! NSMutableDictionary
        refresh()
        // Do any additional setup after loading the view.
        self.lblHeaderTripName.text = dataFromBack["trip_name"] as? String
        if dataFromBack["trip_status"] as! String == "start" {
            self.lblHeaderTripStatus.text = "TRIP IN PROGRESS"
        }
        else {
            self.lblHeaderTripStatus.text = "VIEW TRIP"
        }
        setUserData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        TripInProgressComplateVC_obj = nil
    }
    
    func setUserData() {
        
        let userData = GlobalClass.getUserDefaultDICT()
        
        lblDriverName.text = userData["name"] as? String
        
        let imageUrl = userData["profile"] as? String
        if let validUrl = imageUrl {
            imgProfile.sd_setImage(with: URL(string: validUrl), placeholderImage: UIImage.init(named: "userProfile"), options: .transformAnimatedImage) { (img, error, type , url) in
                if error == nil {
                }
            }
        } else{
        }
    }
    
    var oneTimeRefresh = false
    func refresh() {
        setCurrentTrip()
        sizeHeaderToFit()
        self.tableViewTrip.reloadData()
        
        if oneTimeRefresh {
            oneTimeRefresh = false
            if TripInProgressComplateVC_obj != nil {
                if (allTripStops["current"] as! NSArray).count == 0 {  // if there is no more current trip thin go back page
                    TripInProgressComplateVC_obj.btnBackAction(UIButton())
                }
                else {
                   // self.startCurrentTrip() // this is for start trip on next button
                    TripInProgressComplateVC_obj.nextStopDataFromBack = (allTripStops["current"] as! NSMutableArray)[0] as! NSMutableDictionary
                    TripInProgressComplateVC_obj.refreshForNextStop()
                }
            }
        }
        
    }
    
    
    
    func setCurrentTrip() {
        
        // this is for user alrady complet  stop
        if dataFromBack["trip_status"] as? String == "stop" {
            self.showViews(const: nil)
            return
        }
        
        // this is when there are one trip started and user see another trip
        if CURRENT_TRIP.count != 0 {
            if dataFromBack["trip_id"] as? String != CURRENT_TRIP["trip_id"] as? String {
                self.showViews(const: nil)
                self.isCurrentTrip = false
                return
            }
            
        }
        
        // this is for user thre is no trip started and user can start new trip
        if CURRENT_TRIP.count == 0 {
            if dataFromBack["can_accept"] as? String == "1" { // this is for user can start trip
                self.showViews(const: constHeightViewStartTrip)
                return
                
            }
            else {  // this is for user can't start trip (this is due to trip is not on same day)
                self.showViews(const: nil)
                self.isCurrentTrip = false
                return
            }
        }
        
        
        // this is for set current trip ( only for show in header)
        if (allTripStops["current"] as! NSArray).count == 0 {
            if (allTripStops["remaining"] as! NSArray).count == 0 {
                // change status
            }
            else{
                (allTripStops["current"] as! NSMutableArray).add((allTripStops["remaining"] as! NSMutableArray)[0] as! NSMutableDictionary)
                (allTripStops["remaining"] as! NSMutableArray).removeObject(at: 0)
            }
        }
        
        if CURRENT_TRIP.count != 0 {
            if dataFromBack["trip_id"] as? String == CURRENT_TRIP["trip_id"] as? String {
                self.lblHeaderTripStatus.text = "TRIP IN PROGRESS"
                self.isCurrentTrip = true
                self.showViews(const: constHeightViewStartStop)
            }
            else {
                self.lblHeaderTripStatus.text = "VIEW TRIP"
            }
        }
        
        if (allTripStops["current"] as! NSMutableArray).count == 0 {
            if dataFromBack["trip_status"] as? String == "start" {
                
                if (allTripStops["skipped"] as! NSMutableArray).count != 0 {
                    self.showViews(const: constHeightViewCompletedOrSkiptStop)
                }
                else {
                    self.showViews(const: constHeightViewCompletedAllStops)
                }
            }
        }
        
        setHeaderUI()
    }
    
    func setHeaderUI() {
        if dataFromBack["trip_status"] as? String == "stop" { return }
        if (allTripStops["current"] as! NSArray).count == 0 { return}
        
        setCurrentUIWithStatus()
        
        
    }
    
    func showViews(const : NSLayoutConstraint?) {
        
       
        if const == constHeightViewStartStop &&  (allTripStops["current"] as! NSArray).count != 0 {
             var addressHeight = CGFloat(0)
            let dic = ((allTripStops["current"] as! NSArray)[0] as! NSDictionary)["stop"] as! NSDictionary
            let address = dic["address"] as? String ?? ""
            addressHeight = address.height(withConstrainedWidth: lblAddress.frame.size.width, font: lblAddress.font)
            // Ben10 print("addressHeight)
            constHeightViewStartStop.constant = (187 +  addressHeight)
        }
        else {
            constHeightViewStartStop.constant = (212)
        }
        
      //  constHeightViewStartStop.constant = (241)
        constHeightViewStartTrip.constant = 155
        constHeightViewCompletedAllStops.constant = 320
        constHeightViewCompletedOrSkiptStop.constant = 300
        
        let constArray = [constHeightViewStartStop, constHeightViewStartTrip, constHeightViewCompletedAllStops,constHeightViewCompletedOrSkiptStop]
        
        for i in constArray {
            if i != const {
                i!.constant = 0
            }
        }
     //   self.viewHeaderTable.layoutIfNeeded()
        self.setHeaderData(const: const ?? NSLayoutConstraint())
    }
    
    func setHeaderData(const : NSLayoutConstraint?) {
        
        
        if const == constHeightViewStartStop {
            if (allTripStops["current"] as! NSArray).count == 0 {return}
            let dic = ((allTripStops["current"] as! NSArray)[0] as! NSDictionary)["stop"] as! NSDictionary

            self.lblTitle.text = dic["stop_name"] as? String
            self.lblAddress.text = dic["address"] as? String
            self.lblNumber.text = dic["priority"] as? String
            self.lblOrder.text = dic["total_order"] as? String
            self.lblItems.text = dic["total_item"] as? String
            
            self.lblCurrentOrNextStop.text = "NEXT STOP"
            self.btnStartHeader.addTarget(self, action: #selector(btnStartHeaderAction(_:)), for: .touchUpInside)
            self.btnStartHeader.tag = 9999
            self.btnStartHeader.setTitle("START", for: .normal)
            
            
            if dic["trip_current_status"] as! String == "skipped" {
                self.imgBoderLine.isHidden = true
            }
            else if dic["trip_current_status"] as! String == "remaining" {
                self.imgBoderLine.isHidden = true
                
            }
            else {
                self.imgBoderLine.isHidden = false
                self.lblCurrentOrNextStop.text = "CURRENT STOP"
               // self.btnStartHeader.addTarget(self, action: #selector(btnCompletHeaderAction(_:)), for: .touchUpInside)
                self.btnStartHeader.tag = 8888
                self.btnStartHeader.setTitle("COMPLETE", for: .normal)
            }
            
            let lat = dic["latitude"] as? String ?? "0"
            let lon = dic["longitude"] as? String ?? "0"
            self.viewShowRootHeader.letLon = lat + "," + lon
            
        }
        else if const == constHeightViewStartTrip {
            // dataFromBack["trip_stop"] as! NSMutableDictionary
            self.lblOrder2.text = dataFromBack["total_orders"] as? String
            self.lblItems2.text = dataFromBack["total_items"] as? String
            self.lblStops2.text = dataFromBack["total_stops"] as? String
            self.lblTripName2.text = dataFromBack["trip_name"] as? String
        }
        else if const == constHeightViewCompletedAllStops {
            self.lblOrder3.text = dataFromBack["total_orders"] as? String
            self.lblItems3.text = dataFromBack["total_items"] as? String
            self.lblStops3.text = dataFromBack["total_stops"] as? String
            self.lblSkipped3.text = String((allTripStops["skipped"] as! NSMutableArray).count )
            self.lblCanceled3.text = String((allTripStops["completed"] as! NSMutableArray).count) // This is for COMPLETED
            self.lblReturens3.text = countReturnItem()
            //self.TripsVC_obj.locationManager.stopUpdatingLocation()
            isTripComplet = true
           // self.completTrip()
            self.TripsVC_obj.stopLocationUpdate()
        }
        else if const == constHeightViewStartStop {
            self.lblOrder3.text = dataFromBack["total_orders"] as? String
            self.lblItems3.text = dataFromBack["total_items"] as? String
            self.lblStops3.text = dataFromBack["total_stops"] as? String
            
            
        }
        else if const == constHeightViewCompletedOrSkiptStop {
            self.lblOrder4.text = dataFromBack["total_orders"] as? String
            self.lblItems4.text = dataFromBack["total_items"] as? String
            self.lblStops4.text = dataFromBack["total_stops"] as? String

            self.lblSkipped4.text = String((allTripStops["skipped"] as! NSMutableArray).count )
            self.lblCanceled4.text = String((allTripStops["completed"] as! NSMutableArray).count) // This is for COMPLETED
            self.lblReturens4.text = countReturnItem()
        }
    }
    
    func setCurrentUIWithStatus() {
        // constHeightViewStartStop
        // constHeightViewStartTrip
        // constHeightViewCompletedAllStops
        // constHeightViewCompletedOrSkiptStop
        
        
        if CURRENT_TRIP.count != 0 {
            if dataFromBack["trip_id"] as? String != CURRENT_TRIP["trip_id"] as? String {
               self.showViews(const: nil)
//                return
            }
        }
//        if dataFromBack["trip_status"] as? String == "stop" {
//            tableViewTrip.tableHeaderView = nil
//        }
//        else {
//            tableViewTrip.tableHeaderView = headerView
//        }

        sizeHeaderToFit()
    }
    
    func countReturnItem() -> String{
        
        let completedStops = (CURRENT_TRIP["trip_stop"] as! NSDictionary)["completed"] as! NSArray
        var returnCount = 0
        for i in completedStops {
            let orders = (((i as! NSDictionary)["stop"] as! NSDictionary)["orders"] as! NSArray)
            
            for j in orders {
                let returnItemCountStr = (j as! NSDictionary)["order_return_item_count"] as? String ?? "0"
                let returnItemCountInt = Int(returnItemCountStr) ?? 0
                returnCount = returnCount + returnItemCountInt
            }
        }
        
        return String(returnCount)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    func sizeHeaderToFit() {
        if tableViewTrip.tableHeaderView == nil {return}
        let headerView = tableViewTrip.tableHeaderView!
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        var frame = headerView.frame
        frame.size.height = height
        headerView.frame = frame
        
        tableViewTrip.tableHeaderView = headerView
        
    }
    
    func checkIsAllStopProcessed() -> Bool{
        if (allTripStops["remaining"] as! NSMutableArray).count == 0 {
            return true
        }
        
        return false
    }
    
    //MARK:- All Button Action
    
    @IBAction func btnCompletWSkipStopAction(_ sender: UIButton) {
        self.TripsVC_obj.stopLocationUpdate()
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
//            self.apiTripComplete ()
//        })
        
        self.apiTripComplete ()
        
    }
    
    
    @IBAction func btnTripListAction(_ sender: UIButton) {
        if CURRENT_TRIP.count != 0 && isTripComplet{
            self.completTrip()
            return
        }
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnDriverProfileAction(_ sender: UIButton) {
        NavigationCustom.navToDriverProfile(viewController: self)
    }
    
    @IBAction func btnDetailsHeaderAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "ToTripInProgressComplateVC", sender: nil)
    }
    
    @IBAction func btnShowDetails(_ sender: UIButton) {
        let point = sender.convert(CGPoint.zero, to: tableViewTrip as UIView)
        let indexPath = tableViewTrip.indexPathForRow(at: point)
       
        self.performSegue(withIdentifier: "ToTripInProgressComplateVC", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ToTripInProgressComplateVC" {
            let indexPath = sender as? IndexPath
            var dic = NSMutableDictionary()
            var isType = ""
            var isForCompletTrip = false
            if indexPath?.section == 0 {
                dic = (allTripStops["remaining"] as! NSMutableArray)[indexPath?.row ?? 0] as! NSMutableDictionary
                isType = "remaining"
            }
            else if indexPath?.section == 1 {
                dic = (allTripStops["skipped"] as! NSMutableArray)[indexPath?.row ?? 0] as! NSMutableDictionary
                isType = "skipped"
            }
            else if indexPath?.section == 2 {
                dic = (allTripStops["completed"] as! NSMutableArray)[indexPath?.row ?? 0] as! NSMutableDictionary
                isType = "completed"
            }
            else if ((sender as? String) != nil) {
                dic = (allTripStops["current"] as! NSMutableArray)[indexPath?.row ?? 0] as! NSMutableDictionary
                isType = "current"
                isForCompletTrip = true
            }
            else {
                dic = (allTripStops["current"] as! NSMutableArray)[indexPath?.row ?? 0] as! NSMutableDictionary
                isType = "current"
                
            }
            let vc = segue.destination as! TripInProgressComplateVC
            vc.stopDataFromBack = dic
            vc.dataFromBack = self.dataFromBack
            vc.isType = isType
            vc.TripInProgressVC_obj = self
            vc.isForCompletTrip = isForCompletTrip
            // send at index path if you want
            
            self.TripInProgressComplateVC_obj = vc
        }
    }
    
    @IBAction func btnSkipHeaderAction(_ sender: UIButton) {
        if (allTripStops["current"] as! NSMutableArray).count == 0 {return}
        
        self.apiStarTrip(tripStatus: "skip")
        
        (((allTripStops["current"] as! NSMutableArray)[0] as! NSMutableDictionary)["stop"] as! NSMutableDictionary).setValue("skipped", forKey: "trip_current_status")
        (allTripStops["skipped"] as! NSMutableArray).add(((allTripStops["current"] as! NSMutableArray)[0] as! NSMutableDictionary))
        (allTripStops["current"] as! NSMutableArray).removeAllObjects()
        
       // if !checkIsAllStopProcessed() { self.refresh() }
        self.refresh()
        
    }
    
    
    @IBAction func btnStartTripHeaderAction(_ sender: UIButton) {
        let tripId = self.dataFromBack["trip_id"] as? String ?? ""
        self.TripsVC_obj.apiStartTrip(tripId: tripId, indexPath2: nil)
        
    }
    @IBAction func btnStartHeaderAction(_ sender: UIButton) {
        
        if sender.tag == 9999 {
            self.startCurrentTrip()
        }
        else if sender.tag == 8888 {
            // call when trip complit
            
            self.performSegue(withIdentifier: "ToTripInProgressComplateVC", sender: "showSign")
            
        }
        
        
    }
    
    func startCurrentTrip() {
        self.apiStarTrip(tripStatus: "start")
        (((allTripStops["current"] as! NSMutableArray)[0] as! NSMutableDictionary)["stop"] as! NSMutableDictionary).setValue("current", forKey: "trip_current_status")
        self.refresh()
    }

    func completAction() {
        self.TripsVC_obj.getNewLetLon()
        (((allTripStops["current"] as! NSMutableArray)[0] as! NSMutableDictionary)["stop"] as! NSMutableDictionary).setValue("completed", forKey: "trip_current_status")
        
        (allTripStops["completed"] as! NSMutableArray).add((((allTripStops["current"] as! NSMutableArray)[0] as! NSMutableDictionary)))
        
        (allTripStops["current"] as! NSMutableArray).removeAllObjects()
        self.refresh()
    }

    
    @IBAction func btnUnSkipCellAction(_ sender: UIButton) {
        let point = sender.convert(CGPoint.zero, to: tableViewTrip as UIView)
        let indexPath = tableViewTrip.indexPathForRow(at: point)
        
        // there are 2 conditions 1. in header there are in processing stop or skiped resumed stop
        // if its come from skipt then first i will send it to remaining and call refresh function that refresh function automaticaly send this to curent arrya
        if (allTripStops["current"] as! NSMutableArray).count != 0 {
            // this will call when there is any trip in current

           /* if (((allTripStops["current"] as! NSMutableArray)[0] as! NSMutableDictionary)["stop"] as! NSMutableDictionary)["trip_current_status"] as! String == "skipped" {
             //   let cData = ((allTripStops["current"] as! NSMutableArray)[0] as! NSMutableDictionary)
                
                (allTripStops["remaining"] as! NSMutableArray).insert(((allTripStops["skipped"] as! NSMutableArray)[indexPath?.row ?? 0] as! NSMutableDictionary), at: 0)
                (allTripStops["skipped"] as! NSMutableArray).add(((allTripStops["current"] as! NSMutableArray)[0] as! NSMutableDictionary))
                (allTripStops["skipped"] as! NSMutableArray).removeObject(at: indexPath?.row ?? 0)
                
                (allTripStops["current"] as! NSMutableArray).removeAllObjects()
                self.refresh()
            }
            else */ if (((allTripStops["current"] as! NSMutableArray)[0] as! NSMutableDictionary)["stop"] as! NSMutableDictionary)["trip_current_status"] as! String == "remaining" {
                
                (allTripStops["remaining"] as! NSMutableArray).insert(((allTripStops["skipped"] as! NSMutableArray)[indexPath?.row ?? 0] as! NSMutableDictionary), at: 0)
                (allTripStops["remaining"] as! NSMutableArray).insert(((allTripStops["current"] as! NSMutableArray)[0] as! NSMutableDictionary), at: 1)
                
                (allTripStops["skipped"] as! NSMutableArray).removeObject(at: indexPath?.row ?? 0)
                (allTripStops["current"] as! NSMutableArray).removeAllObjects()
                self.refresh()
                 self.apiStarTrip(tripStatus: "start")
            }
            else if ((((allTripStops["current"] as! NSMutableArray)[0] as! NSMutableDictionary)["stop"] as! NSMutableDictionary)["trip_current_status"] as! String == "current") || (((allTripStops["current"] as! NSMutableArray)[0] as! NSMutableDictionary)["stop"] as! NSMutableDictionary)["trip_current_status"] as! String == "skipped" {
                GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Please complete or skip current stop", view: self)
            }
        }
        else {
            // this will call when there are no any trip in current
            (allTripStops["remaining"] as! NSMutableArray).insert(((allTripStops["skipped"] as! NSMutableArray)[indexPath?.row ?? 0] as! NSMutableDictionary), at: 0)
            (((allTripStops["remaining"] as! NSMutableArray)[0] as! NSMutableDictionary)["stop"] as! NSMutableDictionary).setValue("current", forKey: "trip_current_status")  // this line is because trip will start start automaticaly when driver unskip trip
            (allTripStops["skipped"] as! NSMutableArray).removeObject(at: indexPath?.row ?? 0)
            (allTripStops["current"] as! NSMutableArray).removeAllObjects()
            self.refresh()
            self.apiStarTrip(tripStatus: "start")
        }
        
    }
    
   
    

    
    
    
    @IBAction func btnBackToTrips(_ sender: UIButton) {
//
        if CURRENT_TRIP.count == 0 {
            self.btnTripListAction(UIButton())
        }
        else {
            self.apiTripComplete ()
        }
        
    }
    
    
}

class TableViewTripInProgressCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var imgRightClick: UIImageView!
    @IBOutlet weak var btnUnskipHeightConst: NSLayoutConstraint!
    @IBOutlet weak var viewBG: UIView!
    
    @IBOutlet weak var viewShowRoot: ViewShowRoot!
    
}

extension TripInProgressVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.collapesedHeader.contains(section) {
            return 0
        }
        if section == 0 {
            return ((allTripStops["remaining"] as! NSArray).count)
        }
        if section == 1 {
            return ((allTripStops["skipped"] as! NSArray).count)
        }
        if section == 2 {
            return ((allTripStops["completed"] as! NSArray).count)
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       // return 35
        
        if ((allTripStops["remaining"] as! NSArray).count == 0) && section == 0 {
            return 0
        }
        else if ((allTripStops["skipped"] as! NSArray).count == 0) && section == 1 {
            return 0
        }
        else if ((allTripStops["completed"] as! NSArray).count == 0) && section == 2 {
            return 0
        }
        else {
            return 35
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewTripInProgressCell", for: indexPath) as! TableViewTripInProgressCell
        
        var dic = NSMutableDictionary()
        if indexPath.section == 0 {
            dic = ((allTripStops["remaining"] as! NSMutableArray)[indexPath.row] as! NSMutableDictionary)["stop"] as! NSMutableDictionary
        }
        else if indexPath.section == 1 {
            dic = ((allTripStops["skipped"] as! NSMutableArray)[indexPath.row] as! NSMutableDictionary)["stop"] as! NSMutableDictionary
        }
        else if indexPath.section == 2 {
            dic = ((allTripStops["completed"] as! NSMutableArray)[indexPath.row] as! NSMutableDictionary)["stop"] as! NSMutableDictionary
        }
        

        cell.lblTitle.text = dic["stop_name"] as? String
        cell.lblAddress.text = dic["address"] as? String
        cell.lblNumber.text = dic["priority"] as? String

        if sectionArr[indexPath.section] == "REMANING STOPS" {
            cell.imgRightClick.isHidden = true
            cell.btnUnskipHeightConst.constant = 0
        }else if sectionArr[indexPath.section] == "SKIPPED STOPS" && self.isCurrentTrip {
            cell.imgRightClick.isHidden = true
            cell.btnUnskipHeightConst.constant = 30
        }else if sectionArr[indexPath.section] == "SKIPPED STOPS" {
            cell.imgRightClick.isHidden = true
            cell.btnUnskipHeightConst.constant = 0
        }else {
            cell.btnUnskipHeightConst.constant = 0
            cell.viewBG.backgroundColor = #colorLiteral(red: 0.5568627451, green: 0.8078431373, blue: 0.8862745098, alpha: 1)
            cell.lblNumber.backgroundColor = #colorLiteral(red: 0.5568627451, green: 0.8078431373, blue: 0.8862745098, alpha: 1)
        }
        
        let lat = dic["latitude"] as? String ?? "0"
        let lon = dic["longitude"] as? String ?? "0"
        cell.viewShowRoot.letLon = lat + "," + lon
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width:tableView.frame.size.width, height: 35))
        viewHeader.backgroundColor = #colorLiteral(red: 0.1960784314, green: 0.5215686275, blue: 0.2509803922, alpha: 1)
        
        let labelTitle = UILabel(frame: CGRect(x: 50, y: 0, width: (tableView.frame.size.width) - 50 * 2, height: 35))
        labelTitle.text = sectionArr[section]
        labelTitle.textColor = .white
        labelTitle.textAlignment = .center
        labelTitle.font = UIFont(name: "Arial", size: 17)
        viewHeader.addSubview(labelTitle)
        
        let buttonTap = UIButton(frame: CGRect(x: (viewHeader.frame.size.width) - 35, y: 0, width: 35, height: 35))
        if self.collapesedHeader.contains(section) {
            buttonTap.setImage(UIImage(named: "down-arrow"), for: .normal)
        }else{
            buttonTap.setImage(UIImage(named: "up-arrow"), for: .normal)
        }
        buttonTap.addTarget(self, action: #selector(btnAction(_:)), for: .touchUpInside)
        viewHeader.addSubview(buttonTap)
        buttonTap.tag = section + 1
        // Ben10 print("buttonTap.tag - 1)
        
        return viewHeader
    }
    
  
    
    @objc func btnAction(_ sender: UIButton) {
        let section = sender.tag - 1
        if self.collapesedHeader.contains(section) {
            self.collapesedHeader.remove(section)
        }else{
            self.collapesedHeader.add(section)
        }
        // Ben10 print("self.collapesedHeader)
        self.tableViewTrip.reloadSections([section], with: .automatic)
    }
}


extension TripInProgressVC {
    func apiStarTrip (tripStatus : String) {
        
//        if !isApiCall { return }
        
//        isApiCall = false
        
      //  let index = trackArray[dic] as! NSMutableDictionary
        
//        self.viewLoderHightConstant.constant = 35
        var dic = NSMutableDictionary()
        if (allTripStops["current"] as! NSMutableArray).count != 0 {
             dic = (((allTripStops["current"] as! NSMutableArray)[0] as! NSMutableDictionary)["stop"] as! NSMutableDictionary)
        }
        else {
            if (allTripStops["remaining"] as! NSMutableArray).count == 0 {return}
             dic = (((allTripStops["remaining"] as! NSMutableArray)[0] as! NSMutableDictionary)["stop"] as! NSMutableDictionary)
        }
        
        
        let parm = ["trip_id" : dataFromBack["trip_id"] as! String,//(index)["trip_id"],
                    "stop_id" : dic["stop_id"] as! String,//(index)["id"],
                    "status" : tripStatus,
                    "driver_id": User("user_id")] as NSMutableDictionary
        
        // Ben10 print("parm)
        
        let dicLog = ["api" : "trip_status", "parms" : GlobalClass.dicToJson(dictionary: parm as NSDictionary), "status" : "1"] as NSDictionary
        let last_id = InsertInTo_TableBackLog(is_type: "backLog", dataDic: dicLog)
        let backlog_data = GlobalClass.makeBackLogKey()
        parm.setValue(backlog_data, forKey: "backlog_data")
      
        TripsVC_obj.getNewLetLon()

        GlobalClass.multipartData("trip_status", parameter: parm , MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
            DeleteItem_TableBackLog(is_type: "backLog", id: last_id) // this is for remove all backlogs

            
            
//            self.isApiCall = true
            
//            index.setValue("2", forKey: "stop_status")
            
//            self.getAllTrip()
            
            //            self.tableView.reloadData()
            
//            let userDefoult = UserDefaults.standard
            
//            userDefoult.set("relodeVC", forKey: "isVcFrome")
            
            //            GlobalClass.hideHud()
            
//            self.viewLoderHightConstant.constant = 0
            
            
        }, successWithStatus0Closure: { (response) in
            
            //            GlobalClass.hideHud()
            
//            self.viewLoderHightConstant.constant = 0
            
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: response?.object(forKey: "responsemsg") as! String, view: self)
            
//            self.isApiCall = true
            
            let responsemsg = (response as! NSDictionary)["responsemsg"] as? String ?? ""
            let responsecode = (response as! NSDictionary)["responsecode"] as? String ?? ""
            
            if responsecode as? String ?? "" == "-2" {
                
                GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
                    
                    GlobalClass.setUserDefault(value: "", for: "landingPage")
                    GlobalClass.removeUserDefaults("getDistributorList")
                    
                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                    return
                    
                })
            }
            
            
        }) { (error) in
            
//            self.viewLoderHightConstant.constant = 0
            
//            self.isApiCall = true
            
            if error == "Not Show" {
                return
            }
        }
    }
    
    func completTrip() {
        
        CURRENT_TRIP.setValue("stop", forKey: "trip_status")
       
        for i in 0 ..< self.TripsVC_obj.allDataTrip.count {
            let tripId = (self.TripsVC_obj.allDataTrip[i] as! NSMutableDictionary)["trip_id"] as! String
            let currentTripId = CURRENT_TRIP["trip_id"] as! String
            if tripId == currentTripId {
                let dic = CURRENT_TRIP.mutableCopy() as! NSMutableDictionary
                self.TripsVC_obj.allDataTrip.replaceObject(at: i, with: dic)
                break
            }
        }
        
        if !Reachability.isConnectedToNetwork(){
             let dic41 = ["status" : "stop", "trip_id" : CURRENT_TRIP["trip_id"] as! String] as NSMutableDictionary
            let dicLog = ["api" : "tripComplete", "parms" : GlobalClass.dicToJson(dictionary: dic41), "status" : "1"] as NSDictionary
            _ = InsertInTo_TableBackLog(is_type: "backLog", dataDic: dicLog)
        }
       
        
        CURRENT_TRIP.removeAllObjects()
        
      //  self.TripsVC_obj.tripCompletChangeUI()
        self.btnTripListAction(UIButton())
      
    }
    
    func apiTripComplete () {
        
       // complet trip
       
        
        
        
        let parm = ["trip_id" : dataFromBack["trip_id"] as! String,//(index)["trip_id"],
            "device_token" : DEVICE_TOKEN,
            "driver_id": User("user_id")] as NSMutableDictionary
        
         self.completTrip()
        // Ben10 print("parm)
        
        let dicLog = ["api" : "tripComplete", "parms" : GlobalClass.dicToJson(dictionary: parm as NSDictionary), "status" : "1"] as NSDictionary
        let last_id = InsertInTo_TableBackLog(is_type: "backLog", dataDic: dicLog)
        let backlog_data = GlobalClass.makeBackLogKey()
        parm.setValue(backlog_data, forKey: "backlog_data")
        
        GlobalClass.multipartData("tripComplete", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (response) in
            DeleteItem_TableBackLog(is_type: "backLog", id: last_id) // this is for remove all backlogs

            self.TripsVC_obj.locationManager.stopUpdatingLocation()
            
        }, successWithStatus0Closure: { (response) in

            
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: response?.object(forKey: "responsemsg") as! String, view: self)
            
            let responsemsg = (response as! NSDictionary)["responsemsg"] as? String ?? ""
            let responsecode = (response as! NSDictionary)["responsecode"] as? String ?? ""
            
            if responsecode as? String ?? "" == "-2" {
                
                GlobalClass.showAlertOKAction(APP_NAME, responsemsg, "OK", self, successClosure: { (ok) in
                    
                    GlobalClass.setUserDefault(value: "", for: "landingPage")
                    GlobalClass.removeUserDefaults("getDistributorList")
                    
                    let viewControllerToBeRoot = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "logoutNav")
                    UIApplication.shared.keyWindow?.rootViewController = viewControllerToBeRoot
                    return
                    
                })
            }
            
            
        }) { (error) in
            
            
            if error == "Not Show" {
                return
            }
        }
    }
}
