//
//  OrderDetailVC.swift
//  Driver New
//
//  Created by Technical Lead on 26/07/19.
//  Copyright © 2019 Bijender Singh. All rights reserved.
//

import UIKit

class TableCell: UITableViewCell{
    
   
    
    @IBOutlet weak var imgItem: UIImageView!
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblOrderCount: UILabel!
    @IBOutlet weak var viewAddRemove: UIView!
    @IBOutlet weak var btnAddItem: UIButton!
    @IBOutlet weak var btnRemoveItem: UIButton!
    @IBOutlet weak var lblItemQty: UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var btnReturn: UIButton!
    
    @IBOutlet weak var constHeightConfirmBtn: NSLayoutConstraint!
    
    @IBOutlet weak var constTop: NSLayoutConstraint!
    @IBOutlet weak var constBottom: NSLayoutConstraint!
    
    
    @IBOutlet weak var txtItemQty: UITextField!
    

}

class OrderDetailVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var lblHeaderTripStatus: UILabel!
    @IBOutlet weak var lblHeaderTripName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblDriverName: UILabel!
    
    @IBOutlet weak var table_view: UITableView!
    
    @IBOutlet weak var viewHeaderTitle: UIView!
    @IBOutlet weak var lblOrderNo: UILabel!
    @IBOutlet weak var lblItemNo: UILabel!
    @IBOutlet weak var lblReturnedNo: UILabel!
    @IBOutlet weak var btnSummary: UIButton!
    @IBOutlet weak var btnDetailed: UIButton!
    @IBOutlet weak var lblDistributor: UILabel!
    var toolBar = UIToolbar()
    var isType = "" 
    var order_status = ""
    var orderDataFromBack = NSMutableDictionary()
    var dataFromBack = NSMutableDictionary()
    var itemsArray = NSMutableArray()
    var showType = "cell"
    var itemHeights = [CGFloat](repeating: UITableView.automaticDimension, count: 1000)
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Ben10 print(""orderDataFromBack = \(orderDataFromBack)")
        itemsArray = orderDataFromBack["items"] as! NSMutableArray
        
        self.lblHeaderTripName.text = dataFromBack["trip_name"] as? String
        if dataFromBack["trip_status"] as! String == "start" {
            self.lblHeaderTripStatus.text = "TRIP IN PROGRESS"
        }
        else {
            self.lblHeaderTripStatus.text = "VIEW TRIP"
        }
        
        order_status = (self.orderDataFromBack["order_status"] as? String ?? "")
        
        self.refresh()
        setUserData()
    }
    
    // set user data
    func setUserData() {
        
        let userData = GlobalClass.getUserDefaultDICT()
        
        lblDriverName.text = userData["name"] as? String
        
        let imageUrl = userData["profile"] as? String
        if let validUrl = imageUrl {
            imgProfile.sd_setImage(with: URL(string: validUrl), placeholderImage: UIImage.init(named: "userProfile"), options: .transformAnimatedImage) { (img, error, type , url) in
                if error == nil {
                }
            }
        } else{
        }
    }
    
    func refresh() {
        lblOrderNo.text = "Order #: \(self.orderDataFromBack["order_increment_id"] as? String ?? "")"
        lblItemNo.text = "\(self.orderDataFromBack["order_item_count"] as? String ?? "0")"
        lblReturnedNo.text = "\(self.orderDataFromBack["order_return_item_count"] as? String ?? "0")"
        lblDistributor.text = "Distributor #: \(self.orderDataFromBack["distributor_code"] as? String ?? "0")"
    }
    
    @IBAction func btnDriverAction(_ sender: Any) {
        NavigationCustom.navToDriverProfile(viewController: self)
    }

    @IBAction func btnSummary(_ sender: Any) {
        self.btnSummary.backgroundColor = UIColor.white
        self.btnSummary.setTitleColor(UIColor.init(hexString: "328540"), for: .normal)
        self.btnDetailed.backgroundColor = UIColor.init(hexString: "328540")
        self.btnDetailed.setTitleColor(UIColor.white, for: .normal)

        self.showType = "cellSummary"
        self.table_view.reloadData()
    }
    
    @IBAction func btnDetailed(_ sender: Any) {
        self.btnDetailed.backgroundColor = UIColor.white
        self.btnDetailed.setTitleColor(UIColor.init(hexString: "328540"), for: .normal)
        self.btnSummary.backgroundColor = UIColor.init(hexString: "328540")
        self.btnSummary.setTitleColor(UIColor.white, for: .normal)

        
        self.showType = "cell"
        self.table_view.reloadData()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if itemHeights[indexPath.row] == UITableView.automaticDimension {
            itemHeights[indexPath.row] = cell.bounds.height
        }
        // fetch more cells, etc.
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return itemHeights[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dic = self.itemsArray[indexPath.row] as! NSMutableDictionary
        
        let cell = tableView.dequeueReusableCell(withIdentifier: showType, for: indexPath) as! TableCell
        
        cell.lblItemName.text = dic["item_name"] as? String
        cell.lblOrderCount.text = "ORDERED: \(dic["qty_ordered"] as? String ?? "")"
       
        
        let qty = Int(dic["qty_ordered"] as? String ?? "0") ?? 0
        let return_qty = Int(dic["return_qty"] as! String == "" ? "0" : dic["return_qty"] as! String) ?? 0
        
        let finalQTY = qty - return_qty
        
        
        
        if showType == "cellSummary" {
            cell.lblItemQty.text = String(finalQTY)//dic["qty_ordered"] as? String
            cell.lblOrderCount.text = "\(dic["qty_ordered"] as? String ?? "")"
            cell.constTop.constant = 3
            cell.constBottom.constant = 3
            if indexPath.row == 0 {
                cell.constTop.constant = 40
            }
            if indexPath.row == self.itemsArray.count - 1 {
                cell.constBottom.constant = 40
            }
            
            if (return_qty == 0) {
                //g
                cell.lblItemName.textColor = UIColor.init(hexString: "013D4F")
            }
            else if return_qty == qty {
                //r
                cell.lblItemName.textColor = UIColor.init(hexString: "eb5a46")
            }
            else {
                //y
                cell.lblItemName.textColor = UIColor.init(hexString: "f2d600")
            }
//            viewHolder.tvItemName.setTextColor(Color.parseColor("#013D4F"));
//            else if (Integer.parseInt(list.get(position).getRealQuantity()) == 0)
//            viewHolder.tvItemName.setTextColor(Color.parseColor("#eb5a46"));
//            else viewHolder.tvItemName.setTextColor(Color.parseColor("#f2d600"));
            
        }
        else {
            cell.txtItemQty.text = String(finalQTY)
            cell.lblOrderCount.text = "ORDERED: \(dic["qty_ordered"] as? String ?? "")"
            
            if finalQTY == 0 {
                cell.btnConfirm.backgroundColor = .clear
                cell.btnReturn.backgroundColor = #colorLiteral(red: 0.1568627451, green: 0.3764705882, blue: 0.4823529412, alpha: 1)
                
                cell.btnConfirm.setTitleColor(#colorLiteral(red: 0.1568627451, green: 0.3764705882, blue: 0.4823529412, alpha: 1), for: .normal)
                cell.btnReturn.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            }
            else {
                cell.btnReturn.backgroundColor = .clear
                cell.btnConfirm.backgroundColor = #colorLiteral(red: 0.1568627451, green: 0.3764705882, blue: 0.4823529412, alpha: 1)
                
                cell.btnReturn.setTitleColor(#colorLiteral(red: 0.1568627451, green: 0.3764705882, blue: 0.4823529412, alpha: 1), for: .normal)
                cell.btnConfirm.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
            }
            
            let imageUrl = dic["item_image"] as? String
            
            if let validUrl = imageUrl {
                cell.imgItem.sd_setImage(with: URL(string: validUrl), placeholderImage: #imageLiteral(resourceName: "Loading"), options: .transformAnimatedImage) { (img, error, type , url) in
                    if error != nil {
                        cell.imgItem.image = #imageLiteral(resourceName: "no_image")
                    }
                }
            }
            
            if isType != "current" {
                cell.viewAddRemove.isUserInteractionEnabled = false
                cell.constHeightConfirmBtn.constant = 0
            }
            
            self.addDoneButtonOnKeyboard(cell, indexPath)
        }
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @IBAction func btnRemoveItem(_ sender: UIButton) {
        let point = sender.convert(CGPoint.zero, to: table_view as UIView)
        let indexPath = table_view.indexPathForRow(at: point)

        
        let qtyRtnStr = (self.itemsArray[indexPath!.row] as! NSMutableDictionary)["return_qty"] as! String
        let qtyOrdStr = (self.itemsArray[indexPath!.row] as! NSMutableDictionary)["qty_ordered"] as! String

        if qtyOrdStr == qtyRtnStr {return}
        let qtyRtnInt = Int(qtyRtnStr == "" ? "0" : qtyRtnStr) ?? 0
        
        (self.itemsArray[indexPath!.row] as! NSMutableDictionary).setValue(String(qtyRtnInt + 1), forKey: "return_qty")
        self.totalReturnItemsCal()
        
        
    }
    @IBAction func btnAddItem(_ sender: UIButton) {
        let point = sender.convert(CGPoint.zero, to: table_view as UIView)
        let indexPath = table_view.indexPathForRow(at: point)
        
        let qtyRtnStr = (self.itemsArray[indexPath!.row] as! NSMutableDictionary)["return_qty"] as! String
        
        if (qtyRtnStr == "" || qtyRtnStr == "0" ) {return}
        let qtyRtnInt = Int(qtyRtnStr == "" ? "0" : qtyRtnStr) ?? 0
        
        (self.itemsArray[indexPath!.row] as! NSMutableDictionary).setValue(String(qtyRtnInt - 1), forKey: "return_qty")
        self.totalReturnItemsCal()
    }
    @IBAction func btnConfirm(_ sender: UIButton) {
        let point = sender.convert(CGPoint.zero, to: table_view as UIView)
        let indexPath = table_view.indexPathForRow(at: point)
        
        (self.itemsArray[indexPath!.row] as! NSMutableDictionary).setValue("0", forKey: "return_qty")
        self.totalReturnItemsCal()
        
    }
    @IBAction func btnReturn(_ sender: UIButton) {
        let point = sender.convert(CGPoint.zero, to: table_view as UIView)
        let indexPath = table_view.indexPathForRow(at: point)
        
        let qtyOrdStr = (self.itemsArray[indexPath!.row] as! NSMutableDictionary)["qty_ordered"] as! String
        
        (self.itemsArray[indexPath!.row] as! NSMutableDictionary).setValue(qtyOrdStr, forKey: "return_qty")
        
        self.totalReturnItemsCal()
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    func totalReturnItemsCal() {
        var totalReturned = 0
        for i in 0 ..< self.itemsArray.count {
            let qtyRtnStr = (self.itemsArray[i] as! NSMutableDictionary)["return_qty"] as! String
            
            let qtyRtnInt = Int(qtyRtnStr == "" ? "0" : qtyRtnStr) ?? 0
            totalReturned = totalReturned + qtyRtnInt
        }
        self.orderDataFromBack.setValue(String(totalReturned), forKey: "order_return_item_count")
        self.refresh()
        self.table_view.reloadData()
    }
    
    func addDoneButtonOnKeyboard(_ cell: TableCell, _ indexPath: IndexPath){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(DoneButtonkeyBordInCell(_:)))
        
        
        done.tag = indexPath.row
        
        doneToolbar.barStyle = UIBarStyle.blackOpaque
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        done.tintColor = COLOR_WHITE
        
        cell.txtItemQty.inputAccessoryView = doneToolbar
        
    }
    @objc func DoneButtonkeyBordInCell(_ item : UIBarButtonItem) {
        
        // Ben10 print("item.tag)
        
        self.view.endEditing(true)
        
        let dic = self.itemsArray[item.tag] as! NSMutableDictionary
        
        let cell = table_view.cellForRow(at: IndexPath(row: item.tag, section: 0))  as! TableCell
        
        let setNewValue = cell.txtItemQty.text ?? ""
        
        if setNewValue == "" {
            
            let qtyOrdStr = (self.itemsArray[item.tag] as! NSMutableDictionary)["qty_ordered"] as! String
            (self.itemsArray[item.tag] as! NSMutableDictionary).setValue(qtyOrdStr, forKey: "return_qty")
            self.totalReturnItemsCal()

            return
        }
        
        if setNewValue == "0" || setNewValue == "00" || setNewValue == "000"{
            let qtyOrdStr = (self.itemsArray[item.tag] as! NSMutableDictionary)["qty_ordered"] as! String
            (self.itemsArray[item.tag] as! NSMutableDictionary).setValue(qtyOrdStr, forKey: "return_qty")
            self.totalReturnItemsCal()
            return
        } else {
            let qty = Int(dic["qty_ordered"] as? String ?? "0") ?? 0
            let return_qty = Int(dic["return_qty"] as! String == "" ? "0" : dic["return_qty"] as! String) ?? 0
            let setNewValueInt = Int(setNewValue) ?? 1
            
            if qty <= setNewValueInt {
                (self.itemsArray[item.tag] as! NSMutableDictionary).setValue("0", forKey: "return_qty")
                self.totalReturnItemsCal()
                return
            }
            else {
                let returnQty = qty - setNewValueInt
                (self.itemsArray[item.tag] as! NSMutableDictionary).setValue(String(returnQty), forKey: "return_qty")
                self.totalReturnItemsCal()
            }
        }
        
//        // Ben10 print(""new less quantity : \(setNewValue ?? "") ")
//
//        Dic.setValue(setNewValue, forKey: "real_quantity")
//
//        tableView.reloadData()
        
    }
}
