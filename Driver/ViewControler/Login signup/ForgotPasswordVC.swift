//
//  ForgotPasswordVC.swift
//  Restaurant Depot
//
//  Created by Bijender Singh on 08/02/19.
//  Copyright © 2019 Bijender Singh. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var lblPageTitle: UILabel!
    @IBOutlet weak var lblEnterYourEmailHard: UILabel!
    @IBOutlet weak var lblEmailAddressHard: UILabel!
    @IBOutlet weak var viewSIngalLineHard: UIView!
    
    @IBOutlet weak var btnSubmit: ZFRippleButton!
    
    @IBOutlet weak var txtEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        colorCodeSetup()
        txtEmail.becomeFirstResponder()
        
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Enter email",attributes: [NSAttributedString.Key.foregroundColor: lblEmailAddressHard.textColor ?? UIColor.white])
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtEmail {
            txtEmail.returnKeyType = .done
            txtEmail.resignFirstResponder()
        }
        return true
    }
    
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        
        if txtEmail.text == "" {
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Please enter email", view: self)
            return
        }
        
        if !GlobalClass.isValidEmail(testStr: txtEmail.text!){
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "Please enter valid email", view: self)
            return
        }
        
        if Reachability.isConnectedToNetwork(){
            apiForgotPassword()
        } else {
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: "No Network Avalable", view: self)
        }
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    func apiForgotPassword() {
        GlobalClass.showHUD()
        let parm = ["email": txtEmail.text?.removingWhitespaces() as Any] as [String : Any] //txtEmail.text?.removingWhitespaces() as Any
        
        GlobalClass.multipartData("forget_password", parameter: parm as NSDictionary, MediaParams: NSDictionary(),  successWithStatus1Closure: { (responce) in
            
            let responce = responce as! NSDictionary
            
            let massage = (responce).object(forKey: "responsemsg") as! String
            
            GlobalClass.showAlertOKAction(APP_NAME, massage, "OK", self, successClosure: { (str) in
                self.navigationController?.popViewController(animated: true)
            })
            
            GlobalClass.hideHud()
            
        }, successWithStatus0Closure: { (responce) in
            
            // Ben10 print(""responce = \(responce as! NSDictionary)")
            let msg = (responce as! NSDictionary).object(forKey: "responsemsg") as! String
            GlobalClass.showAlert(alertTitle: APP_NAME, alertMsg: msg, view: self)
            GlobalClass.hideHud()
            
        }) { (error) in
            
            GlobalClass.hideHud()
            
            if error == "Not Show" {
                return
            }
            
            GlobalClass.showAlertOKAction(APP_NAME, error!, "OK", self, successClosure: { (str) in
            })
        }
    }
    
    func colorCodeSetup () {
        
        //        lblPageTitle.textColor = COLOR_PRIMARY_TEXT_COLOR
        //        lblEnterYourEmailHard.textColor = color//COLOR_PRIMARY_TEXT_COLOR
        //        lblEnterYourEmailHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        //        lblEmailAddressHard.textColor = COLOR_PRIMARY_TEXT_COLOR
        //        viewSIngalLineHard.backgroundColor = COLOR_ACCENT_LINE
        //        btnSubmit.setTitleColor(COLOR_WHITE, for: .normal)
        //        txtEmail.textColor = COLOR_PRIMARY_TEXT_COLOR
        //        txtEmail.returnKeyType = .done
        
    }
}
