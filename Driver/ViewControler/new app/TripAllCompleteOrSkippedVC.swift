//
//  TripAllCompleteOrSkippedVC.swift
//  Driver New
//
//  Created by Technical Lead on 08/07/19.
//  Copyright © 2019 Bijender Singh. All rights reserved.
//

import UIKit


class TripAllCompleteOrSkippedVC: UIViewController {
   
    @IBOutlet weak var tableViewTrip: UITableView!
    @IBOutlet weak var lblTripIdHeader: UILabel!
    @IBOutlet weak var lblStops: UILabel!
    @IBOutlet weak var lblOrders: UILabel!
    @IBOutlet weak var lblItems: UILabel!
    @IBOutlet weak var lblSkiped: UILabel!
    @IBOutlet weak var lblCanceled: UILabel!
    @IBOutlet weak var lblReturns: UILabel!
    
    var sectionArray = ["SKIPPED STOPS", "COMPLETED STOPS"]
    var titleArray = [["Palace Dinning Room"], ["Zack's Oak Bar & Restaurant"]]
    var addressArray = [["456 Neato Street Clifton, NJ 07014"], ["380 Super Cool Road Clifton, NJ 07014"]]
    var collapesedHeader = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK:- All Button Action
    @IBAction func btnTripListAction(_ sender: UIButton) {
    }
    
    @IBAction func btnDriverProfileAction(_ sender: UIButton) {
    }
    
    @IBAction func btnCompleteSkip(_ sender: UIButton) {
    }
    
    @IBAction func btnUnSkipAction(_ sender: UIButton) {
    }
    
    @IBAction func btnDetailsCellAction(_ sender: UIButton) {
    }
    
}


class TableViewSkipCell: UITableViewCell {
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var viewBGColor: UIView!
    @IBOutlet weak var btnUnSkipHeightConst: NSLayoutConstraint!
}


extension TripAllCompleteOrSkippedVC: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.collapesedHeader.contains(section) {
            return 0
        }
        return titleArray[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewSkipCell", for: indexPath) as! TableViewSkipCell
        cell.lblTitle.text = titleArray[indexPath.section][indexPath.row]
        cell.lblAddress.text = addressArray[indexPath.section][indexPath.row]
        if sectionArray[indexPath.section] == "SKIPPED STOPS" {
            cell.imgCheck.isHidden = true
        }else {
            cell.btnUnSkipHeightConst.constant = 0
            cell.viewBGColor.backgroundColor = #colorLiteral(red: 0.5568627451, green: 0.8078431373, blue: 0.8862745098, alpha: 1)
            cell.lblNumber.backgroundColor = #colorLiteral(red: 0.5568627451, green: 0.8078431373, blue: 0.8862745098, alpha: 1)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width:tableView.frame.size.width, height: 35))
        viewHeader.backgroundColor = #colorLiteral(red: 0.1960784314, green: 0.5215686275, blue: 0.2509803922, alpha: 1)
        
        let labelTitle = UILabel(frame: CGRect(x: 50, y: 0, width: (tableView.frame.size.width) - 50 * 2, height: 35))
        labelTitle.text = sectionArray[section]
        labelTitle.textColor = .white
        labelTitle.textAlignment = .center
        labelTitle.font = UIFont(name: "Arial", size: 17)
        viewHeader.addSubview(labelTitle)
        
        let buttonTap = UIButton(frame: CGRect(x: (viewHeader.frame.size.width) - 35, y: 0, width: 35, height: 35))
        if self.collapesedHeader.contains(section) {
            buttonTap.setImage(UIImage(named: "down-arrow"), for: .normal)
        }else{
            buttonTap.setImage(UIImage(named: "up-arrow"), for: .normal)
        }
        buttonTap.addTarget(self, action: #selector(btnTapAction(_:)), for: .touchUpInside)
        viewHeader.addSubview(buttonTap)
        buttonTap.tag = section + 1
        // Ben10 print("buttonTap.tag - 1)
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    @objc func btnTapAction(_ sender: UIButton) {
        let section = sender.tag - 1
        if self.collapesedHeader.contains(section) {
            self.collapesedHeader.remove(section)
        }else{
            self.collapesedHeader.add(section)
        }
        // Ben10 print("self.collapesedHeader)
        self.tableViewTrip.reloadSections([section], with: .automatic)
    }
    
}
