//
//  MapVc.swift
//  Driver
//
//  Created by Apple on 18/02/19.
//  Copyright © 2019 Arthonsys. All rights reserved.
//

import UIKit
import MapKit

class MapVc: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!

     let locationManager = CLLocationManager()
     var events = [Event]()
     var coordinates = [CLLocationCoordinate2D]()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = 10

        mapView.showsUserLocation = true
        
        

        let event1 = Event(latitude: 26.877049, longitude: 75.751492)
        let event2 = Event(latitude: 26.877902, longitude: 75.767575)
        let event3 = Event(latitude: 26.877678, longitude: 75.761490)
        
        // Ben10 print("events)
        
        // USA
        
//        let event1 = Event(latitude: 42.390416, longitude: -102.136388)
//        let event2 = Event(latitude: 41.012461, longitude: -85.525061)
//        let event3 = Event(latitude: 36.208141, longitude: -94.138331)

        events = [event1, event2, event3]
        var anotions = [EventAnnotation]()

        for event in events {
            let ano = EventAnnotation(coordinate: event.coordinates)
            anotions.append(ano)
            mapView.addAnnotation(ano as MKAnnotation)
        }

        // 1.
        mapView.delegate = self as MKMapViewDelegate

        // 2.
        let sourceLocation = CLLocationCoordinate2D(latitude: 26.877049, longitude: 75.751492)
        let destinationLocation = CLLocationCoordinate2D(latitude: 26.904486, longitude: 75.750398)

        // 3.
        let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)

        // 4.
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)

        // 5.
        let sourceAnnotation = MKPointAnnotation()
        sourceAnnotation.title = "arthonsys technologies"

        if let location = sourcePlacemark.location {
            sourceAnnotation.coordinate = location.coordinate
        }


        let destinationAnnotation = MKPointAnnotation()
        destinationAnnotation.title = "Mansarovar Metro Station Parking"

        if let location = destinationPlacemark.location {
            destinationAnnotation.coordinate = location.coordinate
        }

        // 6.
        self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )

        // 7.
        let directionRequest = MKDirections.Request()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile

        // Calculate the direction
        let directions = MKDirections(request: directionRequest)

        // 8.
        directions.calculate {
            (response, error) -> Void in

            guard let response = response else {
                if let error = error {
                    // Ben10 print(""Error: \(error)")
                }
                
                

                return
            }

            let route = response.routes[0]
            self.mapView.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)

            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
        }

    }

    func getDirections(fromLocationCoord: CLLocationCoordinate2D, toLocationCoord: CLLocationCoordinate2D) {
        let fromLocationMapItem = MKMapItem(placemark: MKPlacemark(coordinate: toLocationCoord, addressDictionary: nil))
        let toLocationMapItem = MKMapItem(placemark: MKPlacemark(coordinate: toLocationCoord, addressDictionary: nil))

        let directionsRequest = MKDirections.Request()
        directionsRequest.transportType = .automobile
        directionsRequest.source = fromLocationMapItem
        directionsRequest.destination = toLocationMapItem

        let directions = MKDirections(request: directionsRequest)
        directions.calculate { (directionsResponse, error) -> Void in
            if let error = error {
                // Ben10 print(""Error getting directions: \(error.localizedDescription)")
            } else {
                let route = directionsResponse?.routes[0] as! MKRoute
                // draw the route in the map
                self.mapView.addOverlay(route.polyline)

                // get the next closest location
                let closestLocation = self.getClosestLocation(location: toLocationCoord, locations: self.coordinates)
                if let closest = closestLocation {
                    self.getDirections(fromLocationCoord: toLocationCoord, toLocationCoord: closest)
                }

                // remove the current location's coordinates from the array
//                self.coordinates = self.coordinates.filter({ $0 != toLocationCoord })
            }
        }
    }

   func getClosestLocation(location: CLLocationCoordinate2D, locations: [CLLocationCoordinate2D]) -> CLLocationCoordinate2D? {
        var closestLocation: (distance: Double, coordinates: CLLocationCoordinate2D)?

        for loc in locations {

            let distance = round(location.distance(from: loc))

//            let distance = round(location.distanceFromLocation(loc.location)) as Double
            if closestLocation == nil {
                closestLocation = (distance, loc)
            } else {
                if distance < closestLocation!.distance {
                    closestLocation = (distance, loc)
                }
            }
        }
        return closestLocation?.coordinates
    }


//    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
//
//        let renderer = MKPolylineRenderer(overlay: overlay)
//        renderer.strokeColor = UIColor.black
//        renderer.lineWidth = 4.0
//
//        return renderer
//    }

    @IBAction func btnBack(_ sender: UIButton) {

        navigationController?.popViewController(animated: true)
    }




}



// MARK: - MKMapViewDelegate
extension MapVc {

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.black
        renderer.lineWidth = 5
        return renderer

    }

//    func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
//        let renderer = MKPolylineRenderer(overlay: overlay)
//        renderer.strokeColor = UIColor.black
//        renderer.lineWidth = 5
//        return renderer
//    }


    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        mapView.removeOverlays(mapView.overlays)
        coordinates = events.map({ $0.coordinates })
        let closestLocation = getClosestLocation(location: userLocation.coordinate, locations: coordinates)
        if let closest = closestLocation {
            getDirections(fromLocationCoord: userLocation.coordinate, toLocationCoord: closest)
        }
    }

//    func mapView(mapView: MKMapView!, didUpdateUserLocation userLocation: MKUserLocation!) {
//        mapView.removeOverlays(mapView.overlays)
//
//        coordinates = events.map({ $0.coordinates })
//        let closestLocation = getClosestLocation(location: userLocation.coordinate, locations: coordinates)
//        if let closest = closestLocation {
//            getDirections(fromLocationCoord: userLocation.coordinate, toLocationCoord: closest)
//        }
//    }
}

//extension CLLocationCoordinate2D {
//    //distance in meters, as explained in CLLoactionDistance definition
//    func distance(from: CLLocationCoordinate2D) -> CLLocationDistance {
//        let destination=CLLocation(latitude:from.latitude,longitude:from.longitude)
//        return CLLocation(latitude: latitude, longitude: longitude).distance(from: destination)
//    }
//}

//class Event: NSObject {
//    var latitude: CLLocationDegrees!
//    var longitude: CLLocationDegrees!
//
//    var coordinates: CLLocationCoordinate2D {
//        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
//    }
//    override init() {
//        super.init()
//    }
//
//    init(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
//        self.latitude = latitude
//        self.longitude = longitude
//    }
//}
//
//class EventAnnotation : NSObject {
//    var coordinate: CLLocationCoordinate2D
//    init(coordinate : CLLocationCoordinate2D ) {
//        self.coordinate = coordinate
//    }
//}
